                    this.then(function() {
                      this.wait(5000, function () {
                        this.thenOpen('https://vendorcentral.amazon.com/analytics/data/dashboard/preOrders/report/preOrdersDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                            method: 'POST',
                            data:   {"reportParameters":[{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"aggregationFilter","values":[{"val":"ASINLevel"}]},{"parameterId":"periodStartDay","values":[{"val":date}]},{"parameterId":"periodEndDay","values":[{"val":date}]},{"parameterId":"isPeriodToDate","values":[{"val":false}]},{"parameterId":"isCustomDateRange","values":[{"val":"false"}]},{"parameterId":"hideLastYear","values":[{"val":false}]},{"parameterId":"period","values":[{"val":"DAILY"}]},{"parameterId":"dataRefreshDate","values":[{"val":"0000502050172"}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]},{"parameterId":"parentASINVisibility","values":[{"val":true}]},{"parameterId":"eanVisibility","values":[{"val":true}]},{"parameterId":"isbn13Visibility","values":[{"val":true}]},{"parameterId":"upcVisibility","values":[{"val":true}]},{"parameterId":"janVisibility","values":[{"val":false}]},{"parameterId":"brandVisibility","values":[{"val":true}]},{"parameterId":"brandCodeVisibility","values":[{"val":false}]},{"parameterId":"subcatVisibility","values":[{"val":true}]},{"parameterId":"catVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeWidthVisibility","values":[{"val":true}]},{"parameterId":"authorVisibility","values":[{"val":true}]},{"parameterId":"bindingVisibility","values":[{"val":true}]},{"parameterId":"catalogNumberVisibility","values":[{"val":false}]},{"parameterId":"colorVisibility","values":[{"val":true}]},{"parameterId":"colorCountVisibility","values":[{"val":false}]},{"parameterId":"manufactureOnDemandVisibility","values":[{"val":false}]},{"parameterId":"listPriceVisibility","values":[{"val":false}]},{"parameterId":"modelStyleVisibility","values":[{"val":true}]},{"parameterId":"productGroupVisibility","values":[{"val":false}]},{"parameterId":"releaseDateVisibility","values":[{"val":true}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"preorderedamount","ascending":false}],"reportPagination":{"pageIndex":0,"pageSize":100}}},
                            headers: {
                                'Accept':'application/json',
                                'Content-Type':'application/json',
                            }
                        });
                      });

                      this.then(function(){
                        var parts = [];
                        fs.write('storage/pre_count.json', this.page.plainText);
                        var obj = JSON.parse( this.getPageContent());
                          if(obj && obj['payload'] && obj['payload']['reportParts'] && obj['payload']['reportParts'][0] && obj['payload']['reportParts'][0]['rowCount']) {
                            var rowCount = obj['payload']['reportParts'][0]['rowCount'];
                            console.log('RowCount: '+rowCount+'');

                            var paginate = rowCount.replace(/\,/g,'');
                            paginate = parseInt(paginate,10);
                            var number = paginate/10000;
                            var pages = Math.floor(number);
                            for (h = 0; h <= pages; h++) {
                              parts.push(h);
                            }
                            this.each(parts, function (self, part) {
                              if (fs.isFile('storage/preorder/PreOrdersDetail_'+vgid+'_'+date+'_part'+part+'.json') && compare_to != date){ 
                                console.log('PreOrders Already Scraped!');
                              } else {
                              this.wait(5000, function() {
                                this.thenOpen('https://vendorcentral.amazon.com/analytics/data/dashboard/preOrders/report/preOrdersDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                                    method: 'POST',
                                    data:   {"reportParameters":[{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"aggregationFilter","values":[{"val":"ASINLevel"}]},{"parameterId":"periodStartDay","values":[{"val":date}]},{"parameterId":"periodEndDay","values":[{"val":date}]},{"parameterId":"isPeriodToDate","values":[{"val":true}]},{"parameterId":"isCustomDateRange","values":[{"val":"true"}]},{"parameterId":"hideLastYear","values":[{"val":false}]},{"parameterId":"period","values":[{"val":"DAILY"}]},{"parameterId":"dataRefreshDate","values":[{"val":"0000419636133"}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]},{"parameterId":"parentASINVisibility","values":[{"val":true}]},{"parameterId":"eanVisibility","values":[{"val":true}]},{"parameterId":"isbn13Visibility","values":[{"val":true}]},{"parameterId":"upcVisibility","values":[{"val":true}]},{"parameterId":"janVisibility","values":[{"val":false}]},{"parameterId":"brandVisibility","values":[{"val":true}]},{"parameterId":"brandCodeVisibility","values":[{"val":false}]},{"parameterId":"subcatVisibility","values":[{"val":true}]},{"parameterId":"catVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeWidthVisibility","values":[{"val":true}]},{"parameterId":"authorVisibility","values":[{"val":true}]},{"parameterId":"bindingVisibility","values":[{"val":true}]},{"parameterId":"catalogNumberVisibility","values":[{"val":false}]},{"parameterId":"colorVisibility","values":[{"val":true}]},{"parameterId":"colorCountVisibility","values":[{"val":false}]},{"parameterId":"manufactureOnDemandVisibility","values":[{"val":false}]},{"parameterId":"listPriceVisibility","values":[{"val":false}]},{"parameterId":"modelStyleVisibility","values":[{"val":true}]},{"parameterId":"productGroupVisibility","values":[{"val":false}]},{"parameterId":"releaseDateVisibility","values":[{"val":true}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"preorderedamount","ascending":false}],"reportPagination":{"pageIndex":part,"pageSize":10000}}},
                                    headers: {
                                        'Accept':'application/json',
                                        'Content-Type':'application/json',
                                    }
                                });
                                this.then(function(){
                                  console.log('PreOrdersDetail_'+vgid+'_'+date+'_part'+part+'.json');
                                  fs.write('storage/preorder/PreOrdersDetail_'+vgid+'_'+date+'_part'+part+'.json', this.page.plainText);
                                });
                              });
                            }
                            });
                          } else {
                            console.log('PreOrdersDetail_'+vgid+'_'+date+' : NONE');
                          }
                      });

                    });