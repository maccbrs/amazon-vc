                    this.then(function() {

                      this.wait(1000, function () {
                        this.thenOpen('https://vendorcentral.amazon.com/analytics/data/dashboard/itemComparisonAndAlternativePurchase/report/alternativePurchase?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                            method: 'POST',
                            data:   {"requestId":"8d07b226-5105-49cc-994e-f4371692ddeb","reportParameters":[{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"alternativePurchaseAggregationFilter","values":[{"val":"allProductsLevel"}]},{"parameterId":"period","values":[{"val":"DAILY"}]},{"parameterId":"periodEndDay","values":[{"val":date}]},{"parameterId":"dataRefreshDate","values":[{"val":"0001361388692"}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]},{"parameterId":"parentASINVisibility","values":[{"val":false}]},{"parameterId":"eanVisibility","values":[{"val":false}]},{"parameterId":"isbn13Visibility","values":[{"val":false}]},{"parameterId":"upcVisibility","values":[{"val":false}]},{"parameterId":"janVisibility","values":[{"val":false}]},{"parameterId":"brandVisibility","values":[{"val":false}]},{"parameterId":"brandCodeVisibility","values":[{"val":false}]},{"parameterId":"subcatVisibility","values":[{"val":false}]},{"parameterId":"catVisibility","values":[{"val":false}]},{"parameterId":"apparelSizeVisibility","values":[{"val":false}]},{"parameterId":"apparelSizeWidthVisibility","values":[{"val":false}]},{"parameterId":"authorVisibility","values":[{"val":false}]},{"parameterId":"bindingVisibility","values":[{"val":false}]},{"parameterId":"catalogNumberVisibility","values":[{"val":false}]},{"parameterId":"colorVisibility","values":[{"val":false}]},{"parameterId":"colorCountVisibility","values":[{"val":false}]},{"parameterId":"manufactureOnDemandVisibility","values":[{"val":false}]},{"parameterId":"listPriceVisibility","values":[{"val":false}]},{"parameterId":"modelStyleVisibility","values":[{"val":false}]},{"parameterId":"productGroupVisibility","values":[{"val":false}]},{"parameterId":"releaseDateVisibility","values":[{"val":false}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"orders","ascending":false}],"reportPagination":{"pageIndex":0,"pageSize":100}}},
                            headers: {
                                'Accept':'application/json',
                                'Content-Type':'application/json',
                            }
                        });
                      });

                      this.then(function(){
                        fs.write('storage/alt_count.json', this.page.plainText);
                        var obj = JSON.parse( this.getPageContent());

                          if(obj && obj['payload'] && obj['payload']['reportParts'] && obj['payload']['reportParts'][0] && obj['payload']['reportParts'][0]['rowCount']) {
                            var rowCount = obj['payload']['reportParts'][0]['rowCount'];
                            console.log('RowCount: '+rowCount+'');

                            var paginate = rowCount.replace(/\,/g,'');
                            paginate = parseInt(paginate,10);
                            var number = paginate/10000;
                            var pages = Math.floor(number);

                            var parts = [];
                            for (h = 0; h <= pages; h++) {
                              parts.push(h);
                            }

                            this.each(parts, function (self, part) {
                              if (fs.isFile('storage/alternate/AlternativePurchaseDetail_'+vgid+'_'+date+'_part'+part+'.json') && compare_to != date){ 
                                console.log('Alternative Already Scraped!');
                              } else {
                              this.wait(1000, function() {
                                this.thenOpen('https://vendorcentral.amazon.com/analytics/data/dashboard/itemComparisonAndAlternativePurchase/report/alternativePurchase?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                                    method: 'POST',
                                    data:   {"requestId":"8d07b226-5105-49cc-994e-f4371692ddeb","reportParameters":[{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"alternativePurchaseAggregationFilter","values":[{"val":"allProductsLevel"}]},{"parameterId":"period","values":[{"val":"DAILY"}]},{"parameterId":"periodEndDay","values":[{"val":date}]},{"parameterId":"dataRefreshDate","values":[{"val":"0001361388692"}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]},{"parameterId":"parentASINVisibility","values":[{"val":false}]},{"parameterId":"eanVisibility","values":[{"val":false}]},{"parameterId":"isbn13Visibility","values":[{"val":false}]},{"parameterId":"upcVisibility","values":[{"val":false}]},{"parameterId":"janVisibility","values":[{"val":false}]},{"parameterId":"brandVisibility","values":[{"val":false}]},{"parameterId":"brandCodeVisibility","values":[{"val":false}]},{"parameterId":"subcatVisibility","values":[{"val":false}]},{"parameterId":"catVisibility","values":[{"val":false}]},{"parameterId":"apparelSizeVisibility","values":[{"val":false}]},{"parameterId":"apparelSizeWidthVisibility","values":[{"val":false}]},{"parameterId":"authorVisibility","values":[{"val":false}]},{"parameterId":"bindingVisibility","values":[{"val":false}]},{"parameterId":"catalogNumberVisibility","values":[{"val":false}]},{"parameterId":"colorVisibility","values":[{"val":false}]},{"parameterId":"colorCountVisibility","values":[{"val":false}]},{"parameterId":"manufactureOnDemandVisibility","values":[{"val":false}]},{"parameterId":"listPriceVisibility","values":[{"val":false}]},{"parameterId":"modelStyleVisibility","values":[{"val":false}]},{"parameterId":"productGroupVisibility","values":[{"val":false}]},{"parameterId":"releaseDateVisibility","values":[{"val":false}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"orders","ascending":false}],"reportPagination":{"pageIndex":part,"pageSize":10000}}},
                                    headers: {
                                        'Accept':'application/json',
                                        'Content-Type':'application/json',
                                    }
                                });
                                this.then(function(){
                                  console.log('AlternativePurchaseDetail_'+vgid+'_'+date+'_part'+part+'.json');
                                  fs.write('storage/alternate/AlternativePurchaseDetail_'+vgid+'_'+date+'_part'+part+'.json', this.page.plainText);
                                });
                              });
                            }
                            });
                          } else {
                            console.log('AlternativePurchaseDetail_'+vgid+'_'+date+' : NONE');
                          }
                      });
                      
                    });