<?php

namespace Tests\Feature;

use App\Models\Schedule;
use App\Traits\Tests\Schedulable;
use Error;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class ScheduleTest extends TestCase
{
    use
        Schedulable,
        WithoutMiddleware;

    /**
     * Tests if retrieving a Schedule with invalid type throws Error.
     *
     * @return void
     *
     * @group schedule
     */
    public function testGetScheduleWithInvalidTypeShouldThrowError(): void
    {
        $schedule = static::_schedule();

        $this
            // invalid type QSP should throw error
            ->expectException(Error::class)

            ->get(route("webApi.schedule", [
                "type" => Schedule::morphClass(),
                "id" => $schedule->reportable_id,
            ]));
    }

    /**
     * Tests if retrieving a Schedule with invalid ID throws Error.
     *
     * @return void
     *
     * @group schedule
     */
    public function testGetScheduleWithInvalidIdShouldThrowError(): void
    {
        $types = Schedule::types();

        $this
            // invalid id QSP should throw Error
            ->expectException(Error::class)

            ->get(route("webApi.schedule", [
                "type" => reset($types),
                "id" => 0,
            ]));
    }

    /**
     * Tests if retrieving a Schedule with invalid ID returns HTTP 500.
     *
     * @return void
     *
     * @group schedule
     */
    public function testGetScheduleWithValidTypeAndIdShouldReturnWeeklyAgenda(): void
    {
        $schedule = static::_schedule();

        $response = $this->get(route("webApi.schedule", [
            "type" => $schedule->schedulable_type,
            "id" => $schedule->schedulable_id,
        ]));

        $response
            // HTTP 200
            ->assertOk()

            // assert response is exact JSON
            ->assertJson([
                "data" => $schedule->weekly_agenda,
            ]);
    }

    /**
     * Tests if upserting a Schedule with invalid campaign_ids throws Error.
     *
     * @return void
     *
     * @group schedule
     */
    public function testUpsertScheduleWithInvalidCampaignIdsShouldThrowError(): void
    {
        $schedule = static::_schedule();

        $this
            // invalid campaign_ids should throw Error
            ->expectException(Error::class)

            ->post(
                route("webApi.schedule"),
                [
                    // required to mock PUT request via POST
                    "_method" => "PUT",

                    "campaign_ids" => [],
                    "dayparts" => $schedule->weekly_agenda,
                ]
            );
    }

    /**
     * Tests if upserting a Schedule with invalid dayparts throws Error.
     *
     * @return void
     *
     * @group schedule
     */
    public function testUpsertScheduleWithInvalidDaypartsShouldThrowError(): void
    {
        $schedule = static::_schedule();
        $weeklyAgenda = $schedule->weekly_agenda;
        array_pop($weeklyAgenda);

        $this
            // invalid dayparts should throw Error
            ->expectException(Error::class)

            ->post(
                route("webApi.schedule"),
                [
                    // required to mock PUT request via POST
                    "_method" => "PUT",

                    "campaign_ids" => [
                        $schedule->schedulable_id,
                    ],
                    "dayparts" => $weeklyAgenda,
                ]
            );
    }

    /**
     * Tests if upserting a Schedule with invalid dayparts throws Error.
     *
     * @return void
     *
     * @group schedule
     */
    public function testUpsertScheduleWithValidCampaignIdsAndDaypartsShouldReturnNoContent(): void
    {
        $schedule = static::_schedule();

        $response = $this->put(
            route("webApi.schedule"),
            [
                "campaign_ids" => [
                    $schedule->schedulable_id,
                ],
                "dayparts" => $schedule->weekly_agenda,
            ]
        );

        // successful upsert responds with HTTP 204 no content
        $response->assertNoContent();
    }
}
