<?php

namespace Tests\Unit;

use App\Models\Schedule;
use App\Traits\Tests\Schedulable;
use Exception;
use Illuminate\Database\Query\Expression;
use Illuminate\Support\Arr;
use Tests\TestCase;

class ScheduleTest extends TestCase
{
    use
        Schedulable;

    /**
     * Returns array of bits matching binary representation of given integer.
     *
     * @param int|null $hours
     * @return int[]
     */
    private static function _agenda($hours): array
    {
        $agenda = str_split(sprintf(
            Schedule::SPRINTF_HOURS,
            $hours
        ));

        return array_map("intval", $agenda);
    }

    /**
     * Find a day and hour where a schedule is enabled or disabled.  This is
     * similar to Schedule::getWeeklyAgendaAttribute(), but implements it here
     * specifically for unit testing.
     *
     * @param \App\Models\Schedule $schedule
     * @param int $value
     * @return int[]
     */
    private static function _findDayHour(Schedule $schedule, $value): array
    {
        foreach (Schedule::days(true) as $dayIdx => $day) {
            // returns integer or null
            $hours = $schedule->$day;

            if ($hours === null) {
                // day is null, all hours considered disabled
                if (!$value) {
                    return [
                        $dayIdx,
                        0,
                    ];
                }

                continue;
            }

            // array of bits representing 24 hours
            $agenda = static::_agenda($hours);

            $hour = array_search($value, $agenda);

            if ($hour !== false) {
                return [
                    $dayIdx,
                    $hour,
                ];
            }
        }

        throw new Exception("Enabled/disabled day and hour not found in Schedule.");
    }

    /**
     * Tests agenda accessor if it returns an array of length 24 representing
     * attribute as binary.
     *
     * @param int $dayIdx
     * @return void
     */
    private function _getDayAgendaAttribute($dayIdx): void
    {
        $schedule = new Schedule;
        $accessor = "{$schedule->dayFromIndex($dayIdx)}_agenda";
        $hours = static::_randomHours();
        $agenda = static::_agenda($hours);

        $schedule->$accessor = $agenda;

        // agenda should be the binary representation of hours as an array
        $this->assertEquals(
            $schedule->$accessor,
            $agenda
        );
    }

    /**
     * Tests agenda mutator if it sets attribute to either null or Expression.
     *
     * @param int $dayIdx
     * @return void
     */
    private function _setDayAgendaAttribute($dayIdx): void
    {
        $schedule = new Schedule;
        $day = $schedule->dayFromIndex($dayIdx);
        $mutator = "{$day}_agenda";

        $schedule->$mutator = static::_agenda(0);

        // falsy agenda should set attribute to null
        $this->assertNull($schedule->$day);

        $hours = static::_randomHours(1);
        $hoursBinaryStr = sprintf(
            Schedule::SPRINTF_HOURS,
            $hours
        );

        $schedule->$mutator = static::_agenda($hours);

        // truthy agenda should set attribute to Expression since setting data Eloquently will not work
        $this->assertInstanceOf(
            Expression::class,
            $schedule->$day
        );

        $expressionValue = $schedule
            ->$day
            ->getValue();

        // Exression value should resemble binary representation
        $this->assertEquals(
            $expressionValue,
            "b'{$hoursBinaryStr}'"
        );
    }

    /**
     * Tests if Schedule::dayFromIndex() with invalid day index throws
     * Exception.
     *
     * @return void
     *
     * @group schedule
     */
    public function testDayFromIndexWithInvalidDayIndexShouldThrowException(): void
    {
        // out of range day index should throw Exception
        $this->expectException(Exception::class);

        Schedule::dayFromIndex(-1);
    }

    /**
     * Tests if Schedule::dayFromIndex() with valid day index returns
     * uppercase day.
     *
     * @return void
     *
     * @group schedule
     */
    public function testDayFromIndexWithValidDayIndexShouldReturnLowercaseDay(): void
    {
        $dayIdxs = Schedule::days(true);
        $dayIdx = array_rand($dayIdxs);
        $day = Arr::get(
            $dayIdxs,
            $dayIdx
        );

        // should return appropriate lowercase day
        $this->assertEquals(Schedule::dayFromIndex($dayIdx), $day);
    }

    /**
     * Tests if Schedule::schedulable() returns instance of schedulable type.
     *
     * @return void
     *
     * @group schedule
     */
    public function testSchedulableShouldReturnInstanceOfSchedulableType(): void
    {
        $schedule = static::_schedule();
        $schedulableClass = array_search(
            $schedule->schedulable_type,
            Schedule::types()
        );

        // schedulable should return appropriate instance
        $this->assertInstanceOf($schedulableClass, $schedule->schedulable);
    }

    /**
     * Tests if Schedule::scopeDayDisabled() returns Schedule with null day.
     *
     * @return void
     *
     * @group schedule
     */
    public function testScopeDayDisabledShouldReturnTrueWithNullDay(): void
    {
        // start with random Schedule
        $schedule = static::_schedule(true);

        // set random day to null
        $weeklyAgenda = $schedule->weekly_agenda;
        $dayIdx = Arr::random(array_keys($weeklyAgenda));
        Arr::set(
            $weeklyAgenda,
            $dayIdx,
            Schedule::parseAgenda([]) ?? []
        );
        $schedule->weekly_agenda = $weeklyAgenda;
        $schedule->save();

        $exists = Schedule
            ::whereKey($schedule->getKey())
            ->dayDisabled(
                $dayIdx,
                0
            )
            ->exists();

        // null day should match the retrieved Schedule
        $this->assertTrue($exists);
    }

    /**
     * Tests if Schedule::scopeDayDisabled() returns Schedule with disabled
     * day and hour.
     *
     * @return void
     *
     * @group schedule
     */
    public function testScopeDayDisabledShouldReturnScheduleWithDisabledDayAndHour(): void
    {
        $schedule = static::_schedule();
        $dayHour = static::_findDayHour($schedule, 0);

        $exists = Schedule
            ::whereKey($schedule->getKey())
            ->dayDisabled(...$dayHour)
            ->exists();

        // disabled day and hour should match the retrieved Schedule
        $this->assertTrue($exists);
    }

    /**
     * Tests if Schedule::scopeDayEnabled() returns Schedule with enabled day
     * and hour.
     *
     * @return void
     *
     * @group schedule
     */
    public function testScopeDayEnabledShouldReturnScheduleWithEnabledDayAndHour(): void
    {
        $schedule = static::_schedule();
        $dayHour = static::_findDayHour($schedule, 1);

        $exists = Schedule
            ::whereKey($schedule->getKey())
            ->dayEnabled(...$dayHour)
            ->exists();

        // enabled day and hour should match the retrieved Schedule
        $this->assertTrue($exists);
    }

    /**
     * Tests if Schedule::scopeEntity() returns Schedule for the specified
     * entity.
     *
     * @return void
     *
     * @group schedule
     */
    public function testScopeEntityShouldReturnScheduleForSpecifiedEntity(): void
    {
        $schedule = static::_schedule();

        $exists = Schedule
            ::whereKey($schedule->getKey())
            ->entity($schedule->schedulable)
            ->exists();

        // schedulable entity should match the retrieved Schedule
        $this->assertTrue($exists);
    }

    /**
     * Tests if Schedule::getSundayAgendaAttribute() returns array of length
     * 24 matching binary representation of attribute.
     *
     * @return void
     *
     * @group schedule
     */
    public function testGetSundayAgendaAttributeShouldReturnArrayMatchingBinaryRepresentation(): void
    {
        $this->_getDayAgendaAttribute(Schedule::DAY_SUNDAY);
    }

    /**
     * Tests if Schedule::setSundayAgendaAttribute() sets sunday attribute to
     * either null or Expression.
     *
     * @return void
     *
     * @group schedule
     */
    public function testSetSundayAgendaAttributeShouldSetSundayAttributeToNullOrExpressionWithValidValue(): void
    {
        $this->_setDayAgendaAttribute(Schedule::DAY_SUNDAY);
    }

    /**
     * Tests if Schedule::getMondayAgendaAttribute() returns array of length
     * 24 matching binary representation of attribute.
     *
     * @return void
     *
     * @group schedule
     */
    public function testGetMondayAgendaAttributeShouldReturnArrayMatchingBinaryRepresentation(): void
    {
        $this->_getDayAgendaAttribute(Schedule::DAY_MONDAY);
    }

    /**
     * Tests if Schedule::setMondayAgendaAttribute() sets monday attribute to
     * either null or Expression.
     *
     * @return void
     *
     * @group schedule
     */
    public function testSetMondayAgendaAttributeShouldSetMondayAttributeToNullOrExpressionWithValidValue(): void
    {
        $this->_setDayAgendaAttribute(Schedule::DAY_MONDAY);
    }

    /**
     * Tests if Schedule::getTuesdayAgendaAttribute() returns array of length
     * 24 matching binary representation of attribute.
     *
     * @return void
     *
     * @group schedule
     */
    public function testGetTuesdayAgendaAttributeShouldReturnArrayMatchingBinaryRepresentation(): void
    {
        $this->_getDayAgendaAttribute(Schedule::DAY_TUESDAY);
    }

    /**
     * Tests if Schedule::setTuesdayAgendaAttribute() sets tuesday attribute to
     * either null or Expression.
     *
     * @return void
     *
     * @group schedule
     */
    public function testSetTuesdayAgendaAttributeShouldSetTuesdayAttributeToNullOrExpressionWithValidValue(): void
    {
        $this->_setDayAgendaAttribute(Schedule::DAY_TUESDAY);
    }

    /**
     * Tests if Schedule::getWednesdayAgendaAttribute() returns array of length
     * 24 matching binary representation of attribute.
     *
     * @return void
     *
     * @group schedule
     */
    public function testGetWednesdayAgendaAttributeShouldReturnArrayMatchingBinaryRepresentation(): void
    {
        $this->_getDayAgendaAttribute(Schedule::DAY_WEDNESDAY);
    }

    /**
     * Tests if Schedule::setWednesdayAgendaAttribute() sets wednesday attribute to
     * either null or Expression.
     *
     * @return void
     *
     * @group schedule
     */
    public function testSetWednesdayAgendaAttributeShouldSetWednesdayAttributeToNullOrExpressionWithValidValue(): void
    {
        $this->_setDayAgendaAttribute(Schedule::DAY_WEDNESDAY);
    }

    /**
     * Tests if Schedule::getThursdayAgendaAttribute() returns array of length
     * 24 matching binary representation of attribute.
     *
     * @return void
     *
     * @group schedule
     */
    public function testGetThursdayAgendaAttributeShouldReturnArrayMatchingBinaryRepresentation(): void
    {
        $this->_getDayAgendaAttribute(Schedule::DAY_THURSDAY);
    }

    /**
     * Tests if Schedule::setThursdayAgendaAttribute() sets thursday attribute to
     * either null or Expression.
     *
     * @return void
     *
     * @group schedule
     */
    public function testSetThursdayAgendaAttributeShouldSetThursdayAttributeToNullOrExpressionWithValidValue(): void
    {
        $this->_setDayAgendaAttribute(Schedule::DAY_THURSDAY);
    }

    /**
     * Tests if Schedule::getFridayAgendaAttribute() returns array of length
     * 24 matching binary representation of attribute.
     *
     * @return void
     *
     * @group schedule
     */
    public function testGetFridayAgendaAttributeShouldReturnArrayMatchingBinaryRepresentation(): void
    {
        $this->_getDayAgendaAttribute(Schedule::DAY_FRIDAY);
    }

    /**
     * Tests if Schedule::setFridayAgendaAttribute() sets friday attribute to
     * either null or Expression.
     *
     * @return void
     *
     * @group schedule
     */
    public function testSetFridayAgendaAttributeShouldSetFridayAttributeToNullOrExpressionWithValidValue(): void
    {
        $this->_setDayAgendaAttribute(Schedule::DAY_FRIDAY);
    }

    /**
     * Tests if Schedule::getSaturdayAgendaAttribute() returns array of length
     * 24 matching binary representation of attribute.
     *
     * @return void
     *
     * @group schedule
     */
    public function testGetSaturdayAgendaAttributeShouldReturnArrayMatchingBinaryRepresentation(): void
    {
        $this->_getDayAgendaAttribute(Schedule::DAY_SATURDAY);
    }

    /**
     * Tests if Schedule::setSaturdayAgendaAttribute() sets saturday attribute to
     * either null or Expression.
     *
     * @return void
     *
     * @group schedule
     */
    public function testSetSaturdayAgendaAttributeShouldSetSaturdayAttributeToNullOrExpressionWithValidValue(): void
    {
        $this->_setDayAgendaAttribute(Schedule::DAY_SATURDAY);
    }

    /**
     * Tests if Schedule::getIsClearAttribute() returns true when all day
     * values are falsy, else false.
     *
     * @return void
     *
     * @group schedule
     */
    public function testGetIsClearAttributeShouldReturnTrueWhenAllDaysFalsyElseFalse(): void
    {
        $schedule = new Schedule;
        $days = Schedule::days(true);

        // Schedule has no data
        $this->assertTrue($schedule->is_clear);

        $agenda0 = static::_agenda(0);

        foreach ($days as $day) {
            $schedule->{"{$day}_agenda"} = $agenda0;
        }

        // Schedule still has empty agendas (days should be null)
        $this->assertTrue($schedule->is_clear);

        $dayRandom = Arr::get(
            $days,
            array_rand($days)
        );
        $hourRandom = 1 << mt_rand(0, Schedule::HOURS - 1);

        // random day set with one enabled random hour
        $schedule->{"{$dayRandom}_agenda"} = static::_agenda($hourRandom);

        // Schedule has single random enabled hour on a single day
        $this->assertFalse($schedule->is_clear);

        // all days set with random hours
        foreach ($days as $day) {
            $agenda = static::_agenda(static::_randomHours(1));
            $schedule->{"{$day}_agenda"} = $agenda;
        }

        // Schedule has random hours enabled for all days
        $this->assertFalse($schedule->is_clear);
    }
}
