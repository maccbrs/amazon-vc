var casper = require('casper').create({
    clientScripts: ["jquery.js"],
    viewportSize: {width: 1920, height: 1080}
});


casper.javascriptEnabled = true;
//page.settings.javascriptEnabled = true;
//runner.page.javascriptEnabled = true;
//casper.page.javascriptEnabled = true;
phantom.cookiesEnabled = true;
//casper.settings.userAgent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0';

//casper.options.exitOnError = false;

casper.options.waitTimeout = 999999;
var x = require('casper').selectXPath;

var fs = require('fs');
var AMAZON_SITE = 'https://vendorcentral.amazon.jp/analytics/dashboard/snapshot';
var AMAZON_USER = 'vendorcentral@channelbakers.com';
var AMAZON_PASS = ''; 
var cookie_dir = 'scraper/cookies';
var cookie_path = cookie_dir + '/cookies_jp.txt';
var emailInput = 'input#ap_email';
var passInput = 'input#ap_password';
var otp = '#auth-mfa-otpcode';
var otp_remember = '#auth-mfa-remember-device';
var captcha = 'input#auth-captcha-guess';
var x      = require('casper').selectXPath;
var system = require('system');
var utils = require('utils');
var http = require('http');
var moment = require('moment.js');
var env = system.env;

var now = new Date();
var year_to = now.getFullYear();
var month_to = now.getMonth();
var date_to = now.getDate() - 1;

var prev = new Date();
prev.setDate(prev.getDate() - 30);
// var year_from = prev.getFullYear();
var year_from = '2019'
// var month_from = prev.getMonth();
var month_from = '11';
var date_from = '01';

var dt = new Date();
var dd = dt.getDate();
var mm = dt.getMonth() + 1; 
var yyyy = dt.getFullYear();

var d = new Date();
var getTot = daysInMonth(d.getMonth(),d.getFullYear());
/* console.log(daysInMonth(d.getMonth(),d.getFullYear())) */
var sat = new Array();
var sun = new Array();

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('');
}

for(var i=1;i<=getTot;i++){
    var newDate = new Date(d.getFullYear(),d.getMonth(),i)
    /* console.log(newDate) */;
/*     console.log(i+"-"+newDate.getDay()); */
    if(newDate.getDay()==0){
        sat.push(formatDate(newDate))
    }
    if(newDate.getDay()==6){
        sun.push(formatDate(newDate))
    }
    
}
/*document.write(JSON.stringify(sat));
document.write(JSON.stringify(sun));*/

// console.log(JSON.stringify(sat));
// console.log(JSON.stringify(sun));

  var date1 = new Date('2020', 0, 1);
  while (date1.getDay() != 0) {
    date1.setDate(date1.getDate() + 1);
  }
  var sundays = [];
  while (date1.getFullYear() == '2020') {
    var m = date1.getMonth() + 1;
    var d = date1.getDate();
    sundays.push(
      '2020' +
      (m < 10 ? '0' + m : m) +
      (d < 10 ? '0' + d : d)
    );
    date1.setDate(date1.getDate() + 7);
  }
  
  var date2 = new Date('2020', 0, 1);
  while (date2.getDay() !== 6) {
    date2.setDate(date2.getDate() + 1);
  }
  var saturdays = [];
  while (date2.getFullYear() == '2020') {
    var m = date2.getMonth() + 1;
    var d = date2.getDate();
    saturdays.push(
      '2020' +
      (m < 10 ? '0' + m : m) +
      (d < 10 ? '0' + d : d)
    );
    
    date2.setDate(date2.getDate() + 7);
  }
  // console.log(saturdays);

function daysInMonth(month,year) {
    return new Date(year, month, 0).getDate();
}

if (dd < 10) {
  dd = '0' + dd;
} 
if (mm < 10) {
  mm = '0' + mm;
}
var prod_today = yyyy + '' + mm + '' + dd;

if (date_to < 10) {
  date_to = '0' + date_to;
} 
if (month_to < 10) {
  month_to = '0' + month_to;
}

var compare_to = year_to + '' + (month_to+1) + '' + date_to;

var from = new Date(year_from,month_from,date_from);
var to = new Date(year_to,month_to,date_to);
var dates = [];
    
// loop for every day
for (var day = from; day <= to; day.setDate(day.getDate() + 1)) {
      var year = day.getFullYear();
      var month = day.getMonth() + 1;
      var date = day.getDate();
      
      if (date < 10) {
        date = '0' + date;
      } 
      if (month < 10) {
        month = '0' + month;
      }

      dates.push(year+''+month+''+date);
  
}

casper.userAgent('Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0');

function isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

function logged_in() {
    casper.echo("already logged in");
      var vgids = [];
      var userid = [];

      casper.thenOpen("https://hc-ping.com/899d98df-29b0-478b-a810-21b1eea1e8b2/start");

        casper.then(function() {

          this.thenOpen('https://vendorcentral.amazon.jp/analytics/dashboard/snapshot');

          this.then(function() {
            var user_info = JSON.stringify(this.getGlobal('userInfo'));
            var obj = JSON.parse(user_info);
            var subaccounts = obj.subAccounts;
            fs.write('storage/accounts/accounts_'+prod_today+'.json', JSON.stringify(subaccounts));
            var userpref = JSON.parse(obj.userPreferencesString); 
            userid.push(userpref.obfuscatedUserId);

            subaccounts.forEach(function (subaccount) {
              if(subaccount.retailSubscriptionLevel == 'PREMIUM' && subaccount.status == 'VALID'){
              var x = subaccount.vendorGroupId;
                vgids.push(x);
              }
            });
          });


      this.then(function() {

            this.each(vgids, function (self, vgid) {
            this.thenOpen('https://vendorcentral.amazon.eu/hz/vendor/members/user-management/switch-accounts-checker?vendorGroup='+vgid+'&customerId='+userid+'');

            this.thenOpen('https://vendorcentral.amazon.jp/analytics/dashboard/snapshot');

            this.then(function() {
              var token = this.getGlobal('token');
                //netppm
                var q = 0;
                this.each(sundays, function (index, sunday)
                {
                  var items = [];
                      q++;
                      items.push(q);
                this.wait(5000, function () {
                  console.log(items);
                  console.log(sunday);
                  console.log(saturdays[items]);
                  this.thenOpen('https://vendorcentral.amazon.eu/analytics/data/dashboard/netPPM/report/netPPMDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara', {
                      method: 'POST',
                      data:   {"reportParameters":[{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"aggregationFilter","values":[{"val":"ASINLevel"}]},{"parameterId":"period","values":[{"val":"WEEKLY"}]},{"parameterId":"periodStartDay","values":[{"val":sunday}]},{"parameterId":"periodEndDay","values":[{"val":saturdays[items]}]},{"parameterId":"dataRefreshDate","values":[{"val":"0001024108864"}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"netppmpercentoftotal","ascending":false}],"reportPagination":{"pageIndex":0,"pageSize":100}}},
                      headers: {'Accept':'application/json', 'Content-Type':'application/json'}
                    });
                    this.then(function(){
                      fs.write('storage/countppm.json', this.page.plainText);
                      console.log(this.getPageContent());
                      var obj = JSON.parse(this.getPageContent());
                      
                        if (obj && obj['payload'] && obj['payload']['reportParts'] && obj['payload']['reportParts'][0] && obj['payload']['reportParts'][0]['rowCount']) {
                        var rowCount = obj['payload']['reportParts'][0]['rowCount'];
                        console.log('RowCount: '+rowCount+'');



                        var paginate = rowCount.replace(/\,/g,'');
                          paginate = parseInt(paginate,10);
                          var number = paginate/10000;
                          var pages = Math.floor(number);

                          var parts = [];
                          for (h = 0; h <= pages; h++) {
                            parts.push(h);
                          }

                          this.each(parts, function (self, part) {
                            if (fs.isFile('storage/netppm/NetPPM'+vgid+'_'+sunday+'-'+saturdays[items]+'_part'+part+'.json') && compare_to != date){ 
                              console.log('NetPPM Already Scraped!');
                            } else {
                            this.wait(5000, function() {
                              

                  this.thenOpen('https://vendorcentral.amazon.eu/analytics/data/dashboard/netPPM/report/netPPMDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara', {
                      method: 'POST',
                      data:   {"reportParameters":[{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"aggregationFilter","values":[{"val":"ASINLevel"}]},{"parameterId":"period","values":[{"val":"WEEKLY"}]},{"parameterId":"periodStartDay","values":[{"val":sunday}]},{"parameterId":"periodEndDay","values":[{"val":saturdays[items]}]},{"parameterId":"dataRefreshDate","values":[{"val":"0001024108864"}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"netppmpercentoftotal","ascending":false}],"reportPagination":{"pageIndex":0,"pageSize":100}}},
                      headers: {'Accept':'application/json', 'Content-Type':'application/json'}
                    });
                                this.then(function(){
                                  console.log('NetPPM'+vgid+'_'+date+'_part'+part+'.json');
/*                                  console.log(this.page.plainText);*/
                                  fs.write('storage/netppm/NetPPM'+'_'+vgid+'_'+sunday+'-'+saturdays[items]+'_part'+part+'.json', this.page.plainText);
                                });
                            });
                          }
                          });
                        } else {
                          console.log('NetPPM'+vgid+'_'+sunday+'-'+saturdays[items]+' : NONE');
                        }
                    });
                  });                  
                });



                //product
                this.then(function() {

                  this.then(function() {
                      this.then(function() {
                        if (vgid != '') {
                          this.wait(1000, function () {
                            this.thenOpen('https://vendorcentral.amazon.eu/analytics/data/dashboard/productCatalog/report/productCatalogDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                                method: 'POST',
                                data:   {"reportParameters":[{"parameterId":"distributorView","values":[{"val":"manufacturer"}]},{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"dataRefreshDate","values":[{"val":prod_today}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"releasedate","ascending":false}],"reportPagination":{"pageIndex":0,"pageSize":100}}},
                                headers: {
                                    'Accept':'application/json',
                                    'Content-Type':'application/json',
                                }
                            });
                          });

                          this.then(function(){
                            fs.write('storage/count.json', this.page.plainText);
                            var obj = JSON.parse(this.getPageContent());

                              if(obj && obj['payload'] && obj['payload']['reportParts'] && obj['payload']['reportParts'][0] && obj['payload']['reportParts'][0]['rowCount']) {
                                var rowCount = obj['payload']['reportParts'][0]['rowCount'];
                              console.log('RowCount: '+rowCount+'');

                                var paginate = rowCount.replace(/\,/g,'');
                                paginate = parseInt(paginate,10);
                                var number = paginate/10000;
                                var pages = Math.floor(number);

                                var parts = [];
                                for (h = 0; h <= pages; h++) {
                                  parts.push(h);
                                }

                                this.each(parts, function (self, part) {
                                  if (fs.isFile('storage/products/ProductCatalog_manufacturing_'+vgid+'_'+prod_today+'_part'+part+'.json')){ 
                                    console.log('Product Already Scraped!');
                                  } else {
                                  this.wait(1000, function() {
                                    this.thenOpen('https://vendorcentral.amazon.eu/analytics/data/dashboard/productCatalog/report/productCatalogDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                                        method: 'POST',
                                        data:   {"reportParameters":[{"parameterId":"distributorView","values":[{"val":"manufacturer"}]},{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"dataRefreshDate","values":[{"val":prod_today}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"releasedate","ascending":false}],"reportPagination":{"pageIndex":part,"pageSize":10000}}},
                                        headers: {
                                            'Accept':'application/json',
                                            'Content-Type':'application/json',
                                        }
                                    });
                                    this.then(function(){
                                      console.log('ProductCatalog_manufacturing_'+vgid+'_'+prod_today+'_part'+part+'.json');
                                      fs.write('storage/products/ProductCatalog_manufacturing_'+vgid+'_'+prod_today+'_part'+part+'.json', this.page.plainText);
                                    });
                                  });
                                  }
                                });
                              } else {
                                console.log('ProductCatalog_manufacturing_'+vgid+'_'+prod_today+' : NONE');
                              }
                          });
                        }
                      });
                  });

                  this.then(function() {
                      this.then(function() {
                        if (vgid != '') {
                          this.wait(1000, function () {
                            this.thenOpen('https://vendorcentral.amazon.eu/analytics/data/dashboard/productCatalog/report/productCatalogDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                                method: 'POST',
                                data:   {"reportParameters":[{"parameterId":"distributorView","values":[{"val":"sourcing"}]},{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"dataRefreshDate","values":[{"val":prod_today}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"releasedate","ascending":false}],"reportPagination":{"pageIndex":0,"pageSize":100}}},
                                headers: {
                                    'Accept':'application/json',
                                    'Content-Type':'application/json',
                                }
                            });
                          });

                          this.then(function(){
                            fs.write('storage/count.json', this.page.plainText);
                            var obj = JSON.parse(this.getPageContent());

                              if(obj && obj['payload'] && obj['payload']['reportParts'] && obj['payload']['reportParts'][0] && obj['payload']['reportParts'][0]['rowCount']) {
                                var rowCount = obj['payload']['reportParts'][0]['rowCount'];
                              console.log('RowCount: '+rowCount+'');

                                var paginate = rowCount.replace(/\,/g,'');
                                paginate = parseInt(paginate,10);
                                var number = paginate/10000;
                                var pages = Math.floor(number);

                                var parts = [];
                                for (h = 0; h <= pages; h++) {
                                  parts.push(h);
                                }

                                this.each(parts, function (self, part) {
                                  if (fs.isFile('storage/products/ProductCatalog_sourcing_'+vgid+'_'+prod_today+'_part'+part+'.json')){ 
                                    console.log('Product Already Scraped!');
                                  } else {
                                  this.wait(1000, function() {
                                    this.thenOpen('https://vendorcentral.amazon.eu/analytics/data/dashboard/productCatalog/report/productCatalogDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                                        method: 'POST',
                                        data:   {"reportParameters":[{"parameterId":"distributorView","values":[{"val":"sourcing"}]},{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"dataRefreshDate","values":[{"val":prod_today}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"releasedate","ascending":false}],"reportPagination":{"pageIndex":part,"pageSize":10000}}},
                                        headers: {
                                            'Accept':'application/json',
                                            'Content-Type':'application/json',
                                        }
                                    });
                                    this.then(function(){
                                      console.log('ProductCatalog_sourcing_'+vgid+'_'+prod_today+'_part'+part+'.json');
                                      fs.write('storage/products/ProductCatalog_sourcing_'+vgid+'_'+prod_today+'_part'+part+'.json', this.page.plainText);
                                    });
                                  });
                                  }
                                });
                              } else {
                                console.log('ProductCatalog_sourcing_'+vgid+'_'+prod_today+' : NONE');
                              }
                          });
                        }
                      });
                  });

                  this.then(function() {
                      this.then(function() {
                        if (vgid != '') {
                          this.wait(1000, function () {
                            this.thenOpen('https://vendorcentral.amazon.eu/analytics/data/dashboard/productCatalog/report/productCatalogDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                                method: 'POST',
                                data:   {"reportParameters":[{"parameterId":"distributorView","values":[{"val":"consignment"}]},{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"dataRefreshDate","values":[{"val":prod_today}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"releasedate","ascending":false}],"reportPagination":{"pageIndex":0,"pageSize":100}}},
                                headers: {
                                    'Accept':'application/json',
                                    'Content-Type':'application/json',
                                }
                            });
                          });

                          this.then(function(){
                            fs.write('storage/count.json', this.page.plainText);
                            var obj = JSON.parse(this.getPageContent());

                              if(obj && obj['payload'] && obj['payload']['reportParts'] && obj['payload']['reportParts'][0] && obj['payload']['reportParts'][0]['rowCount']) {
                                var rowCount = obj['payload']['reportParts'][0]['rowCount'];
                              console.log('RowCount: '+rowCount+'');

                                var paginate = rowCount.replace(/\,/g,'');
                                paginate = parseInt(paginate,10);
                                var number = paginate/10000;
                                var pages = Math.floor(number);

                                var parts = [];
                                for (h = 0; h <= pages; h++) {
                                  parts.push(h);
                                }

                                this.each(parts, function (self, part) {
                                  if (fs.isFile('storage/products/ProductCatalog_consignment_'+vgid+'_'+prod_today+'_part'+part+'.json')){ 
                                    console.log('Product Already Scraped!');
                                  } else {
                                  this.wait(1000, function() {
                                    this.thenOpen('https://vendorcentral.amazon.eu/analytics/data/dashboard/productCatalog/report/productCatalogDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                                        method: 'POST',
                                        data:   {"reportParameters":[{"parameterId":"distributorView","values":[{"val":"consignment"}]},{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"dataRefreshDate","values":[{"val":prod_today}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"releasedate","ascending":false}],"reportPagination":{"pageIndex":part,"pageSize":10000}}},
                                        headers: {
                                            'Accept':'application/json',
                                            'Content-Type':'application/json',
                                        }
                                    });
                                    this.then(function(){
                                      console.log('ProductCatalog_consignment_'+vgid+'_'+prod_today+'_part'+part+'.json');
                                      fs.write('storage/products/ProductCatalog_consignment_'+vgid+'_'+prod_today+'_part'+part+'.json', this.page.plainText);
                                    });
                                  });
                                  }
                                });
                              } else {
                                console.log('ProductCatalog_consignment_'+vgid+'_'+prod_today+' : NONE');
                              }
                          });
                        }
                      });
                  });


                });


                this.each(dates, function (self, date) {

                    //geographic
                    this.wait(1000, function () {
                    this.thenOpen('https://vendorcentral.amazon.eu/analytics/data/dashboard/geographicSalesInsights/report/geographicSalesInsightsDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara', {
                        method: 'POST',
                        data:   {"reportParameters":[{"parameterId":"viewFilter","values":[{"val":"shippedCOGSLevel"}]},{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"country","values":[{"val":"US"}]},{"parameterId":"state","values":[{"val":"ALL"}]},{"parameterId":"city","values":[{"val":"ALL"}]},{"parameterId":"zip","values":[{"val":"ALL"}]},{"parameterId":"aggregationFilter","values":[{"val":"ASINLevel"}]},{"parameterId":"countryCode","values":[{"val":"US"}]},{"parameterId":"periodStartDay","values":[{"val":date}]},{"parameterId":"periodEndDay","values":[{"val":date}]},{"parameterId":"isPeriodToDate","values":[{"val":false}]},{"parameterId":"isCustomDateRange","values":[{"val":"false"}]},{"parameterId":"hideLastYear","values":[{"val":false}]},{"parameterId":"period","values":[{"val":"DAILY"}]},{"parameterId":"dataRefreshDate","values":[{"val":"0000093999302"}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]},{"parameterId":"parentASINVisibility","values":[{"val":true}]},{"parameterId":"eanVisibility","values":[{"val":true}]},{"parameterId":"isbn13Visibility","values":[{"val":true}]},{"parameterId":"upcVisibility","values":[{"val":true}]},{"parameterId":"janVisibility","values":[{"val":false}]},{"parameterId":"brandVisibility","values":[{"val":true}]},{"parameterId":"brandCodeVisibility","values":[{"val":false}]},{"parameterId":"subcatVisibility","values":[{"val":true}]},{"parameterId":"catVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeWidthVisibility","values":[{"val":true}]},{"parameterId":"authorVisibility","values":[{"val":true}]},{"parameterId":"bindingVisibility","values":[{"val":true}]},{"parameterId":"catalogNumberVisibility","values":[{"val":false}]},{"parameterId":"colorVisibility","values":[{"val":true}]},{"parameterId":"colorCountVisibility","values":[{"val":false}]},{"parameterId":"manufactureOnDemandVisibility","values":[{"val":false}]},{"parameterId":"listPriceVisibility","values":[{"val":false}]},{"parameterId":"modelStyleVisibility","values":[{"val":true}]},{"parameterId":"productGroupVisibility","values":[{"val":false}]},{"parameterId":"releaseDateVisibility","values":[{"val":false}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"shippedcogs","ascending":false}],"reportPagination":{"pageIndex":0,"pageSize":100}}},
                        headers: {'Accept':'application/json', 'Content-Type':'application/json'}
                      });
                      this.then(function(){
                        fs.write('storage/count.json', this.page.plainText);
                        var obj = JSON.parse(this.getPageContent());
                        
                          if (obj && obj['payload'] && obj['payload']['reportParts'] && obj['payload']['reportParts'][0] && obj['payload']['reportParts'][0]['rowCount']) {
                          var rowCount = obj['payload']['reportParts'][0]['rowCount'];
                          console.log('RowCount: '+rowCount+'');

                          var paginate = rowCount.replace(/\,/g,'');
                            paginate = parseInt(paginate,10);
                            var number = paginate/10000;
                            var pages = Math.floor(number);

                            var parts = [];
                            for (h = 0; h <= pages; h++) {
                              parts.push(h);
                            }

                            this.each(parts, function (self, part) {
                              if (fs.isFile('storage/geographic/GeographicSalesDetail_retail_'+vgid+'_'+date+'_part'+part+'.json') && compare_to != date){ 
                                console.log('Geographic Already Scraped!');
                              } else {
                              this.wait(1000, function() {
                                

                                  this.thenOpen('https://vendorcentral.amazon.eu/analytics/data/dashboard/geographicSalesInsights/report/geographicSalesInsightsDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                                      method: 'POST',
                                      data:   {"reportParameters":[{"parameterId":"viewFilter","values":[{"val":"shippedCOGSLevel"}]},{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"country","values":[{"val":"US"}]},{"parameterId":"state","values":[{"val":"ALL"}]},{"parameterId":"city","values":[{"val":"ALL"}]},{"parameterId":"zip","values":[{"val":"ALL"}]},{"parameterId":"aggregationFilter","values":[{"val":"ASINLevel"}]},{"parameterId":"countryCode","values":[{"val":"US"}]},{"parameterId":"periodStartDay","values":[{"val":date}]},{"parameterId":"periodEndDay","values":[{"val":date}]},{"parameterId":"isPeriodToDate","values":[{"val":false}]},{"parameterId":"isCustomDateRange","values":[{"val":"false"}]},{"parameterId":"hideLastYear","values":[{"val":false}]},{"parameterId":"period","values":[{"val":"DAILY"}]},{"parameterId":"dataRefreshDate","values":[{"val":"0000093999302"}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]},{"parameterId":"parentASINVisibility","values":[{"val":true}]},{"parameterId":"eanVisibility","values":[{"val":true}]},{"parameterId":"isbn13Visibility","values":[{"val":true}]},{"parameterId":"upcVisibility","values":[{"val":true}]},{"parameterId":"janVisibility","values":[{"val":false}]},{"parameterId":"brandVisibility","values":[{"val":true}]},{"parameterId":"brandCodeVisibility","values":[{"val":false}]},{"parameterId":"subcatVisibility","values":[{"val":true}]},{"parameterId":"catVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeWidthVisibility","values":[{"val":true}]},{"parameterId":"authorVisibility","values":[{"val":true}]},{"parameterId":"bindingVisibility","values":[{"val":true}]},{"parameterId":"catalogNumberVisibility","values":[{"val":false}]},{"parameterId":"colorVisibility","values":[{"val":true}]},{"parameterId":"colorCountVisibility","values":[{"val":false}]},{"parameterId":"manufactureOnDemandVisibility","values":[{"val":false}]},{"parameterId":"listPriceVisibility","values":[{"val":false}]},{"parameterId":"modelStyleVisibility","values":[{"val":true}]},{"parameterId":"productGroupVisibility","values":[{"val":false}]},{"parameterId":"releaseDateVisibility","values":[{"val":false}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"shippedcogs","ascending":false}],"reportPagination":{"pageIndex":part,"pageSize":10000}}},
                                      headers: {
                                          'Accept':'application/json',
                                          'Content-Type':'application/json',
                                      }
                                  });
                                  this.then(function(){
                                    console.log('GeographicSalesDetail_retail_'+vgid+'_'+date+'_part'+part+'.json');
                                    fs.write('storage/geographic/GeographicSalesDetail_retail_'+vgid+'_'+date+'_part'+part+'.json', this.page.plainText);
                                  });
                              });
                            }
                            });
                          } else {
                            console.log('GeographicSalesDetail_retail_'+vgid+'_'+date+' : NONE');
                          }
                      });
                    });

                    //sales
                    this.then(function() {
                      //retail
                      this.then(function() {
                        this.wait(1000, function () {
                          this.thenOpen('https://vendorcentral.amazon.eu/analytics/data/dashboard/salesDiagnostic/report/salesDiagnosticDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                            method: 'POST',
                            data:   {"requestId":"b8e3de1a-2a10-4fe2-9e58-f19e68753e3a","reportParameters":[{"parameterId":"distributorView","values":[{"val":"manufacturer"}]},{"parameterId":"viewFilter","values":[{"val":"shippedCOGSLevel"}]},{"parameterId":"productView","values":[{"val":"kindleExcluded"}]},{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"aggregationFilter","values":[{"val":"ASINLevel"}]},{"parameterId":"periodStartDay","values":[{"val":date}]},{"parameterId":"periodEndDay","values":[{"val":date}]},{"parameterId":"isPeriodToDate","values":[{"val":false}]},{"parameterId":"isCustomDateRange","values":[{"val":"false"}]},{"parameterId":"hideLastYear","values":[{"val":false}]},{"parameterId":"period","values":[{"val":"DAILY"}]},{"parameterId":"dataRefreshDate","values":[{"val":"0000790022800"}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]},{"parameterId":"parentASINVisibility","values":[{"val":true}]},{"parameterId":"eanVisibility","values":[{"val":true}]},{"parameterId":"isbn13Visibility","values":[{"val":true}]},{"parameterId":"upcVisibility","values":[{"val":true}]},{"parameterId":"janVisibility","values":[{"val":false}]},{"parameterId":"brandVisibility","values":[{"val":true}]},{"parameterId":"brandCodeVisibility","values":[{"val":false}]},{"parameterId":"subcatVisibility","values":[{"val":true}]},{"parameterId":"catVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeWidthVisibility","values":[{"val":true}]},{"parameterId":"authorVisibility","values":[{"val":true}]},{"parameterId":"bindingVisibility","values":[{"val":true}]},{"parameterId":"catalogNumberVisibility","values":[{"val":false}]},{"parameterId":"colorVisibility","values":[{"val":true}]},{"parameterId":"colorCountVisibility","values":[{"val":false}]},{"parameterId":"manufactureOnDemandVisibility","values":[{"val":false}]},{"parameterId":"listPriceVisibility","values":[{"val":false}]},{"parameterId":"modelStyleVisibility","values":[{"val":true}]},{"parameterId":"productGroupVisibility","values":[{"val":false}]},{"parameterId":"releaseDateVisibility","values":[{"val":true}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"shippedcogs","ascending":false}],"reportPagination":{"pageIndex":0,"pageSize":100}}},
                            headers: {
                                'Accept':'application/json',
                                'Content-Type':'application/json',
                            }
                          });
                        });

                        this.then(function(){
                          fs.write('storage/sales_retail_count.json', this.page.plainText);

                          var obj = JSON.parse(this.getPageContent());

                            if (obj && obj['payload'] && obj['payload']['reportParts'] && obj['payload']['reportParts'][0] && obj['payload']['reportParts'][0]['rowCount']) {
                              var rowCount = obj['payload']['reportParts'][0]['rowCount'];
                              console.log('RowCount: '+rowCount+'');
                              var paginate = rowCount.replace(/\,/g,'');
                              paginate = parseInt(paginate,10);
                              var number = paginate/10000;
                              var pages = Math.floor(number);

                              var parts = [];
                              for (h = 0; h <= pages; h++) {
                                parts.push(h);
                              }

                              this.each(parts, function (self, part) {
                              if (fs.isFile('storage/salesdiagnostics/SalesDiagnosticsDetail_retail_'+vgid+'_'+date+'_part'+part+'.json') && compare_to != date){ 
                                console.log('Sales Retail Already Scraped!');
                              } else {
                                this.wait(1000, function() {
                                  
                                  this.thenOpen('https://vendorcentral.amazon.eu/analytics/data/dashboard/salesDiagnostic/report/salesDiagnosticDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                                      method: 'POST',
                                      data:   {
                                          "requestId":"b8e3de1a-2a10-4fe2-9e58-f19e68753e3a","reportParameters":[{"parameterId":"distributorView","values":[{"val":"manufacturer"}]},{"parameterId":"viewFilter","values":[{"val":"shippedCOGSLevel"}]},{"parameterId":"productView","values":[{"val":"kindleExcluded"}]},{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"aggregationFilter","values":[{"val":"ASINLevel"}]},{"parameterId":"periodStartDay","values":[{"val":date}]},{"parameterId":"periodEndDay","values":[{"val":date}]},{"parameterId":"isPeriodToDate","values":[{"val":false}]},{"parameterId":"isCustomDateRange","values":[{"val":"false"}]},{"parameterId":"hideLastYear","values":[{"val":false}]},{"parameterId":"period","values":[{"val":"DAILY"}]},{"parameterId":"dataRefreshDate","values":[{"val":"0000790022800"}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]},{"parameterId":"parentASINVisibility","values":[{"val":true}]},{"parameterId":"eanVisibility","values":[{"val":true}]},{"parameterId":"isbn13Visibility","values":[{"val":true}]},{"parameterId":"upcVisibility","values":[{"val":true}]},{"parameterId":"janVisibility","values":[{"val":false}]},{"parameterId":"brandVisibility","values":[{"val":true}]},{"parameterId":"brandCodeVisibility","values":[{"val":false}]},{"parameterId":"subcatVisibility","values":[{"val":true}]},{"parameterId":"catVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeWidthVisibility","values":[{"val":true}]},{"parameterId":"authorVisibility","values":[{"val":true}]},{"parameterId":"bindingVisibility","values":[{"val":true}]},{"parameterId":"catalogNumberVisibility","values":[{"val":false}]},{"parameterId":"colorVisibility","values":[{"val":true}]},{"parameterId":"colorCountVisibility","values":[{"val":false}]},{"parameterId":"manufactureOnDemandVisibility","values":[{"val":false}]},{"parameterId":"listPriceVisibility","values":[{"val":false}]},{"parameterId":"modelStyleVisibility","values":[{"val":true}]},{"parameterId":"productGroupVisibility","values":[{"val":false}]},{"parameterId":"releaseDateVisibility","values":[{"val":true}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"shippedcogs","ascending":false}],"reportPagination":{"pageIndex":part,"pageSize":10000}}
                                      },
                                      headers: {
                                          'Accept':'application/json',
                                          'Content-Type':'application/json',
                                      }
                                  });
                                  this.then(function(){
                                    console.log('SalesDiagnosticsDetail_retail_'+vgid+'_'+date+'_part'+part+'.json');
                                    fs.write('storage/salesdiagnostics/SalesDiagnosticsDetail_retail_'+vgid+'_'+date+'_part'+part+'.json', this.page.plainText);
                                  });
                                });
                              }
                              });
                            }
                            else {
                              console.log('SalesDiagnosticsDetail_retail_'+vgid+'_'+date+' : NONE');
                            }
                        });
                    });
                      //fresh
                      this.then(function() {
                        this.wait(1000, function () {
                          this.thenOpen('https://vendorcentral.amazon.eu/analytics/data/dashboard/freshSalesDiagnostic/report/freshSalesDiagnosticDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                            method: 'POST',
                            data:   {"requestId":"741b983e-a9c6-449f-a9e8-e885fa13d88b","reportParameters":[{"parameterId":"viewFilter","values":[{"val":"shippedCOGSLevel"}]},{"parameterId":"city","values":[{"val":"ALL"}]},{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"aggregationFilter","values":[{"val":"ASINLevel"}]},{"parameterId":"isPeriodToDate","values":[{"val":false}]},{"parameterId":"isCustomDateRange","values":[{"val":"false"}]},{"parameterId":"hideLastYear","values":[{"val":false}]},{"parameterId":"period","values":[{"val":"DAILY"}]},{"parameterId":"dataRefreshDate","values":[{"val":"0001024832551"}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]},{"parameterId":"parentASINVisibility","values":[{"val":true}]},{"parameterId":"eanVisibility","values":[{"val":true}]},{"parameterId":"isbn13Visibility","values":[{"val":true}]},{"parameterId":"upcVisibility","values":[{"val":true}]},{"parameterId":"janVisibility","values":[{"val":false}]},{"parameterId":"brandVisibility","values":[{"val":true}]},{"parameterId":"brandCodeVisibility","values":[{"val":false}]},{"parameterId":"subcatVisibility","values":[{"val":true}]},{"parameterId":"catVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeWidthVisibility","values":[{"val":true}]},{"parameterId":"authorVisibility","values":[{"val":true}]},{"parameterId":"bindingVisibility","values":[{"val":true}]},{"parameterId":"catalogNumberVisibility","values":[{"val":false}]},{"parameterId":"colorVisibility","values":[{"val":true}]},{"parameterId":"colorCountVisibility","values":[{"val":false}]},{"parameterId":"manufactureOnDemandVisibility","values":[{"val":false}]},{"parameterId":"listPriceVisibility","values":[{"val":false}]},{"parameterId":"modelStyleVisibility","values":[{"val":true}]},{"parameterId":"productGroupVisibility","values":[{"val":false}]},{"parameterId":"releaseDateVisibility","values":[{"val":false}]},{"parameterId":"periodStartDay","values":[{"val":date}]},{"parameterId":"periodEndDay","values":[{"val":date}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"shippedcogs","ascending":false}],"reportPagination":{"pageIndex":0,"pageSize":100}}},
                            headers: {
                                'Accept':'application/json',
                                'Content-Type':'application/json',
                            }
                          });
                        });

                        this.then(function(){
                          fs.write('storage/sales_fresh_count.json', this.page.plainText);
                          var obj = JSON.parse(this.getPageContent());
                            if( obj && obj['payload'] && obj['payload']['reportParts'] && obj['payload']['reportParts'][0] && obj['payload']['reportParts'][0]['rowCount'] ) {
                              var rowCount = obj['payload']['reportParts'][0]['rowCount'];
                              console.log('RowCount: '+rowCount+'');
                              var paginate = rowCount.replace(/\,/g,'');
                              paginate = parseInt(paginate,10);
                              var number = paginate/10000;
                              var pages = Math.floor(number);

                              var parts = [];
                              for (h = 0; h <= pages; h++) {
                                parts.push(h);
                              }

                              this.each(parts, function (self, part) {
                              if (fs.isFile('storage/salesdiagnostics/SalesDiagnosticsDetail_fresh_'+vgid+'_'+date+'_part'+part+'.json') && compare_to != date){ 
                                console.log('Sales Fresh Already Scraped!');
                              } else {
                                this.wait(1000, function() {
                                  
                                  this.thenOpen('https://vendorcentral.amazon.eu/analytics/data/dashboard/freshSalesDiagnostic/report/freshSalesDiagnosticDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                                      method: 'POST',
                                      data:   {"requestId":"741b983e-a9c6-449f-a9e8-e885fa13d88b","reportParameters":[{"parameterId":"viewFilter","values":[{"val":"shippedCOGSLevel"}]},{"parameterId":"city","values":[{"val":"ALL"}]},{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"aggregationFilter","values":[{"val":"ASINLevel"}]},{"parameterId":"isPeriodToDate","values":[{"val":false}]},{"parameterId":"isCustomDateRange","values":[{"val":"false"}]},{"parameterId":"hideLastYear","values":[{"val":false}]},{"parameterId":"period","values":[{"val":"DAILY"}]},{"parameterId":"dataRefreshDate","values":[{"val":"0001024832551"}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]},{"parameterId":"parentASINVisibility","values":[{"val":true}]},{"parameterId":"eanVisibility","values":[{"val":true}]},{"parameterId":"isbn13Visibility","values":[{"val":true}]},{"parameterId":"upcVisibility","values":[{"val":true}]},{"parameterId":"janVisibility","values":[{"val":false}]},{"parameterId":"brandVisibility","values":[{"val":true}]},{"parameterId":"brandCodeVisibility","values":[{"val":false}]},{"parameterId":"subcatVisibility","values":[{"val":true}]},{"parameterId":"catVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeWidthVisibility","values":[{"val":true}]},{"parameterId":"authorVisibility","values":[{"val":true}]},{"parameterId":"bindingVisibility","values":[{"val":true}]},{"parameterId":"catalogNumberVisibility","values":[{"val":false}]},{"parameterId":"colorVisibility","values":[{"val":true}]},{"parameterId":"colorCountVisibility","values":[{"val":false}]},{"parameterId":"manufactureOnDemandVisibility","values":[{"val":false}]},{"parameterId":"listPriceVisibility","values":[{"val":false}]},{"parameterId":"modelStyleVisibility","values":[{"val":true}]},{"parameterId":"productGroupVisibility","values":[{"val":false}]},{"parameterId":"releaseDateVisibility","values":[{"val":false}]},{"parameterId":"periodStartDay","values":[{"val":date}]},{"parameterId":"periodEndDay","values":[{"val":date}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"shippedcogs","ascending":false}],"reportPagination":{"pageIndex":part,"pageSize":10000}}},
                                      headers: {
                                          'Accept':'application/json',
                                          'Content-Type':'application/json',
                                      }
                                  });
                                  this.then(function(){
                                    console.log('SalesDiagnosticsDetail_fresh_'+vgid+'_'+date+'_part'+part+'.json');
                                    fs.write('storage/salesdiagnostics/SalesDiagnosticsDetail_fresh_'+vgid+'_'+date+'_part'+part+'.json', this.page.plainText);
                                  });
                                });
                              }
                              });
                            } else {
                              console.log('SalesDiagnosticsDetail_fresh_'+vgid+'_'+date+' : NONE');
                            }
                        });
                    });
                      //prime
                      this.then(function() {
                        this.wait(1000, function () {
                          this.thenOpen('https://vendorcentral.amazon.eu/analytics/data/dashboard/primeNowSalesDiagnostic/report/primeNowSalesDiagnosticDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                            method: 'POST',
                            data:   {"requestId":"23399442-a038-4ba4-a7db-fee9c55c3c4b","reportParameters":[{"parameterId":"viewFilter","values":[{"val":"shippedCOGSLevel"}]},{"parameterId":"city","values":[{"val":"ALL"}]},{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"aggregationFilter","values":[{"val":"ASINLevel"}]},{"parameterId":"isPeriodToDate","values":[{"val":false}]},{"parameterId":"isCustomDateRange","values":[{"val":"false"}]},{"parameterId":"hideLastYear","values":[{"val":false}]},{"parameterId":"period","values":[{"val":"DAILY"}]},{"parameterId":"dataRefreshDate","values":[{"val":"0001116193202"}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]},{"parameterId":"parentASINVisibility","values":[{"val":true}]},{"parameterId":"eanVisibility","values":[{"val":true}]},{"parameterId":"isbn13Visibility","values":[{"val":true}]},{"parameterId":"upcVisibility","values":[{"val":true}]},{"parameterId":"janVisibility","values":[{"val":false}]},{"parameterId":"brandVisibility","values":[{"val":true}]},{"parameterId":"brandCodeVisibility","values":[{"val":false}]},{"parameterId":"subcatVisibility","values":[{"val":true}]},{"parameterId":"catVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeWidthVisibility","values":[{"val":true}]},{"parameterId":"authorVisibility","values":[{"val":true}]},{"parameterId":"bindingVisibility","values":[{"val":true}]},{"parameterId":"catalogNumberVisibility","values":[{"val":false}]},{"parameterId":"colorVisibility","values":[{"val":true}]},{"parameterId":"colorCountVisibility","values":[{"val":false}]},{"parameterId":"manufactureOnDemandVisibility","values":[{"val":false}]},{"parameterId":"listPriceVisibility","values":[{"val":false}]},{"parameterId":"modelStyleVisibility","values":[{"val":true}]},{"parameterId":"productGroupVisibility","values":[{"val":false}]},{"parameterId":"releaseDateVisibility","values":[{"val":false}]},{"parameterId":"periodStartDay","values":[{"val":date}]},{"parameterId":"periodEndDay","values":[{"val":date}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"shippedcogs","ascending":false}],"reportPagination":{"pageIndex":0,"pageSize":100}}},
                            headers: {
                                'Accept':'application/json',
                                'Content-Type':'application/json',
                            }
                          });
                        });

                        this.then(function(){
                          fs.write('storage/sales_prime_count.json', this.page.plainText);
                          var obj = JSON.parse(this.getPageContent());
                            if( obj && obj['payload'] && obj['payload']['reportParts'] && obj['payload']['reportParts'][0] && obj['payload']['reportParts'][0]['rowCount'] ) {
                              var rowCount = obj['payload']['reportParts'][0]['rowCount'];
                              console.log('RowCount: '+rowCount+'');
                              var paginate = rowCount.replace(/\,/g,'');
                              paginate = parseInt(paginate,10);
                              var number = paginate/10000;
                              var pages = Math.floor(number);

                              var parts = [];
                              for (h = 0; h <= pages; h++) {
                                parts.push(h);
                              }

                              this.each(parts, function (self, part) {
                              if (fs.isFile('storage/salesdiagnostics/SalesDiagnosticsDetail_prime_'+vgid+'_'+date+'_part'+part+'.json') && compare_to != date){ 
                                console.log('Sales Prime Already Scraped!');
                              } else {
                                this.wait(1000, function() {
                                  
                                  this.thenOpen('https://vendorcentral.amazon.eu/analytics/data/dashboard/primeNowSalesDiagnostic/report/primeNowSalesDiagnosticDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                                      method: 'POST',
                                      data:   {"requestId":"23399442-a038-4ba4-a7db-fee9c55c3c4b","reportParameters":[{"parameterId":"viewFilter","values":[{"val":"shippedCOGSLevel"}]},{"parameterId":"city","values":[{"val":"ALL"}]},{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"aggregationFilter","values":[{"val":"ASINLevel"}]},{"parameterId":"isPeriodToDate","values":[{"val":false}]},{"parameterId":"isCustomDateRange","values":[{"val":"false"}]},{"parameterId":"hideLastYear","values":[{"val":false}]},{"parameterId":"period","values":[{"val":"DAILY"}]},{"parameterId":"dataRefreshDate","values":[{"val":"0001116193202"}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]},{"parameterId":"parentASINVisibility","values":[{"val":true}]},{"parameterId":"eanVisibility","values":[{"val":true}]},{"parameterId":"isbn13Visibility","values":[{"val":true}]},{"parameterId":"upcVisibility","values":[{"val":true}]},{"parameterId":"janVisibility","values":[{"val":false}]},{"parameterId":"brandVisibility","values":[{"val":true}]},{"parameterId":"brandCodeVisibility","values":[{"val":false}]},{"parameterId":"subcatVisibility","values":[{"val":true}]},{"parameterId":"catVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeWidthVisibility","values":[{"val":true}]},{"parameterId":"authorVisibility","values":[{"val":true}]},{"parameterId":"bindingVisibility","values":[{"val":true}]},{"parameterId":"catalogNumberVisibility","values":[{"val":false}]},{"parameterId":"colorVisibility","values":[{"val":true}]},{"parameterId":"colorCountVisibility","values":[{"val":false}]},{"parameterId":"manufactureOnDemandVisibility","values":[{"val":false}]},{"parameterId":"listPriceVisibility","values":[{"val":false}]},{"parameterId":"modelStyleVisibility","values":[{"val":true}]},{"parameterId":"productGroupVisibility","values":[{"val":false}]},{"parameterId":"releaseDateVisibility","values":[{"val":false}]},{"parameterId":"periodStartDay","values":[{"val":date}]},{"parameterId":"periodEndDay","values":[{"val":date}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"shippedcogs","ascending":false}],"reportPagination":{"pageIndex":part,"pageSize":100}}},
                                      headers: {
                                          'Accept':'application/json',
                                          'Content-Type':'application/json',
                                      }
                                  });
                                  this.then(function(){
                                    console.log('SalesDiagnosticsDetail_prime_'+vgid+'_'+date+'_part'+part+'.json');
                                    fs.write('storage/salesdiagnostics/SalesDiagnosticsDetail_prime_'+vgid+'_'+date+'_part'+part+'.json', this.page.plainText);
                                  }); 
                                });
                              }
                              });
                            } else {
                              console.log('SalesDiagnosticsDetail_prime_'+vgid+'_'+date+' : NONE');
                            }
                        });
                    });
                      //business
                      this.then(function() {
                        this.wait(1000, function () {
                          this.thenOpen('https://vendorcentral.amazon.eu/analytics/data/dashboard/businessSalesDiagnostic/report/businessSalesDiagnosticDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                            method: 'POST',
                            data:   {"requestId":"3d2ae09e-77e1-4b5c-9b00-ec0e02cb8499","reportParameters":[{"parameterId":"viewFilter","values":[{"val":"shippedCOGSLevel"}]},{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"aggregationFilter","values":[{"val":"ASINLevel"}]},{"parameterId":"periodStartDay","values":[{"val":date}]},{"parameterId":"periodEndDay","values":[{"val":date}]},{"parameterId":"isPeriodToDate","values":[{"val":true}]},{"parameterId":"isCustomDateRange","values":[{"val":"false"}]},{"parameterId":"hideLastYear","values":[{"val":false}]},{"parameterId":"period","values":[{"val":"DAILY"}]},{"parameterId":"dataRefreshDate","values":[{"val":"0000078742193"}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]},{"parameterId":"parentASINVisibility","values":[{"val":true}]},{"parameterId":"eanVisibility","values":[{"val":true}]},{"parameterId":"isbn13Visibility","values":[{"val":true}]},{"parameterId":"upcVisibility","values":[{"val":true}]},{"parameterId":"janVisibility","values":[{"val":false}]},{"parameterId":"brandVisibility","values":[{"val":true}]},{"parameterId":"brandCodeVisibility","values":[{"val":false}]},{"parameterId":"subcatVisibility","values":[{"val":true}]},{"parameterId":"catVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeWidthVisibility","values":[{"val":true}]},{"parameterId":"authorVisibility","values":[{"val":true}]},{"parameterId":"bindingVisibility","values":[{"val":true}]},{"parameterId":"catalogNumberVisibility","values":[{"val":false}]},{"parameterId":"colorVisibility","values":[{"val":true}]},{"parameterId":"colorCountVisibility","values":[{"val":false}]},{"parameterId":"manufactureOnDemandVisibility","values":[{"val":false}]},{"parameterId":"listPriceVisibility","values":[{"val":false}]},{"parameterId":"modelStyleVisibility","values":[{"val":true}]},{"parameterId":"productGroupVisibility","values":[{"val":false}]},{"parameterId":"releaseDateVisibility","values":[{"val":false}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"shippedcogs","ascending":false}],"reportPagination":{"pageIndex":0,"pageSize":100}}},
                            headers: {
                                'Accept':'application/json',
                                'Content-Type':'application/json',
                            }
                          });
                        });

                        this.then(function() {

                        });

                        this.then(function(){
                          fs.write('storage/sales_business_count.json', this.page.plainText);

                          var obj = JSON.parse(this.getPageContent());

                            if( obj && obj['payload'] && obj['payload']['reportParts'] && obj['payload']['reportParts'][0] && obj['payload']['reportParts'][0]['rowCount'] ) {
                              var rowCount = obj['payload']['reportParts'][0]['rowCount'];
                              console.log('RowCount: '+rowCount+'');

                              var paginate = rowCount.replace(/\,/g,'');
                              paginate = parseInt(paginate,10);
                              var number = paginate/10000;
                              var pages = Math.floor(number);

                              var parts = [];
                              for (h = 0; h <= pages; h++) {
                                parts.push(h);
                              }

                              this.each(parts, function (self, part) {
                              if (fs.isFile('storage/salesdiagnostics/SalesDiagnosticsDetail_business_'+vgid+'_'+date+'_part'+part+'.json') && compare_to != date){ 
                                console.log('Sales Business Already Scraped!');
                              } else {
                                this.wait(1000, function() {
                                  
                                  this.thenOpen('https://vendorcentral.amazon.eu/analytics/data/dashboard/businessSalesDiagnostic/report/businessSalesDiagnosticDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                                      method: 'POST',
                                      data:   {"requestId":"3d2ae09e-77e1-4b5c-9b00-ec0e02cb8499","reportParameters":[{"parameterId":"viewFilter","values":[{"val":"shippedCOGSLevel"}]},{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"aggregationFilter","values":[{"val":"ASINLevel"}]},{"parameterId":"periodStartDay","values":[{"val":date}]},{"parameterId":"periodEndDay","values":[{"val":date}]},{"parameterId":"isPeriodToDate","values":[{"val":true}]},{"parameterId":"isCustomDateRange","values":[{"val":"false"}]},{"parameterId":"hideLastYear","values":[{"val":false}]},{"parameterId":"period","values":[{"val":"DAILY"}]},{"parameterId":"dataRefreshDate","values":[{"val":"0000078742193"}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]},{"parameterId":"parentASINVisibility","values":[{"val":true}]},{"parameterId":"eanVisibility","values":[{"val":true}]},{"parameterId":"isbn13Visibility","values":[{"val":true}]},{"parameterId":"upcVisibility","values":[{"val":true}]},{"parameterId":"janVisibility","values":[{"val":false}]},{"parameterId":"brandVisibility","values":[{"val":true}]},{"parameterId":"brandCodeVisibility","values":[{"val":false}]},{"parameterId":"subcatVisibility","values":[{"val":true}]},{"parameterId":"catVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeWidthVisibility","values":[{"val":true}]},{"parameterId":"authorVisibility","values":[{"val":true}]},{"parameterId":"bindingVisibility","values":[{"val":true}]},{"parameterId":"catalogNumberVisibility","values":[{"val":false}]},{"parameterId":"colorVisibility","values":[{"val":true}]},{"parameterId":"colorCountVisibility","values":[{"val":false}]},{"parameterId":"manufactureOnDemandVisibility","values":[{"val":false}]},{"parameterId":"listPriceVisibility","values":[{"val":false}]},{"parameterId":"modelStyleVisibility","values":[{"val":true}]},{"parameterId":"productGroupVisibility","values":[{"val":false}]},{"parameterId":"releaseDateVisibility","values":[{"val":false}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"shippedcogs","ascending":false}],"reportPagination":{"pageIndex":part,"pageSize":10000}}},
                                      headers: {
                                          'Accept':'application/json',
                                          'Content-Type':'application/json',
                                      }
                                  });
                                  this.then(function(){
                                    console.log('SalesDiagnosticsDetail_business_'+vgid+'_'+date+'_part'+part+'.json');
                                    fs.write('storage/salesdiagnostics/SalesDiagnosticsDetail_business_'+vgid+'_'+date+'_part'+part+'.json', this.page.plainText);
                                  });
                                });
                              }
                              });
                            }
                            else {
                              console.log('SalesDiagnosticsDetail_business_'+vgid+'_'+date+' : NONE')
                            }
                        });
                    });
                });

                    //preorders
                    this.then(function() {
                      this.wait(5000, function () {
                        this.thenOpen('https://vendorcentral.amazon.eu/analytics/data/dashboard/preOrders/report/preOrdersDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                            method: 'POST',
                            data:   {"reportParameters":[{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"aggregationFilter","values":[{"val":"ASINLevel"}]},{"parameterId":"periodStartDay","values":[{"val":date}]},{"parameterId":"periodEndDay","values":[{"val":date}]},{"parameterId":"isPeriodToDate","values":[{"val":false}]},{"parameterId":"isCustomDateRange","values":[{"val":"false"}]},{"parameterId":"hideLastYear","values":[{"val":false}]},{"parameterId":"period","values":[{"val":"DAILY"}]},{"parameterId":"dataRefreshDate","values":[{"val":"0000502050172"}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]},{"parameterId":"parentASINVisibility","values":[{"val":true}]},{"parameterId":"eanVisibility","values":[{"val":true}]},{"parameterId":"isbn13Visibility","values":[{"val":true}]},{"parameterId":"upcVisibility","values":[{"val":true}]},{"parameterId":"janVisibility","values":[{"val":false}]},{"parameterId":"brandVisibility","values":[{"val":true}]},{"parameterId":"brandCodeVisibility","values":[{"val":false}]},{"parameterId":"subcatVisibility","values":[{"val":true}]},{"parameterId":"catVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeWidthVisibility","values":[{"val":true}]},{"parameterId":"authorVisibility","values":[{"val":true}]},{"parameterId":"bindingVisibility","values":[{"val":true}]},{"parameterId":"catalogNumberVisibility","values":[{"val":false}]},{"parameterId":"colorVisibility","values":[{"val":true}]},{"parameterId":"colorCountVisibility","values":[{"val":false}]},{"parameterId":"manufactureOnDemandVisibility","values":[{"val":false}]},{"parameterId":"listPriceVisibility","values":[{"val":false}]},{"parameterId":"modelStyleVisibility","values":[{"val":true}]},{"parameterId":"productGroupVisibility","values":[{"val":false}]},{"parameterId":"releaseDateVisibility","values":[{"val":true}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"preorderedamount","ascending":false}],"reportPagination":{"pageIndex":0,"pageSize":100}}},
                            headers: {
                                'Accept':'application/json',
                                'Content-Type':'application/json',
                            }
                        });
                      });

                      this.then(function(){
                        var parts = [];
                        fs.write('storage/pre_count.json', this.page.plainText);
                        var obj = JSON.parse( this.getPageContent());
                          if(obj && obj['payload'] && obj['payload']['reportParts'] && obj['payload']['reportParts'][0] && obj['payload']['reportParts'][0]['rowCount']) {
                            var rowCount = obj['payload']['reportParts'][0]['rowCount'];
                            console.log('RowCount: '+rowCount+'');

                            var paginate = rowCount.replace(/\,/g,'');
                            paginate = parseInt(paginate,10);
                            var number = paginate/10000;
                            var pages = Math.floor(number);
                            for (h = 0; h <= pages; h++) {
                              parts.push(h);
                            }
                            this.each(parts, function (self, part) {
                              if (fs.isFile('storage/preorder/PreOrdersDetail_'+vgid+'_'+date+'_part'+part+'.json') && compare_to != date){ 
                                console.log('PreOrders Already Scraped!');
                              } else {
                              this.wait(5000, function() {
                                this.thenOpen('https://vendorcentral.amazon.eu/analytics/data/dashboard/preOrders/report/preOrdersDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                                    method: 'POST',
                                    data:   {"reportParameters":[{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"aggregationFilter","values":[{"val":"ASINLevel"}]},{"parameterId":"periodStartDay","values":[{"val":date}]},{"parameterId":"periodEndDay","values":[{"val":date}]},{"parameterId":"isPeriodToDate","values":[{"val":true}]},{"parameterId":"isCustomDateRange","values":[{"val":"true"}]},{"parameterId":"hideLastYear","values":[{"val":false}]},{"parameterId":"period","values":[{"val":"DAILY"}]},{"parameterId":"dataRefreshDate","values":[{"val":"0000419636133"}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]},{"parameterId":"parentASINVisibility","values":[{"val":true}]},{"parameterId":"eanVisibility","values":[{"val":true}]},{"parameterId":"isbn13Visibility","values":[{"val":true}]},{"parameterId":"upcVisibility","values":[{"val":true}]},{"parameterId":"janVisibility","values":[{"val":false}]},{"parameterId":"brandVisibility","values":[{"val":true}]},{"parameterId":"brandCodeVisibility","values":[{"val":false}]},{"parameterId":"subcatVisibility","values":[{"val":true}]},{"parameterId":"catVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeWidthVisibility","values":[{"val":true}]},{"parameterId":"authorVisibility","values":[{"val":true}]},{"parameterId":"bindingVisibility","values":[{"val":true}]},{"parameterId":"catalogNumberVisibility","values":[{"val":false}]},{"parameterId":"colorVisibility","values":[{"val":true}]},{"parameterId":"colorCountVisibility","values":[{"val":false}]},{"parameterId":"manufactureOnDemandVisibility","values":[{"val":false}]},{"parameterId":"listPriceVisibility","values":[{"val":false}]},{"parameterId":"modelStyleVisibility","values":[{"val":true}]},{"parameterId":"productGroupVisibility","values":[{"val":false}]},{"parameterId":"releaseDateVisibility","values":[{"val":true}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"preorderedamount","ascending":false}],"reportPagination":{"pageIndex":part,"pageSize":10000}}},
                                    headers: {
                                        'Accept':'application/json',
                                        'Content-Type':'application/json',
                                    }
                                });
                                this.then(function(){
                                  console.log('PreOrdersDetail_'+vgid+'_'+date+'_part'+part+'.json');
                                  fs.write('storage/preorder/PreOrdersDetail_'+vgid+'_'+date+'_part'+part+'.json', this.page.plainText);
                                });
                              });
                            }
                            });
                          } else {
                            console.log('PreOrdersDetail_'+vgid+'_'+date+' : NONE');
                          }
                      });

                    });

                    //inventory
                    this.then(function() {

                      this.wait(1000, function () {
                        this.thenOpen('https://vendorcentral.amazon.eu/analytics/data/dashboard/inventoryHealth/report/inventoryHealthDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                            method: 'POST',
                            data:   {"requestId":"bdefc92c-7660-4b77-b928-83bed94f0682","reportParameters":[{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"aggregationFilter","values":[{"val":"ASINLevel"}]},{"parameterId":"distributorView","values":[{"val":"manufacturer"}]},{"parameterId":"isFCAllowed","values":[{"val":"false"}]},{"parameterId":"periodStartDay","values":[{"val":date}]},{"parameterId":"periodEndDay","values":[{"val":date}]},{"parameterId":"isPeriodToDate","values":[{"val":false}]},{"parameterId":"isCustomDateRange","values":[{"val":"false"}]},{"parameterId":"hideLastYear","values":[{"val":false}]},{"parameterId":"period","values":[{"val":"DAILY"}]},{"parameterId":"dataRefreshDate","values":[{"val":"0002032000337"}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]},{"parameterId":"parentASINVisibility","values":[{"val":true}]},{"parameterId":"eanVisibility","values":[{"val":true}]},{"parameterId":"isbn13Visibility","values":[{"val":true}]},{"parameterId":"upcVisibility","values":[{"val":true}]},{"parameterId":"janVisibility","values":[{"val":false}]},{"parameterId":"brandVisibility","values":[{"val":true}]},{"parameterId":"brandCodeVisibility","values":[{"val":true}]},{"parameterId":"subcatVisibility","values":[{"val":true}]},{"parameterId":"catVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeWidthVisibility","values":[{"val":true}]},{"parameterId":"authorVisibility","values":[{"val":true}]},{"parameterId":"bindingVisibility","values":[{"val":true}]},{"parameterId":"catalogNumberVisibility","values":[{"val":false}]},{"parameterId":"colorVisibility","values":[{"val":true}]},{"parameterId":"colorCountVisibility","values":[{"val":true}]},{"parameterId":"manufactureOnDemandVisibility","values":[{"val":false}]},{"parameterId":"listPriceVisibility","values":[{"val":true}]},{"parameterId":"modelStyleVisibility","values":[{"val":true}]},{"parameterId":"productGroupVisibility","values":[{"val":true}]},{"parameterId":"releaseDateVisibility","values":[{"val":true}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"sellableonhandinventory","ascending":false}],"reportPagination":{"pageIndex":0,"pageSize":100}}},
                            headers: {
                                'Accept':'application/json',
                                'Content-Type':'application/json',
                            }
                        });
                      });

                      this.then(function(){
                        fs.write('storage/inv_count.json', this.page.plainText);
                        var obj = JSON.parse( this.getPageContent());
                        
                          if(obj && obj['payload'] && obj['payload']['reportParts'] && obj['payload']['reportParts'][0] && obj['payload']['reportParts'][0]['rowCount']) {
                            var rowCount = obj['payload']['reportParts'][0]['rowCount']; 
                            console.log('RowCount: '+rowCount+'');

                            var paginate = rowCount.replace(/\,/g,'');
                            paginate = parseInt(paginate,10);
                            var number = paginate/10000;
                            var pages = Math.floor(number);

                            var parts = [];
                            for (h = 0; h <= pages; h++) {
                              parts.push(h);
                            }

                            this.each(parts, function (self, part) {
                              if (fs.isFile('storage/inventory/InventoryHealthDetail_'+vgid+'_'+date+'_part'+part+'.json') && compare_to != date){ 
                                console.log('Inventory Already Scraped!');
                              } else {
                              this.wait(1000, function() {
                                this.thenOpen('https://vendorcentral.amazon.eu/analytics/data/dashboard/inventoryHealth/report/inventoryHealthDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                                    method: 'POST',
                                    data:   {"requestId":"bdefc92c-7660-4b77-b928-83bed94f0682","reportParameters":[{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"aggregationFilter","values":[{"val":"ASINLevel"}]},{"parameterId":"distributorView","values":[{"val":"manufacturer"}]},{"parameterId":"isFCAllowed","values":[{"val":"false"}]},{"parameterId":"periodStartDay","values":[{"val":date}]},{"parameterId":"periodEndDay","values":[{"val":date}]},{"parameterId":"isPeriodToDate","values":[{"val":false}]},{"parameterId":"isCustomDateRange","values":[{"val":"false"}]},{"parameterId":"hideLastYear","values":[{"val":false}]},{"parameterId":"period","values":[{"val":"DAILY"}]},{"parameterId":"dataRefreshDate","values":[{"val":"0002032000337"}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]},{"parameterId":"parentASINVisibility","values":[{"val":true}]},{"parameterId":"eanVisibility","values":[{"val":true}]},{"parameterId":"isbn13Visibility","values":[{"val":true}]},{"parameterId":"upcVisibility","values":[{"val":true}]},{"parameterId":"janVisibility","values":[{"val":false}]},{"parameterId":"brandVisibility","values":[{"val":true}]},{"parameterId":"brandCodeVisibility","values":[{"val":true}]},{"parameterId":"subcatVisibility","values":[{"val":true}]},{"parameterId":"catVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeWidthVisibility","values":[{"val":true}]},{"parameterId":"authorVisibility","values":[{"val":true}]},{"parameterId":"bindingVisibility","values":[{"val":true}]},{"parameterId":"catalogNumberVisibility","values":[{"val":false}]},{"parameterId":"colorVisibility","values":[{"val":true}]},{"parameterId":"colorCountVisibility","values":[{"val":true}]},{"parameterId":"manufactureOnDemandVisibility","values":[{"val":false}]},{"parameterId":"listPriceVisibility","values":[{"val":true}]},{"parameterId":"modelStyleVisibility","values":[{"val":true}]},{"parameterId":"productGroupVisibility","values":[{"val":true}]},{"parameterId":"releaseDateVisibility","values":[{"val":true}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"sellableonhandinventory","ascending":false}],"reportPagination":{"pageIndex":part,"pageSize":10000}}},
                                    headers: {
                                        'Accept':'application/json',
                                        'Content-Type':'application/json',
                                    }
                                });
                                this.then(function(){
                                  console.log('InventoryHealthDetail_'+vgid+'_'+date+'_part'+part+'.json');
                                  fs.write('storage/inventory/InventoryHealthDetail_'+vgid+'_'+date+'_part'+part+'.json', this.page.plainText);
                                });
                              });
                            }
                            });
                          } else {
                            console.log('InventoryHealthDetail_'+vgid+'_'+date+' : NONE');
                          }
                      });

                    });

                    //alternative
                    this.then(function() {

                      this.wait(1000, function () {
                        this.thenOpen('https://vendorcentral.amazon.eu/analytics/data/dashboard/itemComparisonAndAlternativePurchase/report/alternativePurchase?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                            method: 'POST',
                            data:   {"requestId":"8d07b226-5105-49cc-994e-f4371692ddeb","reportParameters":[{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"alternativePurchaseAggregationFilter","values":[{"val":"allProductsLevel"}]},{"parameterId":"period","values":[{"val":"DAILY"}]},{"parameterId":"periodEndDay","values":[{"val":date}]},{"parameterId":"dataRefreshDate","values":[{"val":"0001361388692"}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]},{"parameterId":"parentASINVisibility","values":[{"val":false}]},{"parameterId":"eanVisibility","values":[{"val":false}]},{"parameterId":"isbn13Visibility","values":[{"val":false}]},{"parameterId":"upcVisibility","values":[{"val":false}]},{"parameterId":"janVisibility","values":[{"val":false}]},{"parameterId":"brandVisibility","values":[{"val":false}]},{"parameterId":"brandCodeVisibility","values":[{"val":false}]},{"parameterId":"subcatVisibility","values":[{"val":false}]},{"parameterId":"catVisibility","values":[{"val":false}]},{"parameterId":"apparelSizeVisibility","values":[{"val":false}]},{"parameterId":"apparelSizeWidthVisibility","values":[{"val":false}]},{"parameterId":"authorVisibility","values":[{"val":false}]},{"parameterId":"bindingVisibility","values":[{"val":false}]},{"parameterId":"catalogNumberVisibility","values":[{"val":false}]},{"parameterId":"colorVisibility","values":[{"val":false}]},{"parameterId":"colorCountVisibility","values":[{"val":false}]},{"parameterId":"manufactureOnDemandVisibility","values":[{"val":false}]},{"parameterId":"listPriceVisibility","values":[{"val":false}]},{"parameterId":"modelStyleVisibility","values":[{"val":false}]},{"parameterId":"productGroupVisibility","values":[{"val":false}]},{"parameterId":"releaseDateVisibility","values":[{"val":false}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"orders","ascending":false}],"reportPagination":{"pageIndex":0,"pageSize":100}}},
                            headers: {
                                'Accept':'application/json',
                                'Content-Type':'application/json',
                            }
                        });
                      });

                      this.then(function(){
                        fs.write('storage/alt_count.json', this.page.plainText);
                        var obj = JSON.parse( this.getPageContent());

                          if(obj && obj['payload'] && obj['payload']['reportParts'] && obj['payload']['reportParts'][0] && obj['payload']['reportParts'][0]['rowCount']) {
                            var rowCount = obj['payload']['reportParts'][0]['rowCount'];
                            console.log('RowCount: '+rowCount+'');

                            var paginate = rowCount.replace(/\,/g,'');
                            paginate = parseInt(paginate,10);
                            var number = paginate/10000;
                            var pages = Math.floor(number);

                            var parts = [];
                            for (h = 0; h <= pages; h++) {
                              parts.push(h);
                            }

                            this.each(parts, function (self, part) {
                              if (fs.isFile('storage/alternate/AlternativePurchaseDetail_'+vgid+'_'+date+'_part'+part+'.json') && compare_to != date){ 
                                console.log('Alternative Already Scraped!');
                              } else {
                              this.wait(1000, function() {
                                this.thenOpen('https://vendorcentral.amazon.eu/analytics/data/dashboard/itemComparisonAndAlternativePurchase/report/alternativePurchase?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                                    method: 'POST',
                                    data:   {"requestId":"8d07b226-5105-49cc-994e-f4371692ddeb","reportParameters":[{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"alternativePurchaseAggregationFilter","values":[{"val":"allProductsLevel"}]},{"parameterId":"period","values":[{"val":"DAILY"}]},{"parameterId":"periodEndDay","values":[{"val":date}]},{"parameterId":"dataRefreshDate","values":[{"val":"0001361388692"}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]},{"parameterId":"parentASINVisibility","values":[{"val":false}]},{"parameterId":"eanVisibility","values":[{"val":false}]},{"parameterId":"isbn13Visibility","values":[{"val":false}]},{"parameterId":"upcVisibility","values":[{"val":false}]},{"parameterId":"janVisibility","values":[{"val":false}]},{"parameterId":"brandVisibility","values":[{"val":false}]},{"parameterId":"brandCodeVisibility","values":[{"val":false}]},{"parameterId":"subcatVisibility","values":[{"val":false}]},{"parameterId":"catVisibility","values":[{"val":false}]},{"parameterId":"apparelSizeVisibility","values":[{"val":false}]},{"parameterId":"apparelSizeWidthVisibility","values":[{"val":false}]},{"parameterId":"authorVisibility","values":[{"val":false}]},{"parameterId":"bindingVisibility","values":[{"val":false}]},{"parameterId":"catalogNumberVisibility","values":[{"val":false}]},{"parameterId":"colorVisibility","values":[{"val":false}]},{"parameterId":"colorCountVisibility","values":[{"val":false}]},{"parameterId":"manufactureOnDemandVisibility","values":[{"val":false}]},{"parameterId":"listPriceVisibility","values":[{"val":false}]},{"parameterId":"modelStyleVisibility","values":[{"val":false}]},{"parameterId":"productGroupVisibility","values":[{"val":false}]},{"parameterId":"releaseDateVisibility","values":[{"val":false}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"orders","ascending":false}],"reportPagination":{"pageIndex":part,"pageSize":10000}}},
                                    headers: {
                                        'Accept':'application/json',
                                        'Content-Type':'application/json',
                                    }
                                });
                                this.then(function(){
                                  console.log('AlternativePurchaseDetail_'+vgid+'_'+date+'_part'+part+'.json');
                                  fs.write('storage/alternate/AlternativePurchaseDetail_'+vgid+'_'+date+'_part'+part+'.json', this.page.plainText);
                                });
                              });
                            }
                            });
                          } else {
                            console.log('AlternativePurchaseDetail_'+vgid+'_'+date+' : NONE');
                          }
                      });
                      
                    });
                  });
                });
              });
            });
          });

  casper.then(function() {
      console.log('Scraped Successfully!');
  });

  casper.thenOpen("https://hc-ping.com/899d98df-29b0-478b-a810-21b1eea1e8b2");
  }

// Read Cookies
if (fs.isFile(cookie_path)){ 
  phantom.cookies = JSON.parse(fs.read(cookie_path));
}

casper.start(AMAZON_SITE, function() {  
    this.echo("Amazon website opened [" + AMAZON_SITE + "]");
    this.capture('scraper/opened.png');
});


casper.wait(1000, function() {
  if(this.exists("form[name=signIn]")) {
    console.log("need to login");

    this.capture('scraper/login_form.png');//print screen shot after login

    if(casper.exists('#ap_email')){
      this.wait(1000, function(){
        this.mouseEvent('click', emailInput, '15%', '48%');
        this.sendKeys('input#ap_email', AMAZON_USER, { reset: true});
      });
    }

    this.wait(3000, function () {
      this.mouseEvent('click', passInput, '12%', '67%');
      this.sendKeys('input#ap_password', AMAZON_PASS);
      this.click('input[name=rememberMe]');
   });

    this.then(function() {
        this.click('input#signInSubmit');
    }).wait(5000, function(){});

    this.capture('scraper/login_submitted.png');

    // write the cookies
    this.wait(1000, function() {
        var cookies = JSON.stringify(phantom.cookies);
        fs.write(cookie_path, cookies, 644);
    });

    casper.then(function (e) {
      if(casper.exists('#auth-captcha-image-container')){
          this.captureSelector('scraper/captcha.png', '#auth-captcha-image');
          console.log('captcha:');
          var captcha_code = system.stdin.readLine();

    this.wait(3000, function () {
      this.mouseEvent('click', passInput, '12%', '67%');
      this.sendKeys('input#ap_password', AMAZON_PASS);
    });

    this.wait(3000, function () {
      this.mouseEvent('click', captcha, '12%', '67%');
      this.sendKeys(captcha, captcha_code);
    });

    this.then(function() {
        console.log('submit captcha');
        this.click('input#signInSubmit');
    }).wait(5000, function(){});

      }
      else if(this.exists('#auth-mfa-otpcode')) {
        console.log('otp:');
        var otp_code = system.stdin.readLine();

        this.wait(1000, function () {
          this.mouseEvent('click', otp, '12%', '67%');
          this.sendKeys(otp, otp_code, {keepFocus: true});
/*          this.sendKeys(otp, '253821');*/
          this.mouseEvent('click', otp_remember);
          this.click('input#auth-signin-button');
          this.sendKeys(otp, casper.page.event.key.Enter , {keepFocus: true});
          this.capture('scraper/otp.png');
          console.log('otp sent');

          this.then(function() {
            this.wait(3000, function() {
                var cookies = JSON.stringify(phantom.cookies);
                fs.write(cookie_path, cookies, 644);
            });

            this.capture('scraper/submit_otp_2.png');
          });

          this.then(function() {
            this.waitForSelector("#logo_topNav", function() {
              logged_in();
            });
          });

        });

      }

      else {

        console.log("Logged in successfully.");

        logged_in();
      }

      this.capture('scraper/amazon1.png');//print screen shot after login

      });

    // triggerFail('Login');
  }
  else {
  logged_in();
}
});

casper.on('error', function(msg,backtrace) {
  this.open("https://hc-ping.com/899d98df-29b0-478b-a810-21b1eea1e8b2/fail");
  logged_in();
}); 

casper.run();
