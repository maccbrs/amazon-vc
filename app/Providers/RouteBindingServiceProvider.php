<?php

namespace App\Providers;

use App\Models\ {
    SalesDiagnostics,
    User
};

use mmghv\LumenRouteBinding\RouteBindingServiceProvider as BaseServiceProvider;

class RouteBindingServiceProvider extends BaseServiceProvider
{
    /**
     * Boot the service provider
     */
    public function boot()
    {
        // The binder instance
        $binder = $this->binder;

        // Here we define our bindings
        $binder->bind('user', User::class);
        $binder->bind('diagnostics', SalesDiagnostics::class);
        $binder->bind('categories', Category::class);
        $binder->bind('asins', Asin::class);
        $binder->bind('brands', Brand::class);
        $binder->bind('alt', AlternativePurchase::class);
        $binder->bind('geo', GeographicSalesInsight::class);
        $binder->bind('inv', InventoryHealth::class);
        $binder->bind('sales', SalesDiagnostics::class);
        $binder->bind('pre', Preorder::class);

    }
}