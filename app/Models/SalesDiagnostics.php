<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

use App\Models\ {
  Account
};

class SalesDiagnostics extends Model
{
    /**
     * Define columns for mass assignment
     *
     * @var array
     */
    protected $guarded = [
        "id",
        "report_type"
    ];

    protected $hidden = ['created_at','updated_at'];

    /**
     * Gets the arrayable attributes.
     *
     * @return     <type>  The arrayable attributes.
     */
    protected function getArrayableAttributes()
    {
        foreach ($this->attributes as $key => $value)
        {
            $null_values = array('—', 'UNKNOWN', 'UNKNO', 'N/A', ' ', '', NULL);
            if (in_array($value, $null_values)):
                $this->attributes[$key] = NULL;
            endif;
        }

        return $this->getArrayableItems($this->attributes);
    }

    /**
     * Gets the shipped cogs attribute.
     * @param      <type>  $value  The value
     * @return     string  The shipped cogs attribute.
     */
/*    public function getShippedCogsAttribute($value)
    {
        $account = Account::where('id', $this->attributes['account_id'])->first();
        $currency = $account->marketplace;

        if ($value == null):
            return null;
        else:
            if ($currency == 'UK'):
                return '£'.$value;
            elseif ($currency == 'CA'):
                return '$'.$value;
            elseif ($currency == 'US'):
                return '$'.$value;
            else:
                return '€'.$value;
            endif;
        endif;
    }*/

    /**
     * Gets the shipped revenue attribute.
     * @param      <type>  $value  The value
     * @return     string  The shipped revenue attribute.
     */
/*    public function getShippedRevenueAttribute($value)
    {
        $account = Account::where('id', $this->attributes['account_id'])->first();
        $currency = $account->marketplace;

        if ($value == null):
            return null;
        else:
            if ($currency == 'UK'):
                return '£'.$value;
            elseif ($currency == 'CA'):
                return '$'.$value;
            elseif ($currency == 'US'):
                return '$'.$value;
            else:
                return '€'.$value;
            endif;
        endif;
    }*/

    /**
     * Gets the shipped cogs percentage total attribute.
     * @param      string  $value  The value
     * @return     string  The shipped cogs percentage total attribute.
     */
    public function getShippedRevenuePercentageOfTotalAttribute($value)
    {
        if ($value == null):
            return null;
        else:
            return $value.'%';
        endif;
    }

    /**
     * Gets the shipped cogs percentage total attribute.
     * @param      string  $value  The value
     * @return     string  The shipped cogs percentage total attribute.
     */
    public function getShippedRevenuePriorAttribute($value)
    {
        if ($value == null):
            return null;
        else:
            return $value.'%';
        endif;
    }

    /**
     * Gets the shipped cogs percentage total attribute.
     * @param      string  $value  The value
     * @return     string  The shipped cogs percentage total attribute.
     */
    public function getShippedRevenuePriorYearAttribute($value)
    {
        if ($value == null):
            return null;
        else:
            return $value.'%';
        endif;
    }

    /**
     * Gets the shipped cogs percentage total attribute.
     * @param      string  $value  The value
     * @return     string  The shipped cogs percentage total attribute.
     */
    public function getShippedCogsPercentageTotalAttribute($value)
    {
        if ($value == null):
            return null;
        else:
            return $value.'%';
        endif;
    }

    /**
     * Gets the shipped cogs prior attribute.
     * @param      string  $value  The value
     * @return     string  The shipped cogs prior attribute.
     */
    public function getShippedCogsPriorAttribute($value)
    {
        if ($value == null):
            return null;
        else:
            return $value.'%';
        endif;
    }

    /**
     * Gets the shipped cogs prior attribute.
     * @param      string  $value  The value
     * @return     string  The shipped cogs prior attribute.
     */
    public function getShippedCogsPriorYearAttribute($value)
    {
        if ($value == null):
            return null;
        else:
            return $value.'%';
        endif;
    }

    /**
     * Gets the shipped units percentage total attribute.
     * @param      string  $value  The value
     * @return     string  The shipped units percentage total attribute.
     */
    public function getShippedUnitsPercentageTotalAttribute($value)
    {
        if ($value == null):
            return null;
        else:
            return $value.'%';
        endif;
    }

    /**
     * Gets the shipped units prior attribute.
     * @param      string  $value  The value
     * @return     string  The shipped units prior attribute.
     */
    public function getShippedUnitsPriorAttribute($value)
    {
        if ($value == null):
            return null;
        else:
            return $value.'%';
        endif;
    }

    /**
     * Gets the shipped units prior year attribute.
     * @param      string  $value  The value
     * @return     string  The shipped units prior year attribute.
     */
    public function getShippedUnitsPriorYearAttribute($value)
    {
        if ($value == null):
            return null;
        else:
            return $value.'%';
        endif;
    }

    /**
     * Gets the change in impressions prior attribute.
     * @param      string  $value  The value
     * @return     string  The change in impressions prior attribute.
     */
    public function getChangeInImpressionsPriorAttribute($value)
    {
        if ($value == null):
            return null;
        else:
            return $value.'%';
        endif;
    }

    /**
     * Gets the change in impressions prior year attribute.
     * @param      string  $value  The value
     * @return     string  The change in impressions prior year attribute.
     */
    public function getChangeInImpressionsPriorYearAttribute($value)
    {
        if ($value == null):
            return null;
        else:
            return $value.'%';
        endif;
    }

    /**
     * Gets the displayed in stock attribute.
     * @param      string  $value  The value
     * @return     string  The displayed in stock attribute.
     */
    public function getDisplayedInStockAttribute($value)
    {
        if ($value == null):
            return null;
        else:
            return $value.'%';
        endif;
    }

    /**
     * Gets the displayed in stock total attribute.
     * @param      string  $value  The value
     * @return     string  The displayed in stock total attribute.
     */
    public function getDisplayedInStockTotalAttribute($value)
    {
        if ($value == null):
            return null;
        else:
            return $value.'%';
        endif;
    }

    /**
     * Gets the displayed in stock prior attribute.
     * @param      string  $value  The value
     * @return     string  The displayed in stock prior attribute.
     */
    public function getDisplayedInStockPriorAttribute($value)
    {
        if ($value == null):
            return null;
        else:
            return $value.'%';
        endif;
    }

    /**
     * Gets the ordered units percentage total attribute.
     * @param      string  $value  The value
     * @return     string  The ordered units percentage total attribute.
     */
    public function getOrderedUnitsPercentageTotalAttribute($value)
    {
        if ($value == null):
            return null;
        else:
            return $value.'%';
        endif;
    }

    /**
     * Gets the ordered units prior attribute.
     * @param      string  $value  The value
     * @return     string  The ordered units prior attribute.
     */
    public function getOrderedUnitsPriorAttribute($value)
    {
        if ($value == null):
            return null;
        else:
            return $value.'%';
        endif;
    }

    public function getOrderedUnitsPriorYearAttribute($value)
    {
        if ($value == null):
            return null;
        else:
            return $value.'%';
        endif;
    }

    /**
     * Gets the sub category better worse attribute.
     * @param      string  $value  The value
     * @return     string  The sub category better worse attribute.
     */
    public function getSubcategoryBetterWorseAttribute($value)
    {
        if ($value == null):
            return null;
        else:
            return $value.'%';
        endif;
    }

    /**
     * Gets the average sales price attribute.
     * @param      <type>  $value  The value
     * @return     string  The average sales price attribute.
     */
/*    public function getAverageSalesPriceAttribute($value)
    {
        $account = Account::where('id', $this->attributes['account_id'])->first();
        $currency = $account->marketplace;

        if ($value == null):
            return null;
        else:
            if ($currency == 'UK'):
                return '£'.$value;
            elseif ($currency == 'CA'):
                return '$'.$value;
            elseif ($currency == 'US'):
                return '$'.$value;
            else:
                return '€'.$value;
            endif;
        endif;
    }*/

    /**
     * Gets the average sales price prior attribute.
     * @param      string  $value  The value
     * @return     string  The average sales price prior attribute.
     */
    public function getAverageSalesPricePriorAttribute($value)
    {
        if ($value == null):
            return null;
        else:
            return $value.'%';
        endif;
    }

    /**
     * Gets the change in glance view prior attribute.
     * @param      string  $value  The value
     * @return     string  The change in glance view prior attribute.
     */
    public function getChangeInGlanceViewPriorAttribute($value)
    {
        if ($value == null):
            return null;
        else:
            return $value.'%';
        endif;
    }

    /**
     * Gets the change in glance view prior year attribute.
     * @param      string  $value  The value
     * @return     string  The change in glance view prior year attribute.
     */
    public function getChangeInGlanceViewPriorYearAttribute($value)
    {
        if ($value == null):
            return null;
        else:
            return $value.'%';
        endif;
    }


    /**
     * Gets the rep oos attribute.
     * @param      string  $value  The value
     * @return     string  The rep oos attribute.
     */
    public function getRepOosAttribute($value)
    {
        if ($value == null):
            return null;
        else:
            return $value.'%';
        endif;
    }

    /**
     * Gets the rep oos percentage total attribute.
     * @param      string  $value  The value
     * @return     string  The rep oos percentage total attribute.
     */
    public function getRepOosPercentageTotalAttribute($value)
    {
        if ($value == null):
            return null;
        else:
            return $value.'%';
        endif;
    }

    /**
     * Gets the rep oos prior attribute.
     * @param      string  $value  The value
     * @return     string  The rep oos prior attribute.
     */
    public function getRepOosPriorAttribute($value)
    {
        if ($value == null):
            return null;
        else:
            return $value.'%';
        endif;
    }

    /**
     * Gets the lbb attribute.
     * @param      string  $value  The value
     * @return     string  The lbb attribute.
     */
    public function getLbbAttribute($value)
    {
        if ($value == null):
            return null;
        else:
            return $value.'%';
        endif;
    }

    /**
     * Define relationship to asin
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function asin()
    {
        return $this
            ->hasOne(
                Asin::class,
                'id', 'asin_id'
            )->select('id','asin');
    }

    public function asinDetails()
    {
        return $this
            ->hasOne(
                Asin::class,
                'id', 'asin_id'
            );
    }

    public function mace()
    {
        return $this
            ->hasMany(
                Asin::class,
                'id', 'asin_id'
            )->select('id','asin');
    }

    /**
     * Define relationship to account
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function account()
    {
        return $this
            ->hasOne(
                Account::class,
                'id', 'account_id'
            )->with(['subscription']);
    }

    /**
     * Define relationship to program
     *  
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function program()
    { 
        return $this
            ->hasOne(Program::class,
                'id', 'program_id'
            );
    }

    public function groupedDate()
    { 
        return $this
            ->hasMany(SalesDiagnostics::class,
                'generated_date', 'generated_date'
            );
    }
    
}
