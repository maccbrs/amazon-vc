<?php

namespace App\Models;

use Illuminate\Database\Eloquent\ {
    Model,
    Builder

};

use Carbon\Carbon;

class AlternativePurchase extends Model
{

    protected $fillable = [
        // Fillable Fields   
    ];

    protected $guarded = [
        "id",
        "report_type"
    ];

    protected $dates = ["created_at", "updated_at"];

    protected $hidden = ['created_at','updated_at'];

    public static $rules = [
        // Validation rules
    ];

    /**
     * Gets the arrayable attributes.
     *
     * @return     <type>  The arrayable attributes.
     */
    protected function getArrayableAttributes()
    {
        foreach ($this->attributes as $key => $value)
        {
            $null_values = array('—', 'UNKNOWN', 'UNKNO', 'N/A', ' ', '', NULL);
            if (in_array($value, $null_values)):
                $this->attributes[$key] = NULL;
            endif;
        }

        return $this->getArrayableItems($this->attributes);
    }

    /**
     * Gets the first purchased percentage.
     *
     * @param      string  $value  The value
     *
     * @return     string  The first purchased percentage.
     */
    public function getFirstPurchasedPercentageAttribute($value)
    {
        return $value.'%';
    }


    /**
     * Gets the second purchased percentage.
     *
     * @param      string  $value  The value
     *
     * @return     string  The second purchased percentage.
     */
    public function getSecondPurchasedPercentageAttribute($value)
    {
        return $value.'%';
    }


    /**
     * Gets the third purchased percentage.
     *
     * @param      string  $value  The value
     *
     * @return     string  The third purchased percentage.
     */
    public function getThirdPurchasedPercentageAttribute($value)
    {
        return $value.'%';
    }


    /**
     * Gets the fourth purchased percentage.
     *
     * @param      string  $value  The value
     *
     * @return     string  The fourth purchased percentage.
     */
    public function getFourthPurchasedPercentageAttribute($value)
    {
        return $value.'%';
    }


    /**
     * Gets the fifth purchased percentage.
     *
     * @param      string  $value  The value
     *
     * @return     string  The fifth purchased percentage.
     */
    public function getFifthPurchasedPercentageAttribute($value)
    {
        return $value.'%';
    }

    public function account()
    {
        return $this
            ->hasOne(
                Account::class,
                'id', 'account_id'
            )->with(['subscription']);
    }

    /**
     * Define relationship with asin.
     *
     * @return     <object>  Relation to asin with category.
     */
    public function asin()
    {
        return $this
            ->hasOne(
                Asin::class,
                'id', 'asin_id'
            )->with(['category']);
    }

    /**
     * Define relationship with first alternate asin.
     *
     * @return     <object>  Relation to first alternate with category
     */
    public function firstAlternativeAsin()
    {
        return $this
            ->hasOne(
                Asin::class,
                'asin', 'first_alternative_asin'
            )->with(['category']);
    }

    /**
     * Define relationship with second alternate asin.
     *
     * @return     <object>  Relation to second alternate with category
     */
    public function secondAlternativeAsin()
    {
        return $this
            ->hasOne(
                Asin::class,
                'asin', 'second_alternative_asin'
            )->with(['category']);
    }

    /**
     * Define relationship with third alternate asin.
     *
     * @return     <object>  Relation to third alternate with category
     */
    public function thirdAlternativeAsin()
    {
        return $this
            ->hasOne(
                Asin::class,
                'asin', 'third_alternative_asin'
            )->with(['category']);
    }

    /**
     * Define relationship with fourth alternate asin.
     *
     * @return     <object>  Relation to fourth alternate with category
     */
    public function fourthAlternativeAsin()
    {
        return $this
            ->hasOne(
                Asin::class,
                'asin', 'fourth_alternative_asin'
            )->with(['category']);
    }

    /**
     * Define relationship with fifth alternate asin.
     *
     * @return     <object>  Relation to fifth alternate with category
     */
    public function fifthAlternativeAsin()
    {
        return $this
            ->hasOne(
                Asin::class,
                'asin', 'fifth_alternative_asin'
            )->with(['category']);
    }
}
