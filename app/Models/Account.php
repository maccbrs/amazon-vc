<?php

namespace App\Models;

use Illuminate\Database\Eloquent\ {
    Model,
    Builder

};

class Account extends Model
{
    /**
     * Mass assignment except columns define here.
     *
     * @var array
     */
    protected $guarded = [''];

    protected $hidden = ['created_at','updated_at'];

    /**
     * Define relationship with sales diagnostics
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function salesDiagnostics()
    {
        return $this
            ->belongsTo(
                SalesDiagnostics::class,
                'account_id'
            );

    }

    public function subscription()
    {
        return $this
            ->hasOne(
                Subscription::class,
                'id','subscription_id'
            );
    }
}