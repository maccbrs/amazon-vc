<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

use App\Models\ {
  Account
};

class InventoryHealth extends Model
{

    protected $guarded = [
        "id",
        "report_type"
    ];

    protected $dates = ["created_at", "updated_at"];

    protected $hidden = ['created_at','updated_at'];

    public static $rules = [
        // Validation rules
    ];

    /**
     * Gets the arrayable attributes.
     *
     * @return     <type>  The arrayable attributes.
     */
    protected function getArrayableAttributes()
    {
        foreach ($this->attributes as $key => $value)
        {
            $null_values = array('—', 'UNKNOWN', 'UNKNO', 'N/A', ' ', '', NULL);
            if (in_array($value, $null_values)):
                $this->attributes[$key] = NULL;
            endif;
        }

        return $this->getArrayableItems($this->attributes);
    }

    /**
     * Gets the net received attribute.
     *
     * @param      <type>  $value  The value
     *
     * @return     string  The net received attribute.
     */
/*    public function getNetReceivedAttribute($value)
    {
        $account = Account::where('id', $this->attributes['account_id'])->first();
        $currency = $account->marketplace;

        if ($value == null):
            return null;
        else:
            if ($currency == 'UK'):
                return '£'.$value;
            elseif ($currency == 'CA'):
                return 'C$'.$value;
            elseif ($currency == 'US'):
                return '$'.$value;
            else:
                return '€'.$value;
            endif;
        endif;
    }*/

    /**
     * Gets the sell through rate attribute.
     *
     * @param      string  $value  The value
     *
     * @return     string  The sell through rate attribute.
     */
    public function getSellThroughRateAttribute($value)
    {
        if ($value == null):
            return null;
        else:
            return $value.'%';
        endif;
    }

    /**
     * Gets the sellable on hand inventory attribute.
     *
     * @param      <type>  $value  The value
     *
     * @return     string  The sellable on hand inventory attribute.
     */
/*    public function getSellableOnHandInventoryAttribute($value)
    {
        $account = Account::where('id', $this->attributes['account_id'])->first();
        $currency = $account->marketplace;

        if ($value == null):
            return null;
        else:
            if ($currency == 'UK'):
                return '£'.$value;
            elseif ($currency == 'CA'):
                return 'C$'.$value;
            elseif ($currency == 'US'):
                return '$'.$value;
            else:
                return '€'.$value;
            endif;
        endif;
    }*/

    /**
     * Gets the sellable on hand inventory trailing attribute.
     *
     * @param      <type>  $value  The value
     *
     * @return     string  The sellable on hand inventory trailing attribute.
     */
/*    public function getSellableOnHandInventoryTrailingAttribute($value)
    {
        $account = Account::where('id', $this->attributes['account_id'])->first();
        $currency = $account->marketplace;

        if ($value == null):
            return null;
        else:
            if ($currency == 'UK'):
                return '£'.$value;
            elseif ($currency == 'CA'):
                return 'C$'.$value;
            elseif ($currency == 'US'):
                return '$'.$value;
            else:
                return '€'.$value;
            endif;
        endif;
    }*/

    /**
     * Gets the unsellable on hand inventory attribute.
     *
     * @param      <type>  $value  The value
     *
     * @return     string  The unsellable on hand inventory attribute.
     */
/*    public function getUnsellableOnHandInventoryAttribute($value)
    {
        $account = Account::where('id', $this->attributes['account_id'])->first();
        $currency = $account->marketplace;

        if ($value == null):
            return null;
        else:
            if ($currency == 'UK'):
                return '£'.$value;
            elseif ($currency == 'CA'):
                return 'C$'.$value;
            elseif ($currency == 'US'):
                return '$'.$value;
            else:
                return '€'.$value;
            endif;
        endif;
    }*/

    /**
     * Gets the unsellable on hand inventory trailing attribute.
     *
     * @param      <type>  $value  The value
     *
     * @return     string  The unsellable on hand inventory trailing attribute.
     */
/*    public function getUnsellableOnHandInventoryTrailingAttribute($value)
    {
        $account = Account::where('id', $this->attributes['account_id'])->first();
        $currency = $account->marketplace;

        if ($value == null):
            return null;
        else:
            if ($currency == 'UK'):
                return '£'.$value;
            elseif ($currency == 'CA'):
                return 'C$'.$value;
            elseif ($currency == 'US'):
                return '$'.$value;
            else:
                return '€'.$value;
            endif;
        endif;
    }*/

    /**
     * Gets the ninety days sellable inventory attribute.
     *
     * @param      <type>  $value  The value
     *
     * @return     string  The ninety days sellable inventory attribute.
     */
/*    public function getNinetyDaysSellableInventoryAttribute($value)
    {
        $account = Account::where('id', $this->attributes['account_id'])->first();
        $currency = $account->marketplace;

        if ($value == null):
            return null;
        else:
            if ($currency == 'UK'):
                return '£'.$value;
            elseif ($currency == 'CA'):
                return 'C$'.$value;
            elseif ($currency == 'US'):
                return '$'.$value;
            else:
                return '€'.$value;
            endif;
        endif;
    }*/

    /**
     * Gets the ninety days sellable inventory trailing attribute.
     *
     * @param      <type>  $value  The value
     *
     * @return     string  The ninety days sellable inventory trailing attribute.
     */
/*    public function getNinetyDaysSellableInventoryTrailingAttribute($value)
    {
        $account = Account::where('id', $this->attributes['account_id'])->first();
        $currency = $account->marketplace;

        if ($value == null):
            return null;
        else:
            if ($currency == 'UK'):
                return '£'.$value;
            elseif ($currency == 'CA'):
                return 'C$'.$value;
            elseif ($currency == 'US'):
                return '$'.$value;
            else:
                return '€'.$value;
            endif;
        endif;
    }
*/
    /**
     * Gets the unhealthy inventory attribute.
     *
     * @param      <type>  $value  The value
     *
     * @return     string  The unhealthy inventory attribute.
     */
/*    public function getUnhealthyInventoryAttribute($value)
    {
        $account = Account::where('id', $this->attributes['account_id'])->first();
        $currency = $account->marketplace;

        if ($value == null):
            return null;
        else:
            if ($currency == 'UK'):
                return '£'.$value;
            elseif ($currency == 'CA'):
                return 'C$'.$value;
            elseif ($currency == 'US'):
                return '$'.$value;
            else:
                return '€'.$value;
            endif;
        endif;
    }*/

    /**
     * Gets the unhealthy inventory trailing attribute.
     *
     * @param      <type>  $value  The value
     *
     * @return     string  The unhealthy inventory trailing attribute.
     */
/*    public function getUnhealthyInventoryTrailingAttribute($value)
    {
        $account = Account::where('id', $this->attributes['account_id'])->first();
        $currency = $account->marketplace;

        if ($value == null):
            return null;
        else:
            if ($currency == 'UK'):
                return '£'.$value;
            elseif ($currency == 'CA'):
                return 'C$'.$value;
            elseif ($currency == 'US'):
                return '$'.$value;
            else:
                return '€'.$value;
            endif;
        endif;
    }*/
    
    /**
     * Defines Relationship with asin
     *
     * @return     <object>  Relation to asin
     */
    public function asin()
    {
        return $this
            ->hasOne(
                Asin::class,
                'id', 'asin_id'
            )->with(['category']);
    }

    public function account()
    {
        return $this
            ->hasOne(
                Account::class,
                'id', 'account_id'
            )->with(['subscription']);
    }

    public function replenishCategory()
    {   
        return $this
            ->hasOne(
                ReplenishmentCategory::class,
                'id' ,'replenishment_category_id'
            );
    }
}
