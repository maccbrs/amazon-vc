<?php

namespace App\Models;

use Illuminate\Database\Eloquent\ {
    Model,
    Builder

};

class NetPpm extends Model
{
    /**
     * Mass assignment except columns define here.
     *
     * @var array
     */
    protected $guarded = ['id'];

    protected $hidden = ['created_at','updated_at'];

    /**
     * Define relationship with sales diagnostics
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function account()
    {
        return $this
            ->belongsTo(
                Account::class,
                'id','account_id'
            );

    }

    public function asin()
    {
        return $this
            ->hasOne(
                Asin::class,
                'id','asin_id'
            );
    }

    /**
     * Gets the net ppm.
     *
     * @param      string  $value  The value
     *
     * @return     string  The net ppm.
     */
    public function getNetPpm($value)
    {
        if ($value == null):
            return null;
        else:
            return $value.'%';
        endif;
    }

    /**
     * Gets the net ppm percent total.
     *
     * @param      string  $value  The value
     *
     * @return     string  The net ppm percent total.
     */
    public function getNetPpmPercentTotal($value)
    {
        if ($value == null):
            return null;
        else:
            return $value.'%';
        endif;
    }

    /**
     * Gets the net ppm prior period.
     *
     * @param      string  $value  The value
     *
     * @return     string  The net ppm prior period.
     */
    public function getNetPpmPriorPeriod($value)
    {
        if ($value == null):
            return null;
        else:
            return $value.'%';
        endif;
    }

    /**
     * Gets the net ppm last year.
     *
     * @param      string  $value  The value
     *
     * @return     string  The net ppm last year.
     */
    public function getNetPpmLastYear($value)
    {
        if ($value == null):
            return null;
        else:
            return $value.'%';
        endif;
    }

    /**
     * Gets the net ppm relationship.
     *
     * @param      string  $value  The value
     *
     * @return     string  The net ppm relationship.
     */
    public function getNetPpmRelationship($value)
    {
        if ($value == null):
            return null;
        else:
            return $value.'%';
        endif;
    }

    /**
     * Gets the net ppm category.
     *
     * @param      string  $value  The value
     *
     * @return     string  The net ppm category.
     */
    public function getNetPpmCategory($value)
    {
        if ($value == null):
            return null;
        else:
            return $value.'%';
        endif;
    }

    /**
     * Gets the net ppm product title.
     *
     * @param      string  $value  The value
     *
     * @return     string  The net ppm product title.
     */
    public function getNetPpmProductTitle($value)
    {
        if ($value == null):
            return null;
        else:
            return $value.'%';
        endif;
    }

    /**
     * Gets the net ppm asin.
     *
     * @param      string  $value  The value
     *
     * @return     string  The net ppm asin.
     */
    public function getNetPpmAsin($value)
    {
        if ($value == null):
            return null;
        else:
            return $value.'%';
        endif;
    }

    /**
     * Gets the net ppm summary.
     *
     * @param      string  $value  The value
     *
     * @return     string  The net ppm summary.
     */
    public function getNetPpmSummary($value)
    {
        if ($value == null):
            return null;
        else:
            return $value.'%';
        endif;

    }

    /**
     * Gets the net worth.
     *
     * @param      string  $value    The value
     * @param      <type>  $account  The account
     *
     * @return     string  The net worth.
     */
    public function getNetWorth($value, $account)
    {
        if ($value == null):
            return null;
        else:
            return $value.'%';
        endif;
    }

    /**
     * Gets the net ppm sub category.
     *
     * @param      string  $value  The value
     *
     * @return     string  The net ppm sub category.
     */
    public function getNetPpmSubCategory($value)
    {
        if ($value == null):
            return null;
        else:
            return $value.'%';
        endif;
    }

    public function postNetPPm($value)
    {
        if ($value == null):
            return null;
        else:
            return $value.'%';
        endif;
    }

    public function postNetPpmAsin($value)
    {
        if ($value == null):
            return null;
        else:
            return $value.'%';
        endif;
    }

    public function postNetPpmPrior($value)
    {
        if ($value == null):
            return null;
        else:
            return $value.'%';
        endif;
    }

    public function postNetPpmLastYear($value)
    {
        if ($value == null):
            return null;
        else:
            return $value.'%';
        endif;
    }

    public function postNetPpmAverage($value)
    {
        if ($value == null):
            return null;
        else:
            return $value.'%';
        endif;
    }

    public function postNetPpmAve($value)
    {
        if ($value == null):
            return null;
        else:
            return $value.'%';
        endif;
    }

    public function postNetPpmAvePrior($value)
    {
        if ($value == null):
            return null;
        else:
            return $value.'%';
        endif;
    }
}   