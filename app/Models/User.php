<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;
use Laravel\Passport\HasApiTokens;
use Carbon\Carbon;

class User extends Model implements AuthorizableContract, AuthenticatableContract 
{

    use
        Authenticatable,
        Authorizable,
        HasApiTokens;


    protected $fillable = [
        "name",
        "email",
        "email_verified_at",
        "password",

    ];

    protected $dates = ["created_at", "updated_at"];

    public static $rules = [
        // Validation rules
    ];

    protected $hidden = ['created_at','updated_at'];

}
