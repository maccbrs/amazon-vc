<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Distributor extends Model {

    protected $fillable = [
        "distributor",
    ];

    protected $hidden = ['created_at','updated_at'];

    protected $dates = ["created_at", "updated_at"];

    public static $rules = [
        // Validation rules
    ];

}
