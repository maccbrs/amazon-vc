<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Category extends Model {

    protected $fillable = [
        "category",
    ];

    protected $dates = ["created_at", "updated_at"];

    protected $hidden = ['created_at','updated_at'];

    public static $rules = [
        // Validation rules
    ];

    /**
     * Defines Relationship with Asin
     *
     * @return     <object>  Relation to Asin
     */
    
    protected function getArrayableAttributes()
    {
        foreach ($this->attributes as $key => $value)
        {
            $null_values = array('—', 'UNKNOWN', 'UNKNO', 'N/A', ' ', '', NULL);
            if (in_array($value, $null_values)):
                $this->attributes[$key] = NULL;
            endif;
        }

        return $this->getArrayableItems($this->attributes);
    }

    /**
     * Asins Relations
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function asins()
    {
        return $this
            ->hasMany(
                Asin::class,
                'category_id', 'id'
            )->with([
            	'brand',
            	'category',
            	'subcategory',
            	'binding',
            	'productGroup',
            	'replenishCode',
            	'manufacturerCode',
            	'parentManufacturerCode'
            ]);
    }

}
