<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

use App\Models\ {
  Account
};

class Asin extends Model
{

    /**
     * @var array
     */
    protected $guarded = [
        "id"
    ];

    /**
     * @var array
     */
    protected $dates = ["created_at", "updated_at"];

    public static $rules = [
        // Validation rules
    ];

    /**
     * Gets the arrayable relations.
     *
     * @return     <object>  Global Accessors with relations.
     */
    protected function getArrayableAttributes()
    {
        foreach ($this->attributes as $key => $value)
        {
            $null_values = array('—','UNKNOWN', 'UNKNO', 'N/A', ' ', '', NULL);
            if (in_array($value, $null_values)):
                $this->attributes[$key] = NULL;
            endif;
        }

        return $this->getArrayableItems($this->attributes);
    }


    /**
     * Gets the list price attribute.   
     *
     * @param      <string>  $value  The value
     *
     * @return     string  The list price attribute.
     */
/*    public function getListPriceAttribute($value)
    {
        $account = Account::where('id', $this->attributes['account_id'])->first();
        $currency = $account->marketplace;

        if ($value == null):
            return null;
        else:
            if ($currency == 'UK'):
                return '£'.$value;
            elseif ($currency == 'CA'):
                return 'C$'.$value;
            elseif ($currency == 'US'):
                return '$'.$value;
            else:
                return '€'.$value;
            endif;
        endif;
    }*/


    /**
     * { function_description }
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function brand()
    {
        return $this
            ->hasOne(
                Brand::class,
                'id', 'brand_id'
            );
    }

    /**
     * Defines relationship with category
     *w
     * @return     \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function category()
    {
        return $this
            ->hasOne(
                Category::class,
                'id', 'category_id'
            );
    }

    public function subcategory()
    {
        return $this
            ->hasOne(
                Subcategory::class,
                'id', 'subcategory_id'
            );
    }

    /**
     * Defines relationship with account
     *
     * @return     <object>  Relation to account.
     */
    public function account()
    {
        return $this
            ->hasOne(
                Account::class,
                'id', 'account_id'
            )->with(['subscription']);
    }

    /**
     * Defines relationship with binding
     *
     * @return     <object>  Relation to biniding.
     */
    public function binding()
    {
        return $this
            ->hasOne(
                Binding::class,
                'id', 'binding_id'
            );
    }

    /**
     * Defines relationship with product group
     *
     * @return     <object>  Relation to product group.
     */
    public function productGroup()
    {
        return $this
            ->hasOne(
                ProductGroup::class,
                'id', 'product_group_id'
            );
    }

    /**
     * Defines relationship with replenishment
     *
     * @return     <object>  Relation to Replenishment.
     */
    public function replenishCode()
    {
        return $this
            ->hasOne(
                ReplenishCode::class,
                'id', 'replenish_id'
            );
    }

    /**
     * Defines relationship with manufacturer code
     *
     * @return     <object>  Relation to Manufacturer Code
     */
    public function manufacturerCode()
    {
        return $this
            ->hasOne(
                ManufacturerCode::class,
                'id', 'manufacturer_code_id'
            )->with(['parentManufacturerCode']);
    }

    /**
     * Defines relationship with parent manufacturer code
     *
     * @return     <object>  Relation to Parent Manufacturer Code
     */
    public function parentManufacturerCode()
    {
        return $this
            ->hasOne(
                ParentManufacturerCode::class,
                'id', 'parent_manufacturer_code_id'
            );
    }

    public function inventory_health()
    {
        return $this
            ->hasMany(
                InventoryHealth::class,
                'asin_id', 'id')
            ->select('asin_id','account_id','net_received','net_received_units','sellable_on_hand_units','open_purchase_order_quantity','generated_date');
    }

    public function sales()
    {
        return $this
            ->hasMany(
                SalesDiagnostics::class,
                'asin_id', 'id')
            ->select('asin_id','account_id','shipped_cogs','shipped_units','generated_date');
    }

}
