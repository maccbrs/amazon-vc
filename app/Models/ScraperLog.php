<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class ScraperLog extends Model {

    protected $fillable = [
        "filename",
    ];

    protected $hidden = ['created_at','updated_at'];

    protected $dates = ["created_at", "updated_at"];

    public static $rules = [
        // Validation rules
    ];

}
