<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Program extends Model {

    protected $fillable = [
        "program",
    ];

    protected $guarded = [
        "id"
    ];

    protected $hidden = ['created_at','updated_at'];

}
