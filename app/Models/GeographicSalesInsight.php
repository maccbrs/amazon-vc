<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

use App\Models\ {
  Account
};

class GeographicSalesInsight extends Model
{

    protected $guard = [
        "id",
        "report_type"
    ];

    protected $dates = ["created_at", "updated_at"];

    protected $hidden = ['created_at','updated_at'];

    public static $rules = [
        // Validation rules
    ];

    /**
     * Gets the arrayable attributes.
     *
     * @return     <type>  The arrayable attributes.
     */
    protected function getArrayableAttributes()
    {
        foreach ($this->attributes as $key => $value)
        {
            $null_values = array('—', 'UNKNOWN', 'UNKNO', 'N/A', ' ', '', NULL);
            if (in_array($value, $null_values)):
                $this->attributes[$key] = NULL;
            endif;
        }

        return $this->getArrayableItems($this->attributes);
    }

    /**
     * Gets the shipped revenue attribute.
     *
     * @param      <type>  $value  The value
     *
     * @return     string  The shipped revenue attribute.
     */
/*    public function getShippedRevenueAttribute($value)
    {
        $account = Account::where('id', $this->attributes['account_id'])->first();
        $currency = $account->marketplace;

        if ($value == null):
            return null;
        else:
            if ($currency == 'UK'):
                return '£'.$value;
            elseif ($currency == 'CA'):
                return 'C$'.$value;
            elseif ($currency == 'US'):
                return '$'.$value;
            else:
                return '€'.$value;
            endif;
        endif;
    }
*/
    /**
     * Gets the shipped revenue total attribute.
     *
     * @param      string  $value  The value
     *
     * @return     string  The shipped revenue total attribute.
     */
    public function getShippedRevenueTotalAttribute($value)
    {
        if ($value == null):
            return null;
        else:
            return $value.'%';
        endif;
    }

    /**
     * Gets the shipped revenue prior period attribute.
     *
     * @param      string  $value  The value
     *
     * @return     string  The shipped revenue prior period attribute.
     */
    public function getShippedRevenuePriorPeriodAttribute($value)
    {
        if ($value == null):
            return null;
        else:
            return $value.'%';
        endif;
    }

    /**
     * Gets the shipped revenue last year attribute.
     *
     * @param      string  $value  The value
     *
     * @return     string  The shipped revenue last year attribute.
     */
    public function getShippedRevenueLastYearAttribute($value)
    {
        if ($value == null):
            return null;
        else:
            return $value.'%';
        endif;
    }

    /**
     * Gets the shipped units total attribute.
     *
     * @param      string  $value  The value
     *
     * @return     string  The shipped units total attribute.
     */
    public function getShippedUnitsTotalAttribute($value)
    {
        if ($value == null):
            return null;
        else:
            return $value.'%';
        endif;
    }

    /**
     * Gets the shipped units prior period attribute.
     *
     * @param      string  $value  The value
     *
     * @return     string  The shipped units prior period attribute.
     */
    public function getShippedUnitsPriorPeriodAttribute($value)
    {
        if ($value == null):
            return null;
        else:
            return $value.'%';
        endif;
    }

    /**
     * Gets the shipped units last year attribute.
     *
     * @param      string  $value  The value
     *
     * @return     string  The shipped units last year attribute.
     */
    public function getShippedUnitsLastYearAttribute($value)
    {
        if ($value == null):
            return null;
        else:
            return $value.'%';
        endif;
    }

    /**
     * Gets the average sales price attribute.
     *
     * @param      <type>  $value  The value
     *
     * @return     string  The average sales price attribute.
     */
/*    public function getAverageSalesPriceAttribute($value)
    {
        $account = Account::where('id', $this->attributes['account_id'])->first();
        $currency = $account->marketplace;

        if ($value == null):
            return null;
        else:
            if ($currency == 'UK'):
                return '£'.$value;
            elseif ($currency == 'CA'):
                return 'C$'.$value;
            elseif ($currency == 'US'):
                return '$'.$value;
            else:
                return '€'.$value;
            endif;
        endif;
    }*/

    /**
     * Gets the average sales price prior period attribute.
     *
     * @param      string  $value  The value
     *
     * @return     string  The average sales price prior period attribute.
     */
    public function getAverageSalesPricePriorPeriodAttribute($value)
    {
        if ($value == null):
            return null;
        else:
            return $value.'%';
        endif;
    }

    /**
     * Gets the average sales price last year attribute.
     *
     * @param      string  $value  The value
     *
     * @return     string  The average sales price last year attribute.
     */
    public function getAverageSalesPriceLastYearAttribute($value)
    {
        if ($value == null):
            return null;
        else:
            return $value.'%';
        endif;
    }

    /**
     * Defines Relationship with asin.
     *
     * @return     <object>  Relation to Asin
     */
    public function asin()
    {
        return $this
            ->hasOne(
                Asin::class,
                'id', 'asin_id'
            );
    }

    /**
     * Defines Relationship with country.
     *
     * @return     <object>  Relation to country
     */
    public function country()
    {
        return $this
            ->hasOne(
                Country::class,
                'id', 'country_id'
            );
    }

    /**
     * Defines Relationship with state.
     *
     * @return     <object>  Relation to state
     */
    public function state()
    {
        return $this
            ->hasOne(
                State::class,
                'id', 'state_id'
            );
    }

    public function account()
    {
        return $this
            ->hasOne(
                Account::class,
                'id', 'account_id'
            )->with(['subscription']);
    }

    /**
     * Defines Relationship with city.
     *
     * @return     <object>  Relation to city
     */
    public function city()
    {
        return $this
            ->hasOne(
                City::class,
                'id', 'city_id'
            );
    }

    /**
     * Defines Relationship with zip.
     *
     * @return     <object>  Relation to zip
     */
    public function zip()
    {
        return $this
            ->hasOne(
                Zip::class,
                'id', 'zip_id'
            );
    }
    
}
