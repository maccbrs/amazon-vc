<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class ImportedJson extends Model {

    protected $fillable = [
        "filename",
        "report_type",
        "status"
    ];

    protected $hidden = ['created_at','updated_at'];

    protected $dates = ["created_at", "updated_at"];

    public static $rules = [
        // Validation rules
    ];

}
