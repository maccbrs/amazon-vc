<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

use App\Models\ {
  Account
};

class PreOrder extends Model {

    protected $guarded = [
        "id",
        "report_type"
    ];

    protected $dates = ["created_at", "updated_at"];

    protected $hidden = ['created_at','updated_at'];

    public static $rules = [
        // Validation rules
    ];

    /**
     * Gets the arrayable attributes.
     *
     * @return     <type>  The arrayable attributes.
     */
    protected function getArrayableAttributes()
    {
        foreach ($this->attributes as $key => $value)
        {
            $null_values = array('—', 'UNKNOWN', 'UNKNO', 'N/A', ' ', '', NULL);
            if (in_array($value, $null_values)):
                $this->attributes[$key] = NULL;
            endif;
        }

        return $this->getArrayableItems($this->attributes);
    }

    /**
     * Gets the pre ordered revenue attribute.
     *
     * @param      <type>  $value  The value
     *
     * @return     string  The pre ordered revenue attribute.
     */
/*    public function getPreOrderedRevenueAttribute($value)
    {
        $account = Account::where('id', $this->attributes['account_id'])->first();
        $currency = $account->marketplace;

        if ($value == null):
            return null;
        else:
            if ($currency == 'UK'):
                return '£'.$value;
            elseif ($currency == 'CA'):
                return 'C$'.$value;
            elseif ($currency == 'US'):
                return '$'.$value;
            else:
                return '€'.$value;
            endif;
        endif;
    }*/

    /**
     * Gets the pre ordered revenue prior period attribute.
     *
     * @param      string  $value  The value
     *
     * @return     string  The pre ordered revenue prior period attribute.
     */
    public function getPreOrderedRevenuePriorPeriodAttribute($value)
    {
        if ($value == null):
            return null;
        else:
            return $value.'%';
        endif;
    }

    /**
     * Gets the pre ordered units prior period attribute.
     *
     * @param      string  $value  The value
     *
     * @return     string  The pre ordered units prior period attribute.
     */
    public function getPreOrderedUnitsPriorPeriodAttribute($value)
    {
        if ($value == null):
            return null;
        else:
            return $value.'%';
        endif;
    }

    /**
     * Gets the average pre order sales price attribute.
     *
     * @param      <type>  $value  The value
     *
     * @return     string  The average pre order sales price attribute.
     */
/*    public function getAveragePreOrderSalesPriceAttribute($value)
    {
        $account = Account::where('id', $this->attributes['account_id'])->first();
        $currency = $account->marketplace;

        if ($value == null):
            return null;
        else:
            if ($currency == 'UK'):
                return '£'.$value;
            elseif ($currency == 'CA'):
                return 'C$'.$value;
            elseif ($currency == 'US'):
                return '$'.$value;
            else:
                return '€'.$value;
            endif;
        endif;
    }*/

    /**
     * Gets the average pre order sales price prior period attribute.
     *
     * @param      string  $value  The value
     *
     * @return     string  The average pre order sales price prior period attribute.
     */
    public function getAveragePreOrderSalesPricePriorPeriodAttribute($value)
    {
        if ($value == null):
            return null;
        else:
            return $value.'%';
        endif;
    }

    /**
     * Defines Relationship with Asin
     *
     * @return     <object>  Relation to Asin
     */
    public function asin()
    {
        return $this
            ->hasOne(
                Asin::class,
                'id', 'asin_id'
            )->with(['category']);
    }

    public function account()
    {
        return $this
            ->hasOne(
                Account::class,
                'id', 'account_id'
            )->with(['subscription']);
    }

}
