<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Zip extends Model {

    protected $fillable = [
        "zip",
    ];

    protected $hidden = ['created_at','updated_at'];

    protected $dates = ["created_at", "updated_at"];

    public static $rules = [
        // Validation rules
    ];

    /**
     * Gets the arrayable attributes.
     *
     * @return     <type>  The arrayable attributes.
     */
    protected function getArrayableAttributes()
    {
        foreach ($this->attributes as $key => $value)
        {
            $null_values = array('—', 'UNKNOWN', 'UNKNO', 'N/A', ' ', '', NULL);
            if (in_array($value, $null_values)):
                $this->attributes[$key] = NULL;
            endif;
        }

        return $this->getArrayableItems($this->attributes);
    }

}
