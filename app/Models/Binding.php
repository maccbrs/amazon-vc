<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Binding extends Model {

    protected $fillable = [
        "bindings",
    ];

    protected $dates = ["created_at", "updated_at"];

    protected $hidden = ['created_at','updated_at'];

    public static $rules = [
        // Validation rules
    ];

    /**
     * Defines Relationship with Asin.
     *
     * @return     <object>  Bindings belongs to Asin.
     */
    
    protected function getArrayableAttributes()
    {
        foreach ($this->attributes as $key => $value)
        {
            $null_values = array('—', 'UNKNOWN', 'UNKNO', 'N/A', ' ', '', NULL);
            if (in_array($value, $null_values)):
                $this->attributes[$key] = NULL;
            endif;
        }

        return $this->getArrayableItems($this->attributes);
    }

    /**
     * { function_description }
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function asin()
    {
        return $this
            ->belongsTo(
                Asin::class,
                'binding_id', 'id'
            );

    }

}
