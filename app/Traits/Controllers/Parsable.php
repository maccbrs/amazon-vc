<?php 
namespace App\Traits\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Carbon\Carbon;
use DateTime;

use App\Models\{
    Asin,
    Binding,
    ProductGroup,
    Category,
    Subcategory,
    Brand,
    ManufacturerCode,
    ParentManufacturerCode,
    Program,
    SalesDiagnostics,
    ReplenishCode,
    PreOrder,
    City,
    State,
    Zip,
    Country,
    GeographicSalesInsight,
    InventoryHealth,
    AlternativePurchase,
    FifthPurchasedProduct,
    FourthPurchasedProduct,
    ThirdPurchasedProduct,
    SecondPurchasedProduct,
    FirstPurchasedProduct
};

/*use App\Traits\Controllers\{
    Storable,
    Importable
};
*/
trait Parsable
{
/*    use
        Importable;
    use
        Storable;*/
    /**
     * @param $string
     * @return string|string[]|null
     */
    public function stripChar($string)
    {
        if (!empty($string)):
            $null_values = array('—', 'UNKNOWN', 'UNKNOWN', 'N/A', ' ', '', NULL);

            if (in_array($string, $null_values)):
                $string = NULL;
            elseif (!empty($string)):
                $string = preg_replace("/[^\d.-]+/", "", $string);
            endif;

            return $string;
        else:
            return null;
        endif;
    }

    public function checkIndex($key, $array)
    {
        if (array_key_exists($key,$array)) {
            return $array[$key];
        }
        else {
            return null;
        }
    }

    /**
     * @param $string
     * @return null
     */
    public function stripNull($string)
    {
        if (!empty($string)):
            $null_values = array('—', 'UNKNOWN', 'UNKNOWN', 'N/A', ' ', '', NULL);

            if (in_array($string, $null_values)):
                $string = NULL;
            endif;

            return $string;
        else:
            return null;
        endif;
    }

    /**
     * @param string $file
     * @param string $delimiter
     * @return array|bool
     */
    public function csvToArray($file = '', $delimiter = ',')
    {
        if (!file_exists($file) || !is_readable($file)):
            return false;
        endif;

        $header = null;
        $data = array();
        if (($handle = fopen($file, 'r')) !== false):
            fgetcsv($handle);
        while (($row = fgetcsv($handle, 1000, $delimiter)) !== false):
                if (!$header):
                    $header = $row;
        foreach ($header as $i => $value):
                        if ($value === '' or $value === null):
                            unset($header[$i]);
        endif;
        endforeach; else:
                    $data[] = array_combine(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $header), $row);
        endif;
        endwhile;
        fclose($handle);
        endif;

        return $data;
    }

    /**
     * @param string $file
     * @return array
     */
    public function getFilters($file = '')
    {
        $csv = array_map("str_getcsv", file($file, FILE_SKIP_EMPTY_LINES));

        $array = array_shift($csv);
        $filterArr = array();

        foreach ($array as $key => $value):
            if (empty($value)):
                unset($array[$key]);
        endif;
        endforeach;

        foreach ($array as $data):
            list($k, $v) = explode('=', $data);
        $filterArr[preg_replace('/[\x00-\x1F\x80-\xFF]/', '', str_replace(array('"', '"'), '', trim($k, '"')))] = str_replace(array('[', ']'), '', trim($v, '"  '));
        endforeach;

        return $filterArr;
    }
    
    /**
     * @return mixed
     */
    public function getAsins()
    {
        $asins = Asin::select('asin')->where('generated_date',date('Y-m-d'))->get()->toArray();

        return $asins;
    }

    /**
     * @param string $file
     * @return array
     */
    public function getSpecificColumn($file = '')
    {
        $handle = fopen($file, 'r');
        $data = fgetcsv($handle, 1000, ",");
        $asins = array();

        if (($handle = fopen($file, 'r')) !== false):
            fgetcsv($handle);
        fgetcsv($handle);
        while (($data = fgetcsv($handle, 1000, ",")) !== false):
                $asin = $data[0];
        if (!in_array($asin, $asins)):
                    array_push($asins, $asin);
        endif;
        endwhile;
        endif;

        return $asins;
    }

    /**
     * @param $asins
     * @param $parsed
     * @return array
     */
    public function removeExist($asins, $parsed)
    {
        $asins = array_column($asins, 'asin');

        foreach ($parsed as $k => $v):
            $parsed_asins[] = $v['ASIN'];
        endforeach;

        $unique = array_diff($parsed_asins, $asins);

        if ($unique):
            foreach ($unique as $k => $v):
                $unique_key = array_search($v, array_column($parsed, 'ASIN'));
        $data[] = $parsed[$unique_key];
        endforeach; else:
            $data = array();
        endif;

        return $data;
    }    

    /**
     * @param $header
     * @param $array
     * @param $column
     * @return mixed
     */
    public function findId($header, $array, $column)
    {
        $key = array_search($header, array_column($array, $column));
        if($key === false):
            return null;
        else:
            return $array[$key]['id'];
        endif;
    }

    public function search($array, $key, $value)
    {
        $results = array();

        if (is_array($array)) {
            if (isset($array[$key]) && $array[$key] == $value) {
                $results[] = $array;
            }

            foreach ($array as $subarray) {
                $results = array_merge($results, $this->search($subarray, $key, $value));
            }
        }

        return $results;
    }

    /**
     * Gets the prerequisite.
     * @param      string  $parameter  The parameter
     * @param      array   $array      The array
     * @return     <type>  The prerequisite.
     */
    public function getPrerequisite($parameter, $array)
    {
        if ($parameter === 'asin'):
            foreach ($array as $value):
                $binding = Binding::firstOrCreate([
                    'bindings' => $value['Binding']
                ],
                [
                    'bindings' => $value['Binding']
                ]);

                $productgroup = ProductGroup::firstOrCreate([
                    'product_group' => $value['Product Group']
                ],
                [
                    'product_group' => $value['Product Group']
                ]);

                $brand = Brand::firstOrCreate([
                    'brand' => $value['Brand']
                ],
                [
                    'brand' => $value['Brand'],
                    'brand_code' => $value['Brand Code']
                ]);

                $parentmanufacturercode = ParentManufacturerCode::firstOrCreate([
                    'parent_manufacturer_code' => $value['Parent Manufacturer Code']
                ],
                [
                    'parent_manufacturer_code' => $value['Parent Manufacturer Code']
                ]);

                $manufacturercode = ManufacturerCode::firstOrCreate([
                    'manufacturer_code' => $value['Manufacturer Code']
                ],
                [
                    'manufacturer_code' => $value['Manufacturer Code'],
                    'parent_manufacturer_code_id' => $parentmanufacturercode->id
                ]);

                $replenishcode = ReplenishCode::firstOrCreate([
                    'replenish_code' => $value['Replenishment Code']
                ],
                [
                    'replenish_code' => $value['Replenishment Code']
                ]);
            endforeach;
        elseif ($parameter === 'sales'):
            foreach($array as $i):
                $category = Category::firstOrCreate(
                    ['category' => $i['Category']],
                    ['category' => $i['Category']]
                );

                $subcategory = Subcategory::firstOrCreate(
                    ['subcategory' => $i['Subcategory']],
                    ['subcategory' => $i['Subcategory']]
                );

                $asin = Asin::where('asin', $i['asin'])->first();

                $asin->category_id = $category->id;
                $asin->subcategory_id = $subcategory->id;
                $asin->save();
            endforeach;
        elseif ($parameter === 'salesJSON'):
            foreach($array as $i):
                $category = Category::firstOrCreate(
                    ['category' => $i['category']],
                    ['category' => $i['category']]
                );

                $subcategory = Subcategory::firstOrCreate(
                    ['subcategory' => $i['subcategory']],
                    ['subcategory' => $i['subcategory']]
                );

                $asin = Asin::where('asin', $i['asin'])->first();

                $asin->category_id = $category->id;
                $asin->subcategory_id = $subcategory->id;
                $asin->save();
            endforeach;
        elseif ($parameter === 'geo'):
            $this->importForeignData(new City, $array, 'city', 'City');
            $this->importForeignData(new Country, $array, 'country', 'Country/Region');
            $this->importForeignData(new State, $array, 'state', 'State');
            $this->importForeignData(new Zip, $array, 'zip', 'ZIP');
        endif;

        return $this->respond('done');  
    }

    public function parseReportRows($json)
    {
        foreach ($json as $a):
            foreach ($a as $b):
                foreach ($b as $c):
                    $result[] = $c->val;
                endforeach;
            endforeach;
        endforeach;

        return $result;
    }

    public function searchMultiArray($type, $val, $array)
    {
        switch ($type) {
            case 'geo':
                foreach ($array as $element) {
                    if ($element['asin'] === $val['asin'] &&
                        $element['country'] === $val['country'] &&
                        $element['state'] === $val['state'] &&
                        $element['city'] === $val['city'] &&
                        $element['zip'] === $val['zip'] ) {

                        return true;
                    }
                }
                break;
            case 'alt':
                foreach ($array as $element) {
                    if ($element['asin'] === $val['asin']) {

                        return true;
                    }
                }
                break;
            case 'sales':
                foreach ($array as $element) {
                    if ($element['asin'] === $val['asin']) {

                        return true;
                    }
                }
                break;
            case 'inv':
                foreach ($array as $element) {
                    if ($element['asin'] === $val['asin']) {

                        return true;
                    }
                }
                break;
            default:
                # code...
                break;
        }
    }

/*    public function encodeUtf8($array)
    {
        $result = collect($array)->map(function ($data) {
            return collect($data)->mapWithKeys(function ($value, $key) {
                return [$key => utf8_encode($value)];
            })->all();
        })->all();

        return $result;
    }*/
}
