<?php

namespace App\Traits\Controllers;

use Illuminate\Http\ {
    Request,
    Response
};

use App\Models\{
    Asin,
    Binding,
    ProductGroup,
    Category,
    Subcategory,
    Brand,
    ManufacturerCode,
    ParentManufacturerCode,
    Program,
    SalesDiagnostics,
    ReplenishCode,
    PreOrder,
    City,
    State,
    Zip,
    Country,
    GeographicSalesInsight,
    InventoryHealth,
    AlternativePurchase,
    NetPpm,
    ImportedJson
};

use Carbon\Carbon;
use DateTime;

use App\Traits\Controllers\{
    Storable
};

trait Importable
{
    use
        Storable;

    /**
     * @param $filter
     * @param $model
     * @return mixed
     */
    public function importProgramFilter($filter, $model)
    {
        $program_filter = $filter;

        $program = array();

        if ($program_filter === 'Amazon Retail'):
            $program = array(
                'program' => 'retail',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ); elseif ($program_filter === 'Amazon Fresh'):
            $program = array(
                'program' => 'fresh',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ); elseif ($program_filter === 'Amazon Prime Now'):
            $program = array(
                'program' => 'prime',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ); elseif ($program_filter === 'Amazon Business'):
            $program = array(
                'program' => 'business',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ); else:
            $program = array(
                'program' => 'unknown',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            );
        endif;

        $program_id = $model::firstOrCreate(
            [
                'program' => $program
            ],
            [
                'program' => $program
            ]
        );

        return $program_id;
    }

    /**
     * @param $model
     * @param $array
     * @param $column
     * @param $header
     * @return array
     */
    public function importForeignData($model, $array, $column, $header)
    {
        $collection = $model::select($column)->get()->toArray();
        $parsed_data = array_values(array_unique(array_column($array, $header)));

        foreach ($collection as $array => $value):
            foreach ($value as $k => $v):
                $collectionArr[] = $v;
            endforeach;
        endforeach;

        $unique = [];
        if ($collection):
            $diff = array_diff($parsed_data, $collectionArr);
            foreach ($diff as $k => $v):
                $unique =
                    [
                        $column => $v,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now()
                    ];
            endforeach;
        else:
            foreach ($parsed_data as $k => $v):
                $unique =
                    [
                        $column => $v,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now()
                    ];
            endforeach;
        endif;
        $import = $model::insert($unique);

        return $unique;
    }

    /**
     * @param $model
     * @param $products
     * @param $columns
     * @param $headers
     * @return array
     */
    public function importMultipleForeignData($model, $products, $columns, $headers)
    {
        $collection = $model::select($columns)->get()->toArray();
        $parsed_data = array_column($products, $headers[1], $headers[0]);

        foreach ($collection as $array => $value):
            $collection_keys[] = $value[$columns[0]];
        $collection_values[] = $value[$columns[1]];
        endforeach;

        if ($collection):
            $collectionArr = array_combine($collection_keys, $collection_values);
            $diff = array_diff($parsed_data, $collectionArr);
            foreach ($diff as $k => $v):
                $unique[] =
                    [
                        $columns[0] => $k,
                        $columns[1] => $v,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now()
                    ];
            endforeach;
        else:
            foreach ($parsed_data as $k => $v):
                $unique[] =
                    [
                        $columns[0] => $k,
                        $columns[1] => $v,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now()
                    ];
            endforeach;
        endif;
        $import = Brand::insert($unique);

        return $unique;
    }

    /**
     * @param $model
     * @param $foreign_model
     * @param $model_column
     * @param $foreign_column
     * @param $headers
     * @param $products
     * @return array
     */
    public function importForeignDataWithKey($model, 
                                            $foreign_model, 
                                            $model_column, 
                                            $foreign_column, 
                                            $headers, 
                                            $products)
    {
        $collection = $model::select($model_column[1])->get()->toArray();
        $foreign_collection = $foreign_model::select($foreign_column)->get()->toArray();
        $parsed_data = array_column($products, $headers[0], $headers[1]);

        foreach ($collection as $array => $value):
            foreach ($value as $k => $v):
                $collectionArr[] = $v;
        endforeach;
        endforeach;

        if ($collection):
            $diff = array_diff($parsed_data, $collectionArr);
            foreach ($diff as $k => $v):
                $unique[] =
                    [
                        $model_column[0] => $this->findId($v, $foreign_collection, $foreign_column[1]),
                        $model_column[1] => $k,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now()
                    ];
            endforeach;
        else:
            foreach ($parsed_data as $k => $v):
                $unique[] =
                    [
                        $model_column[0] => $this->findId($v, $foreign_collection, $foreign_column[1]),
                        $model_column[1] => $k,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now()
                    ];
        endforeach;

        endif;

        if ($unique):
            $import = ManufacturerCode::insert($unique);
        endif;

        return $unique;
    }

    /**
     * @return array
     */
    public function importAsin()
    {
        $file = base_path('public/products/ProductCatalog_1320160.csv');
        $parsed_csv = $this->csvToArray($file);
        // dd($parsed_csv);
        $asins = $this->getAsins();

        if ($asins):
            $products = $this->removeExist($asins, $parsed_csv);
            $foreign = $this->getPrerequisite('asin', $parsed_csv);
            $result = $this->storeProduct($products);
        else:
            $foreign = $this->getPrerequisite('asin', $parsed_csv);
            $result = $this->storeProduct($parsed_csv);
        endif;

        return $result;
    }

    /**
     * Stores a product.
     *
     * @param      array  $products  The products
     * @return     array
     */
    public function storeProduct($products)
    {
        // dd($products);
        if ($products):
            $dataArr = $this->productsArray($products);

            $datas = array_chunk($dataArr, 1000);
            foreach ($datas as $key => $data):
                    Asin::insert($data);
            endforeach;

            $result = $dataArr;

            return $result;
        else:
            return $result = [];
        endif;
    }

    public function storeProductJSON($products)
    {
            $dataArr = $this->productsJSON($products);

            $datas = array_chunk($dataArr, 1000);
            foreach ($datas as $key => $data):
                printf("Inserting product data to database.\n");
                Asin::insert($data);
            endforeach;

            return $result = $dataArr;
    }

    public function storeSales()
    {
        $file = base_path('public/Sales Diagnostic_Detail View_US(5).csv');
        $salesArr = $this->csvToArray($file);
        $filterArr = $this->getFilters($file);

        $foreign = $this->getPrerequisite('sales', $salesArr);

        $sales = SalesDiagnostics::select('generated_date')
                ->where('generated_date', date('2018-07-20'))
                ->first();

        $dataArr = array();
        if (empty($sales)):

            $dataArr = $this->salesArray($salesArr, $filterArr);

            $datas = array_chunk($dataArr, 1000);
            foreach ($datas as $key => $data):
                SalesDiagnostics::insert($data);
            endforeach;

            return $result = $dataArr;
        else:
            $result = [];

            return $result;
        endif;
    }

    public function storeSalesJSON($array, $file)
    {
        if(!empty($array)):
            $dataArr = $this->salesArrayJSON($array);
            
            $datas = array_chunk($dataArr, 1000);

            // $this->importLog(new ImportedJson, $file, 'sales', 'in progress');

            foreach ($datas as $key => $data):
                printf("Inserting sales data to database.\n");
                SalesDiagnostics::insert($data);
            endforeach;

            return $result = $dataArr;
        endif;

        return $result = null;
    }

    /**
     * @return     array
     */
    public function storePreOrders()
    {
        $file = base_path('public/Pre-Orders_US.csv');
        $preorderArr = $this->csvToArray($file);

        $preorders = Preorder::select('generated_date')
                    ->where('generated_date', date('Y-m-d'))
                    ->first();

        $dataArr = array();
        if (empty($preorders)):

            $dataArr = $this->preorderArray($preorderArr);

            $datas = array_chunk($dataArr, 500);
            foreach ($datas as $key => $data):
                PreOrder::insert($data);
            endforeach;

            return $result = $dataArr;
        else:
            $result = [];

            return $result;
        endif;
    }

    /**
     * @return     array
     */
    public function storePreOrdersJSON($array)
    {
        $dataArr = array();

            $dataArr = $this->preorderArrayJSON($array);
            $datas = array_chunk($dataArr, 5000);
            foreach ($datas as $key => $data):
                printf("Inserting preOrders data to database.\n");
                PreOrder::insert($data);
            endforeach;

            return $result = $dataArr;
    }

    /**
     * Stores a geographic.
     *
     * @return     array
     */
    public function storeGeographic()
    {
        $file = base_path('public/Geographic Sales Insights_Detail View_US.csv');
        $geoArr = $this->csvToArray($file);

        $foreign = $this->getPrerequisite('geo', $geoArr);

        $geographics = GeographicSalesInsight::select('generated_date')
                        ->where('generated_date', date('Y-m-d'))
                        ->first();

        $dataArr = array();

        if (empty($geographics)):

            $dataArr = $this->geoArray($geoArr);

            $datas = array_chunk($dataArr, 500);
            foreach ($datas as $key => $data):
                GeographicSalesInsight::insert($data);
            endforeach;

            return $result = $dataArr;
        else:
            $result = [];

            return $result;
        endif;
    }

    /**
     * Stores a geographic json.
     * @param      <type>  $array  The array
     * @return     array   ( description_of_the_return_value )
     */
    public function storeGeographicJSON($array)
    {
        // dd($array);
        $dataArr = array();

            $dataArr = $this->geoArrayJSON($array);

            $datas = array_chunk($dataArr, 2500);
            printf("Data Count: ".count($dataArr).".\n");
            // dd($datas);
            foreach ($datas as $key => $data):
                printf("Inserting geographic data to database.\n");
                GeographicSalesInsight::insert($data);
            endforeach;

            // dd($dataArr);
            return $result = $dataArr;
    }
    
    /**
     * Stores an inventory.
     *
     * @return     array  ( imported inventory )
     */
    public function storeInventory()
    {
        $file = base_path('public/Inventory Health_US.csv');
        $invArr = $this->csvToArray($file);

        $foreign = $this->getPrerequisite('inv', $invArr);

        $inventories = InventoryHealth::select('generated_date')
                        ->where('generated_date', date('Y-m-d'))
                        ->first();

        $dataArr = array();

        if (empty($inventories)):

            $dataArr = $this->invArray($invArr);

            $datas = array_chunk($dataArr, 500);
            foreach ($datas as $key => $data):
                InventoryHealth::insert($data);
            endforeach;

            return $result = $dataArr;
        else:
            $result = [];

            return $result;
        endif;
    }

    /**
     * Stores an inventory.
     *
     * @return     array  ( imported inventory )
     */
    public function storeInventoryJSON($array)
    {
        $dataArr = array();

            $dataArr = $this->invArrayJSON($array);

            $datas = array_chunk($dataArr, 2500);
            foreach ($datas as $key => $data):
                printf("Inserting inventory data to database.\n");
                InventoryHealth::insert($data);
            endforeach;

            return $result = $dataArr;
    }

    /**
     * Stores an alternative.
     *
     * @return     array  ( imported Alternative )
     */
    public function storeAlternative()
    {
        $file = base_path('public/Item Comparison and Alternate Purchase Behavior_Alternate Purchase_US.csv');
        $altArr = $this->csvToArray($file);

        $alternatives = AlternativePurchase::select('generated_date')
                        ->where('generated_date', date('Y-m-d'))
                        ->first();

        $dataArr = array();
        if (empty($alternatives)):

            $dataArr = $this->alternativeArray($altArr);

            $datas = array_chunk($dataArr, 500);
            foreach ($datas as $key => $data):
                AlternativePurchase::insert($data);
            endforeach;

            return $result = $dataArr;
        else:
            $result = [];

            return $result;
        endif;
    }

    /**
     * Stores an alternative.
     *
     * @return     array  ( imported Alternative )
     */
    public function storeAlternativeJSON($array)
    {
        $dataArr = array();

        $dataArr = $this->alternativeArrayJSON($array);

        $datas = array_chunk($dataArr, 1500);
        foreach ($datas as $key => $data):
            printf("Inserting alternative data to database.\n");
            AlternativePurchase::insert($data);
        endforeach;

        return $result = $dataArr;
    }

    public function storeNetPpmJSON($array)
    {
        $dataArr = array();

        $dataArr = $this->netArrayJSON($array);

        $datas = array_chunk($dataArr, 1500);
        foreach ($datas as $key => $data):
            printf("Inserting netppm data to database.\n");
            NetPpm::insert($data);
        endforeach;

        return $result = $dataArr;
    }
}
