<?php

namespace App\Traits\Controllers\Reports\Account;

use App\Models\{
    Account
};

trait AccountQueryable
{

    public $dir = 'storage/accounts/';

    /**
     * Gets the account collection.
     * @return     object  The account collection.
     */
    public function getAccountCollection()
    {
        $account = Account::with('subscription')
            ->paginate(100);

        return $account;
    }

    /**
     * Gets the account entity.
     * @param      string  $vgid   The vgid
     * @return     object  The account entity.
     */
    public function getAccountEntity($vgid)
    {
        $account = Account::where('vendor_group_id', $vgid)
            ->with('subscription')
            ->get();

        return $account;
    }
}
