<?php

namespace App\Traits\Controllers\Reports\Category;

use App\Models\{
    Category
};

trait CategoryQueryable
{

    /**
     * Gets the category collection.
     *
     * @param      Request  $request  The request
     *
     * @return     object   The category collection.
     */
    public function getCategoryCollection($request)
    {
        /**
         * Gets specific categories based on account id
         * @var        Request
         */
        $category = Category::whereHas('asins', function ($query) use ($request) {
                $query->where('account_id', $request->accountId);
            })
            ->paginate(100);

        return $category;
    }

}
