<?php

namespace App\Traits\Controllers\Reports\Alternative;

use App\Models\{
    AlternativePurchase
};

trait AlternativeQueryable
{

    /**
     * Gets the alternative collection.
     *
     * @param      object  $param  The parameter
     *
     * @return     JSON    The alternative collection.
     */
    public function getAlternativeCollection($param)
    {
        $alt = AlternativePurchase::where('account_id', $param->accountId)
            ->whereBetween('generated_date', [$param->from, $param->to])
            ->with([
                'asin',
                'firstAlternativeAsin',
                'secondAlternativeAsin',
                'thirdAlternativeAsin',
                'fourthAlternativeAsin',
                'fifthAlternativeAsin'    
            ])
            ->paginate($param->perPage);

        return $alt;
    }

}
