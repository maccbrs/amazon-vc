<?php

namespace App\Traits\Controllers\Reports\Geographic;

use App\Models\{
    GeographicSalesInsight
};

trait GeographicQueryable
{

    /**
     * Gets the geographic collection.
     *
     * @param      object  $param  The parameter
     *
     * @return     JSON    The geographic collection.
     */
    public function getGeographicCollection($param)
    {
        $geo = GeographicSalesInsight::where('account_id', $param->accountId)
                ->whereBetween('generated_date', [$param->from, $param->to])
                ->with([
                    'asin',
                    'country',
                    'state',
                    'city',
                    'zip',
                ])
                ->paginate($param->perPage);

        return $geo;
    }

}
