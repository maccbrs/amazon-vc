<?php

namespace App\Traits\Controllers\Reports\NetPpm;

trait NetPpmParsable
{
    /**
     * Parse the Net PPM collection.
     *
     * @param      array  $param  The parameter
     *
     * @return     array   The net ppm collection.
     */
    public function parseNetPpmCollection($param)
    {
        $array = collect($array)->map(function ($data) {
            return collect($data)->mapWithKeys(function ($value, $key) {
                return [$key => utf8_encode($value)];
            })->all();
        })->all();

        return $array;
    }

}
