<?php

namespace App\Traits\Controllers\Reports\Netppm;

use App\Models\{
    NetPpm
};

trait NetPpmQueryable
{
    /**
     * Gets the Net PPM collection.
     *
     * @param      object  $param  The parameter
     *
     * @return     JSON    The inventory collection.
     */
    public function getNetPpmCollection($param)
    {
        $net = NetPpm::leftJoin('asins', 'net_ppms.asin_id', '=', 'asins.id')
            ->where('net_ppms.account_id', $param->accountId)
            ->where('net_ppms.generated_date_from', date("Y-m-d", strtotime($param->from)))
            ->where('net_ppms.generated_date_to', date("Y-m-d", strtotime($param->to)))
            ->get();

        $result = $this->encodeUtf8($net);

        return $result;
    }

    public function encodeUtf8($array)
    {
        $result = collect($array)->map(function ($data) {
            return collect($data)->mapWithKeys(function ($value, $key) {
                return [$key => utf8_encode($value)];
            })->all();
        })->all();

        return $result;
    }

}
