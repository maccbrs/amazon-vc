<?php

namespace App\Traits\Controllers\Reports\Preorder;

use App\Models\{
    PreOrder
};

trait PreorderQueryable
{

    /**
     * Gets the preorder collection.
     * @param      object  $param  The parameter
     * @return     JSON  The preorder collection.
     */
    public function getPreorderCollection($param)
    {
        $pre = PreOrder::where('account_id', $param->accountId)
                ->whereBetween('generated_date', [$param->from, $param->to])
                ->with([
                    'asin'   
                ])
                ->paginate($param->perPage);

        return $pre;
    }

}
