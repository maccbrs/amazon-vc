<?php

namespace App\Traits\Controllers\Reports\Asin;

use App\Models\{
    Asin
};

trait AsinQueryable
{

    public $dir = 'storage/products/';

    /**
     * Gets the asins collection.
     *
     * @param      Request  $request  The request
     *
     * @return     object   The asins collection.
     */
    public function getAsinsCollection($request)
    {
        $asins = Asin::where('account_id', $request->accountId)
            ->with([
                'account',
                'brand',
                'category',
                'binding',
                'productGroup',
                'replenishCode',
                'manufacturerCode',
                'parentManufacturerCode'
            ])
            ->paginate(100);

        return $asins;
    }

    /**
     * Gets the asin entity.
     *
     * @param      string   $asin     The asin
     * @param      Request  $request  The request
     *
     * @return     object   The asin entity.
     */
    public function getAsinEntity($asin, $request)
    {
        $asins = Asin::where('account_id', $request->accountId)
            ->where('asin', $asin)
            ->with([
                'account',
                'brand',
                'category',
                'subcategory',
                'binding',
                'productGroup',
                'replenishCode',
                'manufacturerCode'
            ])
            ->get();

        return $asins;
    }
}
