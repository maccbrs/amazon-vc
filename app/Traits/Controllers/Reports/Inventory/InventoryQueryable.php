<?php

namespace App\Traits\Controllers\Reports\Inventory;

use App\Models\{
    InventoryHealth
};

trait InventoryQueryable
{

    /**
     * Gets the inventory collection.
     *
     * @param      object  $param  The parameter
     *
     * @return     JSON    The inventory collection.
     */
    public function getInventoryCollection($param)
    {
        $inv = InventoryHealth::where('account_id', $param->accountId)
                ->whereBetween('generated_date', [$param->from, $param->to])
                ->with([
                    'asin' 
                ])
                ->paginate($param->perPage);

        return $inv;
    }

}
