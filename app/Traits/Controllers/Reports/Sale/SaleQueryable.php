<?php

namespace App\Traits\Controllers\Reports\Sale;

use App\Models\{
    SalesDiagnostics,
    Accounts
};

trait SaleQueryable
{

    /**
     * Gets the sales diagnostic collection.
     *
     * @param      array  $param  The parameter
     * @param      array  $keys   The keys
     *
     * @return     object  The sales diagnostic collection.
     */
    public function getSalesDiagnosticCollection($param, $keys)
    {
        try {
            $sale = SalesDiagnostics::leftJoin('asins', 'sales_diagnostics.asin_id', '=', 'asins.id')
                ->where('sales_diagnostics.account_id', $param->accountId)
                ->where(function ($query) use ($param) {
                    if ($param->displayNulls == 'false' or empty($param->displayNulls)) {
                        $query
                            ->whereNotNull('sales_diagnostics.shipped_revenue')
                                ->where(function ($query) {
                                    $query->where('sales_diagnostics.shipped_revenue', '!=', 0);
                                })
                                ->WhereNotNull('sales_diagnostics.shipped_cogs')
                                ->where(function ($query) {
                                    $query->where('sales_diagnostics.shipped_cogs', '!=', 0);
                                });
                    }
                })
                ->whereBetween('sales_diagnostics.generated_date', [$param->from, $param->to])
                ->get(array_unique($keys));

                $sale = collect($sale)->map(function ($sales_data) {
                    return collect($sales_data)->mapWithKeys(function ($value, $key) {
                        return [$key => utf8_encode($value)];
                    })->all();
                })->all();

                return $sale;
        }
        catch (QueryException $e) {
            return response()->json(['msg' => $e->errorInfo[2]], 400);
        }
        catch (PDOException $e) {
            return response()->json(['msg' => $e->errorInfo[2]], 400);
        }
    }

    /**
     * Gets the sales diagnostic collection summary.
     *
     * @param      string  $accountId  The account identifier
     * @param      string  $from       The from
     * @param      string  $to         { parameter_description }
     * @param      string  $perPage    The per page
     *
     * @return     object  The sales diagnostic collection summary.
     */
    public function getSalesDiagnosticCollectionSummary($accountId, $from, $to, $perPage)
    {
        $summary = SalesDiagnostics::selectRaw(
            'asin_id,
            sum(shipped_cogs) as shipped_cogs,
            sum(shipped_units) as shipped_units,
            sum(shipped_cogs_percentage_total) as shipped_cogs_percentage_total,
            sum(shipped_cogs_prior) as shipped_cogs_prior,
            sum(shipped_units_percentage_total) as shipped_units_percentage_total,
            sum(shipped_units_prior) as shipped_units_prior,
            sum(ordered_units) as ordered_units,
            sum(ordered_units_percentage_total) as ordered_units_percentage_total,
            sum(ordered_units_prior) as ordered_units_prior,
            sum(customer_returns) as customer_returns,
            sum(free_replacements) as free_replacements,
            sum(subcategory_sales_rank) as subcategory_sales_rank,
            sum(subcategory_better_worse) as subcategory_better_worse,
            sum(average_sales_price) as average_sales_price,
            sum(average_sales_price_prior) as average_sales_price_prior,
            sum(change_in_glance_view_prior) as change_in_glance_view_prior,
            sum(rep_oos) as rep_oos,
            sum(rep_oos_percentage_total) as rep_oos_percentage_total,
            sum(rep_oos_prior) as rep_oos_prior,
            sum(lbb) as lbb'
            )
            ->where('account_id', $accountId)
            ->whereBetween('generated_date', [$from, $to])
            ->groupBy('asin_id', 'program_id', 'account_id')
            ->with([
                'asin',
                'program'
            ])
            ->get();

        return $summary;
    }

}
