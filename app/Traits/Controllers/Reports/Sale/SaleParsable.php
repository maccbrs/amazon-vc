<?php

namespace App\Traits\Controllers\Reports\Sale;

trait SaleParsable
{

    /**
     * Gets the columns keys.
     * @param      array  $filter_columns  The filter columns
     * @return     array  The columns keys.
     */
    public function getColumnKeys($filter_columns)
    {
        $keys = [];
        foreach($filter_columns as $column) {
            if ($column == 'generated_date') {
                $keys[] = 'sales_diagnostics.generated_date';
            } elseif (!empty($column)) {
                $keys[] = $column;
            }
        }

        return $keys;
    }

    /**
     * Gets the asins.
     * @param      array  $query  The query
     * @return     array  The asins.
     */
    public function createAsinsArray($query)
    {
        $asin = [];

        foreach ($query as $data1) {
            $asin[$data1['asin']] = $data1;
        }

        return $asin;
    }

    /**
     * Gets the data.
     * @param      array  $asin   The asin
     * @return     array  The data.
     */
    public function createDataArray($asin)
    {
        $data = [];

        foreach ($asin as $key => $item) {
            $data[$item['generated_date']][$key] = $item;
        }

        return $data;
    }

    /**
     * Gets the check date.
     * @param      boolean  $d           The formatted date
     * @param      boolean  $check_date  The check date
     * @param      array   $data        The data
     * @param      string   $date        The date
     * @return     string   The check date.
     */
    public function getCheckDate($d, $check_date, $data, $date)
    {
        if ($d == true && $check_date == true && $data == true) {
            return $data[$date];
        }
        elseif ($d == true && $check_date == true && $data == false) {
            return response()
                ->json([
                        'error' => [
                            'date' => $date, 'msg' => 'No Available Data.'
                        ]
                    ], 204);    
        }
        elseif ($d == true && $check_date == false) {
            return response()
                ->json([
                        'error' => [
                            'date' => $date, 'msg' => 'Date not in Range.'
                        ]
                    ], 400);
        }
        else {
            return response()
                ->json([
                        'error' => [
                            'date' => $date, 'msg' => 'Date not Valid.'
                        ]
                    ], 400);
        }
    }

}
