<?php

namespace App\Traits\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Carbon\Carbon;
use DateTime;

use App\Models\{
    ImportedJson
};


trait Loggable
{
    public function importLog($model, $filename, $report_type, $status)
    {
        // dd($status);
        $logs = ImportedJson::updateOrCreate([
            'filename' => $filename
        ],
        [
            'filename' => $filename,
            'report_type' => $report_type,
            'status' => $status
        ]);
    }
}
