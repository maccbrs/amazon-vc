<?php

namespace App\Traits\Controllers\Ingestions\Preorder;

use App\Models\{
    ImportedJson
};

use App\Traits\Controllers\ {
    Loggable,
    Importable,
    Parsable
};

use App\Traits\Controllers\Ingestions\Preorder\ {
    PreorderQueryable,
    PreorderParsable
};

trait PreorderIngestable
{
    use
        Loggable, Importable, Parsable, PreorderQueryable, PreorderParsable;

    public $dir = 'storage/preorder/';

    /**
     * Compare Existing Json files to Imported Json Files.
     * @return     Array  The list of to be imported Json Files.
     */
    public function checkImportedLogs()
    {
        $list = $this->getPreorderJsons();
        printf ("Looking for new Pre Orders JSON. . . \n");
        $import_logs = $this->getImportedLogs();

        $files = $this->getUniqueJsons($import_logs, $list);

        return $files;
    }

    /**
     * Parse Json Files from checkImportedLogs() and insert foreign keys on db.
     * @param      JSON  $file   The file
     */
    public function parseJson($x, $files, $file)
    {
        $data = file_get_contents(base_path($this->dir.$file));
        $json = json_decode($data);
        $result = [];   

        if (!empty($json->payload->reportParts[0]->rowCount)) {
            $result = $this->getProcess($file, $json, $x, $files);
        }
        elseif (!empty($json->payload->reportParts->rowCount)) {
            $result = $this->getProcess($file, $json, $x, $files);
        }

        if(!empty($result)) {
            $this->storePreOrdersJSON($result);
            $this->importLog(new ImportedJson, $file, 'pre', 'success');
        }
        else {
            $this->importLog(new ImportedJson, $file, 'pre', 'no data');
        }

        unlink(base_path($this->dir.$file));
    }

    public function getProcess($file, $json, $x, $files)
    {
        $filename = basename($file, '.json');

        $string = explode('_', $filename);
        $account_id = $string[1]; 
        $date = $string[2];

        $reportRows = $json->payload->reportParts[0]->reportRows;
        $column_names = $json->payload->reportParts[0]->columnNames;

        $acc = $this->getAccountDetail($account_id);

        $val = [];
        $val = $this->getReportDataValue($reportRows);

        $count = count($column_names);
        $values = array_chunk($val, $count);
        
        $result = $this->getMappableData($values, $acc, $date, $column_names, $file, $x, $files);

        return $result;
    }
}
