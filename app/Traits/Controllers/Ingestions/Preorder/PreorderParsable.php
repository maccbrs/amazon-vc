<?php 

namespace App\Traits\Controllers\Ingestions\Preorder;

use App\Traits\Controllers\Ingestions\Preorder\ {
    PreorderQueryable
};

use App\Traits\Controllers\ {
    Parsable
};

trait PreorderParsable
{
    use
    PreorderQueryable, Parsable;

    public $dir = 'storage/preorder/';

    /**
     * Gets the unique jsons.
     *
     * @param      array  $import_logs  The import logs
     * @param      array  $list         The list
     *
     * @return     array  The unique jsons.
     */
    public function getUniqueJsons($import_logs, $list)
    {
        foreach ($import_logs as $import_log) {
            $import_logged[] = $import_log->filename;
        }

        if (!empty($import_logged)) {
            $files = array_diff($list, $import_logged);            
        } else {
            $files = $list;
        }

        return $files;
    }

    /**
     * Gets the report data value.
     *
     * @param      object  $reportRows  The report rows
     *
     * @return     array  The report data value.
     */
    public function getReportDataValue($reportRows)
    {
        foreach ($reportRows as $reportRow) {
            foreach ($reportRow as $reportValue) {
                foreach ($reportValue as $reportData) {
                    $val[] = $reportData->val;
                }
            }
        }

        if (!empty($val)) {
            return $val;            
        }
        else {
            return $val = [];;
        }

    }

    /**
     * Gets the row filters.
     *
     * @param      object  $acc            The acc
     * @param      object  $asin           The asin
     * @param      string  $date           The date
     * @param      object  $replenishment  The replenishment
     *
     * @return     array   The row filters.
     */
    public function getRowFilters($acc, $asin, $date)
    {
        $filters = [
            'account_id' => $acc->id,
            'asin_id' => $asin->id,
            'generated_date' => date("Y-m-d", strtotime($date))
        ]; 

	    return $filters;
    }

	/**
     * Gets the unique values.
     *
     * @param      array  $values  The values
     * @param      array  $inv     The inv
     *
     * @return     array  The unique values.
     */
	public function getUniqueValues($values, $inv)
	{
        foreach ($values as $value) {

            $json_value = [
                "asin" => $value[4],
            ];

            $json_check = $this->searchMultiArray('inv', $json_value, $inv);

            if ($json_check == false) {
                $filtered_values[] = $value;
            }
        }

        return $filtered_values;
	}

    /**
     * Gets the mappable data.
     *
     * @param      array   $filtered_values  The filtered values
     * @param      object   $acc              The acc
     * @param      array   $column_names     The column names
     * @param      array   $filters          The filters
     * @param      string   $file             The file
     * @param      integer  $i                The iteration for values
     * @param      integer  $x                The iteration for files
     * @param      array   $files            The files
     *
     * @return     array   The mappable data.
     */
    public function getMappableData($values, $acc, $date, $column_names, $file, $x, $files)
    {
        $i = 0;
        foreach ($values as $value) {
            $i++;
            $asin = $this->getAsinDetail($acc, $value);
            if (!empty($asin)) {
                $pre = $this->getPreorderCollection($acc, $asin, $date);
                if (empty($pre)) {
                    $filters = $this->getRowFilters($acc, $asin, $date);
                    $result[] = array_merge(array_combine($column_names, $value),$filters);
                }
                $this->getIngestionStatus($asin, $i, $values, $file, $x, $files);
            }
        }

        return $result;
    }
}