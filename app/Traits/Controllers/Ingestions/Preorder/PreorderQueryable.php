<?php

namespace App\Traits\Controllers\Ingestions\Preorder;

use App\Models\{
    Account,
    ImportedJson,
    Asin,
    PreOrder
};

trait PreorderQueryable
{

    public $dir = 'storage/preorder/';

    /**
     * Gets the imported logs.
     *
     * @return     object  The imported logs.
     */
    public function getImportedLogs()
    {
        return ImportedJson::where('report_type', 'pre')->get();
    }

    /**
     * Gets the preorders jsons.
     *
     * @return     array  The preorders jsons.
     */
    public function getPreorderJsons()
    {
        return preg_grep('~^PreOrdersDetail_.*\.json$~',
                  scandir(base_path($this->dir)));
    }

    /**
     * Gets the asin detail.
     * @param      object  $acc    The acc
     * @param      array   $value  The value
     * @return     object  The asin detail.
     */
    public function getAsinDetail($acc, $value)
    {
        return Asin::where('asin', $value[4])
            ->where('account_id', $acc->id)
            ->first();
    }

    /**
     * Gets the account detail.
     * @param      string  $account_id  The account identifier
     * @return     object  The account detail.
     */
    public function getAccountDetail($account_id)
    {
        return Account::where('vendor_group_id', $account_id)->first();
    }

    /**
     * Gets the preorder collection.
     *
     * @param      object  $acc    The acc
     * @param      object  $asin   The asin
     * @param      string  $date   The date
     *
     * @return     array   The preorder collection.
     */
    public function getPreorderCollection($acc, $asin, $date)
    {
        return PreOrder::where('account_id', $acc->id)
            ->where('asin_id', $asin->id)
            ->where('generated_date', date("Y-m-d", strtotime($date)))
            ->first();
    }

    /**
     * Gets the ingestion status.
     *
     * @param      object  $asin    The asin
     * @param      string  $i       The iteration of values
     * @param      array   $values  The values
     * @param      string  $file    The file
     * @param      string  $x       The iteration of files
     * @param      array   $files   The files
     *
     * @return     string  The ingestion status.
     */
    public function getIngestionStatus($asin, $i, $values, $file, $x, $files)
    {
        return printf(
            "Asin Id: ".$asin->id." have been checked. Count: ".$i."/".count($values).".\n".$file.".\n".$x."/".count($files)." remaining.\n"
        );
    }

}
