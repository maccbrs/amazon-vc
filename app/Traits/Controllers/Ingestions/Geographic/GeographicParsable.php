<?php 

namespace App\Traits\Controllers\Ingestions\Geographic;

use App\Traits\Controllers\Ingestions\Geographic\ {
    GeographicQueryable
};

use App\Traits\Controllers\ {
    Parsable
};

trait GeographicParsable
{
    use
    GeographicQueryable, Parsable;

    public $dir = 'storage/geographic/';

    /**
     * Gets the unique jsons.
     *
     * @param      array  $import_logs  The import logs
     * @param      array  $list         The list
     *
     * @return     array  The unique jsons.
     */
    public function getUniqueJsons($import_logs, $list)
    {
        foreach ($import_logs as $import_log) {
            $import_logged[] = $import_log->filename;
        }

        if (!empty($import_logged)) {
            $files = array_diff($list, $import_logged);            
        } else {
            $files = $list;
        }

        return $files;
    }

    /**
     * Gets the row filters.
     *
     * @param      object  $acc      The acc
     * @param      object  $asin     The asin
     * @param      object  $date     The date
     * @param      object  $city     The city
     * @param      object  $country  The country
     * @param      object  $state    The state
     * @param      object  $zip      The zip
     *
     * @return     array   The row filters.
     */
    public function getRowFilters($acc, $asin, $date, $city, $country, $state, $zip)
    {
        $filters = [
            'account_id' => $acc->id,
            'asin_id' => $asin->id,
            'generated_date' => date("Y-m-d", strtotime($date)),
            'city_id' => $city->id,
            'country_id' => $country->id,
            'state_id' => $state->id,
            'zip_id' => $zip->id
        ];

	    return $filters;
    }

	/**
      * Gets the unique values.
      *
      * @param      array  $values  The values
      * @param      object  $geo     The geo
      *
      * @return     array  The unique values.
      */
	public function getUniqueValues($values, $geo)
	{
        foreach ($values as $value) {

            $json_value = [
                "asin" => $value[8],
                "country" => $value[0],
                "state" => $value[1],
                "city" => $value[2],
                "zip" => $value[3]
            ];

            $json_check = $this->searchMultiArray('geo', $json_value, $geo);

            if ($json_check == false) {
                $filtered_values[] = $value;
            }
        }

        return $filtered_values;
	}

    /**
     * Gets the mappable data.
     *
     * @param      array    $filtered_values  The filtered values
     * @param      object   $acc              The acc
     * @param      array   $column_names     The column names
     * @param      array   $filters          The filters
     * @param      string   $file             The file
     * @param      integer  $i                Count
     * @param      string   $x                Count of files
     * @param      array   $files            The files
     *
     * @return     array   The mappable data.
     */
    public function getMappableData($filtered_values, $acc, $date, $column_names, $file, $x, $files)
    {
        $i = 0;
        foreach ($filtered_values as $filtered_value) {
            $i++;
            $asin = $this->getAsinDetail($acc, $filtered_value);

            if (!empty($asin)) {
                if (!empty($filtered_value[2])) {
                    $city = $this->firstOrCreateCity($filtered_value);                                
                }

                if (!empty($filtered_value[0])) {
                    $country = $this->firstOrCreateCountry($filtered_value);                           
                }

                if (!empty($filtered_value[1])) {
                    $state = $this->firstOrCreateState($filtered_value);                                
                }

                if (!empty($filtered_value[3])) {
                    $zip = $this->firstOrCreateZIp($filtered_value);                     
                }

                $filters = $this->getRowFilters($acc, $asin, $date, $city, $country, $state, $zip);

                $result[] = array_merge(array_combine($column_names, $filtered_value),$filters);

                $this->getIngestionStatus($asin, $i, $filtered_values, $file, $x, $files);
            }
        }

        return $result;
    }
}