<?php

namespace App\Traits\Controllers\Ingestions\Geographic;

use App\Models\{
    Account,
    ImportedJson,
    Asin,
    GeographicSalesInsight,
    City,
    Country,
    State,
    Zip
};

trait GeographicQueryable
{

    public $dir = 'storage/geographic/';

    /**
     * Gets the imported logs.
     *
     * @return     object  The imported logs.
     */
    public function getImportedLogs()
    {
        return ImportedJson::where('report_type', 'geo')->get();
    }

    /**
     * Gets the Geographic jsons.
     *
     * @return     array  The Geographic jsons.
     */
    public function getGeographicJsons()
    {
        return preg_grep('~^GeographicSalesDetail_.*\.json$~',
            scandir(base_path($this->dir)));
    }

    /**
     * Gets the asin detail.
     *
     * @param      object  $acc    The acc
     * @param      array   $value  The value
     *
     * @return     object  The asin detail.
     */
    public function getAsinDetail($acc, $value)
    {
        return Asin::where('asin', $value[8])
            ->where('account_id', $acc->id)
            ->first();
    }

    /**
     * Gets the account detail.
     *
     * @param      string  $account_id  The account identifier
     *
     * @return     object  The account detail.
     */
    public function getAccountDetail($account_id)
    {
        return Account::where('vendor_group_id', $account_id)
            ->first();
    }

    /**
     * Gets the geographic collection.
     *
     * @param      string  $acc    The acc
     * @param      string  $date   The date
     *
     * @return     array   The geographic collection.
     */
    public function getGeographicCollection($acc, $date)
    {
        return GeographicSalesInsight::select('asins.asin as asin', 'countries.country', 'states.state', 'cities.city', 'zips.zip')
            ->leftJoin('asins', 'geographic_sales_insights.asin_id', '=', 'asins.id')
            ->leftJoin('countries', 'geographic_sales_insights.country_id', '=', 'countries.id')
            ->leftJoin('states', 'geographic_sales_insights.state_id', '=', 'states.id')
            ->leftJoin('cities', 'geographic_sales_insights.city_id', '=', 'cities.id')
            ->leftJoin('zips', 'geographic_sales_insights.zip_id', '=', 'zips.id')
            ->where('geographic_sales_insights.account_id', $acc->id)
            ->where('geographic_sales_insights.generated_date', date("Y-m-d", strtotime($date)))
            ->get()
            ->toArray();
    }

    /**
     * First or Creates City
     *
     * @param      string  $filtered_value  The filtered value
     *
     * @return     object  The City
     */
    public function firstOrCreateCity($filtered_value)
    {
        return City::firstOrCreate([
                'city' => $filtered_value[2]
            ],
            [
                'city' => $filtered_value[2]
            ]);
    }

    /**
     * First or Creates Country
     *
     * @param      string  $filtered_value  The filtered value
     *
     * @return     object  The Country
     */
    public function firstOrCreateCountry($filtered_value)
    {
        return Country::firstOrCreate([
                'country' => $filtered_value[0]
            ],
            [
                'country' => $filtered_value[0]
            ]);  
    }

    /**
     * First or Creates State
     *
     * @param      string  $filtered_value  The filtered value
     *
     * @return     object  The State
     */
    public function firstOrCreateState($filtered_value)
    {
        return State::firstOrCreate([
                'state' => $filtered_value[1]
            ],
            [
                'state' => $filtered_value[1]
            ]);         
    }

    /**
     * First or Creates Zip
     *
     * @param      string  $filtered_value  The filtered value
     *
     * @return     object  The Zip
     */
    public function firstOrCreateZIp($filtered_value)
    {
        return Zip::firstOrCreate([
                'zip' => $filtered_value[3]
            ],
            [
                'zip' => $filtered_value[3]
            ]);
    }

    /**
     * Gets the ingestion status.
     *
     * @param      object  $asin             The asin
     * @param      string  $i                The iteration of values
     * @param      array   $filtered_values  The filtered values
     * @param      string  $file             The file
     * @param      string  $x                The iteration of files
     * @param      array   $files            The files
     *
     * @return     string  The ingestion status.
     */
    public function getIngestionStatus($asin, $i, $filtered_values, $file, $x, $files)
    {
        return printf(
            "Asin Id: ".$asin->id." had been checked. Count: ".$i."/".count($filtered_values).".\n".$file.".\n".$x."/".count($files)." remaining.\n"
        );
    }

}
