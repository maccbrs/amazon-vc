<?php

namespace App\Traits\Controllers\Ingestions\Geographic;

use App\Models\{
    Account,
    ImportedJson,
    Asin,
    GeographicSalesInsight,
    City,
    Country,
    State,
    Zip
};

use App\Traits\Controllers\ {
    Loggable,
};

use App\Traits\Controllers\Ingestions\Geographic\ {
    GeographicParsable
};

trait GeographicIngestable
{
    use
        Loggable, GeographicParsable;

    public $dir = 'storage/geographic/';

    /**
     * Compare Existing Json files to Imported Json Files.
     *
     * @return     Array  The list of to be imported Json Files.
     */
    public function checkImportedLogs()
    {
        $list = $this->getGeographicJsons();
        printf ("Looking for new Geographic JSON. . . \n");
        $import_logs = $this->getImportedLogs();
        $files = $this->getUniqueJsons($import_logs, $list);
        return $files;
    }

    /**
     * Parse Json Files from checkImportedLogs() and insert foreign keys on db.
     *
     * @param      string  $x      The count
     * @param      array   $files  The files
     * @param      JSON    $file   The file
     */
    public function parseJson($x, $files, $file)
    {
        $data = file_get_contents(base_path($this->dir.$file));
        $json = json_decode($data);
        
        $result = [];
        $val = [];
        $filtered_values = [];
        $json_value = [];

        if (!empty($json->payload->reportParts[0]->rowCount) && $json->payload->reportParts[0]->reportRows) {
            $result = $this->getProcess($x, $files, $file, $json);
        }

        if(!empty($result)) {
            $this->storeGeographicJSON($result);
            $this->importLog(new ImportedJson, $file, 'geo', 'success');
        } else {
            $this->importLog(new ImportedJson, $file, 'geo', 'no data');
        }
        unlink(base_path($this->dir.$file));
    }

    /**
     * Gets the process.
     *
     * @param      string  $x      { parameter_description }
     * @param      array  $files  The files
     * @param      string  $file   The file
     * @param      array  $json   The json
     *
     * @return     array  The process.
     */
    public function getProcess($x, $files, $file, $json)
    {
        $filename = basename($file, '.json');
        $string = explode('_', $filename);
        $view = $string[1];
        $account_id = $string[2]; 
        $date = $string[3];
        $generated_date = date("Y-m-d", strtotime($date));
        $reportRows = $json->payload->reportParts[0]->reportRows;
        $column_names = $json->payload->reportParts[0]->columnNames;
        $val = $this->parseReportRows($reportRows);

        $count = count($column_names);
        $values = array_chunk($val, $count);

        $acc = $this->getAccountDetail($account_id);

        printf ("Getting List of Data based on account id and generated date.\n");

        $geo = $this->getGeographicCollection($acc, $date);

        printf ("Checking Duplicated Data. . .\n");

        $filtered_values = $this->getUniqueValues($values, $geo);

        $result = $this->getMappableData($filtered_values, $acc, $date, $column_names, $file, $x, $files);

        return $result;
    }
}
