<?php

namespace App\Traits\Controllers\Ingestions\Account;

use App\Models\{
    Subscription,
    Account,
    ImportedJson
};

use App\Traits\Controllers\ {
    Loggable
};

use App\Traits\Controllers\Ingestions\Account\ {
    AccountQueryable
};

trait AccountIngestable
{
    use
        AccountQueryable, Loggable;

    public $dir = 'storage/accounts/';

    /**
     * Compare Existing Json files to Imported Json Files.
     * @return     Array  The list of to be imported Json Files.
     */
    public function checkImportedLogs()
    {
        $list = $this->getAccountJsons();

        printf ("Looking for new Accounts JSON. . . \n");

        $import_logs = $this->getImportedLogs();

        foreach ($import_logs as $import_log) {
            $import_logged[] = $import_log->filename;
        }

        if (!empty($import_logged)) {
            $files = array_diff($list, $import_logged);            
        }
        else {
            $files = $list;
        }

        return $files;
    }

    /**
     * Parse Json Files from checkImportedLogs() and insert foreign keys on db.
     * @param      string  $x      The count of loop.
     * @param      array  $files  The files
     * @param      JSON    $file   The file
     */
    public function parseJson($x, $files, $file)
    {
        $json = file_get_contents(base_path($this->dir.$file));
        $accounts = json_decode($json);
        $result = [];

        $i = 0;
        foreach ($accounts as $account) {
            $i++;

            $subscription = $this->updateOrCreateSubscription($account);
            $result[] = $this->updateOrCreateAccount($account, $subscription);

            $this->getIngestionStatus($account, $i, $accounts, $file, $x, $files);
        }

        if(!empty($result)) {
            $this->importLog(new ImportedJson, $file, 'accounts', 'success');
        }
        else {
            $this->importLog(new ImportedJson, $file, 'accounts', 'no data');
        }
    }
}
