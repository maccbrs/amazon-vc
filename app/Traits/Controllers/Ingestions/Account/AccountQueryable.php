<?php

namespace App\Traits\Controllers\Ingestions\Account;

use App\Models\{
    ImportedJson,
    Subscription,
    Account
};

trait AccountQueryable
{

    public $dir = 'storage/accounts/';

    /**
     * Gets the imported logs.
     * @return     object  The imported logs.
     */
    public function getImportedLogs()
    {
        return ImportedJson::where('report_type', 'accounts')->get();
    }

    /**
     * Updates or Creates Subscription
     * @param      object  $account  The account
     * @return     object  The Subscription
     */
    public function updateOrCreateSubscription($account)
    {
        return Subscription::updateOrCreate([
                'subscription_name' => $account->retailSubscriptionLevel
            ],
            [
                'subscription_name' => $account->retailSubscriptionLevel
            ]);
    }

    /**
     * Updates or Creates Account
     * @param      object  $account       The account
     * @param      object  $subscription  The subscription
     * @return     object  The Account
     */
    public function updateOrCreateAccount($account, $subscription)
    {
        return Account::updateOrCreate([
                'vendor_group_id' => $account->vendorGroupId
            ],
            [
                'subscription_id' => $subscription->id,
                'vendor_group_id' => $account->vendorGroupId,
                'vendor_code' => $account->vendorCode,
                'account_name' => $account->name,
                'marketplace' => $account->marketplace
            ]);
    }

    /**
     * Gets the ingestion status.
     * @param      object  $account   The account
     * @param      string  $i         { parameter_description }
     * @param      object  $accounts  The accounts
     * @param      string  $file      The file
     * @param      string  $x         { parameter_description }
     * @param      array  $files     The files
     * @return     string  The ingestion status.
     */
    public function getIngestionStatus($account, $i, $accounts, $file, $x, $files)
    {
        return printf(
            "VGID: "."'".$account->vendorGroupId."'"." have been checked. Count: ".$i."/".count($accounts).".\n".
            $file.".\n".$x."/".count($files)." remaining.\n"
        );
    }

    /**
     * Gets the account jsons.
     * @return     array  The account jsons.
     */
    public function getAccountJsons()
    {
        return preg_grep('~^accounts_.*\.json$~',
                  scandir(base_path($this->dir)));
    }
}
