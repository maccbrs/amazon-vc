<?php

namespace App\Traits\Controllers\Ingestions\Alternative;

use App\Models\{
    ImportedJson,
    AlternativePurchase,
    Asin,
    Account
};

trait AlternativeQueryable
{

    public $dir = 'storage/alternate/';

    /**
     * Gets the imported logs.
     * @return     object  The imported logs.
     */
    public function getImportedLogs()
    {
        return ImportedJson::where('report_type', 'alt')
        ->get();
    }

    /**
     * Gets the alternative collection.
     * @param      string  $acc    The acc
     * @param      string  $date   The date
     * @return     object  The alternative collection.
     */
    public function getAlternativeCollection($acc, $date)
    {
        return AlternativePurchase::select('alternative_purchases.account_id', 'asins.asin')
                ->leftJoin('asins', 'alternative_purchases.asin_id', '=', 'asins.id')
                ->where('alternative_purchases.account_id', $acc->id)
                ->where('alternative_purchases.generated_date', date("Y-m-d", strtotime($date)))
                ->get()
                ->toArray();
    }

    /**
     * Gets the asin detail.
     * @param      string  $filtered_value  The filtered value
     * @param      string  $acc             The acc
     * @return     object  The asin detail.
     */
    public function getAsinDetail($filtered_value, $acc)
    {
        return Asin::where('asin', $filtered_value[0])
                    ->where('account_id', $acc->id)
                    ->first();
    }

    /**
     * Gets the first to fifth asin.
     * @param      string  $filtered_value  The filtered value
     * @param      string  $key             The key
     * @return     object  The first to fifth asin.
     */
    public function getFirstToFifthAsin($filtered_value, $key)
    {
        return Asin::where('asin', $filtered_value[$key])->first();
    }

    /**
     * Gets the ingestion status.
     * @param      object  $asin             The asin
     * @param      string  $i                { parameter_description }
     * @param      object  $filtered_values  The filtered values
     * @param      string  $file             The file
     * @param      string  $x                { parameter_description }
     * @param      array   $files            The files
     * @return     string  The ingestion status.
     */
    public function getIngestionStatus($asin, $i, $filtered_values, $file, $x, $files)
    {
        return printf (
            "Asin Id: ".$asin->id." have been checked. Count: ".$i."/".count($filtered_values).".\n".
            $file.".\n".$x."/".count($files)." remaining.\n"
        );
    }

    /**
     * Gets the alternative jsons.
     * @return     array  The alternative jsons.
     */
    public function getAlternativeJsons()
    {
        return preg_grep('~^AlternativePurchaseDetail_.*\.json$~',
                  scandir(base_path($this->dir)));
    }

    /**
     * Gets the account detail.
     * @param      string  $account_id  The account identifier
     * @return     object  The account detail.
     */
    public function getAccountDetail($account_id)
    {
        return Account::where('vendor_group_id', $account_id)->first();
    }
}
