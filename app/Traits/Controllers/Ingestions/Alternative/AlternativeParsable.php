<?php

namespace App\Traits\Controllers\Ingestions\Alternative;

use App\Traits\Controllers\Ingestions\Alternative\ {
    AlternativeQueryable
};

use App\Traits\Controllers\ {
    Parsable
};


trait AlternativeParsable
{
    use
        AlternativeQueryable, Parsable;

    public $dir = 'storage/alternate/';

    /**
     * Gets the unique jsons.
     * @param      array  $import_logs  The import logs
     * @return     array  $files  The unique jsons.
     */
    public function getUniqueJsons($import_logs, $list)
    {
        foreach ($import_logs as $import_log) {
            $import_logged[] = $import_log->filename;
        }

        if (!empty($import_logged)) {
            $files = array_diff($list, $import_logged);            
        }
        else {
            $files = $list;
        }

        return $files;
    }

    /**
     * Gets the report data value.
     * @param      array  $reportRows  The report rows
     * @return     array  The report data value.
     */
    public function getReportDataValue($reportRows)
    {
        foreach ($reportRows as $reportRow) {
            foreach ($reportRow as $reportValue) {
                foreach ($reportValue as $reportData) {
                    $result[] = $reportData->val;
                }
            }
        }

        return $result;
    }

    /**
     * Gets the unique values.
     * @param      array  $values  The values
     * @param      array  $alt     The alternate
     * @return     array  The unique values.
     */
    public function getUniqueValues($values, $alt)
    {
        foreach ($values as $value) {
            $json_value = [
                "asin" => $value[0],
            ];

            $json_check = $this->searchMultiArray('alt', $json_value, $alt);

            if ($json_check == false) {
                $filtered_values[] = $value;
            }
        }

        if (!empty($filtered_values)) {
            return $filtered_values;
        }
        else {
            return [];
        }
    }

    /**
     * Gets the mappable data.
     * @param      array  $filtered_values  The filtered values
     * @param      string  $acc              The acc
     * @param      string  $date             The date
     * @param      array  $column_names     The column names
     * @param      string  $file             The file
     * @param      string  $x                The file count
     * @param      array  $files            The files
     * @return     array  $result           The mappable data.
     */
    public function getMappableData($filtered_values, $acc, $date, $column_names, $file, $x, $files)
    {
        $i = 0;
        foreach ($filtered_values as $filtered_value) {
            $i++;
            $asin = $this->getAsinDetail($filtered_value, $acc);
            
            if ($asin) {

                $first_asin = $this->getFirstToFifthAsin($filtered_value, 2);
                $second_asin = $this->getFirstToFifthAsin($filtered_value, 5);
                $third_asin = $this->getFirstToFifthAsin($filtered_value, 8);
                $fourth_asin = $this->getFirstToFifthAsin($filtered_value, 11);
                $fifth_asin = $this->getFirstToFifthAsin($filtered_value, 14);

                $filters = [
                'account_id' => $acc->id,
                'asin_id' => $asin->id,
                'first_asin' => !empty($first_asin->id)?$first_asin->id: null,
                'second_asin' => !empty($second_asin->id)?$second_asin->id: null,
                'third_asin' => !empty($third_asin->id)?$third_asin->id: null,
                'fourth_asin' => !empty($fourth_asin->id)?$fourth_asin->id: null,
                'fifth_asin' => !empty($fifth_asin->id)?$fifth_asin->id: null,
                'generated_date' => date("Y-m-d", strtotime($date))
                ]; 

                $result[] = array_merge(array_combine($column_names, $filtered_value),$filters);
                $this->getIngestionStatus($asin, $i, $filtered_values, $file, $x, $files);
            }
        }

        if (!empty($result)) {
            return $result;
        }
        else {
            return [];
        }
    }
}
