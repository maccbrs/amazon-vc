<?php

namespace App\Traits\Controllers\Ingestions\Alternative;

use App\Models\{
    Account,
    ImportedJson,
    Asin,
    AlternativePurchase
};

use App\Traits\Controllers\ {
    Loggable,
    Parsable
};

use App\Traits\Controllers\Ingestions\Alternative\ {
    AlternativeQueryable,
    AlternativeParsable
};

trait AlternativeIngestable
{
    use
        Parsable, Loggable, AlternativeQueryable, AlternativeParsable;

    public $dir = 'storage/alternate/';

    /**
     * Compare Existing Json files to Imported Json Files.
     * @return     Array  The list of to be imported Json Files.
     */
    public function checkImportedLogs()
    {
        $list = $this->getAlternativeJsons();
        printf ("Looking for new Alternative JSON. . . \n");
        $import_logs = $this->getImportedLogs();
        $files = $this->getUniqueJsons($import_logs, $list);

        return $files;
    }

    /**
     * Parse Json Files from checkImportedLogs() and insert foreign keys on db.
     * @param      JSON  $file   The file
     */
    public function parseJson($x, $files, $file)
    {
        $data = file_get_contents(base_path($this->dir.$file));
        $json = json_decode($data);
        $result = [];

        if(!empty($json->payload->reportParts[0]->rowCount)) {
            $result = $this->getProcess($x, $files, $file, $json);
        }

        if ($result) {
            $this->storeAlternativeJSON($result);
            $this->importLog(new ImportedJson, $file, 'alt', 'success');
        }
        else {
            $this->importLog(new ImportedJson, $file, 'alt', 'no data');
            unlink(base_path($this->dir.$file));
        }
    }

    public function getProcess($x, $files, $file, $json)
    {
        $reports = $json->payload->reportParts;
        $filename = basename($file, '.json');
        $string = explode('_', $filename);
        $account_id = $string[1]; 
        $date = $string[2];
        $reportRows = $json->payload->reportParts[0]->reportRows;
        $column_names = $json->payload->reportParts[0]->columnNames;
        $val = [];

        $val = $this->getReportDataValue($reportRows);
        $acc = $this->getAccountDetail($account_id);

        $count = count($column_names);
        $values = array_chunk($val, $count);
        printf ("Getting List of Data based on account id and generated date.\n");

        $alt = $this->getAlternativeCollection($acc, $date);

        $filtered_values = [];
        printf ("Checking Duplicated Data. . .\n");
        
        $filtered_values = $this->getUniqueValues($values, $alt);
        $result = $this->getMappableData($filtered_values, $acc, $date, $column_names, $file, $x, $files);

        return $result;
    }
}
