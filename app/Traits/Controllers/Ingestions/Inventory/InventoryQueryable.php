<?php

namespace App\Traits\Controllers\Ingestions\Inventory;

use App\Models\{
    Account,
    ImportedJson,
    Asin,
    InventoryHealth,
    ReplenishmentCategory
};

trait InventoryQueryable
{

    public $dir = 'storage/inventory/';

    /**
     * Gets the imported logs.
     *
     * @return     object  The imported logs.
     */
    public function getImportedLogs()
    {
        return ImportedJson::where('report_type', 'inv')->get();
    }

    /**
     * Gets the inventory jsons.
     *
     * @return     array  The inventory jsons.
     */
    public function getInventoryJsons()
    {
        return preg_grep('~^InventoryHealthDetail_.*\.json$~',
            scandir(base_path($this->dir)));
    }

    /**
     * Gets the asin detail.
     *
     * @param      object  $acc             The acc
     * @param      array  $filtered_value  The filtered value
     * @param      array  $value  The value
     *
     * @return     object  The asin detail.
     */
    public function getAsinDetail($acc, $filtered_value)
    {
        return Asin::where('asin', $filtered_value[4])
            ->where('account_id', $acc->id)
            ->first();
    }

    /**
     * Gets the account detail.
     *
     * @param      string  $account_id  The account identifier
     *
     * @return     object  The account detail.
     */
    public function getAccountDetail($account_id)
    {
        return Account::where('vendor_group_id', $account_id)->first();
    }

    /**
     * Gets the inventory collection.
     *
     * @param      object  $acc    The acc
     * @param      string  $date   The date
     *
     * @return     array   The inventory collection.
     */
    public function getInventoryCollection($acc, $date)
    {
        return InventoryHealth::select('asins.asin')
            ->leftJoin('asins', 'inventory_healths.asin_id', '=', 'asins.id')
            ->where('inventory_healths.account_id', $acc->id)
            ->where('inventory_healths.generated_date', date("Y-m-d", strtotime($date)))
            ->get()
            ->toArray();
    }

    /**
     * First or Creates Replenishment
     *
     * @param      array   $filtered_value  The filtered value
     *
     * @return     object  The Replenishment
     */
    public function firstOrCreateReplenishment($filtered_value)
    {
        return ReplenishmentCategory::firstOrCreate([
                'replenishment_category' => $filtered_value[36]
            ],
            [
                'replenishment_category' => $filtered_value[36]
        ]);
    }

    /**
     * Gets the ingestion status.
     *
     * @param      object  $asin             The asin
     * @param      string  $i                The iteration of values
     * @param      array   $filtered_values  The filtered values
     * @param      string  $file             The file
     * @param      string  $x                The iteration of files
     * @param      array   $files            The files
     *
     * @return     string  The ingestion status.
     */
    public function getIngestionStatus($asin, $i, $filtered_values, $file, $x, $files)
    {
        return printf(
            "Asin Id: ".$asin->id." have been checked. Count: ".$i."/".count($filtered_values).".\n".$file.".\n".$x."/".count($files)." remaining.\n"
        );
    }

}
