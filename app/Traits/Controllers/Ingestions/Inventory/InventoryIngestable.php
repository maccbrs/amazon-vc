<?php

namespace App\Traits\Controllers\Ingestions\Inventory;

use App\Traits\Controllers\ {
    Loggable
};

use App\Models\{
    ImportedJson
};


use App\Traits\Controllers\Ingestions\Inventory\ {
    InventoryParsable
};

trait InventoryIngestable
{
    use
        Loggable, InventoryParsable;

    public $dir = 'storage/inventory/';

    /**
     * Compare Existing Json files to Imported Json Files.
     *
     * @return     Array  The list of to be imported Json Files.
     */
    public function checkImportedLogs()
    {
        $list = $this->getInventoryJsons();

        printf ("Looking for new Inventory JSON. . . \n");

        $import_logs = $this->getImportedLogs();
        $files = $this->getUniqueJsons($import_logs, $list);

        return $files;
    }

    /**
     * Parse Json Files from checkImportedLogs() and insert foreign keys on db.
     *
     * @param      integer  $x      The iteration of files
     * @param      array    $files  The files
     * @param      string   $file   The file
     */
    public function parseJson($x, $files, $file)
    {
        $data = file_get_contents(base_path($this->dir.$file));
        $json = json_decode($data);
        
        $result = [];
        $val = [];
        $filtered_values = [];
        $json_value = [];

        if (!empty($json->payload->reportParts[0]->rowCount) && $json->payload->reportId == 'inventoryHealthDetail') {
            $this->getProcess($x, $files, $file, $json);
        }
        
        if(!empty($result)) {
            $this->storeInventoryJSON($result);
            $this->importLog(new ImportedJson, $file, 'inv', 'success');
        }
        else {
            $this->importLog(new ImportedJson, $file, 'inv', 'no data');
        }

        unlink(base_path($this->dir.$file));
    }

    /**
     * Gets the process.
     *
     * @param      string  $x      The count
     * @param      array  $files  The files
     * @param      string  $file   The file
     * @param      object  $json   The json
     *
     * @return     array   The process.
     */
    public function getProcess($x, $files, $file, $json)
    {
        $filename = basename($file, '.json');

        $string = explode('_', $filename);
        $account_id = $string[1]; 
        $date = $string[2];

        $reportRows = $json->payload->reportParts[0]->reportRows;
        $column_names = $json->payload->reportParts[0]->columnNames;
        
        $val = [];
        $val = $this->getReportDataValue($reportRows);

        $acc = $this->getAccountDetail($account_id);

        $count = count($column_names);
        $values = array_chunk($val, $count);


        printf ("Getting List of Data based on account id and generated date.\n");

        $inv = $this->getInventoryCollection($acc, $date);

        printf ("Checking Duplicated Data. . .\n");
        $filtered_values = $this->getUniqueValues($values, $inv);
        $result = $this->getMappableData($filtered_values, $acc, $date, $column_names, $file, $x, $files);

        return $result; 
    }
}
