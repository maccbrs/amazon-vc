<?php

namespace App\Traits\Controllers\Ingestions\NetPpm;

use App\Traits\Controllers\ {
    Loggable
};

use App\Models\{
    ImportedJson
};


use App\Traits\Controllers\Ingestions\NetPpm\ {
    NetPpmParsable
};

trait NetPpmIngestable
{
    use
        Loggable, NetPpmParsable;

  /*  public $dir = 'storage/netppm/';*/

    /**
     * Compare Existing Json files to Imported Json Files.
     * @return     Array  The list of to be imported Json Files.
     */
    public function checkImportedLogs()
    {
        $list = $this->getNetPpmJsons();    
        // dd($list);
        printf ("Looking for new NetPpm JSON. . . \n");
        $import_logs = $this->getImportedLogs();
        // dd($import_logs);   
        $files = $this->getUniqueJsons($import_logs, $list);
        // dd($files);
        return $files;
    }

    /**
     * Parse Json Files from checkImportedLogs() and insert foreign keys on db.
     *
     * @param      integer  $x     The iteration of files
     * @param      array   $files  The files
     * @param      string    $file   The file
     */
    public function parseJson($x, $files, $file)
    {
        $data = file_get_contents(base_path($this->dir.$file));
        $json = json_decode($data);
        
        $result = [];
        $val = [];
        $filtered_values = [];
        $json_value = [];


        if (!empty($json->payload->reportParts[0]->rowCount) && $json->payload->reportId == 'netPPMDetail') {
            $result = $this->getProcess($x, $files, $file, $json);
        }
        
        if(!empty($result)) {
            $this->storeNetPpmJSON($result);
            $this->importLog(new ImportedJson, $file, 'net', 'success');
        }
        else {
            $this->importLog(new ImportedJson, $file, 'net', 'no data');
        }

        /*unlink(base_path($this->dir.$file));*/
    }

    /**
     * Gets the process.
     *
     * @param      object  $json   The json
     *
     * @return     array  The process.
     */
    public function getProcess($x, $files, $file, $json)
    {
        $filename = basename($file, '.json');

        $string = explode('_', $filename);
        $account_id = $string[1]; 
        $date = explode('-', $string[2]);

        $from = $date[0];
        $to = $date[1];

        $reportRows = $json->payload->reportParts[0]->reportRows;
        $column_names = $json->payload->reportParts[0]->columnNames;
        
        // dd($reportRows);

        $val = [];
        $val = $this->getReportDataValue($reportRows);

        $acc = $this->getAccountDetail($account_id);

        // dd($acc);

        $count = count($column_names);
        $values = array_chunk($val, $count);

        // dd($values);

        printf ("Getting List of Data based on account id and generated date.\n");

        $net = $this->getNetPpmCollection($acc, $from, $to);

        printf ("Checking Duplicated Data. . .\n");
        $filtered_values = $this->getUniqueValues($values, $net);
        $result = $this->getMappableData($filtered_values, $acc, $from, $to, $column_names, $file, $x, $files);

        return $result; 
    }
}
