<?php 

namespace App\Traits\Controllers\Ingestions\NetPpm;

use App\Traits\Controllers\Ingestions\NetPpm\ {
    NetPpmQueryable
};

use App\Traits\Controllers\ {
    Parsable
};

trait NetPpmParsable
{
    use
    NetPpmQueryable, Parsable;

/*    public $dir = 'storage/NetPpm/';*/

    /**
     * Gets the unique jsons.
     *
     * @param      array  $import_logs  The import logs
     * @param      array  $list         The list
     *
     * @return     array  The unique jsons.
     */
    public function getUniqueJsons($import_logs, $list)
    {
        foreach ($import_logs as $import_log) {
            $import_logged[] = $import_log->filename;
        }

        if (!empty($import_logged)) {
            $files = array_diff($list, $import_logged);            
        } else {
            $files = $list;
        }

        return $files;
    }

    /**
     * Gets the report data value.
     *
     * @param      object  $reportRows  The report rows
     *
     * @return     array  The report data value.
     */
    public function getReportDataValue($reportRows)
    {
        foreach ($reportRows as $reportRow) {
            foreach ($reportRow as $reportValue) {
                foreach ($reportValue as $reportData) {
                    $result[] = $reportData->val;
                }
            }
        }

        return $result;
    }

    /**
     * Gets the row filters.
     *
     * @param      object  $acc    The acc
     * @param      object  $asin   The asin
     * @param      string  $date   The date
     *
     * @return     array   The row filters.
     */
    public function getRowFilters($acc, $asin, $from, $to)
    {
        $filters = [
          'account_id' => $acc->id,
          'asin_id' => !empty($asin->id)?$asin->id: null,
          'generated_date_from' => date("Y-m-d", strtotime($from)),
          'generated_date_to' => date("Y-m-d", strtotime($to)),
        ];

	    return $filters;
    }

	/**
     * Gets the unique values.
     *
     * @param      array  $values  The values
     * @param      array  $inv     The inv
     *
     * @return     array  The unique values.
     */
	public function getUniqueValues($values, $inv)
	{
        foreach ($values as $value) {

            $json_value = [
                "asin" => $value[4],
            ];

            $json_check = $this->searchMultiArray('inv', $json_value, $inv);

            if ($json_check == false) {
                $filtered_values[] = $value;
            }
        }

        return $filtered_values;
	}

    /**
     * Gets the mappable data.
     *
     * @param      array   $filtered_values  The filtered values
     * @param      object   $acc              The acc
     * @param      array   $column_names     The column names
     * @param      array   $filters          The filters
     * @param      string   $file             The file
     * @param      integer  $i                The iteration for values
     * @param      integer  $x                The iteration for files
     * @param      array   $files            The files
     *
     * @return     array   The mappable data.
     */
    public function getMappableData($filtered_values, $acc, $from, $to, $column_names, $file, $x, $files)
    {
        $i = 0;
        foreach ($filtered_values as $filtered_value){
            // dd($filtered_value);
            $i++;
            $asin = $this->getAsinDetail($acc, $filtered_value);

            // dd($filtered_value);

            // dd($asin);

            if (!empty($asin)) {

                    $net = $this->getNetPpmCollection($acc, $from, $to);

                    $filters = $this->getRowFilters($acc, $asin, $from, $to);

                    $result[] = array_merge(array_combine($column_names, $filtered_value), $filters);
                    
                $this->getIngestionStatus($asin, $i, $filtered_values, $file, $x, $files);
            }
        }

        return $result;
    }
}