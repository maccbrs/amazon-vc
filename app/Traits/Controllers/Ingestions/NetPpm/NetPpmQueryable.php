<?php

namespace App\Traits\Controllers\Ingestions\NetPpm;

use App\Models\{
    Account,
    ImportedJson,
    Asin,
    NetPpm,
    ReplenishmentCategory
};

trait NetPpmQueryable
{

    public $dir = 'storage/netPPM/';

    /**
     * Gets the imported logs.
     *
     * @return     object  The imported logs.
     */
    public function getImportedLogs()
    {
        return ImportedJson::where('report_type', 'net')->get();
    }

    /**
     * Gets the NetPpm jsons.
     *
     * @return     array  The NetPpm jsons.
     */
    public function getNetPpmJsons()
    {
        return preg_grep('~^NetPPM.*\.json$~',
            scandir(base_path($this->dir)));
    }

    /**
     * Gets the asin detail.
     * @param      object  $acc    The acc
     * @param      array   $value  The value
     * @return     object  The asin detail.
     */
    public function getAsinDetail($acc, $filtered_value)
    {
        return Asin::where('asin', $filtered_value[0])
            ->where('account_id', $acc->id)
            ->first();
    }

    /**
     * Gets the account detail.
     * @param      string  $account_id  The account identifier
     * @return     object  The account detail.
     */
    public function getAccountDetail($account_id)
    {
        return Account::where('vendor_group_id', $account_id)->first();
    }

    /**
     * Gets the NetPpm collection.
     *
     * @param      object  $acc    The acc
     * @param      string  $date   The date
     *
     * @return     array  The NetPpm collection.
     */
    public function getNetPpmCollection($acc, $from, $to)
    {
        return NetPpm::select('asins.asin')
            ->leftJoin('asins', 'net_ppms.asin_id', '=', 'asins.id')
            ->where('net_ppms.account_id', $acc->id)
            ->where('net_ppms.generated_date_from', date("Y-m-d", strtotime($from)))
            ->where('net_ppms.generated_date_to', date("Y-m-d", strtotime($to)))
            ->get()
            ->toArray();
    }

    /**
     * First or Creates Replenishment
     *
     * @param      array  $filtered_value  The filtered value
     *
     * @return     object  The Replenishment
     */
    public function firstOrCreateReplenishment($filtered_value)
    {
        return ReplenishmentCategory::firstOrCreate([
                'replenishment_category' => $filtered_value[36]
            ],
            [
                'replenishment_category' => $filtered_value[36]
        ]);
    }

    /**
     * Gets the ingestion status.
     * @param      object  $asin             The asin
     * @param      string  $i                The iteration of values
     * @param      array  $filtered_values  The filtered values
     * @param      string  $file             The file
     * @param      string  $x                The iteration of files
     * @param      array  $files            The files
     * @return     string  The ingestion status.
     */
    public function getIngestionStatus($asin, $i, $filtered_values, $file, $x, $files)
    {
        return printf(
            "Asin Id: ".$asin->id." have been checked. Count: ".$i."/".count($filtered_values).".\n".$file.".\n".$x."/".count($files)." remaining.\n"
        );
    }

}
