<?php 

namespace App\Traits\Controllers\Ingestions\Asin;

use App\Traits\Controllers\Ingestions\Asin\ {
    AsinQueryable
};

trait AsinParsable
{
    use
    AsinQueryable;

    public $dir = 'storage/products/';

    /**
     * Gets the unique jsons.
     *
     * @param      array  $import_logs  The import logs
     * @param      array  $list         The list
     *
     * @return     array  The unique jsons.
     */
    public function getUniqueJsons($import_logs, $list)
    {
        foreach ($import_logs as $import_log) {
            $import_logged[] = $import_log->filename;
        }

        if (!empty($import_logged)) {
            $files = array_diff($list, $import_logged);            
        }
        else {
            $files = $list;
        }

        return $files;
    }

    /**
     * Gets the report data value.
     *
     * @param      array  $reportRows  The report rows
     *
     * @return     array  The report data value.
     */
    public function getReportDataValue($reportRows)
    {
        foreach ($reportRows as $reportRow) {
            foreach ($reportRow as $reportValue) {
                foreach ($reportValue as $reportData) {
                    $result[] = $reportData->val;
                }
            }
        }

        return $result;
    }

    /**
     * Gets the row filters.
     *
     * @param      string  $acc                     The acc
     * @param      string  $binding                 The binding
     * @param      string  $replenishcode           The replenishcode
     * @param      string  $productgroup            The productgroup
     * @param      string  $brand                   The brand
     * @param      string  $manufacturercode        The manufacturercode
     * @param      string  $parentmanufacturercode  The parentmanufacturercode
     *
     * @return     array   The row filters.
     */
    public function getRowFilters($acc, $binding, $replenishcode, $productgroup, $brand, $manufacturercode, $parentmanufacturercode)
    {
	    $filters = [
	        'marketplace' => $acc->marketplace,
	        'account_id' => $acc->id,
	        'binding_id' => $binding->id,
	        'replenish_id' => $replenishcode->id,
	        'product_group_id' => $productgroup->id,
	        'brand_id' => !empty($brand->id)?$brand->id: null,
	        'manufacturer_code_id' => !empty($manufacturercode->id)?$manufacturercode->id: null,
	        'parent_manufacturer_code_id' => !empty($parentmanufacturercode->id)?$parentmanufacturercode->id: null
	    ];

	    return $filters;
    }

    /**
     * Gets the mappable data.
     *
     * @param      array   $values        The values
     * @param      string  $acc           The acc
     * @param      array   $column_names  The column names
     * @param      string  $file          The file
     * @param      string  $x             { parameter_description }
     * @param      array   $files         The files
     *
     * @return     array   The mappable data.
     */
    public function getMappableData($values, $acc, $column_names, $file, $x, $files)
    {
        $i = 0;
        foreach ($values as $value) {
            $i++;
            $asin = $this->getAsinDetail($acc, $value);

            if (empty($asin)) {
                $binding = $this->updateOrCreateBinding($value);
                $productgroup = $this->updateOrCreateProductGroup($value);

                if(!empty($value[18])) {
                    $brand = $this->updateOrCreateBrand($value);
                }
                else {
                	$brand = null;
                }

                if(!empty($value[23])) {
                    $parentmanufacturercode = $this->updateOrCreateParentManufacturerCode($value);
                }
                else {
                	$parentmanufacturercode = null;
                }

                if(!empty($value[22])) {
                    $manufacturercode = $this->updateOrCreateManufacturerCode($value, $parentmanufacturercode);
                }
                else {
                	$manufacturercode = null;
                }

                $replenishcode = $this->updateOrCreateReplenishCode($value);
                $filters = $this->getRowFilters($acc, $binding, $replenishcode, $productgroup, $brand, $manufacturercode, $parentmanufacturercode);

                $result[] = array_merge(array_combine($column_names, $value), $filters);                           
            }
            $this->getIngestionStatus($value, $i, $values, $file, $x, $files);
        }

        if (!empty($result)) {
            return $result;
        }
        else {
            return [];
        }
    }

    public function ignoreAsin($values, $acc, $column_names, $files, $x, $file)
    {
        $i = 0;
        foreach ($values as $value) {
            $i++;
            $asin = $this->getAsinDetail($acc, $value);

            if (empty($asin)) {
                $binding = $this->updateOrCreateBinding($value);
                $productgroup = $this->updateOrCreateProductGroup($value);

                if(!empty($value[18])) {
                    $brand = $this->updateOrCreateBrand($value);
                }
                else {
                    $brand = null;
                }

                if(!empty($value[23])) {
                    $parentmanufacturercode = $this->updateOrCreateParentManufacturerCode($value);
                }
                else {
                    $parentmanufacturercode = null;
                }

                if(!empty($value[22])) {
                    $manufacturercode = $this->updateOrCreateManufacturerCode($value, $parentmanufacturercode);
                }
                else {
                    $manufacturercode = null;
                }

                $replenishcode = $this->updateOrCreateReplenishCode($value);
                $filters = $this->getRowFilters($acc, $binding, $replenishcode, $productgroup, $brand, $manufacturercode, $parentmanufacturercode);

                $result[] = array_merge(array_combine($column_names, $value), $filters);                           
            }
            $this->getIngestionStatus($value, $i, $values, $file, $x, $files);
        }

        if (!empty($result)) {
            return $result;
        }
        else {
            return [];
        }
    }
}