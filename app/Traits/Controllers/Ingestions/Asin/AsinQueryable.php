<?php

namespace App\Traits\Controllers\Ingestions\Asin;

use App\Models\{
    Account,
    ImportedJson,
    Asin,
    Binding,
    ProductGroup,
    Brand,
    ParentManufacturerCode,
    ManufacturerCode,
    ReplenishCode
};

trait AsinQueryable
{

    public $dir = 'storage/products/';

    /**
     * Gets the imported logs.
     *
     * @return     object  The imported logs.
     */
    public function getImportedLogs()
    {
        return ImportedJson::select('filename')
            ->where('report_type', 'products')
            ->get();
    }

    /**
     * Gets the asin detail.
     *
     * @param      object  $acc    The acc
     * @param      array   $value  The value
     *
     * @return     object  The asin detail.
     */
    public function getAsinDetail($acc, $value)
    {
        return Asin::where('asin', $value[0])
            ->where('account_id', $acc->id)
            ->first();
    }

    /**
     * Gets the account detail.
     *
     * @param      string  $account_id  The account identifier
     *
     * @return     object  The account detail.
     */
    public function getAccountDetail($account_id)
    {
        return Account::where('vendor_group_id', $account_id)
            ->first();
    }

    /**
     * Updates or Creates Binding
     *
     * @param      array   $value  The value
     *
     * @return     object  The Binding
     */
    public function updateOrCreateBinding($value)
    {
        return Binding::updateOrCreate([
                'bindings' => $value[8]
            ],
            [
                'bindings' => $value[8]
            ]);
    }

    /**
     * Updates or Creates Product Group
     *
     * @param      array   $value  The value
     *
     * @return     object  The Product Group
     */
    public function updateOrCreateProductGroup($value)
    {
        return ProductGroup::updateOrCreate([
                'product_group' => $value[13]
            ],
            [
                'product_group' => $value[13]
            ]);
    }

    /**
     * Updates or Creates Brand
     *
     * @param      array   $value  The value
     *
     * @return     object  The Brand
     */
    public function updateOrCreateBrand($value)
    {
        return Brand::updateOrCreate([
                'brand_code' => $value[18],
            ],
            [
                'brand' => $value[19],
                'brand_code' => $value[18]
            ]);
    }

    /**
     * Updates or Creates Parent Manufacturer Code
     *
     * @param      array   $value  The value
     *
     * @return     object  The Parent Manufacturer Code
     */
    public function updateOrCreateParentManufacturerCode($value)
    {
        return ParentManufacturerCode::updateOrCreate([
                'parent_manufacturer_code' => $value[23]
            ],
            [
                'parent_manufacturer_code' => $value[23]
            ]);
    }

    /**
     * Updates or Creates Manufacturer Code
     *
     * @param      array   $value                   The value
     * @param      object  $parentmanufacturercode  The parentmanufacturercode
     *
     * @return     object  The Manufacturer Code
     */
    public function updateOrCreateManufacturerCode($value, $parentmanufacturercode)
    {
        return ManufacturerCode::updateOrCreate([
                'manufacturer_code' => $value[22]
            ],
            [
                'manufacturer_code' => $value[22],
                'parent_manufacturer_code_id' => !empty($parentmanufacturercode->id)?$parentmanufacturercode->id: null
            ]);
    }

    /**
     * Updates or Creates Replenish Code
     *
     * @param      array   $value  The value
     *
     * @return     object  The Replenish Code
     */
    public function updateOrCreateReplenishCode($value)
    {
        return ReplenishCode::updateOrCreate([
                'replenish_code' => $value[14]
            ],
            [
                'replenish_code' => $value[14]
            ]);
    }

    /**
     * Gets the ingestion status.
     *
     * @param      array   $value   The value
     * @param      string  $i       { parameter_description }
     * @param      array   $values  The values
     * @param      string  $file    The file
     * @param      string  $x       { parameter_description }
     * @param      array   $files   The files
     *
     * @return     string  The ingestion status.
     */
    public function getIngestionStatus($value, $i, $values, $file, $x, $files)
    {
        return printf(
            "Asin: ".$value[0]." have been checked. Count: ".$i."/".count($values).".\n".$file.".\n".$x."/".count($files)." remaining.\n"
        );
    }

    /**
     * Gets the asin jsons.
     *
     * @return     array  The asin jsons.
     */
    public function getAsinJsons()
    {
        return preg_grep('~^ProductCatalog_(sourcing|manufacturing|consignment)_.*\.json$~',
                  scandir(base_path($this->dir)));
    }
}
