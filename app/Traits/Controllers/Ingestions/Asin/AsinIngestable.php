<?php

namespace App\Traits\Controllers\Ingestions\Asin;

use App\Models\{
    Account,
    ImportedJson,
    Asin,
    Binding,
    ProductGroup,
    Brand,
    ParentManufacturerCode,
    ManufacturerCode,
    ReplenishCode
};

use App\Traits\Controllers\ {
    Loggable
};

use App\Traits\Controllers\Ingestions\Asin\ {
    AsinParsable
};

trait AsinIngestable
{
    use
        Loggable, AsinParsable;

    public $dir = 'storage/products/';

    /**
     * Compare Existing Json files to Imported Json Files.
     *
     * @return     Array  The list of to be imported Json Files.
     */
    public function checkImportedLogs()
    {
        $list = $this->getAsinJsons();
        printf ("Looking for new Products JSON. . . \n");
        $import_logs = $this->getImportedLogs();
        $files = $this->getUniqueJsons($import_logs, $list);
        return $files;
    }

    /**
     * Parse Json Files from checkImportedLogs() and insert foreign keys on db.
     *
     * @param      string  $x      The 'x' iteration
     * @param      array   $files  The files
     * @param      JSON    $file   The file
     */
    public function parseJson($x, $files, $file)
    {
        $value = file_get_contents(base_path($this->dir.$file));
        $json = json_decode($value);
        $result = [];

        if(!empty($json->payload->reportParts[0]->rowCount)) { 
            $result = $this->getProcess($x, $files, $file, $json);
        }

        if(!empty($result)) {
            $this->storeProductJSON($result);
            $this->importLog(new ImportedJson, $file, 'products', 'success');
        }
        else {
            $this->importLog(new ImportedJson, $file, 'products', 'no data');
        }

        $print = printf("Processed ".$file."\n");
        unlink(base_path($this->dir.$file));
    }

    /**
     * Gets the process.
     *
     * @param      string  $x      { parameter_description }
     * @param      array  $files  The files
     * @param      string  $file   The file
     * @param      object  $json   The json
     *
     * @return     string  The process.
     */
    public function getProcess($x, $files, $file, $json)
    {
        $filename = basename($file, '.json');
        $string = explode('_', $filename);
        $account_id = $string[2];
        $view = $string[1];
        $generated_date = $string[3]; 
        $reportRows = $json->payload->reportParts[0]->reportRows;
        $column_names = $json->payload->reportParts[0]->columnNames;
        
        $val = [];
        $val = $this->getReportDataValue($reportRows);
        $acc = $this->getAccountDetail($account_id);

        $count = count($column_names);                    
        $values = array_chunk($val, $count);

        $result = $this->getMappableData($values, $acc, $column_names, $file, $x, $files);

        return $result;
    }
}
