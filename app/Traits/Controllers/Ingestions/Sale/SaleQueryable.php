<?php

namespace App\Traits\Controllers\Ingestions\Sale;

use App\Models\{
    Account,
    ImportedJson,
    Asin,
    SalesDiagnostics,
    Program,
    Binding,
    Brand,
    Distributor,
    Category,
    Subcategory
};

use App\Traits\Controllers\Ingestions\Sale\ {
    SaleQueryable,
    SaleParsable
};

trait SaleQueryable
{

    public $dir = 'storage/salesdiagnostics/';

    /**
     * Gets the imported logs.
     *
     * @return     object  The imported logs.
     */
    public function getImportedLogs()
    {
        return ImportedJson::where('report_type', 'sales')
            ->where('status', '!=', 'in progess')
            ->where('status', '!=', 'no data')
            ->get();
    }

    /**
     * Gets the preorders jsons.
     *
     * @return     array  The preorders jsons.
     */
    public function getPreorderJsons()
    {
        return preg_grep('~^SalesDiagnosticsDetail_retail.*.json$~',
            scandir(base_path($this->dir)));
    }

    /**
     * Gets the asin detail.
     *
     * @param      object  $acc           The acc
     * @param      <type>  $product_asin  The product asin
     *
     * @return     object  The asin detail.
     */
    public function getAsinDetail($acc, $product_asin)
    {
        return Asin::where('asin', $product_asin)
            ->where('account_id', $acc->id)
            ->first();
    }

    /**
     * Gets the account detail.
     * @param      string  $account_id  The account identifier
     * @return     object  The account detail.
     */
    public function getAccountDetail($account_id)
    {
        return Account::where('vendor_group_id', $account_id)->first();
    }

    /**
     * Gets the sale collection.
     *
     * @param      <type>  $acc    The acc
     * @param      <type>  $date   The date
     *
     * @return     <type>  The sale collection.
     */
    public function getSaleCollection($acc, $date)
    {
        return SalesDiagnostics::select('asins.asin')
            ->join('asins', 'sales_diagnostics.asin_id', '=', 'asins.id')
            ->where('sales_diagnostics.account_id', $acc->id)
            ->where('sales_diagnostics.generated_date', date("Y-m-d", strtotime($date)))
            ->get()
            ->toArray();
    }

    /**
     * { function_description }
     *
     * @param      <type>  $program_name  The program name
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function firstOrCreateProgram($program_name)
    {
        return Program::firstOrCreate([
                'program' => $program_name
            ],
            [
                'program' => $program_name
            ]);

    }

    /**
     * { function_description }
     *
     * @param      <type>  $filtered_value  The filtered value
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function updateOrCreateBinding($filtered_value)
    {
        return Binding::updateOrCreate([
                'bindings' => $filtered_value[8]
            ],
            [
                'bindings' => $filtered_value[8]
            ]);
    }

    /**
     * { function_description }
     *
     * @param      <type>  $filtered_value  The filtered value
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function updateOrCreateBrand($filtered_value)
    {
        return Brand::updateOrCreate([
                'brand_code' => $filtered_value[18],
            ],
            [
                'brand' => $filtered_value[19],
                'brand_code' => $filtered_value[18]
            ]);
    }

    /**
     * { function_description }
     *
     * @param      <type>  $filtered_value  The filtered value
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function firstOrCreateDistributor($filtered_value)
    {
        return Distributor::firstOrCreate([
                'distributor' => !empty($distributor)?$distributor: 'manufacturer'
            ],
            [
                'distributor' => !empty($distributor)?$distributor: 'manufacturer'
            ]);
    }

    /**
     * { function_description }
     *
     * @param      <type>  $filtered_value  The filtered value
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function firstOrCreateCategory($filtered_value)
    {
        return Category::firstOrCreate([
                'category' => $filtered_value[12]
            ],
            [
                'category' => $filtered_value[12]
            ]);
    }

    /**
     * { function_description }
     *
     * @param      <type>  $filtered_value  The filtered value
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function firstOrCreateSubcategory($filtered_value)
    {
        return Subcategory::firstOrCreate([
                'subcategory' => $filtered_value[13]
            ],
            [
                'subcategory' => $filtered_value[13]
            ]);
    }

    /**
     * Updates Asin from Sales
     *
     * @param      <type>  $asin         The asin
     * @param      <type>  $category     The category
     * @param      <type>  $subcategory  The subcategory
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function updateAsinFromSales($asin, $category, $subcategory)
    {
        return Asin::findOrFail($asin->id)->update([
            'category_id' => $category->id,
            'subcategory_id' => $subcategory->id]);
    }

    /**
     * Gets the ingestion status.
     *
     * @param      object  $asin             The asin
     * @param      string  $i                The iteration of values
     * @param      array   $filtered_values  The filtered values
     * @param      string  $file             The file
     * @param      string  $x                The iteration of files
     * @param      array   $files            The files
     *
     * @return     string  The ingestion status.
     */
    public function getIngestionStatus($asin, $i, $filtered_values, $file, $x, $files)
    {
        return printf(
            "Asin Id: ".$asin->id." have been checked. Count: ".$i."/".count($filtered_values).".\n".$file.".\n".$x."/".count($files)." remaining.\n"
        );
    }

}
