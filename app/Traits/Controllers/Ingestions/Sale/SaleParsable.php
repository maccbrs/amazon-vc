<?php 

namespace App\Traits\Controllers\Ingestions\Sale;

use App\Traits\Controllers\Ingestions\Sale\ {
    SaleQueryable
};

use App\Traits\Controllers\ {
    Parsable
};

trait SaleParsable
{
    use
    SaleQueryable, Parsable;

    public $dir = 'storage/salesdiagnostics/';

    /**
     * Gets the unique jsons.
     *
     * @param      array  $import_logs  The import logs
     * @param      array  $list         The list
     *
     * @return     array  The unique jsons.
     */
    public function getUniqueJsons($import_logs, $list)
    {
        foreach ($import_logs as $import_log) {
            $import_logged[] = $import_log->filename;
        }

        if (!empty($import_logged)) {
            $files = array_diff($list, $import_logged);            
        } else {
            $files = $list;
        }

        return $files;
    }

    /**
     * Gets the row filters.
     *
     * @param      object  $acc            The acc
     * @param      object  $asin           The asin
     * @param      string  $date           The date
     * @param      object  $replenishment  The replenishment
     *
     * @return     array   The row filters.
     */
    public function getRowFilters($acc, $file, $date, $asin, $program, $distributor_view)
    {
        $filters = [
            'marketplace' => $acc->marketplace,
            'file' => $file,
            'generated_date' => date("Y-m-d", strtotime($date)),
            'asin_id' => $asin->id,
            'program' => $program->id,
            'program_name' => $program->program,
            'account_id' => $acc->id,
            'distributor_id' => $distributor_view->id
        ]; 

	    return $filters;
    }

    /**
     * Gets the product filters.
     *
     * @param      object  $acc                     The acc
     * @param      object  $binding                 The binding
     * @param      object  $replenishcode           The replenishcode
     * @param      object  $productgroup            The productgroup
     * @param      object  $brand                   The brand
     * @param      object  $manufacturercode        The manufacturercode
     * @param      object  $parentmanufacturercode  The parentmanufacturercode
     *
     * @return     array   The product filters.
     */
    public function getProductFilters($acc, $binding, $replenishcode, $productgroup, $brand, $manufacturercode, $parentmanufacturercode)
    {
        $filters = [
            'marketplace' => $acc->marketplace,
            'account_id' => $acc->id,
            'binding_id' => $binding->id,
            'replenish_id' => !empty($replenishcode->id)?$replenishcode->id: null,
            'product_group_id' => !empty($productgroup->id)?$productgroup->id: null,
            'brand_id' => !empty($brand->id)?$brand->id: null,
            'manufacturer_code_id' => !empty($manufacturercode->id)?$manufacturercode->id: null,
            'parent_manufacturer_code_id' => !empty($parentmanufacturercode->id)?$parentmanufacturercode->id: null
        ];

        return $filters;
    }

	/**
     * Gets the unique values.
     *
     * @param      array  $values  The values
     * @param      array  $inv     The inv
     *
     * @return     array  The unique values.
     */
	public function getUniqueValues($acc, $values, $sales)
	{ 
        foreach ($values as $value) {
            if ($acc->marketplace == 'JP') {
                $product_asin = $value[0];
            }
            else {
                $product_asin = $value[4];
            }

            $json_value = [
                "asin" => $product_asin,
            ];

            $json_check = $this->searchMultiArray('sales', $json_value, $sales);

            if ($json_check == false) {
                $filtered_values[] = $value;
            }
        }

        return $filtered_values;
	}

    /**
     * Creates asin or update asin from sales.
     *
     * @param      object  $asin              The asin
     * @param      array  $filtered_value    The filtered value
     * @param      object  $acc               The acc
     * @param      object  $binding           The binding
     * @param      object  $brand             The brand
     * @param      array  $column_names      The column names
     * @param      string  $file              The file
     * @param      string  $date              The date
     * @param      object  $program           The program
     * @param      string  $distributor_view  The distributor view
     * @param      object  $category          The category
     * @param      object  $subcategory       The subcategory
     *
     * @return     array  The Result
     */
    public function createAsinOrUpdateAsinFromSales($asin, $filtered_value, $acc, $column_names, $file, $date, $program, $distributor_view)
    {
        if (empty($asin)) {
            $binding = $this->updateOrCreateBinding($filtered_value);

            if(!empty($filtered_value[18])) {
                $brand = $this->updateOrCreateBrand($filtered_value);
            }

            $product_filters = $this->getProductFilters($acc, $binding, $replenishcode = null, $productgroup = null, $brand, $manufacturercode = null, $parentmanufacturercode = null);
            $product[] = array_merge(array_combine($column_names, $filtered_value), $product_filters);
            $this->storeProductJSON($product);
            $distributor_view = $this->firstOrCreateDistributor($filtered_value);
            $category = $this->firstOrCreateCategory($filtered_value);
            $subcategory = $this->firstOrCreateSubcategory($filtered_value);                          
            $filters = $this->getRowFilters($acc, $file, $date, $asin, $program, $distributor_view);

            $result[] = array_merge(array_combine($column_names, $filtered_value),$filters);

        }
        else {
            $distributor_view = $this->firstOrCreateDistributor($filtered_value);
            $category = $this->firstOrCreateCategory($filtered_value);
            $subcategory = $this->firstOrCreateSubcategory($filtered_value);     
            $this->updateAsinFromSales($asin, $category, $subcategory);
            $filters = $this->getRowFilters($acc, $file, $date, $asin, $program, $distributor_view);

            $result[] = array_merge(array_combine($column_names, $filtered_value),$filters);
        }

        return $result;
    }

    /**
     * Gets the mappable data.
     *
     * @param      array   $filtered_values  The filtered values
     * @param      object   $acc              The acc
     * @param      array   $column_names     The column names
     * @param      array   $filters          The filters
     * @param      string   $file             The file
     * @param      integer  $i                The iteration for values
     * @param      integer  $x                The iteration for files
     * @param      array   $files            The files
     *
     * @return     array   The mappable data.
     */
    public function getMappableData($values, $acc, $date, $column_names, $file, $x, $files)
    {
        $i = 0;
        foreach ($values as $value) {
            $i++;
            $asin = $this->getAsinDetail($acc, $value);

            if (!empty($asin)) {
                $pre = $this->getInventoryCollection($acc, $asin, $date);

                if (empty($pre)) {
                    $filters = $this->getRowFilters($acc, $asin, $date);
                    $result[] = array_merge(array_combine($column_names, $value),$filters);
                }
                $this->getIngestionStatus($asin, $i, $values, $file, $x, $files);
            }
        }

        return $result;
    }
}