<?php

namespace App\Traits\Controllers\Ingestions\Sale;

use App\Models\{
    Account,
    ImportedJson,
    SalesDiagnostics,
    Distributor,
    Category,
    Subcategory,
    Asin,
    Program,
    Binding,
    Brand
};

use App\Traits\Controllers\ {
    Loggable
};

use App\Traits\Controllers\Ingestions\Sale\ {
    SaleParsable // SaleQueryable, Parsable -> Storable, Importable
};

trait SaleIngestable
{
    use
        Loggable, SaleParsable;

    public $dir = 'storage/salesdiagnostics/';

    /**
     * Compare Existing Json files to Imported Json Files.
     * @return     Array  The list of to be imported Json Files.
     */
    public function checkImportedLogs()
    {
        $list = $this->getPreorderJsons();

        printf ("Looking for new Sales JSON. . . \n");

        $import_logs = $this->getImportedLogs();

        $files = $this->getUniqueJsons($import_logs, $list);

        return $files;
    }

    /**
     * Parse Json Files from checkImportedLogs() and insert foreign keys on db.
     *
     * @param      <type>  $x      { parameter_description }
     * @param      <type>  $files  The files
     * @param      JSON    $file   The file
     */
    public function parseJson($x, $files, $file)
    {
        $check_progress = ImportedJson::where('filename', $file)->first();   
        $data = file_get_contents(base_path($this->dir.$file));
        $json = json_decode($data);
        $result = [];

        if(!empty($json->payload->reportParts[0]->rowCount) && !empty($json->payload->reportParts[0]->reportRows)) {
            $this->getProcess($json, $x, $files, $file);
        }
        if($result) {
            $this->storeSalesJSON($result, $file);
            $this->importLog(new ImportedJson, $file, 'sales', 'success');
        }
        else {
            $this->importLog(new ImportedJson, $file, 'sales', 'no data');
        }
        unlink(base_path($this->dir.$file));
    }

    public function getProcess($json, $x, $files, $file)
    {
        $filename = basename($file, '.json');
        $string = explode('_', $filename);
        $program_name = $string[1];
        $account_id = $string[2]; 
        $date = $string[3];

        if ($program_name == 'retail') {
            $view = explode(';', $json->payload->viewId);
            $distributor_view = explode('=', $view[0]);
            $distributor = $distributor_view[1];
        }

        $reportRows = $json->payload->reportParts[0]->reportRows;
        $acc = $this->getAccountDetail($account_id);
        $column_names = $json->payload->reportParts[0]->columnNames;

        $program = $this->firstOrCreateProgram($program_name);
        $val = [];
        $val = $this->parseReportRows($reportRows);

        $count = count($column_names);
        $values = array_chunk($val, $count);
        $result = [];

        printf ("Getting List of Data based on account id and generated date.\n");

        $sales = $this->getSaleCollection($acc, $date);

        $filtered_values = [];
        printf ("Checking Duplicated Data. . .\n");

        $filtered_values = $this->getUniqueValues($acc, $values, $sales);

        $i = 0;
        foreach ($filtered_values as $filtered_value) {
            $i++;
            if ($acc->marketplace == 'JP') {
                $product_asin = $filtered_value[0];
            }
            else {
                $product_asin = $filtered_value[4];
            }

            $test = 'test';
            
            $asin = $this->getAsinDetail($acc, $product_asin);

            $result = $this->createAsinOrUpdateAsinFromSales($asin, $filtered_value, $acc, $column_names, $file, $date, $program, $distributor_view);

            $this->getIngestionStatus($asin, $i, $filtered_values, $file, $x, $files);
        }
    }
}
