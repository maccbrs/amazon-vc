<?php 
namespace App\Traits\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Models\{
    Asin,
    Binding,
    ProductGroup,
    Category,
    Subcategory,
    Brand,
    ManufacturerCode,
    ParentManufacturerCode,
    Program,
    SalesDiagnostics,
    ReplenishCode,
    PreOrder,
    City,
    State,
    Zip,
    Country,
    GeographicSalesInsight,
    InventoryHealth,
    AlternativePurchase,
    FifthPurchasedProduct,
    FourthPurchasedProduct,
    ThirdPurchasedProduct,
    SecondPurchasedProduct,
    FirstPurchasedProduct
};

use Carbon\Carbon;
use DateTime;

use App\Traits\Controllers\{
    Parsable
};

trait Storable
{
    use
        Parsable;

    public function alternativeArray($array)
    {
        $asins = Asin::select('id', 'asin')->get()->toArray();
        
        foreach ($array as $key => $i):
            $dataArr[] = array(
                'asin_id' => $this->findId($i['ASIN'], $asins, 'asin'),
                'first_alternative_asin_id' => $this->findId($i['#1 Purchased ASIN'], $asins, 'asin'),
                'first_alternative_asin' => $i['#1 Purchased ASIN'],
                'first_alternative_title' => $this->stripNull($i['#1 Purchased Product Title']),
                'first_purchased_percentage' => $this->stripChar($i['#1 Purchased %']),
                'second_alternative_asin_id' => $this->findId($i['#2 Purchased ASIN'], $asins, 'asin'),
                'second_alternative_asin' => $i['#2 Purchased ASIN'],
                'second_alternative_title' => $this->stripNull($i['#2 Purchased Product Title']),
                'second_purchased_percentage' => $this->stripChar($i['#2 Purchased %']),
                'third_alternative_asin_id' => $this->findId($i['#3 Purchased ASIN'], $asins, 'asin'),
                'third_alternative_asin' => $i['#3 Purchased ASIN'],
                'third_alternative_title' => $this->stripNull($i['#3 Purchased Product Title']),
                'third_purchased_percentage' => $this->stripChar($i['#3 Purchased %']),
                'fourth_alternative_asin_id' => $this->findId($i['#4 Purchased ASIN'], $asins, 'asin'),
                'fourth_alternative_asin' => $i['#4 Purchased ASIN'],
                'fourth_alternative_title' => $this->stripNull($i['#4 Purchased Product Title']),
                'fourth_purchased_percentage' => $this->stripChar($i['#4 Purchased %']),
                'fifth_alternative_asin_id' => $this->findId($i['#5 Purchased ASIN'], $asins, 'asin'),
                'fifth_alternative_asin' => $i['#5 Purchased ASIN'],
                'fifth_alternative_title' => $this->stripNull($i['#5 Purchased Product Title']),
                'fifth_purchased_percentage' => $this->stripChar($i['#5 Purchased %']),
                'generated_date' => date('Y-m-d'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            );
        endforeach;

        return $dataArr;
    }

    public function alternativeArrayJSON($array)
    {        
        // dd($array);
        foreach ($array as $key => $i):
/*            dd($i);*/
            $dataArr[] = array(
                'asin_id' => $i['asin_id'],
                'account_id' => $i['account_id'],
                'first_alternative_asin_id' => $i['first_asin'],
                'first_alternative_asin' => $this->stripNull($i['no1purchasedasin']),
                'first_alternative_title' => $this->stripNull($i['no1purchasedproducttitle']),
                'first_purchased_percentage' => $this->stripChar($i['no1purchasedpercent']),
                'second_alternative_asin_id' => $i['second_asin'],
                'second_alternative_asin' => $this->stripNull($i['no2purchasedasin']),
                'second_alternative_title' => $this->stripNull($i['no2purchasedproducttitle']),
                'second_purchased_percentage' => $this->stripChar($i['no2purchasedpercent']),
                'third_alternative_asin_id' => $i['third_asin'],
                'third_alternative_asin' => $this->stripNull($i['no3purchasedasin']),
                'third_alternative_title' => $this->stripNull($i['no3purchasedproducttitle']),
                'third_purchased_percentage' => $this->stripChar($i['no3purchasedpercent']),
                'fourth_alternative_asin_id' => $i['fourth_asin'],
                'fourth_alternative_asin' => $this->stripNull($i['no4purchasedasin']),
                'fourth_alternative_title' => $this->stripNull($i['no4purchasedproducttitle']),
                'fourth_purchased_percentage' => $this->stripChar($i['no4purchasedpercent']),
                'fifth_alternative_asin_id' => $i['fifth_asin'],
                'fifth_alternative_asin' => $this->stripNull($i['no5purchasedasin']),
                'fifth_alternative_title' => $this->stripNull($i['no5purchasedproducttitle']),
                'fifth_purchased_percentage' => $this->stripChar($i['no5purchasedpercent']),
                'generated_date' => $i['generated_date'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            );
        endforeach;

        return $dataArr;
    }

    public function invArray($array)
    {
        $asins = Asin::select('id', 'asin')->get()->toArray();

        foreach ($array as $key => $i):
            $dataArr[] = array(
                'asin_id' => $this->findId($i['ASIN'], $asins, 'asin'),
                'net_received' => $this->stripChar($i['Net Received']),
                'net_received_units' => $this->stripChar($i['Net Received Units']),
                'sell_through_rate' =>$this->stripChar($i['Sell-Through Rate']),
                'open_purchase_order_quantity' => $this->stripChar($i['Open Purchase Order Quantity']),
                'sellable_on_hand_inventory' => $this->stripChar($i['Sellable On Hand Inventory']),
                'sellable_on_hand_inventory_trailing' => $this->stripChar($i['Sellable On Hand Inventory - Trailing 30-day Average']),
                'sellable_on_hand_units' => $this->stripChar($i['Sellable On Hand Units']),
                'unsellable_on_hand_inventory' => $this->stripChar($i['Unsellable On Hand Inventory']),
                'unsellable_on_hand_inventory_trailing' => $this->stripChar($i['Unsellable On Hand Inventory - Trailing 30-day Average']),
                'unsellable_on_hand_units' =>$this->stripChar($i['Unsellable On Hand Units']),
                'ninety_days_sellable_inventory' => $this->stripChar($i['Aged 90+ Days Sellable Inventory']),
                'ninety_days_sellable_inventory_trailing' => $this->stripChar($i['Aged 90+ Days Sellable Inventory - Trailing 30-day Average']),
                'ninety_days_sellable_units' => $this->stripChar($i['Aged 90+ Days Sellable Units']),
                'unhealthy_inventory' => $this->stripChar($i['Unhealthy Inventory']),
                'unhealthy_inventory_trailing' => $this->stripChar($i['Unhealthy Inventory - Trailing 30-day Average']),
                'unhealthy_units' => $this->stripChar($i['Unhealthy Units']),
                'replenishment_category' => $this->stripNull($i['Replenishment Category']),
                'generated_date' => date('Y-m-d'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            );
        endforeach;

        return $dataArr;
    }

    public function invArrayJSON($array)
    {
        foreach ($array as $key => $i):
            $dataArr[] = array(
                'asin_id' => $i['asin_id'],
                'account_id' => $i['account_id'],
                'net_received' => $this->stripChar($this->checkIndex('netreceived', $i)),
                'net_received_units' => $this->stripChar($this->checkIndex('netunitsreceived', $i)),
                'sell_through_rate' =>$this->stripChar($this->checkIndex('sellthroughrate', $i)),
                'open_purchase_order_quantity' => $this->stripChar($this->checkIndex('openpurchaseorderquantity', $i)),
                'sellable_on_hand_inventory' => $this->stripChar($this->checkIndex('sellableonhandinventory', $i)),
                'sellable_on_hand_inventory_trailing' => $this->stripChar($this->checkIndex('sellableonhandinventorytrailing30dayaverage', $i)),
                'sellable_on_hand_units' => $this->stripChar($this->checkIndex('sellableonhandunits', $i)),
                'unsellable_on_hand_inventory' => $this->stripChar($this->checkIndex('unsellableonhandinventory', $i)),
                'unsellable_on_hand_inventory_trailing' => $this->stripChar($this->checkIndex('unsellableonhandinventorytrailing30dayaverage', $i)),
                'unsellable_on_hand_units' =>$this->stripChar($this->checkIndex('unsellableonhandunits', $i)),
                'ninety_days_sellable_inventory' => $this->stripChar($this->checkIndex('aged90dayssellableinventory', $i)),
                'ninety_days_sellable_inventory_trailing' => $this->stripChar($this->checkIndex('aged90dayssellableinventorytrailing30dayaverage', $i)),
                'ninety_days_sellable_units' => $this->stripChar($this->checkIndex('aged90dayssellableunits', $i)),
                'unhealthy_inventory' => $this->stripChar($this->checkIndex('unhealthyinventory',$i)),
                'unhealthy_inventory_trailing' => $this->stripChar($this->checkIndex('unhealthyinventorytrailing30dayaverage',$i)),
                'unhealthy_units' => $this->stripChar($this->checkIndex('unhealthyunits',$i)),
                'replenishment_category_id' => $i['replenishent_category_id'],
                'generated_date' => $i['generated_date'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            );
        endforeach;

        return $dataArr;
    }

    public function geoArray($array)
    {
        $asins = Asin::select('id', 'asin')->get()->toArray();
        $countries = Country::select('id', 'country')->get()->toArray();
        $states = State::select('id', 'state')->get()->toArray();
        $cities = City::select('id', 'city')->get()->toArray();
        $zips = Zip::select('id', 'zip')->get()->toArray();

        foreach ($array as $key => $i):
            $dataArr[] = array(
                'asin_id' => $this->findId($i['ASIN'], $asins, 'asin'),
                'country_id' => $this->findId($i['Country/Region'], $countries, 'country'),
                'state_id' => $this->findId($i['State'], $states, 'state'),
                'city_id' => $this->findId($i['City'], $cities, 'city'),
                'zip_id' => $this->findId($i['ZIP'], $zips, 'zip'),
                'shipped_revenue' => $this->stripChar($i['Shipped Revenue']),
                'shipped_revenue_total' => $this->stripChar($i['Shipped Revenue - % of Total']),
                'shipped_revenue_prior_period' => $this->stripChar($i['Shipped Revenue - Prior Period']),
                'shipped_revenue_last_year' => $this->stripChar($i['Shipped Revenue - Last Year']),
                'shipped_units' => $this->stripChar($i['Shipped Units']),
                'shipped_units_total' =>$this->stripChar( $i['Shipped Units - % of Total']),
                'shipped_units_prior_period' => $this->stripChar($i['Shipped Units - Prior Period']),
                'shipped_units_last_year' => $this->stripChar($i['Shipped Units - Last Year']),
                'average_sales_price' => $this->stripChar($i['Average Sales Price']),
                'average_sales_price_prior_period' => $this->stripChar($i['Average Sales Price - Prior Period']),
                'average_sales_price_last_year' => $this->stripChar($i['Average Sales Price - Last Year']),
                'generated_date' => date('Y-m-d'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            );
        endforeach;

        return $dataArr;
    }

    public function geoArrayJSON($array)
    {
        foreach ($array as $key => $i):
            $dataArr[] = array(
                'account_id' => $i['account_id'],
                'asin_id' => $i['asin_id'],
                'country_id' => $i['country_id'],
                'state_id' => $i['state_id'],
                'city_id' => $i['city_id'],
                'zip_id' => $i['zip_id'],
                'shipped_revenue' => $this->stripChar($i['shippedrevenue']),
                'shipped_revenue_total' => $this->stripChar($i['shippedrevenuepercentoftotal']),
                'shipped_revenue_prior_period' => $this->stripChar($i['shippedrevenuepriorperiodpercentchange']),
                'shipped_revenue_last_year' => $this->stripChar($i['shippedrevenueprioryearpercentchange']),
                'shipped_cogs' => $this->stripChar($i['shippedcogs']),
                'shipped_cogs_total' => $this->stripChar($i['shippedcogspercentoftotal']),
                'shipped_cogs_prior_period' => $this->stripChar($i['shippedcogspriorperiodpercentchange']),
                'shipped_cogs_last_year' => $this->stripChar($i['shippedcogsprioryearpercentchange']),
                'shipped_units' => $this->stripChar($i['shippedunits']),
                'shipped_units_total' =>$this->stripChar( $i['shippedunitspercentoftotal']),
                'shipped_units_prior_period' => $this->stripChar($i['shippedunitspriorperiodpercentchange']),
                'shipped_units_last_year' => $this->stripChar($i['shippedunitsprioryearpercentchange']),
                'average_sales_price' => $this->stripChar($i['averageshippedprice']),
                'average_sales_price_prior_period' => $this->stripChar($i['averageshippedpricepriorperiodpercentchange']),
                'average_sales_price_last_year' => $this->stripChar($i['averageshippedpriceprioryearpercentchange']),
                'generated_date' => $i['generated_date'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            );
        endforeach;

        return $dataArr;
    }

    public function netArrayJSON($array)
    {
        foreach ($array as $key => $i):
            $dataArr[] = array(
                'account_id' => $i['account_id'],
                'asin_id' => $i['asin_id'],
                'subcategory' => $i['subcategory'],
                'product_title' => $i['producttitle'],
                'net_ppm' => $i['netppm'],
                'net_ppm_total' => $i['netppmpercentoftotal'],
                'net_ppm_prior' => $i['netppmpriorperiodpercentchange'],
                'net_ppm_last_year' => $i['netppmprioryearpercentchange'],
                'generated_date_from' => $i['generated_date_from'],
                'generated_date_to' => $i['generated_date_to'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            );
        endforeach;

        return $dataArr;
    }

    public function preorderArrayJSON($array)
    {
        foreach ($array as $key => $i):
            $dataArr[] = array(
                'asin_id' => $i['asin_id'],
                'account_id' => $i['account_id'],
                'pre_ordered_revenue' => $this->stripChar($i['preorderedamount']),
                'pre_ordered_revenue_prior_period' => $this->stripChar($i['preorderedamountpriorperiod']),
                'pre_ordered_units' => $this->stripChar($i['preorderedunits']),
                'pre_ordered_units_prior_period' => $this->stripChar($i['preorderedunitspriorperiod']),
                'average_pre_order_sales_price' => $this->stripChar($i['averagepreordersalesprice']),
                'average_pre_order_sales_price_prior_period' => $this->stripChar($i['averagepreordersalespricepriorperiod']),
                'generated_date' => $i['generated_date'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            );
        endforeach;

        return $dataArr;
    }

    public function salesArray($array, $filterArray)
    {
        $program = $this->importProgramFilter($filterArray, new Program);
        $asins = Asin::select('id', 'asin')->get()->toArray();

        foreach ($array as $key => $i):
            $dataArr[] = array
            (
                'program_id' => $program->id,
                'asin_id' => $this->findId($i['ASIN'], $asins, 'asin'),
                'shipped_cogs' => $this->stripChar($i['Shipped COGS']),
                'shipped_cogs_percentage_total' => $this->stripChar($i['Shipped COGS - % of Total']),
                'shipped_cogs_prior' => $this->stripChar($i['Shipped COGS - Prior Period']),
                'shipped_units' => $this->stripChar($i['Shipped Units']),
                'shipped_units_percentage_total' => $this->stripChar($i['Shipped Units - % of Total']),
                'shipped_units_prior' => $this->stripChar($i['Shipped Units - Prior Period']),
                'ordered_units' => $this->stripChar($i['Ordered Units']),
                'ordered_units_percentage_total' => $this->stripChar($i['Ordered Units - % of Total']),
                'ordered_units_prior' => $this->stripChar($i['Ordered Units - Prior Period']),
                'customer_returns' => $this->stripChar($i['Customer Returns']),
                'free_replacements' => $this->stripChar($i['Free Replacements']),
                'subcategory_sales_rank' => $this->stripChar($i['Subcategory (Sales Rank)']),
                'subcategory_better_worse' => $this->stripChar($i['Subcategory (Better/Worse)']),
                'average_sales_price' => $this->stripChar($i['Average Sales Price']),
                'average_sales_price_prior' => $this->stripChar($i['Average Sales Price - Prior Period']),
                'change_in_glance_view_prior' => $this->stripChar($i['Change in Glance View - Prior Period']),
                'rep_oos' => $this->stripChar($i['Rep OOS']),
                'rep_oos_percentage_total' => $this->stripChar($i['Rep OOS - % of Total']),
                'rep_oos_prior' => $this->stripChar($i['Rep OOS - Prior Period']),
                'lbb' => $this->stripChar($i['LBB (Price)']),
                'generated_date' => '2018-07-20',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            );
        endforeach;

        return $dataArr;
    }

    public function salesJSON($array)
    {
        // dd($array);
        $program = $array[0]['program_name'];

        $this->salesArrayJSON();
    }

    public function productsArray($array)
    {
        $bindings = Binding::select('id', 'bindings')->get()->toArray();
        $productgroups = ProductGroup::select('id', 'product_group')->get()->toArray();
        $brands = Brand::select('id', 'brand_code')->get()->toArray();
        $parentmanufacturercodes = ParentManufacturerCode::select('id', 'parent_manufacturer_code')->get()->toArray();
        $manufacturercodes = ManufacturerCode::select('id', 'manufacturer_code')->get()->toArray();
        $replenishcodes = ReplenishCode::select('id', 'replenish_code')->get()->toArray();

        foreach ($array as $key => $i):
            $dataArr[] = array(
                'account_id' => 14,
                'asin' => $i['ASIN'],
                'product_title' => $i['Product Title'],
                'parent_asin' => $this->stripNull($i['Parent ASIN']),
                'isbn_13' => $this->stripNull($i['ISBN-13']),
                'ean' => $this->stripNull($i['EAN']),
                'upc' => $this->stripNull($i['UPC']),
                'release_date' => Carbon::parse(DateTime::createFromFormat('d/m/Y', $i['Release Date'])),
                'list_price' => $this->stripChar($i['List Price']),
                'binding_id' => $this->findId($i['Binding'], $bindings, 'bindings'),
                'author_or_artist' => $i['Author / Artist'],
                'is_sitb_enabled' => $i['SITB enabled?'],
                'apparel_size' => $i['Apparel Size'],
                'apparel_size_width' => $i['Apparel Size Width'],
                'product_group_id' => $this->findId($i['Product Group'], $productgroups, 'product_group'),
                'replenish_id' => $this->findId($i['Replenishment Code'], $replenishcodes, 'replenish_code'),
                'model_style_no' => $i['Model / Style Number'],
                'color' => $i['Color'],
                'color_count' => $i['Color Count'],
                'prep_instructions_required' => $this->stripNull($i['Prep Instructions Required']),
                'prep_instructions_vendor_state' => $this->stripNull($i['Prep Instructions Vendor State']),
                'brand_id' => $this->findId($i['Brand Code'], $brands, 'brand_code'),
                'manufacturer_code_id' => $this->findId($i['Manufacturer Code'], $manufacturercodes, 'manufacturer_code'),
                'parent_manufacturer_code_id' => $this->findId($i['Parent Manufacturer Code'], $parentmanufacturercodes, 'parent_manufacturer_code'),
                'generated_date' => date('Y-m-d'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            );
        endforeach;

        return $dataArr;
    }

    public function productsJSON($array)
    {
        // dd($array);
        foreach ($array as $key => $i):
            if (array_key_exists('prepinstructionsvendorstate', $i)):
                $vendor_state = $this->stripNull($i['prepinstructionsvendorstate']);
            else:
                $vendor_state = null;
            endif;

            if ($i['marketplace'] == 'JP') {
                $ean = 'jan';
            }
            else {
                $ean = 'ean';
            }

            $dataArr[] = array(
                'asin' => $i['asin'],
                'account_id' => $i['account_id'],
                'product_title' => $i['producttitle'],
                'parent_asin' => $this->stripNull($i['parentasin']),
                'isbn_13' => $this->stripNull($i['isbn13']),
                'ean' => $this->stripNull($i[$ean]),
                'upc' => $this->stripNull($i['upc']),
                'release_date' => $this->stripNull($i['releasedate']),
                'list_price' => $this->stripChar($this->checkIndex('listprice',$i)),
                'binding_id' => $i['binding_id'],
                'author_or_artist' => $i['author'],
                'is_sitb_enabled' => !empty($this->checkIndex('sitbenabled',$i))?$this->checkIndex('sitbenabled',$i): 'N',
                'apparel_size' => $i['apparelsize'],
                'apparel_size_width' => $i['apparelsizewidth'],
                'product_group_id' => $i['product_group_id'],
                'replenish_id' => $i['replenish_id'],
                'model_style_no' => $i['modelstyle'],
                'color' => $i['color'],
                'color_count' => $this->checkIndex('colorcount', $i),
                'prep_instructions_required' => !empty($this->stripNull($this->checkIndex('prepinstructionsrequired',$i)))?$this->stripNull($i['prepinstructionsrequired']): null,
                'prep_instructions_vendor_state' => $vendor_state,
                'brand_id' => $i['brand_id'],
                'manufacturer_code_id' => $i['manufacturer_code_id'],
                'parent_manufacturer_code_id' =>$i['parent_manufacturer_code_id'],
                'generated_date' => date('Y-m-d'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            );
        endforeach;

        return $dataArr;
    }

    public function salesArrayJSON($array)
    {
        foreach ($array as $key => $i):
            $dataArr[] = array
            (
                'program_id' => $i['program'],
                'account_id' => $i['account_id'],
                'asin_id' => $i['asin_id'],
                'distributor_id' => $i['distributor_id'],
                'shipped_revenue' => !empty($this->stripChar($this->checkIndex('shippedrevenue', $i)))?$this->stripChar($this->checkIndex('shippedrevenue', $i)): null,
                'shipped_revenue_percentage_total' => !empty($this->stripChar($this->checkIndex('shippedrevenuepercentoftotal', $i)))?$this->stripChar($this->checkIndex('shippedrevenuepercentoftotal', $i)): null,
                'shipped_revenue_prior' => !empty($this->stripChar($this->checkIndex('shippedrevenuepriorperiodpercentchange', $i)))?$this->stripChar($this->checkIndex('shippedrevenuepriorperiodpercentchange', $i)): null,
                'shipped_revenue_prior_year' => !empty($this->stripChar($this->checkIndex('shippedrevenueprioryearpercentchange', $i)))?$this->stripChar($this->checkIndex('shippedrevenueprioryearpercentchange', $i)): null,
                'shipped_cogs' => !empty($this->stripChar($this->checkIndex('shippedcogs', $i)))?$this->stripChar($this->checkIndex('shippedcogs', $i)): null,
                'shipped_cogs_percentage_total' => !empty($this->stripChar($this->checkIndex('shippedcogspercentoftotal', $i)))?$this->stripChar($this->checkIndex('shippedcogspercentoftotal', $i)): null,
                'shipped_cogs_prior' => !empty($this->stripChar($this->checkIndex('shippedcogspriorperiodpercentchange', $i)))?$this->stripChar($this->checkIndex('shippedcogspriorperiodpercentchange', $i)): null,
                'shipped_cogs_prior_year' => !empty($this->stripChar($this->checkIndex('shippedcogsprioryearpercentchange', $i)))?$this->stripChar($this->checkIndex('shippedcogsprioryearpercentchange', $i)): null,
                'shipped_units' => !empty($this->stripChar($this->checkIndex('shippedunits', $i)))?$this->stripChar($this->checkIndex('shippedunits', $i)): null,
                'shipped_units_percentage_total' => !empty($this->stripChar($this->checkIndex('shippedunitspercentoftotal', $i)))?$this->stripChar($this->checkIndex('shippedunitspercentoftotal', $i)): null,
                'shipped_units_prior' => !empty($this->stripChar($this->checkIndex('shippedunitspriorperiodpercentchange', $i)))?$this->stripChar($this->checkIndex('shippedunitspriorperiodpercentchange', $i)): null,
                'shipped_units_prior_year' => !empty($this->stripChar($this->checkIndex('shippedunitsprioryearpercentchange', $i)))?$this->stripChar($this->checkIndex('shippedunitsprioryearpercentchange', $i)): null,
                'ordered_units' => !empty($this->stripChar($this->checkIndex('orderedunits', $i)))?$this->stripChar($this->checkIndex('orderedunits', $i)): null,
                'ordered_units_percentage_total' => !empty($this->stripChar($this->checkIndex('orderedunitspercentoftotal', $i)))?$this->stripChar($this->checkIndex('orderedunitspercentoftotal', $i)): null,
                'ordered_units_prior' => !empty($this->stripChar($this->checkIndex('orderedunitspriorperiodpercentchange', $i)))?$this->stripChar($this->checkIndex('orderedunitspriorperiodpercentchange', $i)): null,
                'ordered_units_prior_year' => !empty($this->stripChar($this->checkIndex('orderedunitsprioryearpercentchange', $i)))?$this->stripChar($this->checkIndex('orderedunitsprioryearpercentchange', $i)): null,
                'subcategory_sales_rank' => !empty($this->stripChar($this->checkIndex('subcategoryrank', $i)))?$this->stripChar($this->checkIndex('subcategoryrank', $i)): null,
                'subcategory_better_worse' => !empty($this->stripChar($this->checkIndex('subcategorybetterworse', $i)))?$this->stripChar($this->checkIndex('subcategorybetterworse', $i)): null,
                'average_sales_price' => !empty($this->stripChar($this->checkIndex('averagesellingpriceshippedunits', $i)))?$this->stripChar($this->checkIndex('averagesellingpriceshippedunits', $i)): null,
                'average_sales_price_prior' => !empty($this->stripChar($this->checkIndex('averagesellingpricepriorperiodpercentchange', $i)))?$this->stripChar($this->checkIndex('averagesellingpricepriorperiodpercentchange', $i)): null,
                'change_in_glance_view_prior' => !empty($this->stripChar($this->checkIndex('glanceviewspriorperiodpercentchange', $i)))?$this->stripChar($this->checkIndex('glanceviewspriorperiodpercentchange', $i)): null,
                'change_in_glance_view_prior_year' => !empty($this->stripChar($this->checkIndex('glanceviewsprioryearpercentchange', $i)))?$this->stripChar($this->checkIndex('glanceviewsprioryearpercentchange', $i)): null,
                'rep_oos' => !empty($this->stripChar($this->checkIndex('repoos', $i)))?$this->stripChar($this->checkIndex('repoos', $i)): null,
                'rep_oos_percentage_total' => !empty($this->stripChar($this->checkIndex('repoospercentoftotal', $i)))?$this->stripChar($this->checkIndex('repoospercentoftotal', $i)): null,
                'rep_oos_prior' => !empty($this->stripChar($this->checkIndex('repoospriorperiodpercentchange', $i)))?$this->stripChar($this->checkIndex('repoospriorperiodpercentchange', $i)): null,
                'lbb' => !empty($this->stripChar($this->checkIndex('lostbuybox', $i)))?$this->stripChar($this->checkIndex('lostbuybox', $i)): null,
                'change_in_impressions_prior' => !empty($this->stripChar($this->checkIndex('changeinimpressionspriorperiodpercentchange', $i)))?$this->stripChar($this->checkIndex('changeinimpressionspriorperiodpercentchange', $i)): null,
                'change_in_impressions_prior_year' => !empty($this->stripChar($this->checkIndex('changeinimpressionsprioryearpercentchange', $i)))?$this->stripChar($this->checkIndex('changeinimpressionsprioryearpercentchange', $i)): null,
                'displayed_in_stock' => !empty($this->stripChar($this->checkIndex('displayedinstock', $i)))?$this->stripChar($this->checkIndex('displayedinstock', $i)): null,
                'displayed_in_stock_total' => !empty($this->stripChar($this->checkIndex('displayedinstockpercentoftotal', $i)))?$this->stripChar($this->checkIndex('displayedinstockpercentoftotal', $i)): null,
                'displayed_in_stock_prior' => !empty($this->stripChar($this->checkIndex('displayedinstockpriorperiodpercentchange', $i)))?$this->stripChar($this->checkIndex('displayedinstockpriorperiodpercentchange', $i)): null,
                'customer_returns' => !empty($this->stripChar($this->checkIndex('customerreturns', $i)))?$this->stripChar($this->checkIndex('customerreturns', $i)): null,
                'free_replacements' => !empty($this->stripChar($this->checkIndex('freereplacements', $i)))?$this->stripChar($this->checkIndex('freereplacements', $i)): null,
                'generated_date' => $i['generated_date'],   
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            );
        endforeach;

        return $dataArr;
    }

    public function freshArray($array)
    {
        foreach ($array as $key => $i):
            $dataArr[] = array
            (
                'program_id' => $i['program'],
                'account_id' => $i['account_id'],
                'asin_id' => $i['asin_id'],
                'shipped_cogs' => $this->stripChar($i['shippedcogs']),
                'shipped_cogs_percentage_total' => $this->stripChar($i['shippedcogspercentoftotal']),
                'shipped_cogs_prior' => $this->stripChar($i['shippedcogspriorperiodpercentchange']),
                'shipped_cogs_prior_year' => $this->stripChar($i['shippedcogsprioryearpercentchange']),
                'shipped_units' => $this->stripChar($i['shippedunits']),
                'shipped_units_percentage_total' => $this->stripChar($i['shippedunitspercentoftotal']),
                'shipped_units_prior' => $this->stripChar($i['shippedunitspriorperiodpercentchange']),
                'shipped_units_prior_year' => $this->stripChar($i['shippedunitsprioryearpercentchange']),
                'change_in_impressions_prior' => $this->stripChar($i['changeinimpressionspriorperiodpercentchange']),
                'change_in_impressions_prior_year' => $this->stripChar($i['changeinimpressionsprioryearpercentchange']),
                'displayed_in_stock' => $this->stripChar($i['displayedinstock']),
                'displayed_in_stock_total' => $this->stripChar($i['displayedinstockpercentoftotal']),
                'displayed_in_stock_prior' => $this->stripChar($i['displayedinstockpriorperiodpercentchange']),
                'generated_date' => $i['generated_date'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            );
        endforeach;

        return $dataArr;
    }

    public function businessArray($array)
    {
        foreach ($array as $key => $i):
            dd($i);
            $dataArr[] = array
            (
                'program_id' => $i['program'],
                'account_id' => $i['account_id'],
                'asin_id' => $i['asin_id'],
                'shipped_cogs' => $this->stripChar($i['shippedcogs']),
                'shipped_cogs_percentage_total' => $this->stripChar($i['shippedcogspercentoftotal']),
                'shipped_cogs_prior' => $this->stripChar($i['shippedcogspriorperiodpercentchange']),
                'shipped_cogs_prior_year' => $this->stripChar($i['shippedcogsprioryearpercentchange']),
                'shipped_units' => $this->stripChar($i['shippedunits']),
                'shipped_units_percentage_total' => $this->stripChar($i['shippedunitspercentoftotal']),
                'shipped_units_prior' => $this->stripChar($i['shippedunitspriorperiodpercentchange']),
                'shipped_units_prior_year' => $this->stripChar($i['shippedunitsprioryearpercentchange']),
                'change_in_glance_view_prior' => $this->stripChar($i['glanceviewspriorperiodpercentchange']),
                'change_in_glance_view_prior_year' => $this->stripChar($i['glanceviewsprioryearpercentchange']),
                'rep_oos' => $this->stripChar($i['repoos']),
                'rep_oos_percentage_total' => $this->stripChar($i['repoospercentoftotal']),
                'rep_oos_prior' => $this->stripChar($i['repoospriorperiodpercentchange']),
                'lbb' => $this->stripChar($i['lostbuybox']),
                'generated_date' => $i['generated_date'],   
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            );
        endforeach;

        return $dataArr;
    }

    public function primeArray($array)
    {
        foreach ($array as $key => $i):
            $dataArr[] = array
            (
                'program_id' => $i['program'],
                'account_id' => $i['account_id'],
                'asin_id' => $i['asin_id'],
                'shipped_cogs' => $this->stripChar($i['shippedcogs']),
                'shipped_cogs_percentage_total' => $this->stripChar($i['shippedcogspercentoftotal']),
                'shipped_cogs_prior' => $this->stripChar($i['shippedcogspriorperiodpercentchange']),
                'shipped_cogs_prior_year' => $this->stripChar($i['shippedcogsprioryearpercentchange']),
                'shipped_units' => $this->stripChar($i['shippedunits']),
                'shipped_units_percentage_total' => $this->stripChar($i['shippedunitspercentoftotal']),
                'shipped_units_prior' => $this->stripChar($i['shippedunitspriorperiodpercentchange']),
                'shipped_units_prior_year' => $this->stripChar($i['shippedunitsprioryearpercentchange']),
                'customer_returns' => $this->stripChar($i['customerreturns']),
                'free_replacements' => $this->stripChar($i['freereplacements']),
                'generated_date' => $i['generated_date'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            );
        endforeach;

        return $dataArr;
    }
}
