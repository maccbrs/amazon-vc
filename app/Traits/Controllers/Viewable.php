<?php

namespace App\Traits\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Models\{
    Asin,
    Binding,
    ProductGroup,
    Category,
    Subcategory,
    Brand,
    ManufacturerCode,
    ParentManufacturerCode,
    ReplenishCode,
    Account
};

use Carbon\Carbon;
use DateTime;

trait Viewable
{
    public function appendCurrency($model, $accountId)
    {
        $account = Account::where('id', $accountId)->first();
        $currency = $account->marketplace;

            if ($currency == 'UK'):
                $currency = '£';
            elseif ($currency == 'CA'):
                $currency = 'CA$';
            elseif ($currency == 'US'):
                $currency = 'US$';
            else:
                $currency = '€';
            endif;
            
        return $currency;
    }

    public function currency($accountId)
    {
        $account = Account::where('id', $accountId)->first();
        $currency = $account->marketplace;

            if ($currency == 'UK'):
                $currency = '£';
            elseif ($currency == 'CA'):
                $currency = 'CA$';
            elseif ($currency == 'US'):
                $currency = 'US$';
            else:
                $currency = '€';
            endif;
            
        return $currency;
    }
}
