<?php

namespace App\Http\Controllers;

use App\Traits\Controllers\Ingestions\Account\ {
    AccountIngestable
};

use App\Traits\Controllers\Reports\Account\ {
    AccountQueryable
};

class AccountsController extends Controller
{

    use
        AccountIngestable, AccountQueryable;

    /**
     * Imports Accounts from Json File.
     * @return     boolean  Returns true or false.
     */
    public function importAccounts()
    {
        $files = $this->checkImportedLogs();

        if ($files) {
            $x = 0;
            foreach($files as $file) {
                $x++;
                $this->parseJson($x, $files, $file);
                unlink(base_path('storage/accounts/'.$file));
            }
            return true;
        }
        else {
            return false;
        }
    }

   /**
    * Gets the collection.
    * @return     JSON  The collection.
    */
   public function getCollection()
   {
        $result = $this->getAccountCollection();

        return $result;
   }

   /**
    * Gets the entity.
    * @param      string  $vgid   The vgid
    * @return     JSON  The entity.
    */
   public function getEntity($vgid)
   {
        $result = $this->getAccountEntity($vgid);

        return $result;
   }
}