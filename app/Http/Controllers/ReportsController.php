<?php

namespace App\Http\Controllers;

use App\Models\ {
  AlternativePurchase,
  GeographicSalesInsight,
  SalesDiagnostics,
  PreOrder,
  InventoryHealth,
  Account,
  Asin,
  NetPpm
};

use App\Traits\Controllers\ {
    Importable,
    Actionable,
    Viewable
};

use App\Traits\Controllers\Reports\ {
    Alternative\AlternativeQueryable,
    Geographic\GeographicQueryable,
    Inventory\InventoryQueryable,
    Preorder\PreorderQueryable,
    Sale\SaleQueryable,
    Sale\SaleParsable,
    Netppm\NetPpmQueryable
};

use Illuminate\Http\ {
    Request,
    Response
};

use DateTime;
use DB;

use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Illuminate\Support\Collection;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Schema;
use PDOException;

class ReportsController extends Controller
{

    use
        Importable,
        Viewable,
        Actionable,
        AlternativeQueryable,
        GeographicQueryable,
        PreorderQueryable,
        InventoryQueryable,
        SaleQueryable,
        NetPpmQueryable,
        SaleParsable;

    /**
     * Gets the collection.
     * @param      string   $type     The type
     * @param      Request  $request  The request
     * @return     array    The collection.
     */
    public function getCollection($type, Request $request)
    {
        $defaultParam = (object)[
            'accountId' => $request->accountId,
            'from' => $request->from,
            'to' => $request->to,
            'perPage' => $request->perPage
        ];

        $customParamSales = (object)[
            'request' => $request,
            'page' => $request->page, 
            'metrics' => $request->metrics, 
            'displayNulls' => $request->displayNulls
        ];

        $defaultParamNet = (object)[
            'accountId' => $request->accountId,
            'from' => $request->from,
            'to' => $request->to,
            'perPage' => $request->perPage,
            'page' => $request->page
        ];

        $customParam = (object)array_merge(
            (array)$customParamSales,
            (array)$defaultParam
        );

        switch ($type)
        {
            case 'alt':
                return $this->getAltCollection($defaultParam);
                break;
            case 'geo':
                return $this->getGeoCollection($defaultParam);
                break;
            case 'pre':
                return $this->getPreCollection($defaultParam);
                break;
            case 'inv':
                return $this->getInvCollection($defaultParam);
                break;
            case 'netppm':
                return $this->getNetCollection($defaultParamNet);
                break;
            case 'sales':
                return $this->getSalesCollection($customParam);
                break;
            default:
                return [];
                break;
        }
    }

    /**
     * Gets the geo collection.
     * @param      object  $param  The parameter
     * @return     JSON  The geo collection.
     */
    public function getGeoCollection($param)
    {
        $result = $this->getGeographicCollection($param);

        return $result;
    }

    /**
     * Gets the alternate collection.
     * @param      object  $param  The parameter
     * @return     JSON  The alternate collection.
     */
    public function getAltCollection ($param)
    {
        $result = $this->getAlternativeCollection($param);

        return $result;
    }

    /**
     * Gets the pre collection.
     * @param      object  $param  The parameter
     * @return     JSON The pre collection.
     */
    public function getPreCollection($param)
    {
        $result = $this->getPreorderCollection($param);

        return $result;
    }

    /**
     * Gets the inv collection.
     * @param      object  $param  The parameter
     * @return     JSON  The inv collection.
     */
    public function getInvCollection($param)
    {
        $result = $this->getInventoryCollection($param);

        return $result;
    }

    public function getNetCollection($param)
    {
        $net = $this->getNetPpmCollection($param);

        $result = new Paginator($net, $param->perPage, $param->page);

        return $result;
    }

    /**
     * Gets the sales collection.
     * @param      object     $param  The parameter
     * @return     Paginator  The sales collection.
     */
    public function getSalesCollection($param)
    {
        $sales_columns = Schema::getColumnListing('sales_diagnostics');
        $asins_columns = Schema::getColumnListing('asins');

        $db_columns = array_unique(array_merge($sales_columns, $asins_columns));
        $metric_columns = array_filter(array_map('trim', explode(',', $param->metrics)));
        $matched = array_intersect($db_columns, $metric_columns);
        $diff = array_diff(array_map('trim', $metric_columns), $matched);

        if ($diff) {
            $msg = "Column(s) Not Found.";
            return response()->json(['metrics' => $diff, 'msg' => $msg], 400);
        }

        $keys = [];
        $keys = $this->getColumnKeys($metric_columns);
        array_push($keys, 'sales_diagnostics.generated_date', 'asin');

        $query = $this->getSalesDiagnosticCollection($param, $keys);

        $asin = [];
        $asin = $this->createAsinsArray($query);

        $data = [];
        $data = $this->createDataArray($asin);

        if (!empty($param->date)) {
            $d = DateTime::createFromFormat('Y-m-d', $param->date);
            $check_date = $this->checkInRange($param->from, $param->to, $param->date);

            $data = $this->getCheckDate($d, $check_date, $data, $param->date);
        }

        $currentPage = Paginator::resolveCurrentPage();
        $collection = new Collection((object)$data);

        $currentPageResults = $collection->slice(($currentPage-1) * $param->perPage, $param->perPage)->all();
      
        $result = new Paginator(
            $currentPageResults,  
            count($collection), 
            $param->perPage, 
            $currentPage
        );

        $result->setPath($param->request->fullUrl());

        return response()->json($result);
    }


    /**
     * Gets the sales asin total.
     *
     * @param      Request  $request  The request
     *
     * @return     <type>   The sales asin total.
     */
    public function getSalesAsinTotal(Request $request)
    {
        $param = (object)[
            'accountId' => $request->accountId,
            'request' => $request,
            'from' => $request->start,
            'to' => $request->end,
            'persist' => $request->persist,
            'page' => $request->page, 
            'metrics' => $request->metrics, 
            'displayNulls' => $request->displayNulls
        ];

        $asins = SalesDiagnostics::selectRaw('sum(sales_diagnostics.shipped_revenue) AS cnt, asins.asin, asins.product_title')
                    ->leftjoin('asins', 'sales_diagnostics.asin_id', '=', 'asins.id')
                    ->where('sales_diagnostics.account_id', 5)
                    ->whereBetween('sales_diagnostics.generated_date',[$param->from,$param->to])
                    ->groupBy('asins.asin')
                    ->take(2900)
                    ->get();

        $result = json_encode($asins);

        return $result;
    }

    /**
     * Gets the sales collection summary.
     * @param      string  $accountId  The account identifier
     * @param      string  $from       The from
     * @param      string  $to         The to
     * @param      string  $perPage    The per page
     * @return     object  The sales collection summary.
     */
    public function getSalesCollectionSummary($accountId, $from, $to, $perPage)
    {
        $result = $this->getSalesDiagnosticCollectionSummary($accountId, $from, $to, $perPage);

        return $result;
    }

    /**
     * Check in Date Range
     * @param      string   $start_date      The start date
     * @param      string   $end_date        The end date
     * @param      string   $date_from_user  The date from user
     * @return     boolean  The result of checking dates
     */
    function checkInRange($start_date, $end_date, $date_from_user)
    {
      $start_ts = strtotime($start_date);
      $end_ts = strtotime($end_date);
      $user_ts = strtotime($date_from_user);

      return (($user_ts >= $start_ts) && ($user_ts <= $end_ts));
    }

    public function getSalesRevenue()
    {
        $asins = SalesDiagnostics::selectRaw('sum(sales_diagnostics.shipped_revenue) AS cnt, asins.asin, asins.product_title')
                    ->leftjoin('asins', 'sales_diagnostics.asin_id', '=', 'asins.id')
                    ->where('sales_diagnostics.account_id', 5)
                    ->whereBetween('sales_diagnostics.generated_date',[$param->from,$param->to])
                    ->groupBy('asins.asin')
                    ->take(100)
                    ->get();
    }
}