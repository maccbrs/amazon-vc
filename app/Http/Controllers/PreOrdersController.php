<?php

namespace App\Http\Controllers;

use App\Traits\Controllers\ {
    Importable,
    Actionable,
    Loggable
};

use App\Traits\Controllers\Ingestions\Preorder\ {
    PreorderIngestable
};

class PreOrdersController extends Controller
{

    use
        Actionable,
        Loggable,
        Importable,
        PreorderIngestable;

    /**
     * Imports Pre Orders
     *
     * @return     \Illuminate\Http\JsonResponse
     */
    public function importPreOrders()
    {
    	$this->importAsin();
        $result = $this->storePreOrders();
        if ($result):
            return $this->respond('done', $result);
        else:
            return $this->respond('conflict', 'Pre-orders already been uploaded!');
        endif;
    }

    /**
     * Import PreOrders JSON
     *
     * @return     boolean  ( description_of_the_return_value )
     */
    public function importPreOrdersJSON()
    {
        $files = $this->checkImportedLogs();

        if ($files) {
            $x = 0;
            foreach ($files as $file) {
                $x++;
                $this->parseJson($x, $files, $file);
            }
            return true;
        }
        else {
            return false;
        }
    }

}