<?php

namespace App\Http\Controllers;

use App\Traits\Controllers\ {
    Importable,
    Actionable,
    Loggable,
    Parsable
};

use App\Traits\Controllers\Ingestions\Alternative\ {
    AlternativeIngestable
};

use App\Models\ {
    Asin,
    AlternativePurchase,
    SalesDiagnostics,
};

use DateInterval;
use DatePeriod;

class AlternativePurchaseController extends Controller
{

    use
        Importable,
        Loggable,
        Parsable,
        Actionable,
        AlternativeIngestable;

    /**
     * Imports Alternative Purchases
     *
     * @return     \Illuminate\Http\JsonResponse
     */
    public function importAlternativePurchase()
    {
    	$this->importAsin();
        $result = $this->storeAlternative();
        if ($result):
            return $this->respond('done', $result);
        else:
            return $this->respond('conflict', 'Alternative Purchase already been uploaded!');
        endif;
    }


    /**
     * Imports Alternative Purchases
     * @return     boolean  Returns true of false.
     */
    public function importAlternativePurchaseJSON()
    {
        $files = $this->checkImportedLogs();
        // dd($files);
        if ($files) {
            $x = 0;
            foreach ($files as $file) {
                $x++;
                $this->parseJson($x, $files, $file);            
            }
            return true;
        }
        else {
            return false;
        }
    }

    public function checkMissingDates()
    {
        $dates = AlternativePurchase::where('account_id',12)
        ->distinct()
        ->orderBy('generated_date')
        ->get(['generated_date']);

        $dates_array = [];
        foreach($dates as $date) {
            $dates_array[] = $date->generated_date;
        }

        $dateStart = date_create(reset($dates_array));
        $dateEnd   = date_create(end($dates_array));
        $interval  = new DateInterval('P1D');
        $period    = new DatePeriod($dateStart, $interval, $dateEnd);
        foreach($period as $day) {
          $formatted = $day->format("Y-m-d");
          if(!in_array($formatted, $dates_array)) $missingDates[] = $formatted;
        }
        
        if (!empty($missingDates)) {
            return $missingDates;
        } else {
            return null;
        }

    }

    /**
     * Update Alternative
     */
    public function updateAlt()
    {
        $count = AlternativePurchase::count();
        
        $i = 630086;
        for ($x = 630086; $x <= $count; $x++) {
            $i++;

            $alt = AlternativePurchase::where('id', $x)->first();

            if (!empty($alt)) {
                $first_asin = Asin::where('asin', $alt['first_alternative_asin'])->first();
                $second_asin = Asin::where('asin', $alt['second_alternative_asin'])->first();
                $third_asin = Asin::where('asin', $alt['third_alternative_asin'])->first();
                $fourth_asin = Asin::where('asin', $alt['fourth_alternative_asin'])->first();
                $fifth_asin = Asin::where('asin', $alt['fifth_alternative_asin'])->first();

                $update = AlternativePurchase::updateOrCreate([
                    'id' => $alt->id
                ],
                [
                    'first_alternative_asin_id' => !empty($first_asin->id)?$first_asin->id: null,
                    'second_alternative_asin_id' => !empty($second_asin->id)?$second_asin->id: null,
                    'third_alternative_asin_id' => !empty($third_asin->id)?$third_asin->id: null,
                    'fourth_alternative_asin_id' => !empty($fourth_asin->id)?$fourth_asin->id: null,
                    'fifth_alternative_asin_id' => !empty($fifth_asin->id)?$fifth_asin->id: null,
                ]);

                printf("Row Id: ".$alt->id." have been checked/updated. Count: ".$i."/".$count.".\n");
            }
        }
    }
}