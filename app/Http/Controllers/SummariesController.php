<?php

namespace App\Http\Controllers;

use App\Models\ {
  Asin
};

use App\Traits\Controllers\ {
    Importable,
    Actionable
};

use Illuminate\Http\ {
    Request,
    Response
};

class SummariesController extends Controller
{

    use
        Importable,
        Actionable;

   public function getSummary(Request $request)
   {
        $asin = new Asin;
        $result = $asin
                    ->select('id','asin','product_title','upc','model_style_no','category_id','subcategory_id','replenish_id')
                    ->where('account_id', $request->accountId)
                    ->whereBetween('release_date', [$request->from, $request->to])
                    ->with(['category','subcategory','inventory_health','replenishCode','sales'])
                    ->paginate(100);

        return $result;
   }
}