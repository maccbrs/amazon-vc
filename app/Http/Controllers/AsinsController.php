<?php

namespace App\Http\Controllers;

use App\Models\ {
  Asin,
  ScraperLog
};

use Illuminate\Http\ {
    Request,
    Response
};

use App\Traits\Controllers\ {
    Actionable,
    Viewable
};

use App\Traits\Controllers\Reports\Asin\ {
    AsinQueryable   
};

use App\Traits\Controllers\Ingestions\Asin\ {
    AsinIngestable, //Loggable, AsinParsable -> AsinQueryable
};

class AsinsController extends Controller
{

    use
        AsinQueryable,
        Actionable,
        Viewable,
        AsinIngestable;

    /**
     * Imports The Asins
     * @return     \Illuminate\Http\JsonResponse
     */
    public function importProduct()
    {
        $result = $this->importAsin();

        if ($result):
            return $this->respond('done', $result);
        else:
            return $this->respond('conflict', 'Products already been uploaded!');
        endif;
    }

    /**
     * Logs a product scraper.
     *
     * @param      Request  $request  The request
     */
    public function logProductScraper(Request $request)
    {
        $test = ScraperLog::updateOrCreate(
            ['filename' => $request->filename],
            ['filename' => $request->filename]
        );
    }

    /**
     * Get the Collection of Asin
     * @param      Request  $request  The request
     * @return     JSON     Collection of Asin
     */
    public function getCollection(Request $request)
    {
        $result = $this->getAsinsCollection($request);

        if ($result):
            return $this->respond('done', $result);
        else:
            return $this->respond('not_found', $result);
        endif;

    }

    /**
     * Gets the Single Entity of the Asins
     * @param      string   $asin     The asin
     * @param      Request  $request  The request
     * @return     JSON     Returns Single Entity
     */
    public function getEntity($asin, Request $request)
    {
        $result = $this->getAsinEntity($asin, $request);

        if ($result)
        {
            return $this->respond('done', $json['pobject'][] = array('data'=>$result, 'currency'=>$this->appendCurrency($result, $request->accountId)));
        }
        else
        {
            return $this->respond('not_found', $json['pobject'][] = array('data'=>$result, 'currency'=>$this->appendCurrency($result, $request->accountId)));
        }
    }

    /**
     * Imports Products from JSON files
     * @return     boolean  Returns true or false
     */
    public function importProductJSON()
    {
        $files = $this->checkImportedLogs();

        if ($files) {
            $x = 0;
            foreach ($files as $file) {
                $x++;
                $this->parseJson($x, $files, $file);
            }
            return true;
        }
        else {
            return false;
        }
    }

}