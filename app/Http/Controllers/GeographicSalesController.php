<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

use App\Traits\Controllers\ {
    Importable,
    Actionable,
    Loggable,
};

use App\Traits\Controllers\Ingestions\Geographic\ {
    GeographicIngestable
};

use Carbon\Carbon;
use DateTime;

class GeographicSalesController extends Controller
{

    use
        Actionable,
        Loggable,
        Importable,
        GeographicIngestable;

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function importGeographicSales()
    {
        $this->importAsin();
        $result = $this->storeGeographic();
        if ($result):
            return $this->respond('done', $result);
        else:
            return $this->respond('conflict', 'Geographic Sales Insights already been uploaded!');
        endif;
    }

    /**
     * Import Geographic Sales JSON
     *
     * @return     boolean  ( description_of_the_return_value )
     */
    public function importGeographicSalesJSON()
    {
        $files = $this->checkImportedLogs();

        if ($files) {
            $x = 0;
            foreach ($files as $file) {
                $x++;
                $this->parseJson($x, $files, $file);
            }
            return true;
        }
        else {
            return false;
        }
    }
}
    