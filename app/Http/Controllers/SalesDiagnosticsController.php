<?php

namespace App\Http\Controllers;

use App\Traits\Controllers\ {
    Importable,
    Actionable,
    Loggable
};

use App\Traits\Controllers\Ingestions\Sale\ {
    SaleIngestable
};

class SalesDiagnosticsController extends Controller
{
    use
        Importable,
        Loggable,
        Actionable,
        SaleIngestable;

    /**
     * Imports Sales Diagnostics
     *
     * @return     \Illuminate\Http\JsonResponse
     */
    public function importSalesDiagnostics()
    {
        $this->importAsin();
        $result = $this->storeSales();
        if ($result):
            return $this->respond('done', $result);
        else:
            return $this->respond('conflict', 'Sales diagnostics already been uploaded!');
        endif;
    }

    /**
     * Imports Sales Diagnostics
     *
     * @return     boolean  ( description_of_the_return_value )
     */
    public function importSalesDiagnosticsJSON()
    {   
        $files = $this->checkImportedLogs();

        if ($files) {
            $x = 0;
            foreach ($files as $file) {
                $x++;
                $this->parseJson($x, $files, $file);
            }
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Renames JSON.
     */
    public function renameJSON()
    {
        $files = preg_grep('~^SalesDiagnosticsDetail_708590.*\.json$~',
        scandir(base_path()));

        foreach ($files as $file) {
            $old_name = $file;
            $orig_file = basename($file, '.json');

            $filename = explode("_", $orig_file);

            $new_name = $filename[0]."_retail_".$filename[1]."_".$filename[2].'.'.pathinfo($file, PATHINFO_EXTENSION);;
            
            if(file_exists(base_path($new_name))) 
            {  
                echo "Error While Renaming ".$old_name ; 
            } 
            else
            { 
                if(rename( base_path($old_name), base_path($new_name))) 
                {  
                    echo "Successfully Renamed ".$old_name." to ".$new_name."<br/>" ; 
                } 
                else
                { 
                    echo "A File With The Same Name Already Exists<br/>" ; 
                } 
            }
        }
    }

}