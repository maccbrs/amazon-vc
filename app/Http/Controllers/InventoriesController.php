<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

use App\Traits\Controllers\ {
    Importable,
    Actionable,
    Loggable,
};

use App\Traits\Controllers\Ingestions\Inventory\ {
    InventoryIngestable
};

use Carbon\Carbon;
use DateTime;

class InventoriesController extends Controller
{

    use
        Importable,
        Loggable,
        Actionable,
        InventoryIngestable;

    /**
     * Imports Inventories
     *
     * @return     \Illuminate\Http\JsonResponse
     */
    public function importInventories()
    {
    	$this->importAsin();
        $result = $this->storeInventory();
        if ($result):
            return $this->respond('done', $result);
        else:
            return $this->respond('conflict', 'Inventory Health already been uploaded!');
        endif;
    }

    /**
     * Imports Inventories JSON
     *
     * @return     boolean  ( description_of_the_return_value )
     */
    public function importInventoriesJSON()
    {
        $files = $this->checkImportedLogs();

        if ($files) {
            $x = 0;
            foreach ($files as $file) {
                $x++;
                $this->parseJson($x, $files, $file);
            }
            return true;
        }
        else {
            return false;
        }
    }
}