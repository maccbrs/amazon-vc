<?php

namespace App\Http\Controllers;

use App\Traits\Controllers\Reports\Category\ {
    CategoryQueryable
};

use App\Traits\Controllers\ {
    Actionable
};

use Illuminate\Http\ {
    Request,
    Response
};

class CategoriesController extends Controller
{

    use
        Actionable, CategoryQueryable;

    /**
     * Gets the collection of Categories.
     * @param      Request   $request  The request
     * @return     JSON  The Category collection.
     */
    public function getCollection(Request $request)
    {
        $result = $this->getCategoryCollection($request);

        if ($result) {
            return $this->respond('done', $result);
        }
        else {
            return $this->respond('not_found', $result);
        }
    }

}