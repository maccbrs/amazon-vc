<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

use App\Traits\Controllers\ {
    Importable,
    Actionable,
    Loggable,
};

use App\Traits\Controllers\Ingestions\NetPpm\ {
    NetPpmIngestable
};

use Carbon\Carbon;
use DateTime;

class NetPpmController extends Controller
{

    use
        Actionable,
        Loggable,
        Importable,
        NetPpmIngestable;

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function importNetPpm()
    {
        $this->importAsin();
        $result = $this->storeNetPpmJSON();
        if ($result):
            return $this->respond('done', $result);
        else:
            return $this->respond('conflict', 'Net PPM already been uploaded!');
        endif;
    }

    /**
     * Import Geographic Sales JSON
     *
     * @return     boolean  ( description_of_the_return_value )
     */
    public function importNetPpmJSON()
    {
        $files = $this->checkImportedLogs();

        if ($files) {
            $x = 0;
            foreach ($files as $file) {
                $x++;
                $this->parseJson($x, $files, $file);
            }
            return true;
        }
        else {
            return false;
        }
    }
}
    