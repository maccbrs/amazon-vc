<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Console\Command;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\KeyGenerateCommand::class,
        \App\Console\Commands\ImportCommand::class,
        \App\Console\Commands\ImportAccountCommand::class,
        \App\Console\Commands\ImportAsinCommand::class,
        \App\Console\Commands\ImportSalesCommand::class,
        \App\Console\Commands\ImportAlternativeCommand::class,
        \App\Console\Commands\ImportPreorderCommand::class,
        \App\Console\Commands\ImportGeographicCommand::class,
        \App\Console\Commands\ImportInventoryCommand::class,
        \App\Console\Commands\ImportNetPpmCommand::class,
        \App\Console\Commands\getUSDailyScraperCommand::class,
        \App\Console\Commands\getUSDailyBasicScraperCommand::class,
        \App\Console\Commands\getEUDailyScraperCommand::class,
        \App\Console\Commands\getEUDailyBasicScraperCommand::class,
        \App\Console\Commands\getJPDailyScraperCommand::class,
        \App\Console\Commands\CheckFilesCommand::class,
        \App\Console\Commands\UpdateAlternativeAsinsCommand::class,
        \App\Console\Commands\getTime::class,
        \App\Console\Commands\InsertHashAsinCommand::class,
        \App\Console\Commands\InsertHashSalesCommand::class,
        \App\Console\Commands\serve::class,
        \App\Console\Commands\getScraperCommand::class,
        \App\Console\Commands\UploadFileToAwsCommand::class

        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('importAccount:run')
        ->runInBackground()
        ->everyMinute();

        $schedule->command('importAsin:run')
        ->runInBackground()
        ->everyMinute();

        $us = storage_path('logs/us.log');
        $eu = storage_path('logs/eu.log');
        $scraper = storage_path('logs/scraper.log');

        $schedule->command('getScraper:run')
        ->runInBackground()
        ->cron('0 0 * * *')
        ->sendOutputTo($scraper);
/*
        $schedule->command('getEUDailyBasicScraper:run')
        ->runInBackground()
        ->cron('0 0 * * *')
        ->sendOutputTo($eu_basic);

        $schedule->command('getJPDailyBasicScraper:run')
        ->runInBackground()
        ->cron('0 0 * * *')
        ->sendOutputTo($jp_basic);

        $schedule->command('getUSDailyScraper:run')
        ->runInBackground()
        ->cron('0 0 * * *')
        ->sendOutputTo($us);

        $schedule->command('getEUDailyScraper:run')
        ->runInBackground()
        ->cron('0 0 * * *')
        ->sendOutputTo($eu);
*/
        $schedule->command('import:run')
        ->dailyAt('04:00')
        ->pingBefore('https://hc-ping.com/a0f6591b-ae51-43f7-9a53-0d5c51943e86/start')
        ->thenPing('https://hc-ping.com/a0f6591b-ae51-43f7-9a53-0d5c51943e86')
        ->pingOnFailure('https://hc-ping.com/a0f6591b-ae51-43f7-9a53-0d5c51943e86/fail');
    }
}
