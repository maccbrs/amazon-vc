<?php

namespace App\Console\Commands;

use App\Utilities\Constant;
use Illuminate\Console\Command;

use App\Models\ {
  Asin
};

class InsertHashAsinCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'InsertHashAsin:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Insert Command for inserting hash on Asin';

    /**
     * Create a new command instance.
     *
     * @returGetReportScheduleCommandn void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle(Asin $asin)
    {
            $asins = $asin->whereNull('hash')->get();

            foreach ($asins as $a) {
                try {
                    Asin::where('id', $a->id)
                    ->update(['hash' => md5($a->account_id.$a->asin)]);
                    printf("Updated Id No. ".$a->id."\n");
                }
                catch (\Exception $e) {
                    dd($a);
                }
            }
    }

}
