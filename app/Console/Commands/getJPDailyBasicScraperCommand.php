<?php

namespace App\Console\Commands;

use App\Utilities\Constant;
use Illuminate\Console\Command;

class getJPDailyBasicScraperCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'getJPDailyBasicScraper:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run Command for JP Daily Basic Scraper';

    /**
     * Create a new command instance.
     *
     * @returGetReportScheduleCommandn void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param GetReportRequestListController $requestListController
     * @param GetReportListController $getReportListController
     * @param GetReportController $getReportController
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle()
    {
        try {

            putenv("PHANTOMJS_EXECUTABLE=/opt/phantomjs/bin/phantomjs");
            putenv("DYLD_LIBRARY_PATH");

            $getDaily = shell_exec('/opt/casperjs/bin/casperjs daily_scraper_basic_jp.js > /dev/null 2>&1 &');

            if (!$getDaily) {
                $this->info("No scraper exist");
            return;
            }
            $this->info($getDaily);
        }
        catch (Exception $e) {
            $this->error("An error occurred");
        }
    }

}
