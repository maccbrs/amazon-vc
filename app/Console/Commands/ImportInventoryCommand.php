<?php

namespace App\Console\Commands;

use App\Http\Controllers\{InventoriesController,
};
use App\Utilities\Constant;
use Illuminate\Console\Command;

class ImportInventoryCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'importInventory:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Command for Inventory Health Report';

    /**
     * Create a new command instance.
     *
     * @returGetReportScheduleCommandn void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param GetReportRequestListController $requestListController
     * @param GetReportListController $getReportListController
     * @param GetReportController $getReportController
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle(InventoriesController $inventoryController)
    {
            try {
                $importInventory = $inventoryController->importInventoriesJSON();

                if (!$importInventory) {
                    $this->info("No new inventory exist!");
                return;
                }
                $this->info("Inventory Health has been successfully imported!");
            }
            catch (Exception $e) {
                return $e->getMessage();
            }
    }

}
