<?php

namespace App\Console\Commands;

use App\Utilities\Constant;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class ImportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Command for Different Reports';

    /**
     * Create a new command instance.
     *
     * @returGetReportScheduleCommand void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * { function_description }
     * @return     <type>  ( description_of_the_return_value )
     */
    public function handle()
    {
        try {
            $this->call('importAccount:run');
        }
        catch (Exception $e) {
            return $e->getMessage();
        }
        finally {

            try {
                $this->call('importAsin:run');
            }
            catch (Exception $e) {
                return $e->getMessage();
            }
            finally {
                $commands = [
                    'importSales:run',
                    'importInventory:run',
                    'importGeographic:run',
                    'importAlternative:run',
                    'importPreorder:run'
                ];

                foreach ($commands as $command) {   
                    $this->call('importAsin:run');
                    $this->call($command);
                }

               $this->info("\nSuccessfully Imported All Reports!");
            } 

        }
    }

}
