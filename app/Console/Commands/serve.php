<?php

namespace App\Console\Commands;

use App\Utilities\Constant;
use Illuminate\Console\Command;

class serve extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'serve';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run PHP Artisan Serve';

    /**
     * Create a new command instance.
     *
     * @returGetReportScheduleCommandn void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * { function_description }
     */
    public function handle()
    {
        $port = '8000';

        $this->info('Laravel development server started: <http://localhost:'.$port.'>');
        $result = shell_exec('php -S localhost:'.$port.' -t ./public');
        printf($result);
    }

}
