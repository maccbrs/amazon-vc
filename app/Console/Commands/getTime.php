<?php

namespace App\Console\Commands;

use App\Utilities\Constant;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Carbon\Carbon;

class getTime extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'getTime:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Laravel Time';

    /**
     * Create a new command instance.
     *
     * @returGetReportScheduleCommand void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * { function_description }
     * @return     <type>  ( description_of_the_return_value )
     */
    public function handle()
    {
        dd(Carbon::now());
    }

}
