<?php

namespace App\Console\Commands;

use App\Http\Controllers\{GeographicSalesController,
};
use App\Utilities\Constant;
use Illuminate\Console\Command;

class ImportGeographicCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'importGeographic:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Command for Geographic Sales Report';

    /**
     * Create a new command instance.
     *
     * @returGetReportScheduleCommandn void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param GetReportRequestListController $requestListController
     * @param GetReportListController $getReportListController
     * @param GetReportController $getReportController
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle(GeographicSalesController $geographicController)
    {
        try {
            $importGeographic = $geographicController->importGeographicSalesJSON();

            if (!$importGeographic) {
                $this->info("No new geographic exist!");
            return;
            }
            $this->info("Geographic Sales has been successfully imported!");
        }
        catch (Exception $e) {
            return $e->getMessage();
        }
    }

}
