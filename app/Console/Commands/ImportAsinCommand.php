<?php

namespace App\Console\Commands;

use App\Http\Controllers\{AsinsController,
};
use App\Utilities\Constant;
use Illuminate\Console\Command;

class ImportAsinCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'importAsin:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Command for Asin Reports';

    /**
     * Create a new command instance.
     *
     * @returGetReportScheduleCommandn void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * { function_description }
     *
     * @param      GetReportRequestListController  $asinController
     * @param      GetReportListController  $getReportListController
     * @param      GetReportController      $getReportController
     * @throws     \GuzzleHttp\Exception\GuzzleException
     *
     * @return     <type>                          ( description_of_the_return_value )
     */
    public function handle(AsinsController $asinController)
    {
        try {
            $importProduct = $asinController->importProductJSON();
            if (!$importProduct) {
                $this->info("No new product exist!");
            return;
            }
            $this->info("Products has been successfully imported!");
        }
        catch (Exception $e) {
            return $e->getMessage();
        }
    }

}