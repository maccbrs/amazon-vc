<?php

namespace App\Console\Commands;

use App\Http\Controllers\{SalesDiagnosticsController,
};
use App\Utilities\Constant;
use Illuminate\Console\Command;

class ImportSalesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'importSales:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Command for Sales Diagnostics Reports';

    /**
     * Create a new command instance.
     *
     * @returGetReportScheduleCommandn void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * { function_description }
     * @param      SalesDiagnosticsController  $salesdiagnosticsController  The
     * salesdiagnostics controller
     * @return     <type>                      ( description_of_the_return_value
     * )
     */
    public function handle(SalesDiagnosticsController $salesdiagnosticsController)
    {
        try {
            $importSales = $salesdiagnosticsController->importSalesDiagnosticsJSON();

            if (!$importSales) {
                $this->info("No new sales exist!");
            return;
            }
            $this->info("Sales Diagnostics has been successfully imported!");
        }
        catch (Exception $e) {
            return $e->getMessage();
        }
    }

}
