<?php

namespace App\Console\Commands;

use App\Utilities\Constant;
use Illuminate\Console\Command;

use App\Models\ {
  SalesDiagnostics
};

class InsertHashSalesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'InsertHashSales:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Insert Command for inserting hash on Sales';

    /**
     * Create a new command instance.
     *
     * @returGetReportScheduleCommandn void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle(SalesDiagnostics $sale)
    {
            $sales = $sale->limit(1000000)->get();

            foreach ($sales as $a) {
                try {
                    SalesDiagnostics::where('id', $a->id)
                    ->update(['hash' => md5($a->account_id.$a->asin_id.$a->shipped_cogs.$a->generated_date)]);
                    printf("Updated Id No. ".$a->id."\n");
                }
                catch (\Exception $e) {
                    dd($a);
                }
            }
    }

}
