<?php

namespace App\Console\Commands;

use App\Utilities\Constant;
use Illuminate\Console\Command;

class getScraperCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'getScraper:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get All Scraping Script';

    /**
     * Create a new command instance.
     *
     * @returGetReportScheduleCommandn void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * { function_description }
     */
    public function handle()
    {
       $this->call('getUSDailyScraper:run');
       $this->call('getEUDailyScraper:run');
       $this->call('getJPDailyScraper:run');
    }

}
