<?php

namespace App\Console\Commands;

use App\Http\Controllers\{AlternativePurchaseController,
};
use App\Utilities\Constant;
use Illuminate\Console\Command;

class ImportAlternativeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'importAlternative:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Command for Alternative Reports';

    /**
     * Create a new command instance.
     *
     * @returGetReportScheduleCommandn void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * { function_description }
     * @param      AlternativePurchaseController  $alternativeController  The
     * alternative controller
     * @return     string                         Result Information
     */
    public function handle(AlternativePurchaseController $alternativeController)
    {
        try {
            $importAlternative = $alternativeController->importAlternativePurchaseJSON();

            if (!$importAlternative) {
                $this->info("No new alternative exist!");
            return;
            }
            $this->info("Alternative has been successfully imported!");
        }
        catch (Exception $e) {
            return $e->getMessage();
        }
    }

}
