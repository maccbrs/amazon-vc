<?php

namespace App\Console\Commands;

use App\Http\Controllers\{AccountsController,
};
use App\Utilities\Constant;
use Illuminate\Console\Command;

class ImportAccountCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'importAccount:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Command for Accounts';

    /**
     * Create a new command instance.
     *
     * @returGetReportScheduleCommandn void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * { function_description }
     * @param      AccountsController  $accountController  The account
     * controller
     * @return     string              Result Information
     */
    public function handle(AccountsController $accountController)
    {
        try {
            $importAccounts = $accountController->importAccounts();

            if (!$importAccounts) {
                    $this->info("No new accounts exist!");
                return;
            }
            $this->info("Accounts has been successfully imported!");
        }
        catch (Exception $e) {
            return $e->getMessage();
        }
    }

}
