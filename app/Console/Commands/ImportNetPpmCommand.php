<?php

namespace App\Console\Commands;

use App\Http\Controllers\{NetPpmController,
};
use App\Utilities\Constant;
use Illuminate\Console\Command;

class ImportNetPpmCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'importNetppm:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Command for NetPPM Reports';

    /**
     * Create a new command instance.
     *
     * @returGetReportScheduleCommandn void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * { function_description }
     * @param      SalesDiagnosticsController  $salesdiagnosticsController  The
     * salesdiagnostics controller
     * @return     <type>                      ( description_of_the_return_value
     * )
     */
    public function handle(NetPpmController $netppmController)
    {
        try {
            $importNetppm = $netppmController->importNetPpmJSON();

            if (!$importNetppm) {
                $this->info("No new netppm exist!");
            return;
            }
            $this->info("Net PPM has been successfully imported!");
        }
        catch (Exception $e) {
            return $e->getMessage();
        }
    }

}
