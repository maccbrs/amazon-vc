<?php

namespace App\Console\Commands;

use App\Http\Controllers\{AlternativePurchaseController,
};
use App\Utilities\Constant;
use Illuminate\Console\Command;

class UpdateAlternativeAsinsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'updateAlternativeAsins:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Command for Alternative Reports';

    /**
     * Create a new command instance.
     *
     * @returGetReportScheduleCommandn void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param GetReportRequestListController $requestListController
     * @param GetReportListController $getReportListController
     * @param GetReportController $getReportController
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle(AlternativePurchaseController $alternativeController)
    {
        try {
            $updateAlternative = $alternativeController->updateAlt();

            if (!$updateAlternative) {
                $this->info("test");
            return;
            }
            $this->info("Alternative Purchases has been updated!");
        }
        catch (Exception $e) {
            return $e->getMessage();
        }
    }

}
