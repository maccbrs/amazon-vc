<?php

namespace App\Console\Commands;

use App\Utilities\Constant;
use Illuminate\Console\Command;

use App\Models\{
    Account,
    Subscription,
    ImportedJson
};

class CheckFilesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'CheckFiles:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Command for Different Reports';

    /**
     * Create a new command instance.
     *
     * @returGetReportScheduleCommand void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * { function_description }
     * @return     <type>  ( description_of_the_return_value )
     */
    public function handle()
    {
        try {
            $dir = 'storage/salesdiagnostics';
            $list = preg_grep('~^SalesDiagnosticsDetail_.*\.json$~',
                      scandir(base_path($dir)));
            $import_logs = ImportedJson::where('report_type', 'sales')->get();

            foreach ($import_logs as $import_log) {
                $import_logged[] = $import_log->filename;
            }

            if (!empty($import_logged)) {
                $files = array_diff($list, $import_logged);            
            } else {
                $files = $list;
            }

            if ($files) {
                $this->call('importAsin:run');
                $this->call('importSales:run');
            } else {
                return $this->info("No new json sales exist!");
            }
        }
        catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
