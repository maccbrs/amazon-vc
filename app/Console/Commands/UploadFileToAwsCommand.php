<?php

namespace App\Console\Commands;

use App\Utilities\Constant;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Carbon\Carbon;

class UploadFileToAwsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'uploadAws:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Upload File to AWS';

    /**
     * Create a new command instance.
     *
     * @returGetReportScheduleCommand void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * { function_description }
     * @return     <type>  ( description_of_the_return_value )
     */
    public function handle()
    {
        for ($x = 6; $x <= 160; $x++) {
            shell_exec('aws s3 cp E:\mysql_amzvc\amzvc.zip.'.sprintf("%02d", $x).' s3://visibly-temp/');
        }        
    }

}
