<?php

namespace App\Console\Commands;

use App\Http\Controllers\{PreOrdersController,
};
use App\Utilities\Constant;
use Illuminate\Console\Command;

class ImportPreorderCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'importPreorder:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Command for PreOrder Reports';

    /**
     * Create a new command instance.
     *
     * @returGetReportScheduleCommandn void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param GetReportRequestListController $requestListController
     * @param GetReportListController $getReportListController
     * @param GetReportController $getReportController
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle(PreOrdersController $preorderController)
    {
        try {
            $importPreorder = $preorderController->importPreOrdersJSON();

            if (!$importPreorder) {
                $this->info("No new preorders exist!");
            return;
            }
            $this->info("Pre Orders has been successfully imported!");
        }
        catch (Exception $e) {
            return $e->getMessage();
        }
    }

}
