<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Illuminate\Support\Facades\Redis; 
 
Route::get('/test', function () { 
     $visits = Redis::incr('visits'); 
     return $visits; 
});

    $router->get('/', function () use ($router) {
        return $router->app->version();
    });

    $router->get('/test', function ()  {
        return view('test');
    });

    $router->get('/test_index', function () use ($router) {
        Log::warning('Clicking on test link!!!');
        return 'test';
    });

    Route::get('scraper_logs', 'AsinsController@logProductScraper');
/*
    $router->group([
        "as" => "scraper_logs",
        "prefix" => "scraper_logs",
    ], function() use($router) {
        $router
            ->post("test", [
                "as" => "scraper",
                "uses" => "AsinsController@logProductScraper",
            ]);
    });*/

$router->group(['prefix' => 'api/v1/', 'middleware' => 'auth'], function() use (&$router) {

    $router->get('profile/{user}', function(App\Models\User $user) { // This just a sample route model binding

        return $user;
    });

    $router->group([
        "as" => "cookie",
        "prefix" => "cookies",
    ], function() use($router) {
        $router
            ->get("/", [
                "as" => "cookies",
                "uses" => "CookiesController@getCookies_backup",
            ]);
    });

    $router->group([
        "as" => "import",
        "prefix" => "import",
    ], function() use($router) {
        $router
            ->post("/asin", [
                "as" => ".asin",
                "uses" => "AsinsController@importProduct",
            ])
            ->post("/json/asin", [
                "as" => "json.asin",
                "uses" => "AsinsController@importProductJSON",
            ])
            ->post("/sales", [
                "as" => ".sales",
                "uses" => "SalesDiagnosticsController@importSalesDiagnostics",
            ])
            ->post("/json/sales", [
                "as" => "json.sales",
                "uses" => "SalesDiagnosticsController@importSalesDiagnosticsJSON",
            ])
            ->post("/geo", [
                "as" => ".geo",
                "uses" => "GeographicSalesController@importGeographicSales",
            ])
            ->post("/json/geo", [
                "as" => "json.geo",
                "uses" => "GeographicSalesController@importGeographicSalesJSON",
            ])
            ->post("/netppm/test", [
                "as" => ""
            ])
            ->post("/pre", [
                "as" => ".pre",
                "uses" => "PreOrdersController@importPreOrders",
            ])
            ->post("/json/pre", [
                "as" => "json.pre",
                "uses" => "PreOrdersController@importPreOrdersJSON",
            ])
            ->post("/inv", [
                "as" => ".inv",
                "uses" => "InventoriesController@importInventories",
            ])
            ->post("/json/inv", [
                "as" => "json.inv",
                "uses" => "InventoriesController@importInventoriesJSON",
            ])
            ->post("/alt", [
                "as" => ".alt",
                "uses" => "AlternativePurchaseController@importAlternativePurchase",
            ])
            ->post("/json/alt", [
                "as" => "json.alt",
                "uses" => "AlternativePurchaseController@importAlternativePurchaseJSON",
            ])
            ->post("/acc", [
                "as" => ".acc",
                "uses" => "AccountsController@importAccounts"
            ]);
    });

    $router->group([
        "as" => "netppm",
        "prefix" => "netppm",
    ], function() use($router) {
        $router
            ->get("", [
                "as" => ".collection",
                "uses" => "NetPPMController@getCollection",
            ])
            ->get("/{netppm}", [
                "as" => ".entity",
                "uses" => "NetPPMController@getEntity"
            ]);
    });

    $router->group([
        "as" => "account",
        "prefix" => "account",
    ], function() use($router) {
        $router
            ->get("", [
                "as" => ".collection",
                "uses" => "AccountsController@getCollection",
            ])
            ->get("/{account}", [
                "as" => ".entity",
                "uses" => "AccountsController@getEntity", 
            ]);
    });

    $router->group([
        "as" => "category",
        "prefix" => "category",
    ], function() use($router) {
        $router
            ->get("", [
                "as" => ".collection",
                "uses" => "CategoriesController@getcollection",
            ]);
    });

    $router->group([
        "as" => "sale",
        "prefix" => "sales",
    ], function() use($router) {
        $router
            ->get("", [
                "as" => ".collection",
                "uses" => "SalesDiagnosticsController@collection",
            ])
            ->get("{diagnostics}", [
                "as" => ".get.collection",
                "uses" => "SalesDiagnosticsController@getCollection",
            ]);

    });


    $router->group([
        "as" => "geo",
        "prefix" => "geos",
    ], function() use($router) {
        $router
            ->get("/", [
                "as" => "geo",
                "uses" => "GeographicSalesController@getAll",
            ]);
    });


    $router->group([
        "as" => "preorder",
        "prefix" => "preorders",
    ], function() use($router) {
        $router
            ->get("/", [
                "as" => "preorder",
                "uses" => "PreOrdersController@getAll",
            ]);
    });

    $router->group([
        "as" => "inventory",
        "prefix" => "inventories",
    ], function() use($router) {
        $router
            ->get("/", [
                "as" => "inventory",
                "uses" => "InventoriesController@getAll",
            ]);
    });

    $router->group([
        "as" => "alternative",
        "prefix" => "alternatives",
    ], function() use($router) {
        $router
            ->get("/" ,[
                "as" => "alternative",
                "uses" => "AlternativePurchaseController@getAll",
            ])
            ->get("/altDates" ,[
                "as" => "altDates",
                "uses" => "AlternativePurchaseController@checkMissingDates",
            ]);
    });

    $router->group([
        "as" => "report",
        "prefix" => "report",
    ], function() use($router) {
        $router
            ->get("summary", [
                "as" => ".getSummary",                   
                "uses" => "SummariesController@getSummary",
            ])
            ->get("totalasin", [
                "as" => "totalasin",
                "uses" => "ReportsController@getSalesAsinTotal",
            ])
            ->get("{type}", [
                "as" => ".getCollection",                   
                "uses" => "ReportsController@getCollection",
            ]);
    });

    $router->group([
        "as" => "asin",
        "prefix" => "asin",
    ], function() use($router) {
        $router
            ->get("", [
                "as" => ".collection",
                "uses" => "AsinsController@getCollection",
            ])
            ->get("/{asin}", [
                "as" => ".entity",
                "uses" => "AsinsController@getEntity", 
            ]);
    });

    $router->group([
        "as" => "test",
        "prefix" => "test",
    ], function() use($router) {
        $router
            ->get("/sales", [
                "as" => ".test.sales",                   
                "uses" => "SalesDiagnosticsController@importSalesDiagnosticsJSON",
            ])
            ->get("/sales/rename", [
                "as" => ".test.sales",                   
                "uses" => "SalesDiagnosticsController@renameJSON",
            ])
            ->get("/asins", [
                "as" => ".test.asins",                   
                "uses" => "AsinsController@importProductJSON",
            ]);
    });

    $router->group([
        "as" => "test",
        "prefix" => "test",
    ], function() use ($router) {
        $router
            ->get("/test", [
                "as" => ".test",
                "uses" => "test@test",
            ]);
    });

});