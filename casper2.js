var casper = require('casper').create({
    clientScripts: ["jquery.js"],
    viewportSize: {width: 1920, height: 1080}
});

casper.options.waitTimeout = 999999;
var x = require('casper').selectXPath;

var fs = require('fs');
var AMAZON_SITE = 'https://vendorcentral.amazon.com/analytics/dashboard/salesDiagnostic';
var AMAZON_USER = 'amazon@channelbakers.com';
var AMAZON_PASS = 'CBAmaz0nCB!'; 
var cookie_dir = 'cookies';
var cookie_path = cookie_dir + '/cookies.txt';
var emailInput = 'input#ap_email';
var passInput = 'input#ap_password';
var otp = '#auth-mfa-otpcode';
var otp_remember = '#auth-mfa-remember-device';
var captcha = 'input#auth-captcha-guess';
var x      = require('casper').selectXPath;
var system = require('system');
var utils = require('utils');
var moment = require('moment.js');
var dt = new Date();
var months = ["January","February","March","April","May","June","July","August","September","October","November","December"];
var date = dt.getDate();
var year = dt.getFullYear();
var month = months[dt.getMonth()];

casper.userAgent('Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 Edge/18.18362');
// Read Cookies
if (fs.isFile(cookie_path)){ 
  phantom.cookies = JSON.parse(fs.read(cookie_path));
}

casper.start(AMAZON_SITE, function() {  
    console.log("Amazon website opened [" + AMAZON_SITE + "]");
    this.capture('opened.png');
});


casper.wait(1000, function() {
  if(this.exists("form[name=signIn]")) {
    console.log("need to login");

    this.capture('login_form.png');//print screen shot after login

    if(casper.exists('#ap_email')){
      this.wait(1000, function(){

        this.mouseEvent('click', emailInput, '15%', '48%');
        this.sendKeys('input#ap_email', AMAZON_USER);
      });
    }

    this.wait(3000, function () {
      this.mouseEvent('click', passInput, '12%', '67%');
      this.sendKeys('input#ap_password', AMAZON_PASS);
      this.click('input[name=rememberMe]');
   });

    this.then(function() {
        this.click('input#signInSubmit');
    }).wait(5000, function(){});

    this.capture('login_submitted.png');

    // write the cookies
    this.wait(1000, function() {
        var cookies = JSON.stringify(phantom.cookies);
        fs.write(cookie_path, cookies, 644);
    });

    casper.then(function (e) {
      if(casper.exists('#auth-captcha-image-container')){
          this.captureSelector('captcha.png', '#auth-captcha-image');
          console.log('captcha:');
          var captcha_code = system.stdin.readLine();

    this.wait(3000, function () {
      this.mouseEvent('click', passInput, '12%', '67%');
      this.sendKeys('input#ap_password', AMAZON_PASS);
    });

    this.wait(3000, function () {
      this.mouseEvent('click', captcha, '12%', '67%');
      this.sendKeys(captcha, captcha_code);
    });

    this.then(function() {
        console.log('submit captcha');
        this.click('input#signInSubmit');
    }).wait(5000, function(){});

      }
      else if(this.exists('#auth-mfa-otpcode')) {
        console.log('otp:');
        var otp_code = system.stdin.readLine();

        this.wait(3000, function () {
          this.mouseEvent('click', otp, '12%', '67%');
          this.sendKeys(otp, otp_code, {keepFocus: true});
          this.mouseEvent('click', otp_remember);
          this.sendKeys(otp, casperer.page.event.key.Enter , {keepFocus: true});
          this.capture('otp.png');
        });

        this.wait(1000, function() {
            var cookies = JSON.stringify(phantom.cookies);
            fs.write(cookie_path, cookies, 644);
        });

        this.capture('submit_otp_2.png');
      }

      else {
        console.log("Logged in successfully.");
      }

      this.capture('amazon1.png');//print screen shot after login

      });

    // triggerFail('Login');
  }
  else {
    console.log("already logged in");
    this.echo(this.getCurrentUrl());
    this.capture('vendor.png');

    casper.start(AMAZON_SITE, function() {  
        console.log("Amaxzon website opened [" + AMAZON_SITE + "]");
        this.capture('opened.png');
    });

    casper.waitForSelector('#vendor-group-switch-modal > a', function() {
        console.log('choose account...');
    this.echo(this.getCurrentUrl());
        this.click(x('//*[@id="vendor-group-switch-modal"]/a'));
        this.capture('account.png');

        this.waitForSelector('#vendor-group-switch-account-form > div:nth-child(1) > div > div', function() {
          this.capture('modal.png');
          var accounts = this.getElementInfo(x("//*[@class='a-label a-radio-label']"));
          fs.write('account.txt',accounts);
          this.click(x('//*[contains(text(),"DE - Logitech GmbH")]'));
          var account = this.fetchText(x('//*[contains(text(),"DE - Logitech GmbH")]'));
          this.capture('search.png');
            this.click(x('//*[@id="vendor-group-switch-confirm-button"]/span/input'));
        });
    });

    casper.wait(2000, function(){
      this.thenOpen(AMAZON_SITE);

      this.wait(5000, function(){
        this.capture('sales.png');
      });
    });

    casper.then(function(){
      console.log('navigating...');
      this.click(x('//*[@id="dashboard-filter-viewFilter"]/div/awsui-button-dropdown/div/button'));
      this.capture('view_filter.png');
      this.click(x('//*[@id="dashboard-filter-viewFilter"]/div/awsui-button-dropdown/div/div/ul/li[3]/a'));
      this.capture('select_view.png');
      this.click(x('//*[@id="dashboard-filter-reportingRange"]/div/awsui-button-dropdown/div/button'));
      this.capture('range_filter.png');
      this.click(x('//*[@id="dashboard-filter-reportingRange"]/div/awsui-button-dropdown/div/div/ul/li[1]/a'));
      var view = this.fetchText(x('/html/body/div[2]/div[2]/div/div/div/div[1]/div[2]/div[5]/div[2]/div/div[1]/span[3]/div/awsui-button-dropdown/div/button/span[1]/span/span/strong'));
      this.capture('select_range.png');

      var days = [];
      for (g = 1; g <= 31; g++) {
        days.push(g);
      };
        this.each(days, function (self, day) {
          this.waitForSelector('#salesDiagnosticDetail > div:nth-child(1) > div:nth-child(6)',function(){
            this.click(x('/html/body/div[2]/div[2]/div/div/div/div[1]/div[2]/div[5]/div[2]/div/div[3]/span[2]/div/div/div[4]'));
            this.click(x('/html/body/div[2]/div[2]/div/div/div/div[1]/div[2]/div[5]/div[2]/div/div[3]/span[2]/div/div/div[1]/input'));
            this.capture('month_filter.png');
            console.log('\n'+ 'navigating to required start month');
            var i;
            var limit = 12;
            console.log(text);
              for (i = 11; i <= limit; i++) { 
                  this.click(x('/html/body/div[4]/div/a[1]'));
                  this.capture('check_from.png');
              }
              var text = this.fetchText(x('/html/body/div[4]/div/div[2]/div[1]/div[1]'));
              console.log(text);
              this.click(x('//*[@class="react-datepicker__day" and @aria-label="day-'+day+'"]'));
              this.capture('date_from.png')
            
            this.then(function(){
              this.click(x('/html/body/div[2]/div[2]/div/div/div/div[1]/div[2]/div[5]/div[2]/div/div[3]/span[2]/div/div/div[3]/input'));
              console.log('\n'+ 'navigating to required end month');
              var i;
              var limit = 12;
              var text = this.fetchText(x('/html/body/div[4]/div/div[2]/div[1]/div[1]'));
              console.log(text);

                for (i = 11; i <= limit; i++) {
                    this.click(x('/html/body/div[4]/div/a[1]'));
                    this.capture('check_to.png');                    
                }
                var text = this.fetchText(x('/html/body/div[4]/div/div[2]/div[1]/div[1]'));
                console.log(text);

                month = text;
                date = this.fetchText(x('//*[@class="react-datepicker__day react-datepicker__day--range-start react-datepicker__day--in-range" and contains(text(),'+day+')]'));
                  this.click(x('//*[@class="react-datepicker__day react-datepicker__day--range-start react-datepicker__day--in-range" and @aria-label="day-'+day+'"]'));
                this.capture('date_to.png');
                this.click(x('//*[@id="dashboard-filter-applyCancel"]/div/awsui-button[2]/button'));
                console.log('applied filter.');
            });

            this.then(function(){
            this.page.captureContent = [ /.json/ ];

            this.page.onResourceReceived = function(response) {
            //console.log('Response (#' + response.id + ', stage "' + response.stage + '"): ' + JSON.stringify(response));
            if(response.stage!="end" || !response.bodySize)
              return;

              var split = month.split(" ");

              var matches = response.url.match(/\b(\w*Detail\w*)\b/);
              var fname = matches[1]+'_'+view.replace(/ /g,"_")+'('+account+')'+'-'+split[0]+' '+date+', '+split[1]+'.json';
              console.log(fname);
              fname = fname.replace(/[/\\?%*:|"<>]/g, '-');
              console.log("Saving "+response.bodySize+" bytes to "+fname);
              fs.write(fname,response.body);
              };

            });

            casper.waitForSelector('#salesDiagnosticDetail > div:nth-child(1) > div:nth-child(6)', function(){
              console.log('finished loading.');
              this.capture('applied.png');
            }); 
          
          });
        });
      });
  }
});

casper.run();
