var casper = require('casper').create({
    clientScripts: ["jquery.js"],
    viewportSize: {width: 1920, height: 1080}
});

casper.javascriptEnabled = true;
//page.settings.javascriptEnabled = true;
//runner.page.javascriptEnabled = true;
//casper.page.javascriptEnabled = true;
phantom.cookiesEnabled = true;
//casper.settings.userAgent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0';

casper.options.waitTimeout = 999999;
var x = require('casper').selectXPath;

var fs = require('fs');
var AMAZON_SITE = 'https://vendorcentral.amazon.com/analytics/dashboard/snapshot?ref_=vc_ven-ven-home_subNav';
var AMAZON_USER = 'vendorcentral@channelbakers.com';
var AMAZON_PASS = 'VendorCentral1!'; 
var cookie_dir = 'cookies';
var cookie_path = cookie_dir + '/cookies_sales.txt';
var emailInput = 'input#ap_email';
var passInput = 'input#ap_password';
var otp = '#auth-mfa-otpcode';
var otp_remember = '#auth-mfa-remember-device';
var captcha = 'input#auth-captcha-guess';
var x      = require('casper').selectXPath;
var system = require('system');
var utils = require('utils');
var moment = require('moment.js');
var months = [];
var dt = new Date();
var dd = dt.getDate();
var prod_dd = dt.getDate() - 1;
var mm = dt.getMonth() + 1; //January is 0!

var yyyy = dt.getFullYear();
if (dd < 10) {
  dd = '0' + dd;
} 
if (mm < 10) {
  mm = '0' + mm;
} 

if (prod_dd < 10) {
  prod_dd = '0' + dd;
} 
if (mm < 10) {
  mm = '0' + mm;
} 

var today = yyyy +''+ mm +''+ dd;
var prod_today = yyyy +''+ mm +''+ prod_dd;

casper.userAgent('Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0');

function isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

function logged_in() {
    console.log("already logged in");
      var vgids = [];
      var userid = [];


        casper.then(function() {

          this.thenOpen('https://vendorcentral.amazon.com/analytics/dashboard/snapshot?ref_=vc_ven-ven-home_subNav');

          this.then(function() {
            var user_info = JSON.stringify(this.getGlobal('userInfo'));
            var obj = JSON.parse(user_info);
            var subaccounts = obj.subAccounts;
            fs.write('accounts.json', JSON.stringify(subaccounts));
            var userpref = JSON.parse(obj.userPreferencesString); 
            userid.push(userpref.obfuscatedUserId);

            subaccounts.forEach(function (subaccount) {
              if(subaccount.retailSubscriptionLevel == 'PREMIUM' && subaccount.status == 'VALID' && subaccount.vendorGroupId != '499560' && subaccount.vendorGroupId != '708590' && subaccount.vendorGroupId != '4517650' && subaccount.vendorGroupId != '473520' && subaccount.vendorGroupId != '3256120' && subaccount.vendorGroupId != '453300'){
              var x = subaccount.vendorGroupId;
                vgids.push(x);
              }
            });
          });

      this.then(function() {
        var days = [];
/*        for (g = 1; g <= 31; g++) {
          days.push(g);
        };*/
        var counts = [0];
        console.log(vgids);
          this.each(vgids, function (self, vgid) {

            if (vgid == "473520") {
              var years = ["2019"];
            }
            else {
              var years = ["2018", "2019"];
            }

            this.thenOpen('https://vendorcentral.amazon.com/hz/vendor/members/user-management/switch-accounts-checker?vendorGroup='+vgid+'&customerId='+userid+'');

            this.thenOpen('https://vendorcentral.amazon.com/analytics/dashboard/snapshot?ref_=vc_ven-ven-home_subNav');

            this.then(function() {
              var token = this.getGlobal('token');
                //product
                this.then(function() {

                  this.then(function() {
                      this.then(function() {
                        if (vgid != '') {
                          this.wait(1000, function () {
                            this.thenOpen('https://vendorcentral.amazon.com/analytics/data/dashboard/productCatalog/report/productCatalogDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                                method: 'POST',
                                data:   {"reportParameters":[{"parameterId":"distributorView","values":[{"val":"manufacturer"}]},{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"dataRefreshDate","values":[{"val":prod_today}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"releasedate","ascending":false}],"reportPagination":{"pageIndex":0,"pageSize":100}}},
                                headers: {
                                    'Accept':'application/json',
                                    'Content-Type':'application/json',
                                }
                            });
                          });

                          this.then(function(){
                            fs.write('public/count.json', this.page.plainText);
                            var obj = JSON.parse(this.getPageContent());

                              if(obj && obj['payload'] && obj['payload']['reportParts'] && obj['payload']['reportParts'][0] && obj['payload']['reportParts'][0]['rowCount']) {
                                var rowCount = obj['payload']['reportParts'][0]['rowCount'];
                              console.log('RowCount: '+rowCount+'');

                                var paginate = rowCount.replace(/\,/g,'');
                                paginate = parseInt(paginate,10);
                                var number = paginate/10000;
                                var pages = Math.floor(number);

                                var parts = [];
                                for (h = 0; h <= pages; h++) {
                                  parts.push(h);
                                }

                                this.each(parts, function (self, part) {
                                  if (fs.isFile('public/products/ProductCatalog_manufacturing_'+vgid+'_'+prod_today+'_part'+part+'.json')){ 
                                    console.log('Product Already Scraped!');
                                  } else {
                                  this.wait(1000, function() {
                                    this.thenOpen('https://vendorcentral.amazon.com/analytics/data/dashboard/productCatalog/report/productCatalogDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                                        method: 'POST',
                                        data:   {"reportParameters":[{"parameterId":"distributorView","values":[{"val":"manufacturer"}]},{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"dataRefreshDate","values":[{"val":prod_today}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"releasedate","ascending":false}],"reportPagination":{"pageIndex":part,"pageSize":10000}}},
                                        headers: {
                                            'Accept':'application/json',
                                            'Content-Type':'application/json',
                                        }
                                    });
                                    this.then(function(){
                                      console.log('ProductCatalog_manufacturing_'+vgid+'_'+prod_today+'_part'+part+'.json');
                                      fs.write('public/products/ProductCatalog_manufacturing_'+vgid+'_'+prod_today+'_part'+part+'.json', this.page.plainText);
                                    });
                                  });
                                  }
                                });
                              } else {
                                console.log('ProductCatalog_manufacturing_'+vgid+'_'+prod_today+' : NONE');
                              }
                          });
                        }
                      });
                  });

                  this.then(function() {
                      this.then(function() {
                        if (vgid != '') {
                          this.wait(1000, function () {
                            this.thenOpen('https://vendorcentral.amazon.com/analytics/data/dashboard/productCatalog/report/productCatalogDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                                method: 'POST',
                                data:   {"reportParameters":[{"parameterId":"distributorView","values":[{"val":"sourcing"}]},{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"dataRefreshDate","values":[{"val":prod_today}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"releasedate","ascending":false}],"reportPagination":{"pageIndex":0,"pageSize":100}}},
                                headers: {
                                    'Accept':'application/json',
                                    'Content-Type':'application/json',
                                }
                            });
                          });

                          this.then(function(){
                            fs.write('public/count.json', this.page.plainText);
                            var obj = JSON.parse(this.getPageContent());

                              if(obj && obj['payload'] && obj['payload']['reportParts'] && obj['payload']['reportParts'][0] && obj['payload']['reportParts'][0]['rowCount']) {
                                var rowCount = obj['payload']['reportParts'][0]['rowCount'];
                              console.log('RowCount: '+rowCount+'');

                                var paginate = rowCount.replace(/\,/g,'');
                                paginate = parseInt(paginate,10);
                                var number = paginate/10000;
                                var pages = Math.floor(number);

                                var parts = [];
                                for (h = 0; h <= pages; h++) {
                                  parts.push(h);
                                }

                                this.each(parts, function (self, part) {
                                  if (fs.isFile('public/products/ProductCatalog_sourcing_'+vgid+'_'+prod_today+'_part'+part+'.json')){ 
                                    console.log('Product Already Scraped!');
                                  } else {
                                  this.wait(1000, function() {
                                    this.thenOpen('https://vendorcentral.amazon.com/analytics/data/dashboard/productCatalog/report/productCatalogDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                                        method: 'POST',
                                        data:   {"reportParameters":[{"parameterId":"distributorView","values":[{"val":"sourcing"}]},{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"dataRefreshDate","values":[{"val":prod_today}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"releasedate","ascending":false}],"reportPagination":{"pageIndex":part,"pageSize":10000}}},
                                        headers: {
                                            'Accept':'application/json',
                                            'Content-Type':'application/json',
                                        }
                                    });
                                    this.then(function(){
                                      console.log('ProductCatalog_sourcing_'+vgid+'_'+prod_today+'_part'+part+'.json');
                                      fs.write('public/products/ProductCatalog_sourcing_'+vgid+'_'+prod_today+'_part'+part+'.json', this.page.plainText);
                                    });
                                  });
                                  }
                                });
                              } else {
                                console.log('ProductCatalog_sourcing_'+vgid+'_'+prod_today+' : NONE');
                              }
                          });
                        }
                      });
                  });

                  this.then(function() {
                      this.then(function() {
                        if (vgid != '') {
                          this.wait(1000, function () {
                            this.thenOpen('https://vendorcentral.amazon.com/analytics/data/dashboard/productCatalog/report/productCatalogDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                                method: 'POST',
                                data:   {"reportParameters":[{"parameterId":"distributorView","values":[{"val":"consignment"}]},{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"dataRefreshDate","values":[{"val":prod_today}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"releasedate","ascending":false}],"reportPagination":{"pageIndex":0,"pageSize":100}}},
                                headers: {
                                    'Accept':'application/json',
                                    'Content-Type':'application/json',
                                }
                            });
                          });

                          this.then(function(){
                            fs.write('public/count.json', this.page.plainText);
                            var obj = JSON.parse(this.getPageContent());

                              if(obj && obj['payload'] && obj['payload']['reportParts'] && obj['payload']['reportParts'][0] && obj['payload']['reportParts'][0]['rowCount']) {
                                var rowCount = obj['payload']['reportParts'][0]['rowCount'];
                              console.log('RowCount: '+rowCount+'');

                                var paginate = rowCount.replace(/\,/g,'');
                                paginate = parseInt(paginate,10);
                                var number = paginate/10000;
                                var pages = Math.floor(number);

                                var parts = [];
                                for (h = 0; h <= pages; h++) {
                                  parts.push(h);
                                }

                                this.each(parts, function (self, part) {
                                  if (fs.isFile('public/products/ProductCatalog_consignment_'+vgid+'_'+prod_today+'_part'+part+'.json')){ 
                                    console.log('Product Already Scraped!');
                                  } else {
                                  this.wait(1000, function() {
                                    this.thenOpen('https://vendorcentral.amazon.com/analytics/data/dashboard/productCatalog/report/productCatalogDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                                        method: 'POST',
                                        data:   {"reportParameters":[{"parameterId":"distributorView","values":[{"val":"consignment"}]},{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"dataRefreshDate","values":[{"val":prod_today}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"releasedate","ascending":false}],"reportPagination":{"pageIndex":part,"pageSize":10000}}},
                                        headers: {
                                            'Accept':'application/json',
                                            'Content-Type':'application/json',
                                        }
                                    });
                                    this.then(function(){
                                      console.log('ProductCatalog_consignment_'+vgid+'_'+prod_today+'_part'+part+'.json');
                                      fs.write('public/products/ProductCatalog_consignment_'+vgid+'_'+prod_today+'_part'+part+'.json', this.page.plainText);
                                    });
                                  });
                                  }
                                });
                              } else {
                                console.log('ProductCatalog_consignment_'+vgid+'_'+prod_today+' : NONE');
                              }
                          });
                        }
                      });
                  });


                });

            this.each(years, function (self, year) {
              if (year == '2019') {
                for (x = 1; x <= 9; x++) {
                  months.push(x);
                } 
              }
              else if (year == '2018' && vgid == '3256120') {
                for (x = 10; x <= 12; x++) {
                  months.push(x);
                } 
              }
              else {
                for (x = 9; x <= 12; x++) {
                  months.push(x);
                }
              }
              this.each(months, function (self, month) {
                var sel_month = String("0" + month).slice(-2); //month.toString().padStart(2, "0");
                if (month == 10 && vgid == '3256120' && year == '2018'){
                  for (g = 12; g <= 31; g++) {
                    days.push(g);
                  }
                }
                else {
                  for (g = 1; g <= 31; g++) {
                    days.push(g);
                  }
                }
                this.each(days, function (self, day) {
                  var sel_day = String("0" + day).slice(-2); //day.toString().padStart(2, "0");
                  var sel_date = year+sel_month+sel_day;

                  this.each(counts, function (self, count) {
                    //geographic
                    /*this.wait(1000, function () {
                    this.thenOpen('https://vendorcentral.amazon.com/analytics/data/dashboard/geographicSalesInsights/report/geographicSalesInsightsDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara', {
                        method: 'POST',
                        data:   {"reportParameters":[{"parameterId":"viewFilter","values":[{"val":"shippedCOGSLevel"}]},{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"country","values":[{"val":"US"}]},{"parameterId":"state","values":[{"val":"ALL"}]},{"parameterId":"city","values":[{"val":"ALL"}]},{"parameterId":"zip","values":[{"val":"ALL"}]},{"parameterId":"aggregationFilter","values":[{"val":"ASINLevel"}]},{"parameterId":"countryCode","values":[{"val":"US"}]},{"parameterId":"periodStartDay","values":[{"val":sel_date}]},{"parameterId":"periodEndDay","values":[{"val":sel_date}]},{"parameterId":"isPeriodToDate","values":[{"val":false}]},{"parameterId":"isCustomDateRange","values":[{"val":"false"}]},{"parameterId":"hideLastYear","values":[{"val":false}]},{"parameterId":"period","values":[{"val":"DAILY"}]},{"parameterId":"dataRefreshDate","values":[{"val":"0000093999302"}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]},{"parameterId":"parentASINVisibility","values":[{"val":true}]},{"parameterId":"eanVisibility","values":[{"val":true}]},{"parameterId":"isbn13Visibility","values":[{"val":true}]},{"parameterId":"upcVisibility","values":[{"val":true}]},{"parameterId":"janVisibility","values":[{"val":false}]},{"parameterId":"brandVisibility","values":[{"val":true}]},{"parameterId":"brandCodeVisibility","values":[{"val":false}]},{"parameterId":"subcatVisibility","values":[{"val":true}]},{"parameterId":"catVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeWidthVisibility","values":[{"val":true}]},{"parameterId":"authorVisibility","values":[{"val":true}]},{"parameterId":"bindingVisibility","values":[{"val":true}]},{"parameterId":"catalogNumberVisibility","values":[{"val":false}]},{"parameterId":"colorVisibility","values":[{"val":true}]},{"parameterId":"colorCountVisibility","values":[{"val":false}]},{"parameterId":"manufactureOnDemandVisibility","values":[{"val":false}]},{"parameterId":"listPriceVisibility","values":[{"val":false}]},{"parameterId":"modelStyleVisibility","values":[{"val":true}]},{"parameterId":"productGroupVisibility","values":[{"val":false}]},{"parameterId":"releaseDateVisibility","values":[{"val":false}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"shippedcogs","ascending":false}],"reportPagination":{"pageIndex":0,"pageSize":100}}},
                        headers: {'Accept':'application/json', 'Content-Type':'application/json'}
                      });
                      this.then(function(){
                        fs.write('public/count.json', this.page.plainText);
                        var obj = JSON.parse(this.getPageContent());
                        
                          if (obj && obj['payload'] && obj['payload']['reportParts'] && obj['payload']['reportParts'][0] && obj['payload']['reportParts'][0]['rowCount']) {
                          var rowCount = obj['payload']['reportParts'][0]['rowCount'];
                          var paginate = rowCount.replace(/\,/g,'');
                            paginate = parseInt(paginate,10);
                            var number = paginate/10000;
                            var pages = Math.floor(number);

                            var parts = [];
                            for (h = 0; h <= pages; h++) {
                              parts.push(h);
                            }

                            this.each(parts, function (self, part) {
                              this.wait(1000, function() {
                                

                                  this.thenOpen('https://vendorcentral.amazon.com/analytics/data/dashboard/geographicSalesInsights/report/geographicSalesInsightsDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                                      method: 'POST',
                                      data:   {"reportParameters":[{"parameterId":"viewFilter","values":[{"val":"shippedCOGSLevel"}]},{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"country","values":[{"val":"US"}]},{"parameterId":"state","values":[{"val":"ALL"}]},{"parameterId":"city","values":[{"val":"ALL"}]},{"parameterId":"zip","values":[{"val":"ALL"}]},{"parameterId":"aggregationFilter","values":[{"val":"ASINLevel"}]},{"parameterId":"countryCode","values":[{"val":"US"}]},{"parameterId":"periodStartDay","values":[{"val":sel_date}]},{"parameterId":"periodEndDay","values":[{"val":sel_date}]},{"parameterId":"isPeriodToDate","values":[{"val":false}]},{"parameterId":"isCustomDateRange","values":[{"val":"false"}]},{"parameterId":"hideLastYear","values":[{"val":false}]},{"parameterId":"period","values":[{"val":"DAILY"}]},{"parameterId":"dataRefreshDate","values":[{"val":"0000093999302"}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]},{"parameterId":"parentASINVisibility","values":[{"val":true}]},{"parameterId":"eanVisibility","values":[{"val":true}]},{"parameterId":"isbn13Visibility","values":[{"val":true}]},{"parameterId":"upcVisibility","values":[{"val":true}]},{"parameterId":"janVisibility","values":[{"val":false}]},{"parameterId":"brandVisibility","values":[{"val":true}]},{"parameterId":"brandCodeVisibility","values":[{"val":false}]},{"parameterId":"subcatVisibility","values":[{"val":true}]},{"parameterId":"catVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeWidthVisibility","values":[{"val":true}]},{"parameterId":"authorVisibility","values":[{"val":true}]},{"parameterId":"bindingVisibility","values":[{"val":true}]},{"parameterId":"catalogNumberVisibility","values":[{"val":false}]},{"parameterId":"colorVisibility","values":[{"val":true}]},{"parameterId":"colorCountVisibility","values":[{"val":false}]},{"parameterId":"manufactureOnDemandVisibility","values":[{"val":false}]},{"parameterId":"listPriceVisibility","values":[{"val":false}]},{"parameterId":"modelStyleVisibility","values":[{"val":true}]},{"parameterId":"productGroupVisibility","values":[{"val":false}]},{"parameterId":"releaseDateVisibility","values":[{"val":false}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"shippedcogs","ascending":false}],"reportPagination":{"pageIndex":part,"pageSize":10000}}},
                                      headers: {
                                          'Accept':'application/json',
                                          'Content-Type':'application/json',
                                      }
                                  });
                                  this.then(function(){
                                    console.log('GeographicSalesDetail_retail_'+vgid+'_'+sel_date+'_part'+part+'.json');
                                    fs.write('public/products/geographic/GeographicSalesDetail_retail_'+vgid+'_'+sel_date+'_part'+part+'.json', this.page.plainText);
                                  });
                              });
                            });
                          } else {
                            console.log('GeographicSalesDetail_retail_'+vgid+'_'+sel_date+' : NONE');
                          }
                      });
                    });*/

                    //sales
                    this.then(function() {
                      //retail
/*                      this.then(function() {
                        this.wait(1000, function () {
                          this.thenOpen('https://vendorcentral.amazon.com/analytics/data/dashboard/salesDiagnostic/report/salesDiagnosticDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                            method: 'POST',
                            data:   {"requestId":"b8e3de1a-2a10-4fe2-9e58-f19e68753e3a","reportParameters":[{"parameterId":"distributorView","values":[{"val":"manufacturer"}]},{"parameterId":"viewFilter","values":[{"val":"shippedCOGSLevel"}]},{"parameterId":"productView","values":[{"val":"kindleExcluded"}]},{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"aggregationFilter","values":[{"val":"ASINLevel"}]},{"parameterId":"periodStartDay","values":[{"val":sel_date}]},{"parameterId":"periodEndDay","values":[{"val":sel_date}]},{"parameterId":"isPeriodToDate","values":[{"val":false}]},{"parameterId":"isCustomDateRange","values":[{"val":"false"}]},{"parameterId":"hideLastYear","values":[{"val":false}]},{"parameterId":"period","values":[{"val":"DAILY"}]},{"parameterId":"dataRefreshDate","values":[{"val":"0000790022800"}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]},{"parameterId":"parentASINVisibility","values":[{"val":true}]},{"parameterId":"eanVisibility","values":[{"val":true}]},{"parameterId":"isbn13Visibility","values":[{"val":true}]},{"parameterId":"upcVisibility","values":[{"val":true}]},{"parameterId":"janVisibility","values":[{"val":false}]},{"parameterId":"brandVisibility","values":[{"val":true}]},{"parameterId":"brandCodeVisibility","values":[{"val":false}]},{"parameterId":"subcatVisibility","values":[{"val":true}]},{"parameterId":"catVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeWidthVisibility","values":[{"val":true}]},{"parameterId":"authorVisibility","values":[{"val":true}]},{"parameterId":"bindingVisibility","values":[{"val":true}]},{"parameterId":"catalogNumberVisibility","values":[{"val":false}]},{"parameterId":"colorVisibility","values":[{"val":true}]},{"parameterId":"colorCountVisibility","values":[{"val":false}]},{"parameterId":"manufactureOnDemandVisibility","values":[{"val":false}]},{"parameterId":"listPriceVisibility","values":[{"val":false}]},{"parameterId":"modelStyleVisibility","values":[{"val":true}]},{"parameterId":"productGroupVisibility","values":[{"val":false}]},{"parameterId":"releaseDateVisibility","values":[{"val":true}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"shippedcogs","ascending":false}],"reportPagination":{"pageIndex":0,"pageSize":100}}},
                            headers: {
                                'Accept':'application/json',
                                'Content-Type':'application/json',
                            }
                          });
                        });

                        this.then(function(){
                          fs.write('public/sales_retail_count.json', this.page.plainText);

                          var obj = JSON.parse(this.getPageContent());

                            if (obj && obj['payload'] && obj['payload']['reportParts'] && obj['payload']['reportParts'][0] && obj['payload']['reportParts'][0]['rowCount']) {
                              var rowCount = obj['payload']['reportParts'][0]['rowCount'];
                              console.log('RowCount: '+rowCount+'');
                              var paginate = rowCount.replace(/\,/g,'');
                              paginate = parseInt(paginate,10);
                              var number = paginate/10000;
                              var pages = Math.floor(number);

                              var parts = [];
                              for (h = 0; h <= pages; h++) {
                                parts.push(h);
                              }

                              this.each(parts, function (self, part) {
                                this.wait(1000, function() {
                                  
                                  this.thenOpen('https://vendorcentral.amazon.com/analytics/data/dashboard/salesDiagnostic/report/salesDiagnosticDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                                      method: 'POST',
                                      data:   {
                                          "requestId":"b8e3de1a-2a10-4fe2-9e58-f19e68753e3a","reportParameters":[{"parameterId":"distributorView","values":[{"val":"manufacturer"}]},{"parameterId":"viewFilter","values":[{"val":"shippedCOGSLevel"}]},{"parameterId":"productView","values":[{"val":"kindleExcluded"}]},{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"aggregationFilter","values":[{"val":"ASINLevel"}]},{"parameterId":"periodStartDay","values":[{"val":sel_date}]},{"parameterId":"periodEndDay","values":[{"val":sel_date}]},{"parameterId":"isPeriodToDate","values":[{"val":false}]},{"parameterId":"isCustomDateRange","values":[{"val":"false"}]},{"parameterId":"hideLastYear","values":[{"val":false}]},{"parameterId":"period","values":[{"val":"DAILY"}]},{"parameterId":"dataRefreshDate","values":[{"val":"0000790022800"}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]},{"parameterId":"parentASINVisibility","values":[{"val":true}]},{"parameterId":"eanVisibility","values":[{"val":true}]},{"parameterId":"isbn13Visibility","values":[{"val":true}]},{"parameterId":"upcVisibility","values":[{"val":true}]},{"parameterId":"janVisibility","values":[{"val":false}]},{"parameterId":"brandVisibility","values":[{"val":true}]},{"parameterId":"brandCodeVisibility","values":[{"val":false}]},{"parameterId":"subcatVisibility","values":[{"val":true}]},{"parameterId":"catVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeWidthVisibility","values":[{"val":true}]},{"parameterId":"authorVisibility","values":[{"val":true}]},{"parameterId":"bindingVisibility","values":[{"val":true}]},{"parameterId":"catalogNumberVisibility","values":[{"val":false}]},{"parameterId":"colorVisibility","values":[{"val":true}]},{"parameterId":"colorCountVisibility","values":[{"val":false}]},{"parameterId":"manufactureOnDemandVisibility","values":[{"val":false}]},{"parameterId":"listPriceVisibility","values":[{"val":false}]},{"parameterId":"modelStyleVisibility","values":[{"val":true}]},{"parameterId":"productGroupVisibility","values":[{"val":false}]},{"parameterId":"releaseDateVisibility","values":[{"val":true}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"shippedcogs","ascending":false}],"reportPagination":{"pageIndex":part,"pageSize":10000}}
                                      },
                                      headers: {
                                          'Accept':'application/json',
                                          'Content-Type':'application/json',
                                      }
                                  });
                                  this.then(function(){
                                    console.log('SalesDiagnosticsDetail_retail_'+vgid+'_'+sel_date+'_part'+part+'.json');
                                    fs.write('public/products/salesdiagnostics/SalesDiagnosticsDetail_retail_'+vgid+'_'+sel_date+'_part'+part+'.json', this.page.plainText);
                                  });
                                });
                              });
                            }
                            else {
                              console.log('SalesDiagnosticsDetail_retail_'+vgid+'_'+sel_date+' : NONE');
                            }
                        });
                    });*/
                      //fresh
                      this.then(function() {
                        this.wait(1000, function () {
                          this.thenOpen('https://vendorcentral.amazon.com/analytics/data/dashboard/freshSalesDiagnostic/report/freshSalesDiagnosticDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                            method: 'POST',
                            data:   {"requestId":"741b983e-a9c6-449f-a9e8-e885fa13d88b","reportParameters":[{"parameterId":"viewFilter","values":[{"val":"shippedCOGSLevel"}]},{"parameterId":"city","values":[{"val":"ALL"}]},{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"aggregationFilter","values":[{"val":"ASINLevel"}]},{"parameterId":"isPeriodToDate","values":[{"val":false}]},{"parameterId":"isCustomDateRange","values":[{"val":"false"}]},{"parameterId":"hideLastYear","values":[{"val":false}]},{"parameterId":"period","values":[{"val":"DAILY"}]},{"parameterId":"dataRefreshDate","values":[{"val":"0001024832551"}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]},{"parameterId":"parentASINVisibility","values":[{"val":true}]},{"parameterId":"eanVisibility","values":[{"val":true}]},{"parameterId":"isbn13Visibility","values":[{"val":true}]},{"parameterId":"upcVisibility","values":[{"val":true}]},{"parameterId":"janVisibility","values":[{"val":false}]},{"parameterId":"brandVisibility","values":[{"val":true}]},{"parameterId":"brandCodeVisibility","values":[{"val":false}]},{"parameterId":"subcatVisibility","values":[{"val":true}]},{"parameterId":"catVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeWidthVisibility","values":[{"val":true}]},{"parameterId":"authorVisibility","values":[{"val":true}]},{"parameterId":"bindingVisibility","values":[{"val":true}]},{"parameterId":"catalogNumberVisibility","values":[{"val":false}]},{"parameterId":"colorVisibility","values":[{"val":true}]},{"parameterId":"colorCountVisibility","values":[{"val":false}]},{"parameterId":"manufactureOnDemandVisibility","values":[{"val":false}]},{"parameterId":"listPriceVisibility","values":[{"val":false}]},{"parameterId":"modelStyleVisibility","values":[{"val":true}]},{"parameterId":"productGroupVisibility","values":[{"val":false}]},{"parameterId":"releaseDateVisibility","values":[{"val":false}]},{"parameterId":"periodStartDay","values":[{"val":sel_date}]},{"parameterId":"periodEndDay","values":[{"val":sel_date}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"shippedcogs","ascending":false}],"reportPagination":{"pageIndex":0,"pageSize":100}}},
                            headers: {
                                'Accept':'application/json',
                                'Content-Type':'application/json',
                            }
                          });
                        });

                        this.then(function(){
                          fs.write('public/sales_fresh_count.json', this.page.plainText);
                          var obj = JSON.parse(this.getPageContent());
                            if( obj && obj['payload'] && obj['payload']['reportParts'] && obj['payload']['reportParts'][0] && obj['payload']['reportParts'][0]['rowCount'] ) {
                              var rowCount = obj['payload']['reportParts'][0]['rowCount'];
                              console.log('RowCount: '+rowCount+'');
                              var paginate = rowCount.replace(/\,/g,'');
                              paginate = parseInt(paginate,10);
                              var number = paginate/10000;
                              var pages = Math.floor(number);

                              var parts = [];
                              for (h = 0; h <= pages; h++) {
                                parts.push(h);
                              }

                              this.each(parts, function (self, part) {
/*                                if (fs.isFile('public/products/salesdiagnostics/SalesDiagnosticsDetail_fresh_'+vgid+'_'+sel_date+'_part'+part+'.json')){ 
                                  console.log(sel_date+': Sales fresh already Scraped!');
                                } else {*/
                                  this.wait(3000, function() {
                                    
                                    this.thenOpen('https://vendorcentral.amazon.com/analytics/data/dashboard/freshSalesDiagnostic/report/freshSalesDiagnosticDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                                        method: 'POST',
                                        data:   {"requestId":"741b983e-a9c6-449f-a9e8-e885fa13d88b","reportParameters":[{"parameterId":"viewFilter","values":[{"val":"shippedCOGSLevel"}]},{"parameterId":"city","values":[{"val":"ALL"}]},{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"aggregationFilter","values":[{"val":"ASINLevel"}]},{"parameterId":"isPeriodToDate","values":[{"val":false}]},{"parameterId":"isCustomDateRange","values":[{"val":"false"}]},{"parameterId":"hideLastYear","values":[{"val":false}]},{"parameterId":"period","values":[{"val":"DAILY"}]},{"parameterId":"dataRefreshDate","values":[{"val":"0001024832551"}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]},{"parameterId":"parentASINVisibility","values":[{"val":true}]},{"parameterId":"eanVisibility","values":[{"val":true}]},{"parameterId":"isbn13Visibility","values":[{"val":true}]},{"parameterId":"upcVisibility","values":[{"val":true}]},{"parameterId":"janVisibility","values":[{"val":false}]},{"parameterId":"brandVisibility","values":[{"val":true}]},{"parameterId":"brandCodeVisibility","values":[{"val":false}]},{"parameterId":"subcatVisibility","values":[{"val":true}]},{"parameterId":"catVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeWidthVisibility","values":[{"val":true}]},{"parameterId":"authorVisibility","values":[{"val":true}]},{"parameterId":"bindingVisibility","values":[{"val":true}]},{"parameterId":"catalogNumberVisibility","values":[{"val":false}]},{"parameterId":"colorVisibility","values":[{"val":true}]},{"parameterId":"colorCountVisibility","values":[{"val":false}]},{"parameterId":"manufactureOnDemandVisibility","values":[{"val":false}]},{"parameterId":"listPriceVisibility","values":[{"val":false}]},{"parameterId":"modelStyleVisibility","values":[{"val":true}]},{"parameterId":"productGroupVisibility","values":[{"val":false}]},{"parameterId":"releaseDateVisibility","values":[{"val":false}]},{"parameterId":"periodStartDay","values":[{"val":sel_date}]},{"parameterId":"periodEndDay","values":[{"val":sel_date}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"shippedcogs","ascending":false}],"reportPagination":{"pageIndex":part,"pageSize":10000}}},
                                        headers: {
                                            'Accept':'application/json',
                                            'Content-Type':'application/json',
                                        }
                                    });
                                    this.then(function(){
                                      console.log('SalesDiagnosticsDetail_fresh_'+vgid+'_'+sel_date+'_part'+part+'.json');
                                      fs.write('public/products/salesdiagnostics/SalesDiagnosticsDetail_fresh_'+vgid+'_'+sel_date+'_part'+part+'.json', this.page.plainText);
                                    });
                                  });
/*                                }*/
                              });
                            } else {
                              console.log('SalesDiagnosticsDetail_fresh_'+vgid+'_'+sel_date+' : NONE');
                            }
                        });
                    });
                      //prime
                      this.then(function() {
                        this.wait(1000, function () {
                          this.thenOpen('https://vendorcentral.amazon.com/analytics/data/dashboard/primeNowSalesDiagnostic/report/primeNowSalesDiagnosticDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                            method: 'POST',
                            data:   {"requestId":"23399442-a038-4ba4-a7db-fee9c55c3c4b","reportParameters":[{"parameterId":"viewFilter","values":[{"val":"shippedCOGSLevel"}]},{"parameterId":"city","values":[{"val":"ALL"}]},{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"aggregationFilter","values":[{"val":"ASINLevel"}]},{"parameterId":"isPeriodToDate","values":[{"val":false}]},{"parameterId":"isCustomDateRange","values":[{"val":"false"}]},{"parameterId":"hideLastYear","values":[{"val":false}]},{"parameterId":"period","values":[{"val":"DAILY"}]},{"parameterId":"dataRefreshDate","values":[{"val":"0001116193202"}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]},{"parameterId":"parentASINVisibility","values":[{"val":true}]},{"parameterId":"eanVisibility","values":[{"val":true}]},{"parameterId":"isbn13Visibility","values":[{"val":true}]},{"parameterId":"upcVisibility","values":[{"val":true}]},{"parameterId":"janVisibility","values":[{"val":false}]},{"parameterId":"brandVisibility","values":[{"val":true}]},{"parameterId":"brandCodeVisibility","values":[{"val":false}]},{"parameterId":"subcatVisibility","values":[{"val":true}]},{"parameterId":"catVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeWidthVisibility","values":[{"val":true}]},{"parameterId":"authorVisibility","values":[{"val":true}]},{"parameterId":"bindingVisibility","values":[{"val":true}]},{"parameterId":"catalogNumberVisibility","values":[{"val":false}]},{"parameterId":"colorVisibility","values":[{"val":true}]},{"parameterId":"colorCountVisibility","values":[{"val":false}]},{"parameterId":"manufactureOnDemandVisibility","values":[{"val":false}]},{"parameterId":"listPriceVisibility","values":[{"val":false}]},{"parameterId":"modelStyleVisibility","values":[{"val":true}]},{"parameterId":"productGroupVisibility","values":[{"val":false}]},{"parameterId":"releaseDateVisibility","values":[{"val":false}]},{"parameterId":"periodStartDay","values":[{"val":sel_date}]},{"parameterId":"periodEndDay","values":[{"val":sel_date}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"shippedcogs","ascending":false}],"reportPagination":{"pageIndex":0,"pageSize":100}}},
                            headers: {
                                'Accept':'application/json',
                                'Content-Type':'application/json',
                            }
                          });
                        });

                        this.then(function(){
                          fs.write('public/sales_prime_count.json', this.page.plainText);
                          var obj = JSON.parse(this.getPageContent());
                            if( obj && obj['payload'] && obj['payload']['reportParts'] && obj['payload']['reportParts'][0] && obj['payload']['reportParts'][0]['rowCount'] ) {
                              var rowCount = obj['payload']['reportParts'][0]['rowCount'];
                              console.log('RowCount: '+rowCount+'');
                              var paginate = rowCount.replace(/\,/g,'');
                              paginate = parseInt(paginate,10);
                              var number = paginate/10000;
                              var pages = Math.floor(number);

                              var parts = [];
                              for (h = 0; h <= pages; h++) {
                                parts.push(h);
                              }

                              this.each(parts, function (self, part) {
/*                                  if (fs.isFile('public/products/salesdiagnostics/SalesDiagnosticsDetail_prime_'+vgid+'_'+sel_date+'_part'+part+'.json')){ 
                                    console.log(sel_date+': Sales Business Already Scraped!');
                                  } else {*/
                                    this.wait(3000, function() {
                                      
                                      this.thenOpen('https://vendorcentral.amazon.com/analytics/data/dashboard/primeNowSalesDiagnostic/report/primeNowSalesDiagnosticDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                                          method: 'POST',
                                          data:   {"requestId":"23399442-a038-4ba4-a7db-fee9c55c3c4b","reportParameters":[{"parameterId":"viewFilter","values":[{"val":"shippedCOGSLevel"}]},{"parameterId":"city","values":[{"val":"ALL"}]},{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"aggregationFilter","values":[{"val":"ASINLevel"}]},{"parameterId":"isPeriodToDate","values":[{"val":false}]},{"parameterId":"isCustomDateRange","values":[{"val":"false"}]},{"parameterId":"hideLastYear","values":[{"val":false}]},{"parameterId":"period","values":[{"val":"DAILY"}]},{"parameterId":"dataRefreshDate","values":[{"val":"0001116193202"}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]},{"parameterId":"parentASINVisibility","values":[{"val":true}]},{"parameterId":"eanVisibility","values":[{"val":true}]},{"parameterId":"isbn13Visibility","values":[{"val":true}]},{"parameterId":"upcVisibility","values":[{"val":true}]},{"parameterId":"janVisibility","values":[{"val":false}]},{"parameterId":"brandVisibility","values":[{"val":true}]},{"parameterId":"brandCodeVisibility","values":[{"val":false}]},{"parameterId":"subcatVisibility","values":[{"val":true}]},{"parameterId":"catVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeWidthVisibility","values":[{"val":true}]},{"parameterId":"authorVisibility","values":[{"val":true}]},{"parameterId":"bindingVisibility","values":[{"val":true}]},{"parameterId":"catalogNumberVisibility","values":[{"val":false}]},{"parameterId":"colorVisibility","values":[{"val":true}]},{"parameterId":"colorCountVisibility","values":[{"val":false}]},{"parameterId":"manufactureOnDemandVisibility","values":[{"val":false}]},{"parameterId":"listPriceVisibility","values":[{"val":false}]},{"parameterId":"modelStyleVisibility","values":[{"val":true}]},{"parameterId":"productGroupVisibility","values":[{"val":false}]},{"parameterId":"releaseDateVisibility","values":[{"val":false}]},{"parameterId":"periodStartDay","values":[{"val":sel_date}]},{"parameterId":"periodEndDay","values":[{"val":sel_date}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"shippedcogs","ascending":false}],"reportPagination":{"pageIndex":part,"pageSize":100}}},
                                          headers: {
                                              'Accept':'application/json',
                                              'Content-Type':'application/json',
                                          }
                                      });
                                      this.then(function(){
                                        console.log('SalesDiagnosticsDetail_prime_'+vgid+'_'+sel_date+'_part'+part+'.json');
                                        fs.write('public/products/salesdiagnostics/SalesDiagnosticsDetail_prime_'+vgid+'_'+sel_date+'_part'+part+'.json', this.page.plainText);
                                      }); 
                                    });
/*                                  }*/
                              });
                            } else {
                              console.log('SalesDiagnosticsDetail_prime_'+vgid+'_'+sel_date+' : NONE');
                            }
                        });
                    });
                      //business
                      this.then(function() {
                        this.wait(1000, function () {
                          this.thenOpen('https://vendorcentral.amazon.com/analytics/data/dashboard/businessSalesDiagnostic/report/businessSalesDiagnosticDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                            method: 'POST',
                            data:   {"requestId":"3d2ae09e-77e1-4b5c-9b00-ec0e02cb8499","reportParameters":[{"parameterId":"viewFilter","values":[{"val":"shippedCOGSLevel"}]},{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"aggregationFilter","values":[{"val":"ASINLevel"}]},{"parameterId":"periodStartDay","values":[{"val":sel_date}]},{"parameterId":"periodEndDay","values":[{"val":sel_date}]},{"parameterId":"isPeriodToDate","values":[{"val":true}]},{"parameterId":"isCustomDateRange","values":[{"val":"false"}]},{"parameterId":"hideLastYear","values":[{"val":false}]},{"parameterId":"period","values":[{"val":"DAILY"}]},{"parameterId":"dataRefreshDate","values":[{"val":"0000078742193"}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]},{"parameterId":"parentASINVisibility","values":[{"val":true}]},{"parameterId":"eanVisibility","values":[{"val":true}]},{"parameterId":"isbn13Visibility","values":[{"val":true}]},{"parameterId":"upcVisibility","values":[{"val":true}]},{"parameterId":"janVisibility","values":[{"val":false}]},{"parameterId":"brandVisibility","values":[{"val":true}]},{"parameterId":"brandCodeVisibility","values":[{"val":false}]},{"parameterId":"subcatVisibility","values":[{"val":true}]},{"parameterId":"catVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeWidthVisibility","values":[{"val":true}]},{"parameterId":"authorVisibility","values":[{"val":true}]},{"parameterId":"bindingVisibility","values":[{"val":true}]},{"parameterId":"catalogNumberVisibility","values":[{"val":false}]},{"parameterId":"colorVisibility","values":[{"val":true}]},{"parameterId":"colorCountVisibility","values":[{"val":false}]},{"parameterId":"manufactureOnDemandVisibility","values":[{"val":false}]},{"parameterId":"listPriceVisibility","values":[{"val":false}]},{"parameterId":"modelStyleVisibility","values":[{"val":true}]},{"parameterId":"productGroupVisibility","values":[{"val":false}]},{"parameterId":"releaseDateVisibility","values":[{"val":false}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"shippedcogs","ascending":false}],"reportPagination":{"pageIndex":0,"pageSize":100}}},
                            headers: {
                                'Accept':'application/json',
                                'Content-Type':'application/json',
                            }
                          });
                        });

                        this.then(function(){
                          fs.write('public/sales_business_count.json', this.page.plainText);

                          var obj = JSON.parse(this.getPageContent());

                            if( obj && obj['payload'] && obj['payload']['reportParts'] && obj['payload']['reportParts'][0] && obj['payload']['reportParts'][0]['rowCount'] ) {
                              var rowCount = obj['payload']['reportParts'][0]['rowCount'];
                              console.log('RowCount: '+rowCount+'');

                              var paginate = rowCount.replace(/\,/g,'');
                              paginate = parseInt(paginate,10);
                              var number = paginate/10000;
                              var pages = Math.floor(number);

                              var parts = [];
                              for (h = 0; h <= pages; h++) {
                                parts.push(h);
                              }

                              this.each(parts, function (self, part) {
/*                                if (fs.isFile('public/products/salesdiagnostics/SalesDiagnosticsDetail_business_'+vgid+'_'+sel_date+'_part'+part+'.json')){ 
                                  console.log(sel_date+': Sales Business Already Scraped!');
                                } else {*/
                                  this.wait(3000, function() {
                                    
                                    this.thenOpen('https://vendorcentral.amazon.com/analytics/data/dashboard/businessSalesDiagnostic/report/businessSalesDiagnosticDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                                        method: 'POST',
                                        data:   {"requestId":"3d2ae09e-77e1-4b5c-9b00-ec0e02cb8499","reportParameters":[{"parameterId":"viewFilter","values":[{"val":"shippedCOGSLevel"}]},{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"aggregationFilter","values":[{"val":"ASINLevel"}]},{"parameterId":"periodStartDay","values":[{"val":sel_date}]},{"parameterId":"periodEndDay","values":[{"val":sel_date}]},{"parameterId":"isPeriodToDate","values":[{"val":true}]},{"parameterId":"isCustomDateRange","values":[{"val":"false"}]},{"parameterId":"hideLastYear","values":[{"val":false}]},{"parameterId":"period","values":[{"val":"DAILY"}]},{"parameterId":"dataRefreshDate","values":[{"val":"0000078742193"}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]},{"parameterId":"parentASINVisibility","values":[{"val":true}]},{"parameterId":"eanVisibility","values":[{"val":true}]},{"parameterId":"isbn13Visibility","values":[{"val":true}]},{"parameterId":"upcVisibility","values":[{"val":true}]},{"parameterId":"janVisibility","values":[{"val":false}]},{"parameterId":"brandVisibility","values":[{"val":true}]},{"parameterId":"brandCodeVisibility","values":[{"val":false}]},{"parameterId":"subcatVisibility","values":[{"val":true}]},{"parameterId":"catVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeWidthVisibility","values":[{"val":true}]},{"parameterId":"authorVisibility","values":[{"val":true}]},{"parameterId":"bindingVisibility","values":[{"val":true}]},{"parameterId":"catalogNumberVisibility","values":[{"val":false}]},{"parameterId":"colorVisibility","values":[{"val":true}]},{"parameterId":"colorCountVisibility","values":[{"val":false}]},{"parameterId":"manufactureOnDemandVisibility","values":[{"val":false}]},{"parameterId":"listPriceVisibility","values":[{"val":false}]},{"parameterId":"modelStyleVisibility","values":[{"val":true}]},{"parameterId":"productGroupVisibility","values":[{"val":false}]},{"parameterId":"releaseDateVisibility","values":[{"val":false}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"shippedcogs","ascending":false}],"reportPagination":{"pageIndex":part,"pageSize":10000}}},
                                        headers: {
                                            'Accept':'application/json',
                                            'Content-Type':'application/json',
                                        }
                                    });
                                    this.then(function(){
                                      console.log('SalesDiagnosticsDetail_business_'+vgid+'_'+sel_date+'_part'+part+'.json');
                                      fs.write('public/products/salesdiagnostics/SalesDiagnosticsDetail_business_'+vgid+'_'+sel_date+'_part'+part+'.json', this.page.plainText);
                                    });
                                  });
/*                                }*/
                              });
                            }
                            else {
                              console.log('SalesDiagnosticsDetail_business_'+vgid+'_'+sel_date+' : NONE')
                            }
                        });
                    });
                });

                    //preorders
/*                    this.then(function() {
                      this.wait(1000, function () {
                        this.thenOpen('https://vendorcentral.amazon.com/analytics/data/dashboard/preOrders/report/preOrdersDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                            method: 'POST',
                            data:   {"reportParameters":[{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"aggregationFilter","values":[{"val":"ASINLevel"}]},{"parameterId":"periodStartDay","values":[{"val":sel_date}]},{"parameterId":"periodEndDay","values":[{"val":sel_date}]},{"parameterId":"isPeriodToDate","values":[{"val":false}]},{"parameterId":"isCustomDateRange","values":[{"val":"false"}]},{"parameterId":"hideLastYear","values":[{"val":false}]},{"parameterId":"period","values":[{"val":"DAILY"}]},{"parameterId":"dataRefreshDate","values":[{"val":"0000502050172"}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]},{"parameterId":"parentASINVisibility","values":[{"val":true}]},{"parameterId":"eanVisibility","values":[{"val":true}]},{"parameterId":"isbn13Visibility","values":[{"val":true}]},{"parameterId":"upcVisibility","values":[{"val":true}]},{"parameterId":"janVisibility","values":[{"val":false}]},{"parameterId":"brandVisibility","values":[{"val":true}]},{"parameterId":"brandCodeVisibility","values":[{"val":false}]},{"parameterId":"subcatVisibility","values":[{"val":true}]},{"parameterId":"catVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeWidthVisibility","values":[{"val":true}]},{"parameterId":"authorVisibility","values":[{"val":true}]},{"parameterId":"bindingVisibility","values":[{"val":true}]},{"parameterId":"catalogNumberVisibility","values":[{"val":false}]},{"parameterId":"colorVisibility","values":[{"val":true}]},{"parameterId":"colorCountVisibility","values":[{"val":false}]},{"parameterId":"manufactureOnDemandVisibility","values":[{"val":false}]},{"parameterId":"listPriceVisibility","values":[{"val":false}]},{"parameterId":"modelStyleVisibility","values":[{"val":true}]},{"parameterId":"productGroupVisibility","values":[{"val":false}]},{"parameterId":"releaseDateVisibility","values":[{"val":true}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"preorderedamount","ascending":false}],"reportPagination":{"pageIndex":0,"pageSize":100}}},
                            headers: {
                                'Accept':'application/json',
                                'Content-Type':'application/json',
                            }
                        });
                      });

                      this.then(function(){
                        var parts = [];
                        fs.write('public/pre_count.json', this.page.plainText);
                        var obj = JSON.parse( this.getPageContent());
                          if(obj && obj['payload'] && obj['payload']['reportParts'] && obj['payload']['reportParts'][0] && obj['payload']['reportParts'][0]['rowCount']) {
                            var rowCount = obj['payload']['reportParts'][0]['rowCount'];
                            var paginate = rowCount.replace(/\,/g,'');
                            paginate = parseInt(paginate,10);
                            var number = paginate/10000;
                            var pages = Math.floor(number);
                            for (h = 0; h <= pages; h++) {
                              parts.push(h);
                            }
                            this.each(parts, function (self, part) {
                              this.wait(1000, function() {
                                this.thenOpen('https://vendorcentral.amazon.com/analytics/data/dashboard/preOrders/report/preOrdersDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                                    method: 'POST',
                                    data:   {"reportParameters":[{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"aggregationFilter","values":[{"val":"ASINLevel"}]},{"parameterId":"periodStartDay","values":[{"val":sel_date}]},{"parameterId":"periodEndDay","values":[{"val":sel_date}]},{"parameterId":"isPeriodToDate","values":[{"val":false}]},{"parameterId":"isCustomDateRange","values":[{"val":"false"}]},{"parameterId":"hideLastYear","values":[{"val":false}]},{"parameterId":"period","values":[{"val":"DAILY"}]},{"parameterId":"dataRefreshDate","values":[{"val":"0000502050172"}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]},{"parameterId":"parentASINVisibility","values":[{"val":true}]},{"parameterId":"eanVisibility","values":[{"val":true}]},{"parameterId":"isbn13Visibility","values":[{"val":true}]},{"parameterId":"upcVisibility","values":[{"val":true}]},{"parameterId":"janVisibility","values":[{"val":false}]},{"parameterId":"brandVisibility","values":[{"val":true}]},{"parameterId":"brandCodeVisibility","values":[{"val":false}]},{"parameterId":"subcatVisibility","values":[{"val":true}]},{"parameterId":"catVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeWidthVisibility","values":[{"val":true}]},{"parameterId":"authorVisibility","values":[{"val":true}]},{"parameterId":"bindingVisibility","values":[{"val":true}]},{"parameterId":"catalogNumberVisibility","values":[{"val":false}]},{"parameterId":"colorVisibility","values":[{"val":true}]},{"parameterId":"colorCountVisibility","values":[{"val":false}]},{"parameterId":"manufactureOnDemandVisibility","values":[{"val":false}]},{"parameterId":"listPriceVisibility","values":[{"val":false}]},{"parameterId":"modelStyleVisibility","values":[{"val":true}]},{"parameterId":"productGroupVisibility","values":[{"val":false}]},{"parameterId":"releaseDateVisibility","values":[{"val":true}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"preorderedamount","ascending":false}],"reportPagination":{"pageIndex":part,"pageSize":10000}}},
                                    headers: {
                                        'Accept':'application/json',
                                        'Content-Type':'application/json',
                                    }
                                });
                                this.then(function(){
                                  console.log('PreOrdersDetail_'+vgid+'_'+sel_date+'_part'+part+'.json');
                                  fs.write('public/products/preorder/PreOrdersDetail_'+vgid+'_'+sel_date+'_part'+part+'.json', this.page.plainText);
                                });
                              });
                            });
                          } else {
                            console.log('PreOrdersDetail_'+vgid+'_'+sel_date+' : NONE');
                          }
                      });

                    });*/

                    //inventory
/*                    this.then(function() {

                      this.wait(1000, function () {
                        this.thenOpen('https://vendorcentral.amazon.com/analytics/data/dashboard/inventoryHealth/report/inventoryHealthDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                            method: 'POST',
                            data:   {"requestId":"bdefc92c-7660-4b77-b928-83bed94f0682","reportParameters":[{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"aggregationFilter","values":[{"val":"ASINLevel"}]},{"parameterId":"distributorView","values":[{"val":"manufacturer"}]},{"parameterId":"isFCAllowed","values":[{"val":"false"}]},{"parameterId":"periodStartDay","values":[{"val":sel_date}]},{"parameterId":"periodEndDay","values":[{"val":sel_date}]},{"parameterId":"isPeriodToDate","values":[{"val":false}]},{"parameterId":"isCustomDateRange","values":[{"val":"false"}]},{"parameterId":"hideLastYear","values":[{"val":false}]},{"parameterId":"period","values":[{"val":"DAILY"}]},{"parameterId":"dataRefreshDate","values":[{"val":"0002032000337"}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]},{"parameterId":"parentASINVisibility","values":[{"val":true}]},{"parameterId":"eanVisibility","values":[{"val":true}]},{"parameterId":"isbn13Visibility","values":[{"val":true}]},{"parameterId":"upcVisibility","values":[{"val":true}]},{"parameterId":"janVisibility","values":[{"val":false}]},{"parameterId":"brandVisibility","values":[{"val":true}]},{"parameterId":"brandCodeVisibility","values":[{"val":true}]},{"parameterId":"subcatVisibility","values":[{"val":true}]},{"parameterId":"catVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeWidthVisibility","values":[{"val":true}]},{"parameterId":"authorVisibility","values":[{"val":true}]},{"parameterId":"bindingVisibility","values":[{"val":true}]},{"parameterId":"catalogNumberVisibility","values":[{"val":false}]},{"parameterId":"colorVisibility","values":[{"val":true}]},{"parameterId":"colorCountVisibility","values":[{"val":true}]},{"parameterId":"manufactureOnDemandVisibility","values":[{"val":false}]},{"parameterId":"listPriceVisibility","values":[{"val":true}]},{"parameterId":"modelStyleVisibility","values":[{"val":true}]},{"parameterId":"productGroupVisibility","values":[{"val":true}]},{"parameterId":"releaseDateVisibility","values":[{"val":true}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"sellableonhandinventory","ascending":false}],"reportPagination":{"pageIndex":0,"pageSize":100}}},
                            headers: {
                                'Accept':'application/json',
                                'Content-Type':'application/json',
                            }
                        });
                      });

                      this.then(function(){
                        fs.write('public/inv_count.json', this.page.plainText);
                        var obj = JSON.parse( this.getPageContent());
                        
                          if(obj && obj['payload'] && obj['payload']['reportParts'] && obj['payload']['reportParts'][0] && obj['payload']['reportParts'][0]['rowCount']) {
                            var rowCount = obj['payload']['reportParts'][0]['rowCount'];  
                            var paginate = rowCount.replace(/\,/g,'');
                            paginate = parseInt(paginate,10);
                            var number = paginate/10000;
                            var pages = Math.floor(number);

                            var parts = [];
                            for (h = 0; h <= pages; h++) {
                              parts.push(h);
                            }

                            this.each(parts, function (self, part) {
                              this.wait(1000, function() {
                                this.thenOpen('https://vendorcentral.amazon.com/analytics/data/dashboard/inventoryHealth/report/inventoryHealthDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                                    method: 'POST',
                                    data:   {"requestId":"bdefc92c-7660-4b77-b928-83bed94f0682","reportParameters":[{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"aggregationFilter","values":[{"val":"ASINLevel"}]},{"parameterId":"distributorView","values":[{"val":"manufacturer"}]},{"parameterId":"isFCAllowed","values":[{"val":"false"}]},{"parameterId":"periodStartDay","values":[{"val":sel_date}]},{"parameterId":"periodEndDay","values":[{"val":sel_date}]},{"parameterId":"isPeriodToDate","values":[{"val":false}]},{"parameterId":"isCustomDateRange","values":[{"val":"false"}]},{"parameterId":"hideLastYear","values":[{"val":false}]},{"parameterId":"period","values":[{"val":"DAILY"}]},{"parameterId":"dataRefreshDate","values":[{"val":"0002032000337"}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]},{"parameterId":"parentASINVisibility","values":[{"val":true}]},{"parameterId":"eanVisibility","values":[{"val":true}]},{"parameterId":"isbn13Visibility","values":[{"val":true}]},{"parameterId":"upcVisibility","values":[{"val":true}]},{"parameterId":"janVisibility","values":[{"val":false}]},{"parameterId":"brandVisibility","values":[{"val":true}]},{"parameterId":"brandCodeVisibility","values":[{"val":true}]},{"parameterId":"subcatVisibility","values":[{"val":true}]},{"parameterId":"catVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeWidthVisibility","values":[{"val":true}]},{"parameterId":"authorVisibility","values":[{"val":true}]},{"parameterId":"bindingVisibility","values":[{"val":true}]},{"parameterId":"catalogNumberVisibility","values":[{"val":false}]},{"parameterId":"colorVisibility","values":[{"val":true}]},{"parameterId":"colorCountVisibility","values":[{"val":true}]},{"parameterId":"manufactureOnDemandVisibility","values":[{"val":false}]},{"parameterId":"listPriceVisibility","values":[{"val":true}]},{"parameterId":"modelStyleVisibility","values":[{"val":true}]},{"parameterId":"productGroupVisibility","values":[{"val":true}]},{"parameterId":"releaseDateVisibility","values":[{"val":true}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"sellableonhandinventory","ascending":false}],"reportPagination":{"pageIndex":part,"pageSize":10000}}},
                                    headers: {
                                        'Accept':'application/json',
                                        'Content-Type':'application/json',
                                    }
                                });
                                this.then(function(){
                                  console.log('InventoryHealthDetail_'+vgid+'_'+sel_date+'_part'+part+'.json');
                                  fs.write('public/products/inventory/InventoryHealthDetail_'+vgid+'_'+sel_date+'_part'+part+'.json', this.page.plainText);
                                });
                              });
                            });
                          } else {
                            console.log('InventoryHealthDetail_'+vgid+'_'+sel_date+' : NONE');
                          }
                      });

                    });*/

                    //alternative
/*                    this.then(function() {

                      this.wait(1000, function () {
                        this.thenOpen('https://vendorcentral.amazon.com/analytics/data/dashboard/itemComparisonAndAlternativePurchase/report/alternativePurchase?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                            method: 'POST',
                            data:   {"requestId":"8d07b226-5105-49cc-994e-f4371692ddeb","reportParameters":[{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"alternativePurchaseAggregationFilter","values":[{"val":"allProductsLevel"}]},{"parameterId":"period","values":[{"val":"DAILY"}]},{"parameterId":"periodEndDay","values":[{"val":sel_date}]},{"parameterId":"dataRefreshDate","values":[{"val":"0001361388692"}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]},{"parameterId":"parentASINVisibility","values":[{"val":false}]},{"parameterId":"eanVisibility","values":[{"val":false}]},{"parameterId":"isbn13Visibility","values":[{"val":false}]},{"parameterId":"upcVisibility","values":[{"val":false}]},{"parameterId":"janVisibility","values":[{"val":false}]},{"parameterId":"brandVisibility","values":[{"val":false}]},{"parameterId":"brandCodeVisibility","values":[{"val":false}]},{"parameterId":"subcatVisibility","values":[{"val":false}]},{"parameterId":"catVisibility","values":[{"val":false}]},{"parameterId":"apparelSizeVisibility","values":[{"val":false}]},{"parameterId":"apparelSizeWidthVisibility","values":[{"val":false}]},{"parameterId":"authorVisibility","values":[{"val":false}]},{"parameterId":"bindingVisibility","values":[{"val":false}]},{"parameterId":"catalogNumberVisibility","values":[{"val":false}]},{"parameterId":"colorVisibility","values":[{"val":false}]},{"parameterId":"colorCountVisibility","values":[{"val":false}]},{"parameterId":"manufactureOnDemandVisibility","values":[{"val":false}]},{"parameterId":"listPriceVisibility","values":[{"val":false}]},{"parameterId":"modelStyleVisibility","values":[{"val":false}]},{"parameterId":"productGroupVisibility","values":[{"val":false}]},{"parameterId":"releaseDateVisibility","values":[{"val":false}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"orders","ascending":false}],"reportPagination":{"pageIndex":0,"pageSize":100}}},
                            headers: {
                                'Accept':'application/json',
                                'Content-Type':'application/json',
                            }
                        });
                      });

                      this.then(function(){
                        fs.write('public/alt_count.json', this.page.plainText);
                        var obj = JSON.parse( this.getPageContent());

                          if(obj && obj['payload'] && obj['payload']['reportParts'] && obj['payload']['reportParts'][0] && obj['payload']['reportParts'][0]['rowCount']) {
                            var rowCount = obj['payload']['reportParts'][0]['rowCount'];
                            var paginate = rowCount.replace(/\,/g,'');
                            paginate = parseInt(paginate,10);
                            var number = paginate/10000;
                            var pages = Math.floor(number);

                            var parts = [];
                            for (h = 0; h <= pages; h++) {
                              parts.push(h);
                            }

                            this.each(parts, function (self, part) {
                              this.wait(1000, function() {
                                this.thenOpen('https://vendorcentral.amazon.com/analytics/data/dashboard/itemComparisonAndAlternativePurchase/report/alternativePurchase?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                                    method: 'POST',
                                    data:   {"requestId":"8d07b226-5105-49cc-994e-f4371692ddeb","reportParameters":[{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"alternativePurchaseAggregationFilter","values":[{"val":"allProductsLevel"}]},{"parameterId":"period","values":[{"val":"DAILY"}]},{"parameterId":"periodEndDay","values":[{"val":sel_date}]},{"parameterId":"dataRefreshDate","values":[{"val":"0001361388692"}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]},{"parameterId":"parentASINVisibility","values":[{"val":false}]},{"parameterId":"eanVisibility","values":[{"val":false}]},{"parameterId":"isbn13Visibility","values":[{"val":false}]},{"parameterId":"upcVisibility","values":[{"val":false}]},{"parameterId":"janVisibility","values":[{"val":false}]},{"parameterId":"brandVisibility","values":[{"val":false}]},{"parameterId":"brandCodeVisibility","values":[{"val":false}]},{"parameterId":"subcatVisibility","values":[{"val":false}]},{"parameterId":"catVisibility","values":[{"val":false}]},{"parameterId":"apparelSizeVisibility","values":[{"val":false}]},{"parameterId":"apparelSizeWidthVisibility","values":[{"val":false}]},{"parameterId":"authorVisibility","values":[{"val":false}]},{"parameterId":"bindingVisibility","values":[{"val":false}]},{"parameterId":"catalogNumberVisibility","values":[{"val":false}]},{"parameterId":"colorVisibility","values":[{"val":false}]},{"parameterId":"colorCountVisibility","values":[{"val":false}]},{"parameterId":"manufactureOnDemandVisibility","values":[{"val":false}]},{"parameterId":"listPriceVisibility","values":[{"val":false}]},{"parameterId":"modelStyleVisibility","values":[{"val":false}]},{"parameterId":"productGroupVisibility","values":[{"val":false}]},{"parameterId":"releaseDateVisibility","values":[{"val":false}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"orders","ascending":false}],"reportPagination":{"pageIndex":part,"pageSize":10000}}},
                                    headers: {
                                        'Accept':'application/json',
                                        'Content-Type':'application/json',
                                    }
                                });
                                this.then(function(){
                                  console.log('AlternativePurchaseDetail_'+vgid+'_'+sel_date+'_part'+part+'.json');
                                  fs.write('public/products/alternate/AlternativePurchaseDetail_'+vgid+'_'+sel_date+'_part'+part+'.json', this.page.plainText);
                                });
                              });
                            });
                          } else {
                            console.log('AlternativePurchaseDetail_'+vgid+'_'+sel_date+' : NONE');
                          }
                      });
                    });*/

                  });
                });
              });
            });
          });
        });
      });
    });
  }

// Read Cookies
if (fs.isFile(cookie_path)){ 
  phantom.cookies = JSON.parse(fs.read(cookie_path));
}

casper.start(AMAZON_SITE, function() {  
    console.log("Amazon website opened [" + AMAZON_SITE + "]");
    this.capture('opened.png');
});


casper.wait(1000, function() {
  if(this.exists("form[name=signIn]")) {
    console.log("need to login");

    this.capture('login_form.png');//print screen shot after login

    if(casper.exists('#ap_email')){
      this.wait(1000, function(){
        this.mouseEvent('click', emailInput, '15%', '48%');
        this.sendKeys('input#ap_email', AMAZON_USER, { reset: true});
      });
    }

    this.wait(3000, function () {
      this.mouseEvent('click', passInput, '12%', '67%');
      this.sendKeys('input#ap_password', AMAZON_PASS);
      this.click('input[name=rememberMe]');
   });

    this.then(function() {
        this.click('input#signInSubmit');
    }).wait(5000, function(){});

    this.capture('login_submitted.png');

    // write the cookies
    this.wait(1000, function() {
        var cookies = JSON.stringify(phantom.cookies);
        fs.write(cookie_path, cookies, 644);
    });

    casper.then(function (e) {
      if(casper.exists('#auth-captcha-image-container')){
          this.captureSelector('captcha.png', '#auth-captcha-image');
          console.log('captcha:');
          var captcha_code = system.stdin.readLine();

    this.wait(3000, function () {
      this.mouseEvent('click', passInput, '12%', '67%');
      this.sendKeys('input#ap_password', AMAZON_PASS);
    });

    this.wait(3000, function () {
      this.mouseEvent('click', captcha, '12%', '67%');
      this.sendKeys(captcha, captcha_code);
    });

    this.then(function() {
        console.log('submit captcha');
        this.click('input#signInSubmit');
    }).wait(5000, function(){});

      }
      else if(this.exists('#auth-mfa-otpcode')) {
        console.log('otp:');
        var otp_code = system.stdin.readLine();

        this.wait(1000, function () {
          this.mouseEvent('click', otp, '12%', '67%');
          this.sendKeys(otp, otp_code, {keepFocus: true});
/*          this.sendKeys(otp, '253821');*/
          this.mouseEvent('click', otp_remember);
          this.click('input#auth-signin-button');
          this.sendKeys(otp, casper.page.event.key.Enter , {keepFocus: true});
          this.capture('otp.png');
          console.log('otp sent');
          
          this.then(function() {
            this.wait(3000, function() {
                var cookies = JSON.stringify(phantom.cookies);
                fs.write(cookie_path, cookies, 644);
            });

            this.capture('submit_otp_2.png');
          });

          this.then(function() {
            this.waitForSelector("#logo_topNav", function() {
              logged_in();
            });
          });

        });


      }

      else {

        console.log("Logged in successfully.");
        logged_in();
      }

      this.capture('amazon1.png');//print screen shot after login

      });

    // triggerFail('Login');
  }
  else {
  logged_in();
}
});

casper.run();
