var casper = require('casper').create({
    clientScripts: ["jquery.js"],
    viewportSize: {width: 1920, height: 1080}
});

casper.javascriptEnabled = true;
//page.settings.javascriptEnabled = true;
//runner.page.javascriptEnabled = true;
//casper.page.javascriptEnabled = true;
phantom.cookiesEnabled = true;
//casper.settings.userAgent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0';

casper.options.waitTimeout = 999999;
var x = require('casper').selectXPath;

var fs = require('fs');
var AMAZON_SITE = 'https://vendorcentral.amazon.com/analytics/dashboard/snapshot?ref_=vc_ven-ven-home_subNav';
var AMAZON_USER = 'vendorcentral@channelbakers.com';
var AMAZON_PASS = 'VendorCentral1!'; 
var cookie_dir = 'cookies';
var cookie_path = cookie_dir + '/cookies.txt';
var emailInput = 'input#ap_email';
var passInput = 'input#ap_password';
var otp = '#auth-mfa-otpcode';
var otp_remember = '#auth-mfa-remember-device';
var captcha = 'input#auth-captcha-guess';
var x      = require('casper').selectXPath;
var system = require('system');
var utils = require('utils');
var moment = require('moment.js');
var dt = new Date();

casper.userAgent('Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0');

function isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

// Read Cookies
if (fs.isFile(cookie_path)){ 
  phantom.cookies = JSON.parse(fs.read(cookie_path));
}

casper.start(AMAZON_SITE, function() {  
    console.log("Amazon website opened [" + AMAZON_SITE + "]");
    this.capture('opened.png');
});


casper.wait(1000, function() {
  if(this.exists("form[name=signIn]")) {
    console.log("need to login");

    this.capture('login_form.png');//print screen shot after login

    if(casper.exists('#ap_email')){
      this.wait(1000, function(){

        this.mouseEvent('click', emailInput, '15%', '48%');
        this.sendKeys('input#ap_email', AMAZON_USER);
      });
    }

    this.wait(3000, function () {
      this.mouseEvent('click', passInput, '12%', '67%');
      this.sendKeys('input#ap_password', AMAZON_PASS);
      this.click('input[name=rememberMe]');
   });

    this.then(function() {
        this.click('input#signInSubmit');
    }).wait(5000, function(){});

    this.capture('login_submitted.png');

    // write the cookies
    this.wait(1000, function() {
        var cookies = JSON.stringify(phantom.cookies);
        fs.write(cookie_path, cookies, 644);
    });

    casper.then(function (e) {
      if(casper.exists('#auth-captcha-image-container')){
          this.captureSelector('captcha.png', '#auth-captcha-image');
          console.log('captcha:');
          var captcha_code = system.stdin.readLine();

    this.wait(3000, function () {
      this.mouseEvent('click', passInput, '12%', '67%');
      this.sendKeys('input#ap_password', AMAZON_PASS);
    });

    this.wait(3000, function () {
      this.mouseEvent('click', captcha, '12%', '67%');
      this.sendKeys(captcha, captcha_code);
    });

    this.then(function() {
        console.log('submit captcha');
        this.click('input#signInSubmit');
    }).wait(5000, function(){});

      }
      else if(this.exists('#auth-mfa-otpcode')) {
        console.log('otp:');
        var otp_code = system.stdin.readLine();

        this.wait(3000, function () {
          this.mouseEvent('click', otp, '12%', '67%');
          this.sendKeys(otp, otp_code, {keepFocus: true});
          this.mouseEvent('click', otp_remember);
          this.sendKeys(otp, casper.page.event.key.Enter , {keepFocus: true});
          this.capture('otp.png');
        });

        this.wait(1000, function() {
            var cookies = JSON.stringify(phantom.cookies);
            fs.write(cookie_path, cookies, 644);
        });

        this.capture('submit_otp_2.png');
      }

      else {
        console.log("Logged in successfully.");
      }

      this.capture('amazon1.png');//print screen shot after login

      });

    // triggerFail('Login');
  }
  else {
    console.log("already logged in");

    casper.then(function() {
      this.thenOpen('https://vendorcentral.amazon.com/hz/vendor/members/user-management/switch-accounts-checker?vendorGroup=499560&customerId=A1GW1729DBKXU8');
      this.thenOpen('https://vendorcentral.amazon.com/analytics/dashboard/snapshot?ref_=vc_ven-ven-home_subNav');

        this.then(function() {
          fs.write('page.json', JSON.stringify(this.getPageContent()));
            var user_info = JSON.stringify(this.getGlobal('userInfo'));
            var obj = JSON.parse(user_info);
            var subaccounts = obj.subAccounts;
            var userpref = JSON.parse(obj.userPreferencesString); 
            var userid = userpref.obfuscatedUserId;
            //console.log(JSON.stringify(subaccounts));
            console.log(userid);

            subaccounts.forEach(function (subaccount) {
                if(subaccount.retailSubscriptionLevel == 'PREMIUM' && subaccount.status == 'VALID'){
                var x = subaccount.vendorGroupId;
                  console.log(x);
                }
            });
        });
    });

  }
});

casper.run();
