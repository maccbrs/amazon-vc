var casper = require('casper').create({
    viewportSize: { // adjust window size here
        width:  3840, // resolution for debugging usage only
        height: 2160, // everyone should use 4k screen
    },
    pageSettings: {
        userAgent:   'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36', // use this UA or amazon will reject cookie
        loadImages:  true,
        loadPlugins: false,
    },
});
var x      = require('casper').selectXPath;
var system = require('system');
var fs     = require('fs');
var start, end;
var counter = 0;
var weekly  = false;

var cookie_dir = 'cookies';
var cookie_path = cookie_dir + '/cookies.txt';
    
casper.options.waitTimeout = 120000; 

casper.options.onWaitTimeout = function(timeout, details) {
    var selector = details.selector.type === 'xpath' ? 
            details.selector.path : details.selector;
    this.echo('wait timed out after ' + timeout + ' msec with selector: ' + selector);
    // this.capture('.\\temp\\screenshot.png');
};


// initialization
casper.start('https://arap.amazon.com/analytics', function () {

// Read Cookies
/*if (fs.isFile(cookie_path)){ 
  phantom.cookies = JSON.parse(fs.read(cookie_path));
}
*/
    if (this.exists('#signInSubmit')) {
        c1B#qonsole.log("amzvc");
        if(casper.exists('#ap_email')){
            console.log('email_login');
          this.wait(1000, function(){
            console.log("Login using username and password");
            this.sendKeys('input#ap_email', 'amazon@channelbakers.com');
          });
        }

        this.wait(3000, function () {
            console.log('password');
          this.sendKeys('input#ap_password', 'CBAmaz0nCB!');
        });

        this.then(function() {
            console.log('submit');
            this.click('input#signInSubmit');
            this.capture('login-test.png');
        }).wait(5000, function(){
            this.capture('submitted.png');

            if(this.exists('#auth-captcha-guess')){
                this.captureSelector('captcha.png', '#auth-captcha-image');
                console.log('captcha:');
                var captcha = system.stdin.readLine();
                this.sendKeys('input#ap_password', 'CBAmaz0nCB!');
                this.sendKeys('input#auth-captcha-guess', captcha);
         
                this.then(function() {
                    console.log('submit captcha');
                    this.click('input#signInSubmit');
                    this.capture('test_captcha.png');
                }).wait(5000, function(){this.capture('submit_captcha.png');});
            }
        });




/*
        this.wait(1000, function() {
            var cookies = JSON.stringify(phantom.cookies);
            fs.write(cookie_path, cookies, 644);
        });
*/

/*        console.log('cookie is missing/expired');
        console.log('please enter login info:');
        console.log('username:');
        var username = system.stdin.readLine();
        console.log('password:');
        var password = system.stdin.readLine();
        this.sendKeys('#ap_email', username);
        this.sendKeys('#ap_password', password);
        this.click('input#signInSubmit');
        this.capture('click-test.png');*/
    }
    else {
        console.log('cookie is valid, continue');
    }

    if (this.cli.has('start') && this.cli.has('end')) {
        start = toAbsDate(this.cli.get('start'));
        end   = toAbsDate(this.cli.get('end'));
    }
    else {
        console.log('please enter date range:');
        console.log('start date (yyyy-mm-dd):');
        start = toAbsDate(system.stdin.readLine());
        console.log('end   date (yyyy-mm-dd):');
        end   = toAbsDate(system.stdin.readLine());
    }

    if (this.cli.has('weekly')) {
        weekly = this.cli.get('weekly');
    }
    else if (this.cli.has('daily')) {
        weekly = !this.cli.get('daily');
    }
    else {
        console.log('weekly report? [y/n]:');
        var answer = system.stdin.readLine();
        if (answer === 'y' || answer === 'Y') {
            weekly = true;
        }
    }

    if (weekly) {
        start = start - (start + 5) % 7;
        end   = end   - (end   + 5) % 7;
    }

    console.log('start date (abs): ' + start + ' ' + (start + 5) % 7);
    console.log('end   date (abs): ' + end   + ' ' + (end   + 5) % 7);

    this.capture('test-test.png');
});

// data scraping
casper.thenOpen('https://arap.amazon.com/analytics/dashboard/salesDiagnostic', function () {
    this.capture('test.png');
    for (var date = start; date <= end; date += (weekly ? 7 : 1)) {(function (date) { // issue: async; use function trick
        casper.then(function () { // cannot use 'this' here
            console.log('--------------------------');
            console.log('loading ' + toDateString(date, 'ISO') + ' data...');

            // set the period and date range   
            if (weekly) {
                var dateStartString = toDateString(date, 'US');
                var dateEndString   = toDateString(date + 6, 'US');
                this.evaluate(function (dateStartString, dateEndString) {
                    $('#vcDatePromptWeeklyDateStart').val(dateStartString).change(); // x('//*[@id="vcDatePromptWeeklyDateStart"]')
                    $('#vcDatePromptWeeklyDateEnd').val(dateEndString).change(); // x('//*[@id="vcDatePromptWeeklyDateEnd"]')
                }, dateStartString, dateEndString);
            }
            else {  
                var dateString = toDateString(date, 'US');
                this.evaluate(function (dateString) {
                    $('#vcDatePromptSelect').val('Daily').change(); // issue: constant only; use evalute to pass the argument
                    $('#vcDatePromptDailyDateStart').val(dateString).change(); // x('//*[@id="vcDatePromptDailyDateStart"]')
                    $('#vcDatePromptDailyDateEnd').val(dateString).change(); // x('//*[@id="vcDatePromptDailyDateEnd"]')
                }, dateString);
            }

            var stringViewID = this.getElementAttribute(x('//*[@id="idStatePathDiv"]'), 'statepath');

            // load report
            this.thenClick('#gobtn', function () {
                this.waitForSelector(x('//*[@id="' + stringViewID + 'Links"]'), function () {
                    var stringViewState   = this.getElementAttribute(x('//*[@id="idDownloadDataMenu' + stringViewID + '"]/table/tbody/tr[1]/td[1]/a[1]'), 'onclick').match(/ViewState=[\d\w]{26}/)[0].substr(-26, 26);

                    var xml = this.evaluate(function () {return window.sawXmlIslandidClientStateXml;});
                    var stringSearchID    = xml.match(/searchId="[\d\w]{26}"/)[0].substr(-27, 26);
                    var stringstatepoolId = xml.match(/statepoolId="[\d\w]{26}"/)[0].substr(-27, 26);

                    console.log(typeof stringSearchID);
                    console.log(stringSearchID);

                    fs.write('.\\temp\\page.html', this.getHTML(), 'w');
                    this.capture('.\\temp\\screenshot.png');

                    var form = {
                        'ViewID':stringViewID,
                        'Action':'Download',
                        'SearchID':stringSearchID,
                        'Style':'FusionFx',
                        'ViewState':stringViewState,
                        'ItemName':'All Sales and Inventory Data',
                        'path':'/shared/Sales/All Sales and Inventory Data',
                        'Format':'csv',
                        'Extension':'.csv',
                        'DownloadId':'314159265358979',
                        'clientStateXml':'<sawst:envState xmlns:sawst="com.siebel.analytics.web/state/v1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlVersion="200811100"><sawst:container cid="d:dashboard" xsi:type="sawst:topLevelStateContainer"><sawst:container cid="p:mco0pb0nob7sqjvg" xsi:type="sawst:page"><sawst:container cid="s:42263r43nih80fd1" xsi:type="sawst:section" rendered="true"><sawst:container cid="g:c452lvndqssjqa45" xsi:type="sawst:dashprompt" links="-" promptAutoCompleteState="off"/></sawst:container><sawst:container cid="r:q4g2fiisnvk4nusv" xsi:type="sawst:report" links="fd" defaultView="compoundView!1" searchId="fvup02s9lt0o6urkplv4pqa5ri" folder="/shared/Sales" itemName="All Sales and Inventory Data"/><sawst:container cid="f:dpstate" xsi:type="sawst:dashpromptstate" statepoolId="' + stringstatepoolId + '"/><sawst:container cid="s:b0003tc6gnahvsfq" xsi:type="sawst:section" rendered="true"/><sawst:container cid="s:c5j314uterctfb08" xsi:type="sawst:section" rendered="true"/></sawst:container></sawst:container></sawst:envState>',
                    };

                    console.log('downloading...');
                    this.download('https://vendorcentral.amazon.com/tc/reports/ara-premium/analytics/saw.dll?Go', '.\\data\\' + (weekly ? 'weekly' : 'daily') + '\\' + toDateString(date, 'ISO') + '.csv', 'POST', form);
                    console.log('download complete');
                    //});
                });
            });
        });
    })(date);}
});

// listeners
casper.on('resource.received', function (resource) {
    if (counter === 0)
        fs.write('.\\temp\\resource.txt', (counter + 1) + ' ' + resource.url + '\n', 'w');
    else
        fs.write('.\\temp\\resource.txt', (counter + 1) + ' ' + resource.url + '\n', 'a');
    counter++;
});

casper.run(function () {
    console.log('--------------------------');
    console.log('task complete, exiting... ');
    console.log('     have a nice day!     ');
    console.log('--------------------------');
    this.exit();
});

// converts date to absolute date (abs date of 2000-01-01 is 1)
function toAbsDate(date) {
    var monthDays     = [0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334];
    var monthDaysLeap = [0, 31, 60, 91, 121, 152, 182, 213, 244, 274, 305, 335];

    var year = 2016, month = 7, day = 1;


    if (typeof date == 'number') { // yyyymmdd
        day   = date % 100;
        month = Math.floor(date / 100) % 100;
        year  = Math.floor(date / 10000);

        month = (((month - 1) % 12) + 12) % 12 + 1; // solve month overflow and underflow
    }
    else if (typeof date == 'string') {
        if (date.match(/^\d{4}(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])$/)) { // issue: js parse compact as number
            year  = parseInt(date.substring(0, 4));
            month = parseInt(date.substring(4, 6));
            day   = parseInt(date.substring(6, 8));
        }
        else if (date.match(/^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$/)) { // yyyy-mm-dd
            year  = parseInt(date.substring(0, 4));
            if (date.substring(5, 7).match(/^(0[1-9]|1[012])$/)) {
                month = parseInt(date.substring(5, 7));
                if (date.substring(8, 10).match(/^(0[1-9]|[12][0-9]|3[01])$/))
                    day = parseInt(date.substring(8, 10));
                else
                    day = parseInt(date.substring(8, 9));
            }
            else {
                month = parseInt(date.substring(5, 6));
                if (date.substring(7, 9).match(/^(0?[1-9]|[12][0-9]|3[01])$/))
                    day = parseInt(date.substring(7, 9));
                else
                    day = parseInt(date.substring(7, 8));
            }
        }
        else if (date.match(/^(0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])[\/\-]\d{4}$/)) { // mm/dd/yyyy
            if (date.substring(0, 2).match(/^(0[1-9]|1[012])$/)) {
                month = parseInt(date.substring(0, 2));
                if (date.substring(3, 5).match(/^(0[1-9]|[12][0-9]|3[01])$/)) {
                    day  = parseInt(date.substring(3, 5));
                    year = parseInt(date.substring(6, 10));
                }
                else {
                    day  = parseInt(date.substring(3, 4));
                    year = parseInt(date.substring(5, 9));
                }
            }
            else {
                month = parseInt(date.substring(0, 1));
                if (date.substring(2, 4).match(/^(0[1-9]|[12][0-9]|3[01])$/)) {
                    day  = parseInt(date.substring(2, 4));
                    year = parseInt(date.substring(5, 9));
                }
                else {
                    day  = parseInt(date.substring(2, 3));
                    year = parseInt(date.substring(4, 8));
                }
            }
        }
    }

    var sum = 0;

    if (2000 <= year) {
        for (var y = 2000; y < year; y++) {
            if (isLeap(y)) sum += 366;
            else           sum += 365;
        }
    }
    else {
        for (var y = 2000; y > year; y--) {
            if (isLeap(y)) sum -= 366;
            else           sum -= 365;
        }
    }

    if (isLeap(y)) sum += monthDaysLeap[month - 1];
    else           sum += monthDays[month - 1];
    sum += day;

    return sum;
}

function toDateString(date, mode) {
    var monthDays     = [0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334];
    var monthDaysLeap = [0, 31, 60, 91, 121, 152, 182, 213, 244, 274, 305, 335];
    var year = 2000, month = 0, day = 1;

    if (date > 0) {
        while (date > (isLeap(year) ? 366 : 365)) {
            date -= (isLeap(year) ? 366 : 365);
            year += 1;
        }
    }
    else {
        while (date <= 0) {
            date += (isLeap(year) ? 366 : 365);
            year -= 1;
        }
    }
    if (isLeap(year)) {
        while (monthDaysLeap[month] < date) month++;
        day = date - monthDaysLeap[month - 1];
    }
    else {
        while (monthDays[month] < date) month++;
        day = date - monthDays[month - 1];
    }

    if (mode === 'US')
        return month + '/' + day + '/' + year;
    else
        return year + '-' + (month < 10 ? '0' : '') + month + '-' + (day < 10 ? '0' : '') + day;
}

function isLeap (year) {
    return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
}

function weekDay(date) {
    return (date + 5) % 7;
}