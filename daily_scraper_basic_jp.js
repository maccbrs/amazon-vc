var casper = require('casper').create({
    clientScripts: ["jquery.js"],
    viewportSize: {width: 1920, height: 1080}
});

casper.javascriptEnabled = true;
//page.settings.javascriptEnabled = true;
//runner.page.javascriptEnabled = true;
//casper.page.javascriptEnabled = true;
phantom.cookiesEnabled = true;
//casper.settings.userAgent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0';

// casper.options.exitOnError = false;

casper.options.waitTimeout = 999999;
var x = require('casper').selectXPath;

var fs = require('fs');
var AMAZON_SITE = 'https://vendorcentral.amazon.co.jp/analytics/dashboard/snapshot?ref_=vc_ven-ven-home_subNav';
var AMAZON_USER = 'vendorcentral@channelbakers.com';
var AMAZON_PASS = ''; 
var cookie_dir = 'scraper/cookies';
var cookie_path = cookie_dir + '/cookies_basic_jp.txt';
var emailInput = 'input#ap_email';
var passInput = 'input#ap_password';
var otp = '#auth-mfa-otpcode';
var otp_remember = '#auth-mfa-remember-device';
var captcha = 'input#auth-captcha-guess';
var x      = require('casper').selectXPath;
var system = require('system');
var utils = require('utils');
var http = require('http');
var moment = require('moment.js');

var now = new Date();
var year_to = now.getFullYear();
var month_to = now.getMonth();
var date_to = now.getDate() - 1;

var prev = new Date();
prev.setDate(prev.getDate() - 8);
var year_from = prev.getFullYear(); 
var month_from = prev.getMonth();
var date_from = prev.getDate();

var dt = new Date();
var dd = dt.getDate();
var mm = dt.getMonth() + 1; 
var yyyy = dt.getFullYear();
if (dd < 10) {
  dd = '0' + dd;
} 
if (mm < 10) {
  mm = '0' + mm;
}
var prod_today = yyyy + '' + mm+1 + '' + dd;

if (date_to < 10) {
  date_to = '0' + date_to;
} 
if (month_to < 10) {
  month_to = '0' + month_to;
}

var compare_to = year_to + '' + (month_to+1) + '' + date_to;

var from = new Date(year_from,month_from,date_from);
var to = new Date(year_to,month_to,date_to);
var dates = [];
    
// loop for every day
for (var day = from; day <= to; day.setDate(day.getDate() + 1)) {
      var year = day.getFullYear();
      var month = day.getMonth() + 1;
      var date = day.getDate();
      
      if (date < 10) {
        date = '0' + date;
      } 
      if (month < 10) {
        month = '0' + month;
      }

      dates.push(year+''+month+''+date);
  
}

casper.userAgent('Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0');

function isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

function logged_in() {
    console.log("already logged in");
      var vgids = [];
      var userid = [];

      casper.thenOpen("https://hc-ping.com/8c493292-6ead-42a6-ae54-8dc59d68eddd/start");

        casper.then(function() {
          this.thenOpen('https://vendorcentral.amazon.co.jp/hz/vendor/members/user-management/switch-accounts-checker?vendorGroup=5583753&customerId=A1GW1729DBKXU8');
          this.thenOpen('https://vendorcentral.amazon.co.jp/analytics/dashboard/salesDiagnostic?ref_=vc_xx_subNav');
          this.then(function() {
            var user_info = JSON.stringify(this.getGlobal('userInfo'));
            var obj = JSON.parse(user_info);
            var subaccounts = obj.subAccounts;
            fs.write('storage/accounts/accounts_basic_jp_'+prod_today+'.json', JSON.stringify(subaccounts));
            var userpref = JSON.parse(obj.userPreferencesString); 
            userid.push(userpref.obfuscatedUserId);

            subaccounts.forEach(function (subaccount) {
              if(subaccount.retailSubscriptionLevel == 'BASIC' && subaccount.status == 'VALID'){
              var x = subaccount.vendorGroupId;
                vgids.push(x);
              }
            });
          });

      this.then(function() {
          this.each(vgids, function (self, vgid) {
            if (vgid == '673610' || vgid == '705280') {
              var view = 'sourcing';
            }
            else {
              var view = 'manufacturer';
            }

            console.log(dates);

            this.thenOpen('https://vendorcentral.amazon.co.jp/hz/vendor/members/user-management/switch-accounts-checker?vendorGroup='+vgid+'&customerId=A1GW1729DBKXU8');

            this.thenOpen('https://vendorcentral.amazon.co.jp/analytics/dashboard/salesDiagnostic?ref_=vc_xx_subNav');

            this.then(function() {
              var token = this.getGlobal('token');
                //product
                this.then(function() {

                  this.then(function() {
                      this.then(function() {
                        if (vgid != '') {
                          this.wait(1000, function () {
                            this.thenOpen('https://vendorcentral.amazon.co.jp/analytics/data/dashboard/productCatalog/report/productCatalogDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                                method: 'POST',
                                data:   {"requestId":"d16a1466-8eb0-4394-8fff-503ae68311e7","reportParameters":[{"parameterId":"distributorView","values":[{"val":view}]},{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"dataRefreshDate","values":[{"val":prod_today}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"releasedate","ascending":false}],"reportPagination":{"pageIndex":0,"pageSize":100}}},
                                headers: {
                                    'Accept':'application/json',
                                    'Content-Type':'application/json',
                                }
                            });
                          });

                          this.then(function(){
                            fs.write('storage/count.json', this.page.plainText);
                            var obj = JSON.parse(this.getPageContent());

                              if(obj && obj['payload'] && obj['payload']['reportParts'] && obj['payload']['reportParts'][0] && obj['payload']['reportParts'][0]['rowCount']) {
                                var rowCount = obj['payload']['reportParts'][0]['rowCount'];
                              console.log('RowCount: '+rowCount+'');

                                var paginate = rowCount.replace(/\,/g,'');
                                paginate = parseInt(paginate,10);
                                var number = paginate/10000;
                                var pages = Math.floor(number);

                                var parts = [];
                                for (h = 0; h <= pages; h++) {
                                  parts.push(h);
                                }

                                this.each(parts, function (self, part) {
                                  if (fs.isFile('storage/products/ProductCatalog_manufacturing_'+vgid+'_'+prod_today+'_part'+part+'.json')){ 
                                    console.log('Product Already Scraped!');
                                  } else {
                                  this.wait(1000, function() {
                                    this.thenOpen('https://vendorcentral.amazon.co.jp/analytics/data/dashboard/productCatalog/report/productCatalogDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                                        method: 'POST',
                                        data:   {"requestId":"d16a1466-8eb0-4394-8fff-503ae68311e7","reportParameters":[{"parameterId":"distributorView","values":[{"val":view}]},{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"dataRefreshDate","values":[{"val":prod_today}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"releasedate","ascending":false}],"reportPagination":{"pageIndex":part,"pageSize":10000}}},
                                        headers: {
                                            'Accept':'application/json',
                                            'Content-Type':'application/json',
                                        }
                                    });
                                    this.then(function(){
                                      console.log('ProductCatalog_manufacturing_'+vgid+'_'+prod_today+'_part'+part+'.json');
                                      fs.write('storage/products/ProductCatalog_manufacturing_'+vgid+'_'+prod_today+'_part'+part+'.json', this.page.plainText);
                                    });
                                  });
                                  }
                                });
                              } else {
                                console.log('ProductCatalog_manufacturing_'+vgid+'_'+prod_today+' : NONE');
                              }
                          });
                        }
                      });
                  });

                  this.then(function() {
                      this.then(function() {
                        if (vgid != '') {
                          this.wait(1000, function () {
                            this.thenOpen('https://vendorcentral.amazon.co.jp/analytics/data/dashboard/productCatalog/report/productCatalogDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                                method: 'POST',
                                data:   {"reportParameters":[{"parameterId":"distributorView","values":[{"val":"sourcing"}]},{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"dataRefreshDate","values":[{"val":prod_today}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"releasedate","ascending":false}],"reportPagination":{"pageIndex":0,"pageSize":100}}},
                                headers: {
                                    'Accept':'application/json',
                                    'Content-Type':'application/json',
                                }
                            });
                          });

                          this.then(function(){
                            fs.write('storage/count.json', this.page.plainText);
                            var obj = JSON.parse(this.getPageContent());

                              if(obj && obj['payload'] && obj['payload']['reportParts'] && obj['payload']['reportParts'][0] && obj['payload']['reportParts'][0]['rowCount']) {
                                var rowCount = obj['payload']['reportParts'][0]['rowCount'];
                              console.log('RowCount: '+rowCount+'');

                                var paginate = rowCount.replace(/\,/g,'');
                                paginate = parseInt(paginate,10);
                                var number = paginate/10000;
                                var pages = Math.floor(number);

                                var parts = [];
                                for (h = 0; h <= pages; h++) {
                                  parts.push(h);
                                }

                                this.each(parts, function (self, part) {
                                  if (fs.isFile('storage/products/ProductCatalog_sourcing_'+vgid+'_'+prod_today+'_part'+part+'.json')){ 
                                    console.log('Product Already Scraped!');
                                  } else {
                                  this.wait(1000, function() {
                                    this.thenOpen('https://vendorcentral.amazon.co.jp/analytics/data/dashboard/productCatalog/report/productCatalogDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                                        method: 'POST',
                                        data:   {"reportParameters":[{"parameterId":"distributorView","values":[{"val":"sourcing"}]},{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"dataRefreshDate","values":[{"val":prod_today}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"releasedate","ascending":false}],"reportPagination":{"pageIndex":part,"pageSize":10000}}},
                                        headers: {
                                            'Accept':'application/json',
                                            'Content-Type':'application/json',
                                        }
                                    });
                                    this.then(function(){
                                      console.log('ProductCatalog_sourcing_'+vgid+'_'+prod_today+'_part'+part+'.json');
                                      fs.write('storage/products/ProductCatalog_sourcing_'+vgid+'_'+prod_today+'_part'+part+'.json', this.page.plainText);
                                    });
                                  });
                                  }
                                });
                              } else {
                                console.log('ProductCatalog_sourcing_'+vgid+'_'+prod_today+' : NONE');
                              }
                          });
                        }
                      });
                  });

                  this.then(function() {
                      this.then(function() {
                        if (vgid != '') {
                          this.wait(1000, function () {
                            this.thenOpen('https://vendorcentral.amazon.co.jp/analytics/data/dashboard/productCatalog/report/productCatalogDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                                method: 'POST',
                                data:   {"requestId":"3af45cfa-b994-44d2-82e4-46ffe49029b0","reportParameters":[{"parameterId":"distributorView","values":[{"val":"consignment"}]},{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"dataRefreshDate","values":[{"val":prod_today}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"releasedate","ascending":false}],"reportPagination":{"pageIndex":0,"pageSize":100}}},
                                headers: {
                                    'Accept':'application/json',
                                    'Content-Type':'application/json',
                                }
                            });
                          });

                          this.then(function(){
                            fs.write('storage/count.json', this.page.plainText);
                            var obj = JSON.parse(this.getPageContent());

                              if(obj && obj['payload'] && obj['payload']['reportParts'] && obj['payload']['reportParts'][0] && obj['payload']['reportParts'][0]['rowCount']) {
                                var rowCount = obj['payload']['reportParts'][0]['rowCount'];
                              console.log('RowCount: '+rowCount+'');

                                var paginate = rowCount.replace(/\,/g,'');
                                paginate = parseInt(paginate,10);
                                var number = paginate/10000;
                                var pages = Math.floor(number);

                                var parts = [];
                                for (h = 0; h <= pages; h++) {
                                  parts.push(h);
                                }

                                this.each(parts, function (self, part) {
                                  if (fs.isFile('storage/products/ProductCatalog_consignment_'+vgid+'_'+prod_today+'_part'+part+'.json')){ 
                                    console.log('Product Already Scraped!');
                                  } else {
                                  this.wait(1000, function() {
                                    this.thenOpen('https://vendorcentral.amazon.co.jp/analytics/data/dashboard/productCatalog/report/productCatalogDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                                        method: 'POST',
                                        data:   {"requestId":"3af45cfa-b994-44d2-82e4-46ffe49029b0","reportParameters":[{"parameterId":"distributorView","values":[{"val":"consignment"}]},{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"dataRefreshDate","values":[{"val":prod_today}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"releasedate","ascending":false}],"reportPagination":{"pageIndex":part,"pageSize":10000}}},
                                        headers: {
                                            'Accept':'application/json',
                                            'Content-Type':'application/json',
                                        }
                                    });
                                    this.then(function(){
                                      console.log('ProductCatalog_consignment_'+vgid+'_'+prod_today+'_part'+part+'.json');
                                      fs.write('storage/products/ProductCatalog_consignment_'+vgid+'_'+prod_today+'_part'+part+'.json', this.page.plainText);
                                    });
                                  });
                                  }
                                });
                              } else {
                                console.log('ProductCatalog_consignment_'+vgid+'_'+prod_today+' : NONE');
                              }
                          });
                        }
                      });
                  });


                });
 
                  this.each(dates, function (self, date) {

                    //sales
                    this.then(function() {
                      //retail
                      this.then(function() {
                        this.wait(1000, function () {
                          this.thenOpen('https://vendorcentral.amazon.co.jp/analytics/data/dashboard/salesDiagnostic/report/salesDiagnosticDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                            method: 'POST',
                            data:   {"reportParameters":[{"parameterId":"distributorView","values":[{"val":"sourcing"}]},{"parameterId":"viewFilter","values":[{"val":"shippedCOGSLevel"}]},{"parameterId":"productView","values":[{"val":"kindleExcluded"}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]},{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"aggregationFilter","values":[{"val":"ASINLevel"}]},{"parameterId":"isPeriodToDate","values":[{"val":false}]},{"parameterId":"isCustomDateRange","values":[{"val":"false"}]},{"parameterId":"hideLastYear","values":[{"val":false}]},{"parameterId":"period","values":[{"val":"DAILY"}]},{"parameterId":"dataRefreshDate","values":[{"val":"0001460198914"}]},{"parameterId":"parentASINVisibility","values":[{"val":true}]},{"parameterId":"eanVisibility","values":[{"val":true}]},{"parameterId":"isbn13Visibility","values":[{"val":true}]},{"parameterId":"upcVisibility","values":[{"val":true}]},{"parameterId":"janVisibility","values":[{"val":false}]},{"parameterId":"brandVisibility","values":[{"val":true}]},{"parameterId":"brandCodeVisibility","values":[{"val":false}]},{"parameterId":"subcatVisibility","values":[{"val":true}]},{"parameterId":"catVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeWidthVisibility","values":[{"val":true}]},{"parameterId":"authorVisibility","values":[{"val":true}]},{"parameterId":"bindingVisibility","values":[{"val":true}]},{"parameterId":"catalogNumberVisibility","values":[{"val":false}]},{"parameterId":"colorVisibility","values":[{"val":true}]},{"parameterId":"colorCountVisibility","values":[{"val":false}]},{"parameterId":"manufactureOnDemandVisibility","values":[{"val":false}]},{"parameterId":"listPriceVisibility","values":[{"val":false}]},{"parameterId":"modelStyleVisibility","values":[{"val":true}]},{"parameterId":"productGroupVisibility","values":[{"val":false}]},{"parameterId":"releaseDateVisibility","values":[{"val":true}]},{"parameterId":"periodStartDay","values":[{"val":date}]},{"parameterId":"periodEndDay","values":[{"val":date}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"shippedcogs","ascending":false}],"reportPagination":{"pageIndex":0,"pageSize":100}}},
                            headers: {
                                'Accept':'application/json',
                                'Content-Type':'application/json',
                            }
                          });
                        });

                        this.then(function(){
                          fs.write('storage/sales_retail_count.json', this.page.plainText);

                          var obj = JSON.parse(this.getPageContent());

                            if (obj && obj['payload'] && obj['payload']['reportParts'] && obj['payload']['reportParts'][0] && obj['payload']['reportParts'][0]['rowCount']) {
                              var rowCount = obj['payload']['reportParts'][0]['rowCount'];
                              console.log('RowCount: '+rowCount+'');
                              var paginate = rowCount.replace(/\,/g,'');
                              paginate = parseInt(paginate,10);
                              var number = paginate/10000;
                              var pages = Math.floor(number);

                              var parts = [];
                              for (h = 0; h <= pages; h++) {
                                parts.push(h);
                              }

                              this.each(parts, function (self, part) {
                                  if (fs.isFile('storage/salesdiagnostics/SalesDiagnosticsDetail_retail_'+vgid+'_'+date+'_part'+part+'.json') && compare_to != date){ 
                                    console.log('Sales Already Scraped!');
                                  } else {
                                    this.wait(1000, function() {
                                      
                                      this.thenOpen('https://vendorcentral.amazon.co.jp/analytics/data/dashboard/salesDiagnostic/report/salesDiagnosticDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                                          method: 'POST',
                                          data:   {"reportParameters":[{"parameterId":"distributorView","values":[{"val":"sourcing"}]},{"parameterId":"viewFilter","values":[{"val":"shippedCOGSLevel"}]},{"parameterId":"productView","values":[{"val":"kindleExcluded"}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]},{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"aggregationFilter","values":[{"val":"ASINLevel"}]},{"parameterId":"isPeriodToDate","values":[{"val":false}]},{"parameterId":"isCustomDateRange","values":[{"val":"false"}]},{"parameterId":"hideLastYear","values":[{"val":false}]},{"parameterId":"period","values":[{"val":"DAILY"}]},{"parameterId":"dataRefreshDate","values":[{"val":"0001460198914"}]},{"parameterId":"parentASINVisibility","values":[{"val":true}]},{"parameterId":"eanVisibility","values":[{"val":true}]},{"parameterId":"isbn13Visibility","values":[{"val":true}]},{"parameterId":"upcVisibility","values":[{"val":true}]},{"parameterId":"janVisibility","values":[{"val":false}]},{"parameterId":"brandVisibility","values":[{"val":true}]},{"parameterId":"brandCodeVisibility","values":[{"val":false}]},{"parameterId":"subcatVisibility","values":[{"val":true}]},{"parameterId":"catVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeWidthVisibility","values":[{"val":true}]},{"parameterId":"authorVisibility","values":[{"val":true}]},{"parameterId":"bindingVisibility","values":[{"val":true}]},{"parameterId":"catalogNumberVisibility","values":[{"val":false}]},{"parameterId":"colorVisibility","values":[{"val":true}]},{"parameterId":"colorCountVisibility","values":[{"val":false}]},{"parameterId":"manufactureOnDemandVisibility","values":[{"val":false}]},{"parameterId":"listPriceVisibility","values":[{"val":false}]},{"parameterId":"modelStyleVisibility","values":[{"val":true}]},{"parameterId":"productGroupVisibility","values":[{"val":false}]},{"parameterId":"releaseDateVisibility","values":[{"val":true}]},{"parameterId":"periodStartDay","values":[{"val":date}]},{"parameterId":"periodEndDay","values":[{"val":date}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"shippedcogs","ascending":false}],"reportPagination":{"pageIndex":part,"pageSize":10000}}},
                                            headers: {
                                              'Accept':'application/json',
                                              'Content-Type':'application/json',
                                          }
                                      });
                                      this.then(function(){
                                        console.log('SalesDiagnosticsDetail_retail_'+vgid+'_'+date+'_part'+part+'.json');
                                        fs.write('storage/salesdiagnostics/SalesDiagnosticsDetail_retail_'+vgid+'_'+date+'_part'+part+'.json', this.page.plainText);
                                      });
                                    });
                                  }
                              });
                            }
                            else {
                              console.log('SalesDiagnosticsDetail_retail_'+vgid+'_'+date+' : NONE');
                            }
                        });
                    });
                });

                    //inventory
                    this.then(function() {

                      this.wait(1000, function () {
                        this.thenOpen('https://vendorcentral.amazon.co.jp/analytics/data/dashboard/inventoryHealth/report/inventoryHealthDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                            method: 'POST',
                            data:   {"requestId":"49cf383b-42ed-4efe-9aaa-1cf3e7a10321","reportParameters":[{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"aggregationFilter","values":[{"val":"ASINLevel"}]},{"parameterId":"distributorView","values":[{"val":"sourcing"}]},{"parameterId":"isFCAllowed","values":[{"val":"false"}]},{"parameterId":"isPeriodToDate","values":[{"val":false}]},{"parameterId":"isCustomDateRange","values":[{"val":"false"}]},{"parameterId":"hideLastYear","values":[{"val":false}]},{"parameterId":"period","values":[{"val":"DAILY"}]},{"parameterId":"dataRefreshDate","values":[{"val":"0000279556982"}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]},{"parameterId":"parentASINVisibility","values":[{"val":true}]},{"parameterId":"eanVisibility","values":[{"val":true}]},{"parameterId":"isbn13Visibility","values":[{"val":true}]},{"parameterId":"upcVisibility","values":[{"val":true}]},{"parameterId":"janVisibility","values":[{"val":false}]},{"parameterId":"brandVisibility","values":[{"val":true}]},{"parameterId":"brandCodeVisibility","values":[{"val":true}]},{"parameterId":"subcatVisibility","values":[{"val":true}]},{"parameterId":"catVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeWidthVisibility","values":[{"val":true}]},{"parameterId":"authorVisibility","values":[{"val":true}]},{"parameterId":"bindingVisibility","values":[{"val":true}]},{"parameterId":"catalogNumberVisibility","values":[{"val":false}]},{"parameterId":"colorVisibility","values":[{"val":true}]},{"parameterId":"colorCountVisibility","values":[{"val":true}]},{"parameterId":"manufactureOnDemandVisibility","values":[{"val":false}]},{"parameterId":"listPriceVisibility","values":[{"val":true}]},{"parameterId":"modelStyleVisibility","values":[{"val":true}]},{"parameterId":"productGroupVisibility","values":[{"val":true}]},{"parameterId":"releaseDateVisibility","values":[{"val":true}]},{"parameterId":"periodStartDay","values":[{"val":date}]},{"parameterId":"periodEndDay","values":[{"val":date}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"sellableonhandinventory","ascending":false}],"reportPagination":{"pageIndex":0,"pageSize":100}}},
                            headers: {
                                'Accept':'application/json',
                                'Content-Type':'application/json',
                            }
                        });
                      });

                      this.then(function(){
                        fs.write('storage/inv_count.json', this.page.plainText);
                        var obj = JSON.parse( this.getPageContent());
                        
                          if(obj && obj['payload'] && obj['payload']['reportParts'] && obj['payload']['reportParts'][0] && obj['payload']['reportParts'][0]['rowCount']) {
                            var rowCount = obj['payload']['reportParts'][0]['rowCount'];  
                            var paginate = rowCount.replace(/\,/g,'');
                            paginate = parseInt(paginate,10);
                            var number = paginate/10000;
                            var pages = Math.floor(number);

                            var parts = [];
                            for (h = 0; h <= pages; h++) {
                              parts.push(h);
                            }

                            this.each(parts, function (self, part) {
                              if (fs.isFile('storage/inventory/InventoryHealthDetail_'+vgid+'_'+date+'_part'+part+'.json') && compare_to != date){ 
                                console.log('Inventory Already Scraped!');
                              } else {
                                this.wait(1000, function() {
                                  this.thenOpen('https://vendorcentral.amazon.co.jp/analytics/data/dashboard/inventoryHealth/report/inventoryHealthDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                                      method: 'POST',
                                      data:   {"requestId":"49cf383b-42ed-4efe-9aaa-1cf3e7a10321","reportParameters":[{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"aggregationFilter","values":[{"val":"ASINLevel"}]},{"parameterId":"distributorView","values":[{"val":"sourcing"}]},{"parameterId":"isFCAllowed","values":[{"val":"false"}]},{"parameterId":"isPeriodToDate","values":[{"val":false}]},{"parameterId":"isCustomDateRange","values":[{"val":"false"}]},{"parameterId":"hideLastYear","values":[{"val":false}]},{"parameterId":"period","values":[{"val":"DAILY"}]},{"parameterId":"dataRefreshDate","values":[{"val":"0000279556982"}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]},{"parameterId":"parentASINVisibility","values":[{"val":true}]},{"parameterId":"eanVisibility","values":[{"val":true}]},{"parameterId":"isbn13Visibility","values":[{"val":true}]},{"parameterId":"upcVisibility","values":[{"val":true}]},{"parameterId":"janVisibility","values":[{"val":false}]},{"parameterId":"brandVisibility","values":[{"val":true}]},{"parameterId":"brandCodeVisibility","values":[{"val":true}]},{"parameterId":"subcatVisibility","values":[{"val":true}]},{"parameterId":"catVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeWidthVisibility","values":[{"val":true}]},{"parameterId":"authorVisibility","values":[{"val":true}]},{"parameterId":"bindingVisibility","values":[{"val":true}]},{"parameterId":"catalogNumberVisibility","values":[{"val":false}]},{"parameterId":"colorVisibility","values":[{"val":true}]},{"parameterId":"colorCountVisibility","values":[{"val":true}]},{"parameterId":"manufactureOnDemandVisibility","values":[{"val":false}]},{"parameterId":"listPriceVisibility","values":[{"val":true}]},{"parameterId":"modelStyleVisibility","values":[{"val":true}]},{"parameterId":"productGroupVisibility","values":[{"val":true}]},{"parameterId":"releaseDateVisibility","values":[{"val":true}]},{"parameterId":"periodStartDay","values":[{"val":date}]},{"parameterId":"periodEndDay","values":[{"val":date}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"sellableonhandinventory","ascending":false}],"reportPagination":{"pageIndex":part,"pageSize":10000}}},
                                      headers: {
                                          'Accept':'application/json',
                                          'Content-Type':'application/json',
                                      }
                                  });
                                  this.then(function(){
                                    console.log('InventoryHealthDetail_'+vgid+'_'+date+'_part'+part+'.json');
                                    fs.write('storage/inventory/InventoryHealthDetail_'+vgid+'_'+date+'_part'+part+'.json', this.page.plainText);
                                  });
                                });
                              }
                            });
                          } else {
                            console.log('InventoryHealthDetail_'+vgid+'_'+date+' : NONE');
                          }
                      });

                    });

            });
          });
        });
      });
    });

  casper.then(function() {
      console.log('Scraped Successfully!');
  });

  casper.thenOpen("https://hc-ping.com/8c493292-6ead-42a6-ae54-8dc59d68eddd");
  }

// Read Cookies
if (fs.isFile(cookie_path)){ 
  phantom.cookies = JSON.parse(fs.read(cookie_path));
}

casper.start(AMAZON_SITE, function() {  
    console.log("Amazon website opened [" + AMAZON_SITE + "]");
    this.capture('scraper/opened.png');
});


casper.wait(1000, function() {
  if(this.exists("form[name=signIn]")) {
    console.log("need to login");

    this.capture('scraper/login_form.png');//print screen shot after login

    if(casper.exists('#ap_email')){
      this.wait(1000, function(){
        this.mouseEvent('click', emailInput, '15%', '48%');
        this.sendKeys('input#ap_email', AMAZON_USER, { reset: true});
      });
    }

    this.wait(3000, function () {
      this.mouseEvent('click', passInput, '12%', '67%');
      this.sendKeys('input#ap_password', AMAZON_PASS);
      this.click('input[name=rememberMe]');
   });

    this.then(function() {
        this.click('input#signInSubmit');
    }).wait(5000, function(){});

    this.capture('scraper/login_submitted.png');

    // write the cookies
    this.wait(1000, function() {
        var cookies = JSON.stringify(phantom.cookies);
        fs.write(cookie_path, cookies, 644);
    });

    casper.then(function (e) {
      if(casper.exists('#auth-captcha-image-container')){
          this.captureSelector('scraper/captcha.png', '#auth-captcha-image');
          console.log('captcha:');
          var captcha_code = system.stdin.readLine();

    this.wait(3000, function () {
      this.mouseEvent('click', passInput, '12%', '67%');
      this.sendKeys('input#ap_password', AMAZON_PASS);
    });

    this.wait(3000, function () {
      this.mouseEvent('click', captcha, '12%', '67%');
      this.sendKeys(captcha, captcha_code);
    });

    this.then(function() {
        console.log('submit captcha');
        this.click('input#signInSubmit');
    }).wait(5000, function(){});

      }
      else if(this.exists('#auth-mfa-otpcode')) {
        console.log('otp:');
        var otp_code = system.stdin.readLine();

        this.wait(1000, function () {
          this.mouseEvent('click', otp, '12%', '67%');
          this.sendKeys(otp, otp_code, {keepFocus: true});
/*          this.sendKeys(otp, '253821');*/
          this.mouseEvent('click', otp_remember);
          this.click('input#auth-signin-button');
          this.sendKeys(otp, casper.page.event.key.Enter , {keepFocus: true});
          this.capture('scraper/otp.png');
          console.log('otp sent');

          this.then(function() {
            this.wait(3000, function() {
                var cookies = JSON.stringify(phantom.cookies);
                fs.write(cookie_path, cookies, 644);
            });

            this.capture('scraper/submit_otp_2.png');
          });

          this.then(function() {
            this.waitForSelector("#logo_topNav", function() {
              logged_in();
            });
          });

        });


      }

      else {

        console.log("Logged in successfully.");

        logged_in();
      }

      this.capture('scraper/amazon1.png');//print screen shot after login

      });

    // triggerFail('Login');
  }
  else {
  logged_in();
}
});

casper.on('error', function(msg,backtrace) {
  this.open("https://hc-ping.com/8c493292-6ead-42a6-ae54-8dc59d68eddd/fail");
  logged_in();
});

casper.run();
