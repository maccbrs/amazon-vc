<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHttp/Models/tasksTable extends Migration
{

    public function up()
    {
        Schema::create('Http/Models/tasks', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('project_id')->unsigned();
            $table->date('due');
            $table->integer('project_id')->unsigned();
            $table->foreign('project_id')
                ->references('id')
                ->on('projecs');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('Http/Models/tasks');
    }
}
