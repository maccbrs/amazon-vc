<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManufacturerCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manufacturer_codes', function (Blueprint $table) {
            $table->bigInteger('id')->autoIncrement();
            $table->bigInteger('parent_manufacturer_code_id')->nullable();
            $table->text('manufacturer_code')->nullable();
            $table->index(['id']);
            $table->timestamps();

            $table->foreign('parent_manufacturer_code_id')
                  ->references('id')->on('parent_manufacturer_codes')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('manufacturer_code');
    }
}
