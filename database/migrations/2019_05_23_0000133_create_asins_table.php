<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAsinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asins', function (Blueprint $table) {
            $table->bigInteger('id')->autoIncrement();
            $table->bigInteger('account_id')->nullable();
            $table->bigInteger('brand_id')->nullable();
            $table->bigInteger('category_id')->nullable();
            $table->bigInteger('subcategory_id')->nullable();
            $table->string('asin', 20)->nullable();
            $table->text('product_title')->nullable();
            $table->string('parent_asin', 20)->nullable();
            $table->string('isbn_13', 20)->nullable();
            $table->bigInteger('ean')->nullable();
            $table->bigInteger('upc')->nullable();
            $table->date('release_date')->nullable();
            $table->decimal('list_price')->nullable();
            $table->bigInteger('binding_id')->nullable();
            $table->string('author_or_artist', 100)->nullable();
            $table->enum('is_sitb_enabled', ['Y','N']);
            $table->string('apparel_size', 50)->nullable();
            $table->string('apparel_size_width', 50)->nullable();
            $table->bigInteger('product_group_id')->nullable();
            $table->bigInteger('replenish_id')->nullable();
            $table->string('model_style_no', 50)->nullable();
            $table->string('color', 50)->nullable();
            $table->bigInteger('color_count')->nullable();
            $table->string('prep_instructions_required', 100)->nullable();
            $table->string('prep_instructions_vendor_state', 100)->nullable();
            $table->bigInteger('manufacturer_code_id')->nullable();
            $table->bigInteger('parent_manufacturer_code_id')->nullable();
            $table->date('generated_date');
            $table->index(['id', 'asin', 'generated_date']);
            $table->index(['asin', 'generated_date']);
            $table->index(['id', 'generated_date']);
            $table->index('asin');
            $table->index('generated_date');
            $table->timestamps();

            $table->foreign('account_id')
                  ->references('id')->on('accounts')
                  ->onDelete('cascade');

            $table->foreign('brand_id')
                  ->references('id')->on('brands')
                  ->onDelete('cascade');

            $table->foreign('category_id')
                  ->references('id')->on('categories')
                  ->onDelete('cascade');

            $table->foreign('subcategory_id')
                  ->references('id')->on('subcategories')
                  ->onDelete('cascade');

            $table->foreign('product_group_id')
                  ->references('id')->on('product_groups')
                  ->onDelete('cascade');

            $table->foreign('binding_id')
                  ->references('id')->on('bindings')
                  ->onDelete('cascade');

            $table->foreign('manufacturer_code_id')
                  ->references('id')->on('manufacturer_codes')
                  ->onDelete('cascade');

            $table->foreign('parent_manufacturer_code_id')
                  ->references('id')->on('parent_manufacturer_codes')
                  ->onDelete('cascade');

            $table->foreign('replenish_id')
                  ->references('id')->on('replenish_codes')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asins');
    }
}
