<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePreOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pre_orders', function (Blueprint $table) {
            $table->bigInteger('id')->autoIncrement();
            $table->string('report_type')->default('pre');
            $table->bigInteger('account_id')->nullable();
            $table->bigInteger('asin_id')->nullable();
            $table->bigInteger('pre_ordered_revenue')->nullable();
            $table->bigInteger('pre_ordered_revenue_prior_period')->nullable();
            $table->bigInteger('pre_ordered_units')->nullable();
            $table->bigInteger('pre_ordered_units_prior_period')->nullable();
            $table->bigInteger('average_pre_order_sales_price')->nullable();
            $table->bigInteger('average_pre_order_sales_price_prior_period')->nullable();
            $table->date('generated_date');
            $table->index('generated_date');
            $table->timestamps();

            $table->foreign('account_id')
                  ->references('id')->on('accounts')
                  ->onDelete('cascade');

            $table->foreign('asin_id')
                  ->references('id')->on('asins')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pre_orders');
    }
}
