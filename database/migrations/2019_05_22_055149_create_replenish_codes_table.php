<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReplenishCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('replenish_codes', function (Blueprint $table) {
            $table->bigInteger('id')->autoIncrement();
            $table->string('replenish_code')->nullable();
            $table->unique('replenish_code');
            $table->index(['id', 'replenish_code']);
            $table->index('replenish_code');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('replenish_codes');
    }
}
