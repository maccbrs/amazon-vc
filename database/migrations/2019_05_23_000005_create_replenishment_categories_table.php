<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReplenishmentCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('replenishment_categories', function (Blueprint $table) {
            $table->bigInteger('id')->autoIncrement();
            $table->string('replenishment_category')->nullable();
            $table->unique('replenishment_category');
            $table->index(['id', 'replenishment_category']);
            $table->index('replenishment_category');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('replenishment_categories');
    }
}
