<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeographicSalesInsightsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('geographic_sales_insights', function (Blueprint $table) {
            $table->bigInteger('id')->autoIncrement();
            $table->bigInteger('account_id')->nullable();
            $table->string('report_type')->default('geo');
            $table->bigInteger('asin_id')->nullable();
            $table->bigInteger('country_id')->nullable();
            $table->bigInteger('state_id')->nullable();
            $table->bigInteger('city_id')->nullable();
            $table->bigInteger('zip_id')->nullable();
            $table->decimal('shipped_revenue')->nullable();
            $table->decimal('shipped_revenue_total')->nullable();
            $table->decimal('shipped_revenue_prior_period')->nullable();
            $table->decimal('shipped_revenue_last_year')->nullable();
            $table->decimal('shipped_cogs')->nullable();
            $table->decimal('shipped_cogs_total')->nullable();
            $table->decimal('shipped_cogs_prior_period')->nullable();
            $table->decimal('shipped_cogs_last_year')->nullable();
            $table->decimal('shipped_units')->nullable();
            $table->decimal('shipped_units_total')->nullable();
            $table->decimal('shipped_units_prior_period')->nullable();
            $table->decimal('shipped_units_last_year')->nullable();
            $table->decimal('average_sales_price')->nullable();
            $table->decimal('average_sales_price_prior_period')->nullable();
            $table->decimal('average_sales_price_last_year')->nullable();
            $table->date('generated_date');
            $table->index('id');
            $table->index('account_id');
            $table->index('report_type');
            $table->index('asin_id');
            $table->index('country_id');
            $table->index('state_id');
            $table->index('city_id');
            $table->index('zip_id');
            $table->index('generated_date');
            $table->timestamps();

            $table->foreign('asin_id')
                  ->references('id')->on('asins')
                  ->onDelete('cascade');

            $table->foreign('account_id')
                  ->references('id')->on('accounts')
                  ->onDelete('cascade');

            $table->foreign('country_id')
                  ->references('id')->on('countries')
                  ->onDelete('cascade');

            $table->foreign('state_id')
                  ->references('id')->on('states')
                  ->onDelete('cascade');

            $table->foreign('city_id')
                  ->references('id')->on('cities')
                  ->onDelete('cascade');

            $table->foreign('zip_id')
                  ->references('id')->on('zips')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('geographic_sales_insights');
    }
}
