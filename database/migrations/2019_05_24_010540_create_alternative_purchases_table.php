<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlternativePurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alternative_purchases', function (Blueprint $table) {
            $table->bigInteger('id')->autoIncrement();
            $table->bigInteger('account_id')->nullable();
            $table->bigInteger('asin_id')->nullable();
            $table->string('report_type')->default('alt');
            $table->bigInteger('first_alternative_asin_id')->nullable();
            $table->string('first_alternative_asin')->nullable();
            $table->text('first_alternative_title')->nullable();
            $table->double('first_purchased_percentage')->nullable();
            $table->bigInteger('second_alternative_asin_id')->nullable();
            $table->string('second_alternative_asin')->nullable();
            $table->text('second_alternative_title')->nullable();
            $table->double('second_purchased_percentage')->nullable();
            $table->bigInteger('third_alternative_asin_id')->nullable();
            $table->string('third_alternative_asin')->nullable();
            $table->text('third_alternative_title')->nullable();
            $table->double('third_purchased_percentage')->nullable();
            $table->bigInteger('fourth_alternative_asin_id')->nullable();
            $table->string('fourth_alternative_asin')->nullable();
            $table->text('fourth_alternative_title')->nullable();
            $table->double('fourth_purchased_percentage')->nullable();
            $table->bigInteger('fifth_alternative_asin_id')->nullable();
            $table->string('fifth_alternative_asin')->nullable();
            $table->text('fifth_alternative_title')->nullable();
            $table->double('fifth_purchased_percentage')->nullable();
            $table->date('generated_date')->nullable();
            $table->index('id');
            $table->index('asin_id');
            $table->index(['id', 'asin_id']);
            $table->timestamps();

            $table->foreign('account_id')
                  ->references('id')->on('accounts')
                  ->onDelete('cascade');

            $table->foreign('asin_id')
                  ->references('id')->on('asins')
                  ->onDelete('cascade');               
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alternative_purchases');
    }
}
