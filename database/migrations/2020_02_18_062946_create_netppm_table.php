<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNetppmTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('net_ppms', function (Blueprint $table) {
            $table->bigInteger('id')->autoIncrement();
            $table->bigInteger('account_id')->nullable();
            $table->string('report_type')->default('net');
            $table->bigInteger('asin_id')->nullable();
            $table->string('subcategory')->nullable();
            $table->string('product_title')->nullable();
            $table->decimal('net_ppm')->nullable();
            $table->decimal('net_ppm_total')->nullable();
            $table->decimal('net_ppm_prior')->nullable();
            $table->decimal('net_ppm_last_year')->nullable();
            $table->date('generated_date_from');
            $table->date('generated_date_to');
            $table->index('id');
            $table->index('account_id');
            $table->index('asin_id');
            $table->timestamps();

            $table->foreign('asin_id')
                  ->references('id')->on('asins')
                  ->onDelete('cascade');

            $table->foreign('account_id')
                  ->references('id')->on('accounts')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('netppms');
    }
}
