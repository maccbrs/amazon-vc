<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryHealthTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory_healths', function (Blueprint $table) {
            $table->bigInteger('id')->autoIncrement();
            $table->string('report_type')->default('inv');
            $table->bigInteger('account_id')->nullable();;
            $table->bigInteger('asin_id');
            $table->bigInteger('net_received')->nullable();
            $table->double('net_received_units')->nullable();
            $table->double('sell_through_rate')->nullable();
            $table->bigInteger('open_purchase_order_quantity')->nullable();
            $table->bigInteger('sellable_on_hand_inventory')->nullable();
            $table->double('sellable_on_hand_inventory_trailing')->nullable();
            $table->double('sellable_on_hand_units')->nullable();
            $table->bigInteger('unsellable_on_hand_inventory')->nullable();
            $table->double('unsellable_on_hand_inventory_trailing')->nullable();
            $table->double('unsellable_on_hand_units')->nullable();
            $table->double('ninety_days_sellable_inventory')->nullable();
            $table->double('ninety_days_sellable_inventory_trailing')->nullable();
            $table->bigInteger('ninety_days_sellable_units')->nullable();
            $table->double('unhealthy_inventory')->nullable();
            $table->double('unhealthy_inventory_trailing')->nullable();
            $table->bigInteger('unhealthy_units')->nullable();
            $table->bigInteger('replenishment_category_id')->nullable();
            $table->date('generated_date');
            $table->index('asin_id');
            $table->index('account_id');
            $table->index('generated_date');
            $table->index(['asin_id', 'account_id', 'generated_date']);            
            $table->timestamps();

            $table->foreign('account_id')
                  ->references('id')->on('accounts')
                  ->onDelete('cascade');

            $table->foreign('asin_id')
                  ->references('id')->on('asins')
                  ->onDelete('cascade');

            $table->foreign('replenishment_category_id')
                  ->references('id')->on('replenishment_categories')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventory_health');
    }
}
