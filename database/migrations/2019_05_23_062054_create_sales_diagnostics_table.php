<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesDiagnosticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_diagnostics', function (Blueprint $table) {
            $table->bigInteger('id')->autoIncrement();
            $table->string('report_type')->default('sales');
            $table->bigInteger('program_id')->nullable();
            $table->bigInteger('account_id')->nullable();
            $table->bigInteger('asin_id')->nullable();
            $table->bigInteger('distributor_id')->nullable();
            $table->double('shipped_revenue')->nullable();
            $table->double('shipped_revenue_percentage_total')->nullable();
            $table->double('shipped_revenue_prior')->nullable();
            $table->double('shipped_revenue_prior_year')->nullable();
            $table->double('shipped_cogs')->nullable();
            $table->double('shipped_cogs_percentage_total')->nullable();
            $table->double('shipped_cogs_prior')->nullable();
            $table->double('shipped_cogs_prior_year')->nullable();
            $table->double('shipped_units')->nullable();
            $table->double('shipped_units_percentage_total')->nullable();
            $table->double('shipped_units_prior')->nullable();
            $table->double('shipped_units_prior_year')->nullable();
            $table->double('change_in_impressions_prior')->nullable();
            $table->double('change_in_impressions_prior_year')->nullable();
            $table->double('displayed_in_stock')->nullable();
            $table->double('displayed_in_stock_total')->nullable();
            $table->double('displayed_in_stock_prior')->nullable();
            $table->double('ordered_units')->nullable();
            $table->double('ordered_units_percentage_total')->nullable();
            $table->double('ordered_units_prior')->nullable();
            $table->double('ordered_units_prior_year')->nullable();
            $table->double('customer_returns')->nullable();
            $table->double('free_replacements')->nullable();
            $table->double('subcategory_sales_rank')->nullable();
            $table->double('subcategory_better_worse')->nullable();
            $table->double('average_sales_price')->nullable();
            $table->double('average_sales_price_prior')->nullable();
            $table->double('change_in_glance_view_prior')->nullable();
            $table->double('change_in_glance_view_prior_year')->nullable();
            $table->double('rep_oos')->nullable();
            $table->double('rep_oos_percentage_total')->nullable();
            $table->double('rep_oos_prior')->nullable();
            $table->double('lbb')->nullable();
            $table->date('generated_date')->nullable();
            $table->index('asin_id');
            $table->index('account_id');
            $table->index('program_id');
            $table->index('report_type');
            $table->index('generated_date');
            $table->timestamps();

            $table->foreign('program_id')
                  ->references('id')->on('programs')
                  ->onDelete('cascade');

            $table->foreign('distributor_id')
                  ->references('id')->on('distributors')
                  ->onDelete('cascade');

            $table->foreign('account_id')
                  ->references('id')->on('accounts')
                  ->onDelete('cascade');

            $table->foreign('asin_id')
                  ->references('id')->on('asins')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_diagnostics');
    }
}
