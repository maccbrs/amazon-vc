<?php

use App\Models\{
    User
};

use Illuminate\Support\Str;
use Carbon\Carbon;
use Faker\Generator as Faker;


/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(User::class, function (Faker $faker) {

    return [
        'name' => 'Visibly',
        'email' => 'visibly@channelbakers.com',
        'email_verified_at' => Carbon::now(),
        'password' => password_hash('2IJr0##Zdwo^7', PASSWORD_BCRYPT),
        'remember_token' => Str::random(10),
    ];
});