<?php

use Illuminate\Database\Seeder;

use App\Models\{
    Account
    };

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call('UsersTableSeeder');
    	
/*    	$accounts = [
    		"Vendor Central (CA)" => "Ademco Inc",
			"Vendor Central (CA)" => "Drobo CA",
			"Vendor Central (CA)" => "Guardian Technologies LLC",
			"Vendor Central (CA)" => "Logitech Canada",
			"Vendor Central (CA)" => "VisionTek Products, LLC",
    		"Vendor Central (US)" => "Ademco Inc",
			"Vendor Central (US)" => "Alternative Apparel - Parent Vendor Code",
			"Vendor Central (US)" => "Canon Cameras US",
			"Vendor Central (US)" => "Dallas Manufacturing Company (Brinkmann Pet)",
			"Vendor Central (US)" => "Darex",
			"Vendor Central (US)" => "Easton Sports Inc.",
			"Vendor Central (US)" => "Farnam Companies, Inc.",
			"Vendor Central (US)" => "Gear for Sports, Inc.",
			"Vendor Central (US)" => "Guardian Technologies, LLC",
			"Vendor Central (US)" => "Halo, Purely for Pets",
			"Vendor Central (US)" => "Harrington Plastics",
			"Vendor Central (US)" => "Hestra Gloves",
			"Vendor Central (US)" => "Knight's Apparel",
			"Vendor Central (US)" => "Kyjen",
			"Vendor Central (US)" => "Logitech, Inc",
			"Vendor Central (US)" => "Maui Jim Sunglasses (Maui Jim)",
			"Vendor Central (US)" => "McAfee",
			"Vendor Central (US)" => "NatPets, LLC",
			"Vendor Central (US)" => "Olympus America",
			"Vendor Central (US)" => "PC- Drobo direct",
			"Vendor Central (US)" => "PVH Analytics Portal",
			"Vendor Central (US)" => "Renogy",
			"Vendor Central (US)" => "SAMF()",
			"Vendor Central (US)" => "Samsung - Home",
			"Vendor Central (US)" => "Samsung Telecommunications America, LLC (STA)",
			"Vendor Central (US)" => "The Levy Group Inc - Parent Vendor Code",
			"Vendor Central (US)" => "Urban Armor Gear Inc.",
			"Vendor Central (US)" => "Viome",
			"Vendor Central (US)" => "VisionTek Products, LLC",
			"Vendor Central (US)" => "Warnaco Master Account"
		];

		foreach ($accounts as $key => $value):
	        DB::table('accounts')->insert([
	            'user_id' => '1',
	            'subscription_id' => '1',
	            'account_name' => $value,
	            'marketplace' => 'US',
	        ]);
	    endforeach;*/
    }
}
