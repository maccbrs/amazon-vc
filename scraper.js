var casper = require('casper').create({
    clientScripts: ["jquery.js"],
    viewportSize: {width: 1920, height: 1080}
});

casper.javascriptEnabled = true;
//page.settings.javascriptEnabled = true;
//runner.page.javascriptEnabled = true;
//casper.page.javascriptEnabled = true;
phantom.cookiesEnabled = true;
//casper.settings.userAgent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0';

casper.options.waitTimeout = 999999;
var x = require('casper').selectXPath;

var fs = require('fs');
var AMAZON_SITE = 'https://vendorcentral.amazon.com/analytics/dashboard/salesdiagnostics';
var AMAZON_USER = 'vendorcentral@channelbakers.com';
var AMAZON_PASS = 'VendorCentral1!'; 
var cookie_dir = 'cookies';
var cookie_path = cookie_dir + '/cookies.txt';
var emailInput = 'input#ap_email';
var passInput = 'input#ap_password';
var otp = '#auth-mfa-otpcode';
var otp_remember = '#auth-mfa-remember-device';
var captcha = 'input#auth-captcha-guess';
var x      = require('casper').selectXPath;
var system = require('system');
var utils = require('utils');
var moment = require('moment.js');
var dt = new Date();

casper.userAgent('Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0');

function isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

// Read Cookies
if (fs.isFile(cookie_path)){ 
  phantom.cookies = JSON.parse(fs.read(cookie_path));
}

casper.start(AMAZON_SITE, function() {  
    console.log("Amazon website opened [" + AMAZON_SITE + "]");
    this.capture('opened.png');
});


casper.wait(1000, function() {
  if(this.exists("form[name=signIn]")) {
    console.log("need to login");

    this.capture('login_form.png');//print screen shot after login

    if(casper.exists('#ap_email')){
      this.wait(1000, function(){

        this.mouseEvent('click', emailInput, '15%', '48%');
        this.sendKeys('input#ap_email', AMAZON_USER);
      });
    }

    this.wait(3000, function () {
      this.mouseEvent('click', passInput, '12%', '67%');
      this.sendKeys('input#ap_password', AMAZON_PASS);
      this.click('input[name=rememberMe]');
   });

    this.then(function() {
        this.click('input#signInSubmit');
    }).wait(5000, function(){});

    this.capture('login_submitted.png');

    // write the cookies
    this.wait(1000, function() {
        var cookies = JSON.stringify(phantom.cookies);
        fs.write(cookie_path, cookies, 644);
    });

    casper.then(function (e) {
      if(casper.exists('#auth-captcha-image-container')){
          this.captureSelector('captcha.png', '#auth-captcha-image');
          console.log('captcha:');
          var captcha_code = system.stdin.readLine();

    this.wait(3000, function () {
      this.mouseEvent('click', passInput, '12%', '67%');
      this.sendKeys('input#ap_password', AMAZON_PASS);
    });

    this.wait(3000, function () {
      this.mouseEvent('click', captcha, '12%', '67%');
      this.sendKeys(captcha, captcha_code);
    });

    this.then(function() {
        console.log('submit captcha');
        this.click('input#signInSubmit');
    }).wait(5000, function(){});

      }
      else if(this.exists('#auth-mfa-otpcode')) {
        console.log('otp:');
        var otp_code = system.stdin.readLine();

        this.wait(3000, function () {
          this.mouseEvent('click', otp, '12%', '67%');
          this.sendKeys(otp, otp_code, {keepFocus: true});
          this.mouseEvent('click', otp_remember);
          this.sendKeys(otp, casper.page.event.key.Enter , {keepFocus: true});
          this.capture('otp.png');
        });

        this.wait(1000, function() {
            var cookies = JSON.stringify(phantom.cookies);
            fs.write(cookie_path, cookies, 644);
        });

        this.capture('submit_otp_2.png');
      }

      else {
        console.log("Logged in successfully.");
      }

      this.capture('amazon1.png');//print screen shot after login

      });

    // triggerFail('Login');
  }
  else {
    console.log("already logged in");

        casper.then(function() {

        var token = "FvDKfxj2F9j2Ffj2BL3S3W0zcBJTT1SN8j3D";

        var days = [];
        for (g = 1; g <= 31; g++) {
          days.push(g);
        };
        var counts = [0];

        var vgids = ["499560","3256120","453300","2868260","473520"];

        var years = ["2019"];

          this.each(vgids, function (self, vgid) {

            this.thenOpen('https://vendorcentral.amazon.com/hz/vendor/members/user-management/switch-accounts-checker?vendorGroup='+vgid+'&customerId=A1GW1729DBKXU8');

            this.thenOpen('https://vendorcentral.amazon.com/analytics?ref_=vc_ven-ven-home_subNav');

            this.then(function() {
              fs.write('page.txt', this.getPageContent());

/*              var foo = page.evaluate(function() {
                return window.token;
              });

              fs.write('token.txt', foo);
              console.log(foo);*/
            });

            this.each(years, function (self, year) {
              if (year == '2019') {
                var months = [];
                for (x = 5; x <= 9; x++) {
                  months.push(x);
                }
              }
              else {
                var months = [];
                for (x = 8; x <= 12; x++) {
                  months.push(x);
                }
              }
              this.each(months, function (self, month) {
                var sel_month = month.toString().padStart(2, "0");
                this.each(days, function (self, day) {
                  var sel_day = day.toString().padStart(2, "0");
                  var sel_date = year+sel_month+sel_day;

                  this.then(function() {
                    
                    this.each(counts, function (self, count) {
                      //product
                      this.then(function() {

                        this.wait(1000, function () {
                          this.thenOpen('https://vendorcentral.amazon.com/analytics/data/dashboard/productCatalog/report/productCatalogDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                              method: 'POST',
                              data:   {"reportParameters":[{"parameterId":"distributorView","values":[{"val":"manufacturer"}]},{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"dataRefreshDate","values":[{"val":"20190902"}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"releasedate","ascending":false}],"reportPagination":{"pageIndex":0,"pageSize":100}}},
                              headers: {
                                  'Accept':'application/json',
                                  'Content-Type':'application/json',
                              }
                          });
                        });

                        this.then(function(){
                          fs.write('public/count.json', this.page.plainText);
                          var obj = JSON.parse(this.getPageContent());
                          var rowCount = obj['payload']['reportParts'][0]['rowCount'];
                            if(isEmpty(rowCount)) {
                            } else {
                              var paginate = rowCount.replace(/\,/g,'');
                              var paginate = parseInt(paginate,10);
                              var number = paginate/10000;
                              var pages = Math.floor(number);

                              var parts = [];
                              for (h = 0; h <= pages; h++) {
                                parts.push(h);
                              };

                              this.each(parts, function (self, part) {
                                this.wait(1000, function() {
                                  this.thenOpen('https://vendorcentral.amazon.com/analytics/data/dashboard/productCatalog/report/productCatalogDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                                      method: 'POST',
                                      data:   {"reportParameters":[{"parameterId":"distributorView","values":[{"val":"manufacturer"}]},{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"dataRefreshDate","values":[{"val":"20190902"}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"releasedate","ascending":false}],"reportPagination":{"pageIndex":part,"pageSize":10000}}},
                                      headers: {
                                          'Accept':'application/json',
                                          'Content-Type':'application/json',
                                      }
                                  });
                                  this.then(function(){
                                    fs.write('public/products/ProductCatalog_'+vgid+'_'+sel_date+'_part'+part+'.json', this.page.plainText);
                                  });
                                });
                              });
                            }
                        });

                      });

                      //sales
                      this.then(function() {

                        this.wait(1000, function () {
                          this.thenOpen('https://vendorcentral.amazon.com/analytics/data/dashboard/salesDiagnostic/report/salesDiagnosticDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                            method: 'POST',
                            data:   {"requestId":"b8e3de1a-2a10-4fe2-9e58-f19e68753e3a","reportParameters":[{"parameterId":"distributorView","values":[{"val":"manufacturer"}]},{"parameterId":"viewFilter","values":[{"val":"shippedCOGSLevel"}]},{"parameterId":"productView","values":[{"val":"kindleExcluded"}]},{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"aggregationFilter","values":[{"val":"ASINLevel"}]},{"parameterId":"periodStartDay","values":[{"val":sel_date}]},{"parameterId":"periodEndDay","values":[{"val":sel_date}]},{"parameterId":"isPeriodToDate","values":[{"val":false}]},{"parameterId":"isCustomDateRange","values":[{"val":"false"}]},{"parameterId":"hideLastYear","values":[{"val":false}]},{"parameterId":"period","values":[{"val":"DAILY"}]},{"parameterId":"dataRefreshDate","values":[{"val":"0000790022800"}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]},{"parameterId":"parentASINVisibility","values":[{"val":true}]},{"parameterId":"eanVisibility","values":[{"val":true}]},{"parameterId":"isbn13Visibility","values":[{"val":true}]},{"parameterId":"upcVisibility","values":[{"val":true}]},{"parameterId":"janVisibility","values":[{"val":false}]},{"parameterId":"brandVisibility","values":[{"val":true}]},{"parameterId":"brandCodeVisibility","values":[{"val":false}]},{"parameterId":"subcatVisibility","values":[{"val":true}]},{"parameterId":"catVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeWidthVisibility","values":[{"val":true}]},{"parameterId":"authorVisibility","values":[{"val":true}]},{"parameterId":"bindingVisibility","values":[{"val":true}]},{"parameterId":"catalogNumberVisibility","values":[{"val":false}]},{"parameterId":"colorVisibility","values":[{"val":true}]},{"parameterId":"colorCountVisibility","values":[{"val":false}]},{"parameterId":"manufactureOnDemandVisibility","values":[{"val":false}]},{"parameterId":"listPriceVisibility","values":[{"val":false}]},{"parameterId":"modelStyleVisibility","values":[{"val":true}]},{"parameterId":"productGroupVisibility","values":[{"val":false}]},{"parameterId":"releaseDateVisibility","values":[{"val":true}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"shippedcogs","ascending":false}],"reportPagination":{"pageIndex":count,"pageSize":100}}},
                            headers: {
                                'Accept':'application/json',
                                'Content-Type':'application/json',
                            }
                          });
                        });

                        this.then(function(){
                          fs.write('public/sales_count.json', this.page.plainText);

                          var obj = JSON.parse(this.getPageContent());
                          var rowCount = obj['payload']['reportParts'][0]['rowCount'];

                            if (isEmpty(rowCount)) {}
                            else {
                              var paginate = rowCount.replace(/\,/g,'');
                              var paginate = parseInt(paginate,10);
                              var number = paginate/10000;
                              var pages = Math.floor(number);

                              var parts = [];
                              for (h = 0; h <= pages; h++) {
                                parts.push(h);
                              }

                              this.each(parts, function (self, part) {
                                this.wait(1000, function() {
                                  console.log(sel_date);
                                  this.thenOpen('https://vendorcentral.amazon.com/analytics/data/dashboard/salesDiagnostic/report/salesDiagnosticDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                                      method: 'POST',
                                      data:   {
                                          "requestId":"b8e3de1a-2a10-4fe2-9e58-f19e68753e3a","reportParameters":[{"parameterId":"distributorView","values":[{"val":"manufacturer"}]},{"parameterId":"viewFilter","values":[{"val":"shippedCOGSLevel"}]},{"parameterId":"productView","values":[{"val":"kindleExcluded"}]},{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"aggregationFilter","values":[{"val":"ASINLevel"}]},{"parameterId":"periodStartDay","values":[{"val":sel_date}]},{"parameterId":"periodEndDay","values":[{"val":sel_date}]},{"parameterId":"isPeriodToDate","values":[{"val":false}]},{"parameterId":"isCustomDateRange","values":[{"val":"false"}]},{"parameterId":"hideLastYear","values":[{"val":false}]},{"parameterId":"period","values":[{"val":"DAILY"}]},{"parameterId":"dataRefreshDate","values":[{"val":"0000790022800"}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]},{"parameterId":"parentASINVisibility","values":[{"val":true}]},{"parameterId":"eanVisibility","values":[{"val":true}]},{"parameterId":"isbn13Visibility","values":[{"val":true}]},{"parameterId":"upcVisibility","values":[{"val":true}]},{"parameterId":"janVisibility","values":[{"val":false}]},{"parameterId":"brandVisibility","values":[{"val":true}]},{"parameterId":"brandCodeVisibility","values":[{"val":false}]},{"parameterId":"subcatVisibility","values":[{"val":true}]},{"parameterId":"catVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeWidthVisibility","values":[{"val":true}]},{"parameterId":"authorVisibility","values":[{"val":true}]},{"parameterId":"bindingVisibility","values":[{"val":true}]},{"parameterId":"catalogNumberVisibility","values":[{"val":false}]},{"parameterId":"colorVisibility","values":[{"val":true}]},{"parameterId":"colorCountVisibility","values":[{"val":false}]},{"parameterId":"manufactureOnDemandVisibility","values":[{"val":false}]},{"parameterId":"listPriceVisibility","values":[{"val":false}]},{"parameterId":"modelStyleVisibility","values":[{"val":true}]},{"parameterId":"productGroupVisibility","values":[{"val":false}]},{"parameterId":"releaseDateVisibility","values":[{"val":true}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"shippedcogs","ascending":false}],"reportPagination":{"pageIndex":part,"pageSize":10000}}
                                      },
                                      headers: {
                                          'Accept':'application/json',
                                          'Content-Type':'application/json',
                                      }
                                  });
                                  this.then(function(){
                                    fs.write('public/products/salesdiagnostics/SalesDiagnosticsDetail_retail_'+vgid+'_'+sel_date+'_part'+part+'.json', this.page.plainText);
                                  });
                                });
                              });
                            }

                        });
                      });

                      //geo
                      this.then(function() {
                        
                        this.each(counts, function (self, count) {

                          this.wait(1000, function () {
                            this.thenOpen('https://vendorcentral.amazon.com/analytics/data/dashboard/geographicSalesInsights/report/geographicSalesInsightsDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                                method: 'POST',
                                data:   {"reportParameters":[{"parameterId":"viewFilter","values":[{"val":"shippedCOGSLevel"}]},{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"country","values":[{"val":"US"}]},{"parameterId":"state","values":[{"val":"ALL"}]},{"parameterId":"city","values":[{"val":"ALL"}]},{"parameterId":"zip","values":[{"val":"ALL"}]},{"parameterId":"aggregationFilter","values":[{"val":"ASINLevel"}]},{"parameterId":"countryCode","values":[{"val":"US"}]},{"parameterId":"periodStartDay","values":[{"val":sel_date}]},{"parameterId":"periodEndDay","values":[{"val":sel_date}]},{"parameterId":"isPeriodToDate","values":[{"val":false}]},{"parameterId":"isCustomDateRange","values":[{"val":"false"}]},{"parameterId":"hideLastYear","values":[{"val":false}]},{"parameterId":"period","values":[{"val":"DAILY"}]},{"parameterId":"dataRefreshDate","values":[{"val":"0000093999302"}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]},{"parameterId":"parentASINVisibility","values":[{"val":true}]},{"parameterId":"eanVisibility","values":[{"val":true}]},{"parameterId":"isbn13Visibility","values":[{"val":true}]},{"parameterId":"upcVisibility","values":[{"val":true}]},{"parameterId":"janVisibility","values":[{"val":false}]},{"parameterId":"brandVisibility","values":[{"val":true}]},{"parameterId":"brandCodeVisibility","values":[{"val":false}]},{"parameterId":"subcatVisibility","values":[{"val":true}]},{"parameterId":"catVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeWidthVisibility","values":[{"val":true}]},{"parameterId":"authorVisibility","values":[{"val":true}]},{"parameterId":"bindingVisibility","values":[{"val":true}]},{"parameterId":"catalogNumberVisibility","values":[{"val":false}]},{"parameterId":"colorVisibility","values":[{"val":true}]},{"parameterId":"colorCountVisibility","values":[{"val":false}]},{"parameterId":"manufactureOnDemandVisibility","values":[{"val":false}]},{"parameterId":"listPriceVisibility","values":[{"val":false}]},{"parameterId":"modelStyleVisibility","values":[{"val":true}]},{"parameterId":"productGroupVisibility","values":[{"val":false}]},{"parameterId":"releaseDateVisibility","values":[{"val":false}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"shippedcogs","ascending":false}],"reportPagination":{"pageIndex":count,"pageSize":100}}},
                                headers: {
                                    'Accept':'application/json',
                                    'Content-Type':'application/json',
                                }
                            });
                          });

                          this.then(function(){
                            fs.write('public/geo_count.json', this.page.plainText);
                            var obj = JSON.parse( this.getPageContent());
                            var rowCount = obj['payload']['reportParts'][0]['rowCount'];
                              if(isEmpty(rowCount)) {
                              } else {
                                var paginate = rowCount.replace(/\,/g,'');
                                var paginate = parseInt(paginate,10);
                                var number = paginate/10000;
                                var pages = Math.floor(number);

                                var parts = [];
                                for (h = 0; h <= pages; h++) {
                                  parts.push(h);
                                };

                                this.each(parts, function (self, part) {
                                  this.wait(1000, function() {
                                    this.thenOpen('https://vendorcentral.amazon.com/analytics/data/dashboard/geographicSalesInsights/report/geographicSalesInsightsDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                                        method: 'POST',
                                        data:   {"reportParameters":[{"parameterId":"viewFilter","values":[{"val":"shippedCOGSLevel"}]},{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"country","values":[{"val":"US"}]},{"parameterId":"state","values":[{"val":"ALL"}]},{"parameterId":"city","values":[{"val":"ALL"}]},{"parameterId":"zip","values":[{"val":"ALL"}]},{"parameterId":"aggregationFilter","values":[{"val":"ASINLevel"}]},{"parameterId":"countryCode","values":[{"val":"US"}]},{"parameterId":"periodStartDay","values":[{"val":sel_date}]},{"parameterId":"periodEndDay","values":[{"val":sel_date}]},{"parameterId":"isPeriodToDate","values":[{"val":false}]},{"parameterId":"isCustomDateRange","values":[{"val":"false"}]},{"parameterId":"hideLastYear","values":[{"val":false}]},{"parameterId":"period","values":[{"val":"DAILY"}]},{"parameterId":"dataRefreshDate","values":[{"val":"0000093999302"}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]},{"parameterId":"parentASINVisibility","values":[{"val":true}]},{"parameterId":"eanVisibility","values":[{"val":true}]},{"parameterId":"isbn13Visibility","values":[{"val":true}]},{"parameterId":"upcVisibility","values":[{"val":true}]},{"parameterId":"janVisibility","values":[{"val":false}]},{"parameterId":"brandVisibility","values":[{"val":true}]},{"parameterId":"brandCodeVisibility","values":[{"val":false}]},{"parameterId":"subcatVisibility","values":[{"val":true}]},{"parameterId":"catVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeWidthVisibility","values":[{"val":true}]},{"parameterId":"authorVisibility","values":[{"val":true}]},{"parameterId":"bindingVisibility","values":[{"val":true}]},{"parameterId":"catalogNumberVisibility","values":[{"val":false}]},{"parameterId":"colorVisibility","values":[{"val":true}]},{"parameterId":"colorCountVisibility","values":[{"val":false}]},{"parameterId":"manufactureOnDemandVisibility","values":[{"val":false}]},{"parameterId":"listPriceVisibility","values":[{"val":false}]},{"parameterId":"modelStyleVisibility","values":[{"val":true}]},{"parameterId":"productGroupVisibility","values":[{"val":false}]},{"parameterId":"releaseDateVisibility","values":[{"val":false}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"shippedcogs","ascending":false}],"reportPagination":{"pageIndex":part,"pageSize":10000}}},
                                        headers: {
                                            'Accept':'application/json',
                                            'Content-Type':'application/json',
                                        }
                                    });
                                    this.then(function(){
                                      fs.write('public/products/geographic/GeographicSalesDetail_retail_'+vgid+'_'+sel_date+'_part'+part+'.json', this.page.plainText);
                                    });
                                  });
                                });
                              }
                          });

                        });
                      });


//
                    });

                  });

                  //pre-orders
                  this.then(function() {
                    
                    this.each(counts, function (self, count) {

                      this.wait(1000, function () {
                        this.thenOpen('https://vendorcentral.amazon.com/analytics/data/dashboard/preOrders/report/preOrdersDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                            method: 'POST',
                            data:   {"requestId":"81c2f7f7-f516-4b36-a826-4b6992eaedc2","reportParameters":[{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"aggregationFilter","values":[{"val":"ASINLevel"}]},{"parameterId":"periodStartDay","values":[{"val":sel_date}]},{"parameterId":"periodEndDay","values":[{"val":sel_date}]},{"parameterId":"isPeriodToDate","values":[{"val":false}]},{"parameterId":"isCustomDateRange","values":[{"val":"false"}]},{"parameterId":"hideLastYear","values":[{"val":false}]},{"parameterId":"period","values":[{"val":"DAILY"}]},{"parameterId":"dataRefreshDate","values":[{"val":"0000502050172"}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]},{"parameterId":"parentASINVisibility","values":[{"val":true}]},{"parameterId":"eanVisibility","values":[{"val":true}]},{"parameterId":"isbn13Visibility","values":[{"val":true}]},{"parameterId":"upcVisibility","values":[{"val":true}]},{"parameterId":"janVisibility","values":[{"val":false}]},{"parameterId":"brandVisibility","values":[{"val":true}]},{"parameterId":"brandCodeVisibility","values":[{"val":false}]},{"parameterId":"subcatVisibility","values":[{"val":true}]},{"parameterId":"catVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeWidthVisibility","values":[{"val":true}]},{"parameterId":"authorVisibility","values":[{"val":true}]},{"parameterId":"bindingVisibility","values":[{"val":true}]},{"parameterId":"catalogNumberVisibility","values":[{"val":false}]},{"parameterId":"colorVisibility","values":[{"val":true}]},{"parameterId":"colorCountVisibility","values":[{"val":false}]},{"parameterId":"manufactureOnDemandVisibility","values":[{"val":false}]},{"parameterId":"listPriceVisibility","values":[{"val":false}]},{"parameterId":"modelStyleVisibility","values":[{"val":true}]},{"parameterId":"productGroupVisibility","values":[{"val":false}]},{"parameterId":"releaseDateVisibility","values":[{"val":true}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"preorderedamount","ascending":false}],"reportPagination":{"pageIndex":count,"pageSize":100}}},
                            headers: {
                                'Accept':'application/json',
                                'Content-Type':'application/json',
                            }
                        });
                      });

                      this.then(function(){
                        fs.write('public/pre_count.json', this.page.plainText);
                        var obj = JSON.parse( this.getPageContent());
                        var rowCount = obj['payload']['reportParts'][0]['rowCount'];
                          if(isEmpty(rowCount)) {
                          } else {
                            var paginate = rowCount.replace(/\,/g,'');
                            var paginate = parseInt(paginate,10);
                            var number = paginate/10000;
                            var pages = Math.floor(number);

                            var parts = [];
                            for (h = 0; h <= pages; h++) {
                              parts.push(h);
                            };

                            this.each(parts, function (self, part) {
                              this.wait(1000, function() {
                                this.thenOpen('https://vendorcentral.amazon.com/analytics/data/dashboard/preOrders/report/preOrdersDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                                    method: 'POST',
                                    data:   {"requestId":"81c2f7f7-f516-4b36-a826-4b6992eaedc2","reportParameters":[{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"aggregationFilter","values":[{"val":"ASINLevel"}]},{"parameterId":"periodStartDay","values":[{"val":sel_date}]},{"parameterId":"periodEndDay","values":[{"val":sel_date}]},{"parameterId":"isPeriodToDate","values":[{"val":false}]},{"parameterId":"isCustomDateRange","values":[{"val":"false"}]},{"parameterId":"hideLastYear","values":[{"val":false}]},{"parameterId":"period","values":[{"val":"DAILY"}]},{"parameterId":"dataRefreshDate","values":[{"val":"0000502050172"}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]},{"parameterId":"parentASINVisibility","values":[{"val":true}]},{"parameterId":"eanVisibility","values":[{"val":true}]},{"parameterId":"isbn13Visibility","values":[{"val":true}]},{"parameterId":"upcVisibility","values":[{"val":true}]},{"parameterId":"janVisibility","values":[{"val":false}]},{"parameterId":"brandVisibility","values":[{"val":true}]},{"parameterId":"brandCodeVisibility","values":[{"val":false}]},{"parameterId":"subcatVisibility","values":[{"val":true}]},{"parameterId":"catVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeWidthVisibility","values":[{"val":true}]},{"parameterId":"authorVisibility","values":[{"val":true}]},{"parameterId":"bindingVisibility","values":[{"val":true}]},{"parameterId":"catalogNumberVisibility","values":[{"val":false}]},{"parameterId":"colorVisibility","values":[{"val":true}]},{"parameterId":"colorCountVisibility","values":[{"val":false}]},{"parameterId":"manufactureOnDemandVisibility","values":[{"val":false}]},{"parameterId":"listPriceVisibility","values":[{"val":false}]},{"parameterId":"modelStyleVisibility","values":[{"val":true}]},{"parameterId":"productGroupVisibility","values":[{"val":false}]},{"parameterId":"releaseDateVisibility","values":[{"val":true}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"preorderedamount","ascending":false}],"reportPagination":{"pageIndex":page,"pageSize":10000}}},
                                    headers: {
                                        'Accept':'application/json',
                                        'Content-Type':'application/json',
                                    }
                                });
                                this.then(function(){
                                  fs.write('public/products/preorder/PreOrdersDetail_'+vgid+'_'+sel_date+'_part'+part+'.json', this.page.plainText);
                                });
                              });
                            });
                          }
                      });

                    });
                  });

                  //inventory
                  this.then(function() {
                    
                    this.each(counts, function (self, count) {

                      this.wait(1000, function () {
                        this.thenOpen('https://vendorcentral.amazon.com/analytics/data/dashboard/inventoryHealth/report/inventoryHealthDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                            method: 'POST',
                            data:   {"requestId":"bdefc92c-7660-4b77-b928-83bed94f0682","reportParameters":[{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"aggregationFilter","values":[{"val":"ASINLevel"}]},{"parameterId":"distributorView","values":[{"val":"manufacturer"}]},{"parameterId":"isFCAllowed","values":[{"val":"false"}]},{"parameterId":"periodStartDay","values":[{"val":sel_date}]},{"parameterId":"periodEndDay","values":[{"val":sel_date}]},{"parameterId":"isPeriodToDate","values":[{"val":false}]},{"parameterId":"isCustomDateRange","values":[{"val":"false"}]},{"parameterId":"hideLastYear","values":[{"val":false}]},{"parameterId":"period","values":[{"val":"DAILY"}]},{"parameterId":"dataRefreshDate","values":[{"val":"0002032000337"}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]},{"parameterId":"parentASINVisibility","values":[{"val":true}]},{"parameterId":"eanVisibility","values":[{"val":true}]},{"parameterId":"isbn13Visibility","values":[{"val":true}]},{"parameterId":"upcVisibility","values":[{"val":true}]},{"parameterId":"janVisibility","values":[{"val":false}]},{"parameterId":"brandVisibility","values":[{"val":true}]},{"parameterId":"brandCodeVisibility","values":[{"val":true}]},{"parameterId":"subcatVisibility","values":[{"val":true}]},{"parameterId":"catVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeWidthVisibility","values":[{"val":true}]},{"parameterId":"authorVisibility","values":[{"val":true}]},{"parameterId":"bindingVisibility","values":[{"val":true}]},{"parameterId":"catalogNumberVisibility","values":[{"val":false}]},{"parameterId":"colorVisibility","values":[{"val":true}]},{"parameterId":"colorCountVisibility","values":[{"val":true}]},{"parameterId":"manufactureOnDemandVisibility","values":[{"val":false}]},{"parameterId":"listPriceVisibility","values":[{"val":true}]},{"parameterId":"modelStyleVisibility","values":[{"val":true}]},{"parameterId":"productGroupVisibility","values":[{"val":true}]},{"parameterId":"releaseDateVisibility","values":[{"val":true}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"sellableonhandinventory","ascending":false}],"reportPagination":{"pageIndex":count,"pageSize":100}}},
                            headers: {
                                'Accept':'application/json',
                                'Content-Type':'application/json',
                            }
                        });
                      });

                      this.then(function(){
                        fs.write('public/inv_count.json', this.page.plainText);
                        var obj = JSON.parse( this.getPageContent());
                        var rowCount = obj['payload']['reportParts'][0]['rowCount'];
                          if(isEmpty(rowCount)) {
                          } else {
                            var paginate = rowCount.replace(/\,/g,'');
                            var paginate = parseInt(paginate,10);
                            var number = paginate/10000;
                            var pages = Math.floor(number);

                            var parts = [];
                            for (h = 0; h <= pages; h++) {
                              parts.push(h);
                            };

                            this.each(parts, function (self, part) {
                              this.wait(1000, function() {
                                this.thenOpen('https://vendorcentral.amazon.com/analytics/data/dashboard/inventoryHealth/report/inventoryHealthDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                                    method: 'POST',
                                    data:   {"requestId":"bdefc92c-7660-4b77-b928-83bed94f0682","reportParameters":[{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"aggregationFilter","values":[{"val":"ASINLevel"}]},{"parameterId":"distributorView","values":[{"val":"manufacturer"}]},{"parameterId":"isFCAllowed","values":[{"val":"false"}]},{"parameterId":"periodStartDay","values":[{"val":sel_date}]},{"parameterId":"periodEndDay","values":[{"val":sel_date}]},{"parameterId":"isPeriodToDate","values":[{"val":false}]},{"parameterId":"isCustomDateRange","values":[{"val":"false"}]},{"parameterId":"hideLastYear","values":[{"val":false}]},{"parameterId":"period","values":[{"val":"DAILY"}]},{"parameterId":"dataRefreshDate","values":[{"val":"0002032000337"}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]},{"parameterId":"parentASINVisibility","values":[{"val":true}]},{"parameterId":"eanVisibility","values":[{"val":true}]},{"parameterId":"isbn13Visibility","values":[{"val":true}]},{"parameterId":"upcVisibility","values":[{"val":true}]},{"parameterId":"janVisibility","values":[{"val":false}]},{"parameterId":"brandVisibility","values":[{"val":true}]},{"parameterId":"brandCodeVisibility","values":[{"val":true}]},{"parameterId":"subcatVisibility","values":[{"val":true}]},{"parameterId":"catVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeWidthVisibility","values":[{"val":true}]},{"parameterId":"authorVisibility","values":[{"val":true}]},{"parameterId":"bindingVisibility","values":[{"val":true}]},{"parameterId":"catalogNumberVisibility","values":[{"val":false}]},{"parameterId":"colorVisibility","values":[{"val":true}]},{"parameterId":"colorCountVisibility","values":[{"val":true}]},{"parameterId":"manufactureOnDemandVisibility","values":[{"val":false}]},{"parameterId":"listPriceVisibility","values":[{"val":true}]},{"parameterId":"modelStyleVisibility","values":[{"val":true}]},{"parameterId":"productGroupVisibility","values":[{"val":true}]},{"parameterId":"releaseDateVisibility","values":[{"val":true}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"sellableonhandinventory","ascending":false}],"reportPagination":{"pageIndex":part,"pageSize":10000}}},
                                    headers: {
                                        'Accept':'application/json',
                                        'Content-Type':'application/json',
                                    }
                                });
                                this.then(function(){
                                  fs.write('public/products/inventory/InventoryHealthDetail_'+vgid+'_'+sel_date+'_part'+part+'.json', this.page.plainText);
                                });
                              });
                            });
                          }
                      });

                    });
                  });

                  //alternative
                  this.then(function() {
                    
                    this.each(counts, function (self, count) {

                      this.wait(1000, function () {
                        this.thenOpen('https://vendorcentral.amazon.com/analytics/data/dashboard/itemComparisonAndAlternativePurchase/report/alternativePurchase?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                            method: 'POST',
                            data:   {"requestId":"8d07b226-5105-49cc-994e-f4371692ddeb","reportParameters":[{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"alternativePurchaseAggregationFilter","values":[{"val":"allProductsLevel"}]},{"parameterId":"period","values":[{"val":"DAILY"}]},{"parameterId":"periodEndDay","values":[{"val":sel_date}]},{"parameterId":"dataRefreshDate","values":[{"val":"0001361388692"}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]},{"parameterId":"parentASINVisibility","values":[{"val":false}]},{"parameterId":"eanVisibility","values":[{"val":false}]},{"parameterId":"isbn13Visibility","values":[{"val":false}]},{"parameterId":"upcVisibility","values":[{"val":false}]},{"parameterId":"janVisibility","values":[{"val":false}]},{"parameterId":"brandVisibility","values":[{"val":false}]},{"parameterId":"brandCodeVisibility","values":[{"val":false}]},{"parameterId":"subcatVisibility","values":[{"val":false}]},{"parameterId":"catVisibility","values":[{"val":false}]},{"parameterId":"apparelSizeVisibility","values":[{"val":false}]},{"parameterId":"apparelSizeWidthVisibility","values":[{"val":false}]},{"parameterId":"authorVisibility","values":[{"val":false}]},{"parameterId":"bindingVisibility","values":[{"val":false}]},{"parameterId":"catalogNumberVisibility","values":[{"val":false}]},{"parameterId":"colorVisibility","values":[{"val":false}]},{"parameterId":"colorCountVisibility","values":[{"val":false}]},{"parameterId":"manufactureOnDemandVisibility","values":[{"val":false}]},{"parameterId":"listPriceVisibility","values":[{"val":false}]},{"parameterId":"modelStyleVisibility","values":[{"val":false}]},{"parameterId":"productGroupVisibility","values":[{"val":false}]},{"parameterId":"releaseDateVisibility","values":[{"val":false}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"orders","ascending":false}],"reportPagination":{"pageIndex":count,"pageSize":100}}},
                            headers: {
                                'Accept':'application/json',
                                'Content-Type':'application/json',
                            }
                        });
                      });

                      this.then(function(){
                        fs.write('public/alt_count.json', this.page.plainText);
                        var obj = JSON.parse( this.getPageContent());
                        var rowCount = obj['payload']['reportParts'][0]['rowCount'];
                          if(isEmpty(rowCount)) {
                          } else {
                            var paginate = rowCount.replace(/\,/g,'');
                            var paginate = parseInt(paginate,10);
                            var number = paginate/10000;
                            var pages = Math.floor(number);

                            var parts = [];
                            for (h = 0; h <= pages; h++) {
                              parts.push(h);
                            };

                            this.each(parts, function (self, part) {
                              this.wait(1000, function() {
                                this.thenOpen('https://vendorcentral.amazon.com/analytics/data/dashboard/itemComparisonAndAlternativePurchase/report/alternativePurchase?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                                    method: 'POST',
                                    data:   {"requestId":"8d07b226-5105-49cc-994e-f4371692ddeb","reportParameters":[{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"alternativePurchaseAggregationFilter","values":[{"val":"allProductsLevel"}]},{"parameterId":"period","values":[{"val":"DAILY"}]},{"parameterId":"periodEndDay","values":[{"val":sel_date}]},{"parameterId":"dataRefreshDate","values":[{"val":"0001361388692"}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]},{"parameterId":"parentASINVisibility","values":[{"val":false}]},{"parameterId":"eanVisibility","values":[{"val":false}]},{"parameterId":"isbn13Visibility","values":[{"val":false}]},{"parameterId":"upcVisibility","values":[{"val":false}]},{"parameterId":"janVisibility","values":[{"val":false}]},{"parameterId":"brandVisibility","values":[{"val":false}]},{"parameterId":"brandCodeVisibility","values":[{"val":false}]},{"parameterId":"subcatVisibility","values":[{"val":false}]},{"parameterId":"catVisibility","values":[{"val":false}]},{"parameterId":"apparelSizeVisibility","values":[{"val":false}]},{"parameterId":"apparelSizeWidthVisibility","values":[{"val":false}]},{"parameterId":"authorVisibility","values":[{"val":false}]},{"parameterId":"bindingVisibility","values":[{"val":false}]},{"parameterId":"catalogNumberVisibility","values":[{"val":false}]},{"parameterId":"colorVisibility","values":[{"val":false}]},{"parameterId":"colorCountVisibility","values":[{"val":false}]},{"parameterId":"manufactureOnDemandVisibility","values":[{"val":false}]},{"parameterId":"listPriceVisibility","values":[{"val":false}]},{"parameterId":"modelStyleVisibility","values":[{"val":false}]},{"parameterId":"productGroupVisibility","values":[{"val":false}]},{"parameterId":"releaseDateVisibility","values":[{"val":false}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"orders","ascending":false}],"reportPagination":{"pageIndex":part,"pageSize":10000}}},
                                    headers: {
                                        'Accept':'application/json',
                                        'Content-Type':'application/json',
                                    }
                                });
                                this.then(function(){
                                  fs.write('public/products/alternate/AlternativePurchaseDetail_'+vgid+'_'+sel_date+'_part'+part+'.json', this.page.plainText);
                                });
                              });
                            });
                          }
                      });

                    });
                  });


                });

              });
            });
          });
        });
  }
});

casper.run();
