                this.then(function() {

                  this.then(function() {
                      this.then(function() {
                        if (vgid != '') {
                          this.wait(1000, function () {
                            this.thenOpen('https://vendorcentral.amazon.com/analytics/data/dashboard/productCatalog/report/productCatalogDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                                method: 'POST',
                                data:   {"reportParameters":[{"parameterId":"distributorView","values":[{"val":"manufacturer"}]},{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"dataRefreshDate","values":[{"val":prod_today}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"releasedate","ascending":false}],"reportPagination":{"pageIndex":0,"pageSize":100}}},
                                headers: {
                                    'Accept':'application/json',
                                    'Content-Type':'application/json',
                                }
                            });
                          });

                          this.then(function(){
                            fs.write('storage/count.json', this.page.plainText);
                            var obj = JSON.parse(this.getPageContent());

                              if(obj && obj['payload'] && obj['payload']['reportParts'] && obj['payload']['reportParts'][0] && obj['payload']['reportParts'][0]['rowCount']) {
                                var rowCount = obj['payload']['reportParts'][0]['rowCount'];
                              console.log('RowCount: '+rowCount+'');

                                var paginate = rowCount.replace(/\,/g,'');
                                paginate = parseInt(paginate,10);
                                var number = paginate/10000;
                                var pages = Math.floor(number);

                                var parts = [];
                                for (h = 0; h <= pages; h++) {
                                  parts.push(h);
                                }

                                this.each(parts, function (self, part) {
                                  if (fs.isFile('storage/products/ProductCatalog_manufacturing_'+vgid+'_'+prod_today+'_part'+part+'.json')){ 
                                    console.log('Product Already Scraped!');
                                  } else {
                                  this.wait(1000, function() {
                                    this.thenOpen('https://vendorcentral.amazon.com/analytics/data/dashboard/productCatalog/report/productCatalogDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                                        method: 'POST',
                                        data:   {"reportParameters":[{"parameterId":"distributorView","values":[{"val":"manufacturer"}]},{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"dataRefreshDate","values":[{"val":prod_today}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"releasedate","ascending":false}],"reportPagination":{"pageIndex":part,"pageSize":10000}}},
                                        headers: {
                                            'Accept':'application/json',
                                            'Content-Type':'application/json',
                                        }
                                    });
                                    this.then(function(){
                                      console.log('ProductCatalog_manufacturing_'+vgid+'_'+prod_today+'_part'+part+'.json');
                                      fs.write('storage/products/ProductCatalog_manufacturing_'+vgid+'_'+prod_today+'_part'+part+'.json', this.page.plainText);
                                    });
                                  });
                                  }
                                });
                              } else {
                                console.log('ProductCatalog_manufacturing_'+vgid+'_'+prod_today+' : NONE');
                              }
                          });
                        }
                      });
                  });

                  this.then(function() {
                      this.then(function() {
                        if (vgid != '') {
                          this.wait(1000, function () {
                            this.thenOpen('https://vendorcentral.amazon.com/analytics/data/dashboard/productCatalog/report/productCatalogDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                                method: 'POST',
                                data:   {"reportParameters":[{"parameterId":"distributorView","values":[{"val":"sourcing"}]},{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"dataRefreshDate","values":[{"val":prod_today}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"releasedate","ascending":false}],"reportPagination":{"pageIndex":0,"pageSize":100}}},
                                headers: {
                                    'Accept':'application/json',
                                    'Content-Type':'application/json',
                                }
                            });
                          });

                          this.then(function(){
                            fs.write('storage/count.json', this.page.plainText);
                            var obj = JSON.parse(this.getPageContent());

                              if(obj && obj['payload'] && obj['payload']['reportParts'] && obj['payload']['reportParts'][0] && obj['payload']['reportParts'][0]['rowCount']) {
                                var rowCount = obj['payload']['reportParts'][0]['rowCount'];
                              console.log('RowCount: '+rowCount+'');

                                var paginate = rowCount.replace(/\,/g,'');
                                paginate = parseInt(paginate,10);
                                var number = paginate/10000;
                                var pages = Math.floor(number);

                                var parts = [];
                                for (h = 0; h <= pages; h++) {
                                  parts.push(h);
                                }

                                this.each(parts, function (self, part) {
                                  if (fs.isFile('storage/products/ProductCatalog_sourcing_'+vgid+'_'+prod_today+'_part'+part+'.json')){ 
                                    console.log('Product Already Scraped!');
                                  } else {
                                  this.wait(1000, function() {
                                    this.thenOpen('https://vendorcentral.amazon.com/analytics/data/dashboard/productCatalog/report/productCatalogDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                                        method: 'POST',
                                        data:   {"reportParameters":[{"parameterId":"distributorView","values":[{"val":"sourcing"}]},{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"dataRefreshDate","values":[{"val":prod_today}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"releasedate","ascending":false}],"reportPagination":{"pageIndex":part,"pageSize":10000}}},
                                        headers: {
                                            'Accept':'application/json',
                                            'Content-Type':'application/json',
                                        }
                                    });
                                    this.then(function(){
                                      console.log('ProductCatalog_sourcing_'+vgid+'_'+prod_today+'_part'+part+'.json');
                                      fs.write('storage/products/ProductCatalog_sourcing_'+vgid+'_'+prod_today+'_part'+part+'.json', this.page.plainText);
                                    });
                                  });
                                  }
                                });
                              } else {
                                console.log('ProductCatalog_sourcing_'+vgid+'_'+prod_today+' : NONE');
                              }
                          });
                        }
                      });
                  });

                  this.then(function() {
                      this.then(function() {
                        if (vgid != '') {
                          this.wait(1000, function () {
                            this.thenOpen('https://vendorcentral.amazon.com/analytics/data/dashboard/productCatalog/report/productCatalogDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                                method: 'POST',
                                data:   {"reportParameters":[{"parameterId":"distributorView","values":[{"val":"consignment"}]},{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"dataRefreshDate","values":[{"val":prod_today}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"releasedate","ascending":false}],"reportPagination":{"pageIndex":0,"pageSize":100}}},
                                headers: { ZX
                                    'Accept':'application/json',
                                    'Content-Type':'application/json',
                                }
                            });
                          });

                          this.then(function(){
                            fs.write('storage/count.json', this.page.plainText);
                            var obj = JSON.parse(this.getPageContent());

                              if(obj && obj['payload'] && obj['payload']['reportParts'] && obj['payload']['reportParts'][0] && obj['payload']['reportParts'][0]['rowCount']) {
                                var rowCount = obj['payload']['reportParts'][0]['rowCount'];
                              console.log('RowCount: '+rowCount+'');

                                var paginate = rowCount.replace(/\,/g,'');
                                paginate = parseInt(paginate,10);
                                var number = paginate/10000;
                                var pages = Math.floor(number);

                                var parts = [];
                                for (h = 0; h <= pages; h++) {
                                  parts.push(h);
                                }

                                this.each(parts, function (self, part) {
                                  if (fs.isFile('storage/products/ProductCatalog_consignment_'+vgid+'_'+prod_today+'_part'+part+'.json')){ 
                                    console.log('Product Already Scraped!');
                                  } else {
                                  this.wait(1000, function() {
                                    this.thenOpen('https://vendorcentral.amazon.com/analytics/data/dashboard/productCatalog/report/productCatalogDetail?token='+token+'&vgId='+vgid+'&mcId=0&product=ara',{
                                        method: 'POST',
                                        data:   {"reportParameters":[{"parameterId":"distributorView","values":[{"val":"consignment"}]},{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"dataRefreshDate","values":[{"val":prod_today}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"releasedate","ascending":false}],"reportPagination":{"pageIndex":part,"pageSize":10000}}},
                                        headers: {
                                            'Accept':'application/json',
                                            'Content-Type':'application/json',
                                        }
                                    });
                                    this.then(function(){
                                      console.log('ProductCatalog_consignment_'+vgid+'_'+prod_today+'_part'+part+'.json');
                                      fs.write('storage/products/ProductCatalog_consignment_'+vgid+'_'+prod_today+'_part'+part+'.json', this.page.plainText);
                                    });
                                  });
                                  }
                                });
                              } else {
                                console.log('ProductCatalog_consignment_'+vgid+'_'+prod_today+' : NONE');
                              }
                          });
                        }
                      });
                  });


                });