                    this.then(function() {                    
                      //retail
                      this.then(function() {
                        this.wait(1000, function () {
                          console.log('test_log');
                          console.log(o);
                          console.log(old_vgids[o]);
                          console.log('https://vendorcentral.amazon.com/analytics/data/dashboard/salesDiagnostic/report/salesDiagnosticDetail?token='+token+'&vgId='+old_vgids[o]+'&mcId=0&product=ara');
                          this.thenOpen('https://vendorcentral.amazon.com/analytics/data/dashboard/salesDiagnostic/report/salesDiagnosticDetail?token='+token+'&vgId='+old_vgids[o]+'&mcId=0&product=ara',{
                            method: 'POST',
                            data:   {"requestId":"b8e3de1a-2a10-4fe2-9e58-f19e68753e3a","reportParameters":[{"parameterId":"distributorView","values":[{"val":"manufacturer"}]},{"parameterId":"viewFilter","values":[{"val":"shippedCOGSLevel"}]},{"parameterId":"productView","values":[{"val":"kindleExcluded"}]},{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"aggregationFilter","values":[{"val":"ASINLevel"}]},{"parameterId":"periodStartDay","values":[{"val":date}]},{"parameterId":"periodEndDay","values":[{"val":date}]},{"parameterId":"isPeriodToDate","values":[{"val":false}]},{"parameterId":"isCustomDateRange","values":[{"val":"false"}]},{"parameterId":"hideLastYear","values":[{"val":false}]},{"parameterId":"period","values":[{"val":"DAILY"}]},{"parameterId":"dataRefreshDate","values":[{"val":"0000790022800"}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]},{"parameterId":"parentASINVisibility","values":[{"val":true}]},{"parameterId":"eanVisibility","values":[{"val":true}]},{"parameterId":"isbn13Visibility","values":[{"val":true}]},{"parameterId":"upcVisibility","values":[{"val":true}]},{"parameterId":"janVisibility","values":[{"val":false}]},{"parameterId":"brandVisibility","values":[{"val":true}]},{"parameterId":"brandCodeVisibility","values":[{"val":false}]},{"parameterId":"subcatVisibility","values":[{"val":true}]},{"parameterId":"catVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeWidthVisibility","values":[{"val":true}]},{"parameterId":"authorVisibility","values":[{"val":true}]},{"parameterId":"bindingVisibility","values":[{"val":true}]},{"parameterId":"catalogNumberVisibility","values":[{"val":false}]},{"parameterId":"colorVisibility","values":[{"val":true}]},{"parameterId":"colorCountVisibility","values":[{"val":false}]},{"parameterId":"manufactureOnDemandVisibility","values":[{"val":false}]},{"parameterId":"listPriceVisibility","values":[{"val":false}]},{"parameterId":"modelStyleVisibility","values":[{"val":true}]},{"parameterId":"productGroupVisibility","values":[{"val":false}]},{"parameterId":"releaseDateVisibility","values":[{"val":true}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"shippedcogs","ascending":false}],"reportPagination":{"pageIndex":0,"pageSize":100}}},
                            headers: {
                                'Accept':'application/json',
                                'Content-Type':'application/json',
                            }
                          });
                        });

                        this.then(function(){
                          fs.write('storage/sales_retail_count.json', this.page.plainText);
                          var obj = JSON.parse(this.getPageContent());

                            if (obj && obj['payload'] && obj['payload']['reportParts'] && obj['payload']['reportParts'][0] && obj['payload']['reportParts'][0]['rowCount']) {
                              var rowCount = obj['payload']['reportParts'][0]['rowCount'];
                              console.log('RowCount: '+rowCount+'');
                              var paginate = rowCount.replace(/\,/g,'');
                              paginate = parseInt(paginate,10);
                              var number = paginate/10000;
                              var pages = Math.floor(number);

                              var parts = [];
                              for (h = 0; h <= pages; h++) {
                                parts.push(h);
                              }

                              this.each(parts, function (self, part) {
                              if (fs.isFile('storage/salesdiagnostics/SalesDiagnosticsDetail_retail_'+old_vgids[o]+'_'+date+'_part'+part+'.json') && compare_to != date){ 
                                console.log('Sales Retail Already Scraped!');
                              } else {
                                this.wait(1000, function() {
                                  
                                  this.thenOpen('https://vendorcentral.amazon.com/analytics/data/dashboard/salesDiagnostic/report/salesDiagnosticDetail?token='+token+'&vgId='+old_vgids[o]+'&mcId=0&product=ara',{
                                      method: 'POST',
                                      data:   {
                                          "requestId":"b8e3de1a-2a10-4fe2-9e58-f19e68753e3a","reportParameters":[{"parameterId":"distributorView","values":[{"val":"manufacturer"}]},{"parameterId":"viewFilter","values":[{"val":"shippedCOGSLevel"}]},{"parameterId":"productView","values":[{"val":"kindleExcluded"}]},{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"aggregationFilter","values":[{"val":"ASINLevel"}]},{"parameterId":"periodStartDay","values":[{"val":date}]},{"parameterId":"periodEndDay","values":[{"val":date}]},{"parameterId":"isPeriodToDate","values":[{"val":false}]},{"parameterId":"isCustomDateRange","values":[{"val":"false"}]},{"parameterId":"hideLastYear","values":[{"val":false}]},{"parameterId":"period","values":[{"val":"DAILY"}]},{"parameterId":"dataRefreshDate","values":[{"val":"0000790022800"}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]},{"parameterId":"parentASINVisibility","values":[{"val":true}]},{"parameterId":"eanVisibility","values":[{"val":true}]},{"parameterId":"isbn13Visibility","values":[{"val":true}]},{"parameterId":"upcVisibility","values":[{"val":true}]},{"parameterId":"janVisibility","values":[{"val":false}]},{"parameterId":"brandVisibility","values":[{"val":true}]},{"parameterId":"brandCodeVisibility","values":[{"val":false}]},{"parameterId":"subcatVisibility","values":[{"val":true}]},{"parameterId":"catVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeWidthVisibility","values":[{"val":true}]},{"parameterId":"authorVisibility","values":[{"val":true}]},{"parameterId":"bindingVisibility","values":[{"val":true}]},{"parameterId":"catalogNumberVisibility","values":[{"val":false}]},{"parameterId":"colorVisibility","values":[{"val":true}]},{"parameterId":"colorCountVisibility","values":[{"val":false}]},{"parameterId":"manufactureOnDemandVisibility","values":[{"val":false}]},{"parameterId":"listPriceVisibility","values":[{"val":false}]},{"parameterId":"modelStyleVisibility","values":[{"val":true}]},{"parameterId":"productGroupVisibility","values":[{"val":false}]},{"parameterId":"releaseDateVisibility","values":[{"val":true}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"shippedcogs","ascending":false}],"reportPagination":{"pageIndex":part,"pageSize":10000}}
                                      },
                                      headers: {
                                          'Accept':'application/json',
                                          'Content-Type':'application/json',
                                      }
                                  });
                                  this.then(function(){
                                    console.log('SalesDiagnosticsDetail_retail_'+old_vgids[o]+'_'+date+'_part'+part+'.json');
                                    fs.write('storage/salesdiagnostics/SalesDiagnosticsDetail_retail_'+old_vgids[o]+'_'+date+'_part'+part+'.json', this.page.plainText);
                                  });
                                });
                              }
                              });
                            }
                            else {
                              console.log('SalesDiagnosticsDetail_retail_'+old_vgids[o]+'_'+date+' : NONE');
                            }
                        });
                    });
                      //fresh
                      this.then(function() {
                        this.wait(1000, function () {
                          this.thenOpen('https://vendorcentral.amazon.com/analytics/data/dashboard/freshSalesDiagnostic/report/freshSalesDiagnosticDetail?token='+token+'&vgId='+old_vgids[o]+'&mcId=0&product=ara',{
                            method: 'POST',
                            data:   {"requestId":"741b983e-a9c6-449f-a9e8-e885fa13d88b","reportParameters":[{"parameterId":"viewFilter","values":[{"val":"shippedCOGSLevel"}]},{"parameterId":"city","values":[{"val":"ALL"}]},{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"aggregationFilter","values":[{"val":"ASINLevel"}]},{"parameterId":"isPeriodToDate","values":[{"val":false}]},{"parameterId":"isCustomDateRange","values":[{"val":"false"}]},{"parameterId":"hideLastYear","values":[{"val":false}]},{"parameterId":"period","values":[{"val":"DAILY"}]},{"parameterId":"dataRefreshDate","values":[{"val":"0001024832551"}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]},{"parameterId":"parentASINVisibility","values":[{"val":true}]},{"parameterId":"eanVisibility","values":[{"val":true}]},{"parameterId":"isbn13Visibility","values":[{"val":true}]},{"parameterId":"upcVisibility","values":[{"val":true}]},{"parameterId":"janVisibility","values":[{"val":false}]},{"parameterId":"brandVisibility","values":[{"val":true}]},{"parameterId":"brandCodeVisibility","values":[{"val":false}]},{"parameterId":"subcatVisibility","values":[{"val":true}]},{"parameterId":"catVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeWidthVisibility","values":[{"val":true}]},{"parameterId":"authorVisibility","values":[{"val":true}]},{"parameterId":"bindingVisibility","values":[{"val":true}]},{"parameterId":"catalogNumberVisibility","values":[{"val":false}]},{"parameterId":"colorVisibility","values":[{"val":true}]},{"parameterId":"colorCountVisibility","values":[{"val":false}]},{"parameterId":"manufactureOnDemandVisibility","values":[{"val":false}]},{"parameterId":"listPriceVisibility","values":[{"val":false}]},{"parameterId":"modelStyleVisibility","values":[{"val":true}]},{"parameterId":"productGroupVisibility","values":[{"val":false}]},{"parameterId":"releaseDateVisibility","values":[{"val":false}]},{"parameterId":"periodStartDay","values":[{"val":date}]},{"parameterId":"periodEndDay","values":[{"val":date}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"shippedcogs","ascending":false}],"reportPagination":{"pageIndex":0,"pageSize":100}}},
                            headers: {
                                'Accept':'application/json',
                                'Content-Type':'application/json',
                            }
                          });
                        });

                        this.then(function(){
                          fs.write('storage/sales_fresh_count.json', this.page.plainText);
                          var obj = JSON.parse(this.getPageContent());
                            if( obj && obj['payload'] && obj['payload']['reportParts'] && obj['payload']['reportParts'][0] && obj['payload']['reportParts'][0]['rowCount'] ) {
                              var rowCount = obj['payload']['reportParts'][0]['rowCount'];
                              console.log('RowCount: '+rowCount+'');
                              var paginate = rowCount.replace(/\,/g,'');
                              paginate = parseInt(paginate,10);
                              var number = paginate/10000;
                              var pages = Math.floor(number);

                              var parts = [];
                              for (h = 0; h <= pages; h++) {
                                parts.push(h);
                              }

                              this.each(parts, function (self, part) {
                              if (fs.isFile('storage/salesdiagnostics/SalesDiagnosticsDetail_fresh_'+old_vgids[o]+'_'+date+'_part'+part+'.json') && compare_to != date){ 
                                console.log('Sales Fresh Already Scraped!');
                              } else {
                                this.wait(1000, function() {
                                  
                                  this.thenOpen('https://vendorcentral.amazon.com/analytics/data/dashboard/freshSalesDiagnostic/report/freshSalesDiagnosticDetail?token='+token+'&vgId='+old_vgids[o]+'&mcId=0&product=ara',{
                                      method: 'POST',
                                      data:   {"requestId":"741b983e-a9c6-449f-a9e8-e885fa13d88b","reportParameters":[{"parameterId":"viewFilter","values":[{"val":"shippedCOGSLevel"}]},{"parameterId":"city","values":[{"val":"ALL"}]},{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"aggregationFilter","values":[{"val":"ASINLevel"}]},{"parameterId":"isPeriodToDate","values":[{"val":false}]},{"parameterId":"isCustomDateRange","values":[{"val":"false"}]},{"parameterId":"hideLastYear","values":[{"val":false}]},{"parameterId":"period","values":[{"val":"DAILY"}]},{"parameterId":"dataRefreshDate","values":[{"val":"0001024832551"}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]},{"parameterId":"parentASINVisibility","values":[{"val":true}]},{"parameterId":"eanVisibility","values":[{"val":true}]},{"parameterId":"isbn13Visibility","values":[{"val":true}]},{"parameterId":"upcVisibility","values":[{"val":true}]},{"parameterId":"janVisibility","values":[{"val":false}]},{"parameterId":"brandVisibility","values":[{"val":true}]},{"parameterId":"brandCodeVisibility","values":[{"val":false}]},{"parameterId":"subcatVisibility","values":[{"val":true}]},{"parameterId":"catVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeWidthVisibility","values":[{"val":true}]},{"parameterId":"authorVisibility","values":[{"val":true}]},{"parameterId":"bindingVisibility","values":[{"val":true}]},{"parameterId":"catalogNumberVisibility","values":[{"val":false}]},{"parameterId":"colorVisibility","values":[{"val":true}]},{"parameterId":"colorCountVisibility","values":[{"val":false}]},{"parameterId":"manufactureOnDemandVisibility","values":[{"val":false}]},{"parameterId":"listPriceVisibility","values":[{"val":false}]},{"parameterId":"modelStyleVisibility","values":[{"val":true}]},{"parameterId":"productGroupVisibility","values":[{"val":false}]},{"parameterId":"releaseDateVisibility","values":[{"val":false}]},{"parameterId":"periodStartDay","values":[{"val":date}]},{"parameterId":"periodEndDay","values":[{"val":date}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"shippedcogs","ascending":false}],"reportPagination":{"pageIndex":part,"pageSize":10000}}},
                                      headers: {
                                          'Accept':'application/json',
                                          'Content-Type':'application/json',
                                      }
                                  });
                                  this.then(function(){
                                    console.log('SalesDiagnosticsDetail_fresh_'+old_vgids[o]+'_'+date+'_part'+part+'.json');
                                    fs.write('storage/salesdiagnostics/SalesDiagnosticsDetail_fresh_'+old_vgids[o]+'_'+date+'_part'+part+'.json', this.page.plainText);
                                  });
                                });
                              }
                              });
                            } else {
                              console.log('SalesDiagnosticsDetail_fresh_'+old_vgids[o]+'_'+date+' : NONE');
                            }
                        });
                    });
                      //prime
                      this.then(function() {
                        this.wait(1000, function () {
                          this.thenOpen('https://vendorcentral.amazon.com/analytics/data/dashboard/primeNowSalesDiagnostic/report/primeNowSalesDiagnosticDetail?token='+token+'&vgId='+old_vgids[o]+'&mcId=0&product=ara',{
                            method: 'POST',
                            data:   {"requestId":"23399442-a038-4ba4-a7db-fee9c55c3c4b","reportParameters":[{"parameterId":"viewFilter","values":[{"val":"shippedCOGSLevel"}]},{"parameterId":"city","values":[{"val":"ALL"}]},{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"aggregationFilter","values":[{"val":"ASINLevel"}]},{"parameterId":"isPeriodToDate","values":[{"val":false}]},{"parameterId":"isCustomDateRange","values":[{"val":"false"}]},{"parameterId":"hideLastYear","values":[{"val":false}]},{"parameterId":"period","values":[{"val":"DAILY"}]},{"parameterId":"dataRefreshDate","values":[{"val":"0001116193202"}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]},{"parameterId":"parentASINVisibility","values":[{"val":true}]},{"parameterId":"eanVisibility","values":[{"val":true}]},{"parameterId":"isbn13Visibility","values":[{"val":true}]},{"parameterId":"upcVisibility","values":[{"val":true}]},{"parameterId":"janVisibility","values":[{"val":false}]},{"parameterId":"brandVisibility","values":[{"val":true}]},{"parameterId":"brandCodeVisibility","values":[{"val":false}]},{"parameterId":"subcatVisibility","values":[{"val":true}]},{"parameterId":"catVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeWidthVisibility","values":[{"val":true}]},{"parameterId":"authorVisibility","values":[{"val":true}]},{"parameterId":"bindingVisibility","values":[{"val":true}]},{"parameterId":"catalogNumberVisibility","values":[{"val":false}]},{"parameterId":"colorVisibility","values":[{"val":true}]},{"parameterId":"colorCountVisibility","values":[{"val":false}]},{"parameterId":"manufactureOnDemandVisibility","values":[{"val":false}]},{"parameterId":"listPriceVisibility","values":[{"val":false}]},{"parameterId":"modelStyleVisibility","values":[{"val":true}]},{"parameterId":"productGroupVisibility","values":[{"val":false}]},{"parameterId":"releaseDateVisibility","values":[{"val":false}]},{"parameterId":"periodStartDay","values":[{"val":date}]},{"parameterId":"periodEndDay","values":[{"val":date}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"shippedcogs","ascending":false}],"reportPagination":{"pageIndex":0,"pageSize":100}}},
                            headers: {
                                'Accept':'application/json',
                                'Content-Type':'application/json',
                            }
                          });
                        });

                        this.then(function(){
                          fs.write('storage/sales_prime_count.json', this.page.plainText);
                          var obj = JSON.parse(this.getPageContent());
                            if( obj && obj['payload'] && obj['payload']['reportParts'] && obj['payload']['reportParts'][0] && obj['payload']['reportParts'][0]['rowCount'] ) {
                              var rowCount = obj['payload']['reportParts'][0]['rowCount'];
                              console.log('RowCount: '+rowCount+'');
                              var paginate = rowCount.replace(/\,/g,'');
                              paginate = parseInt(paginate,10);
                              var number = paginate/10000;
                              var pages = Math.floor(number);

                              var parts = [];
                              for (h = 0; h <= pages; h++) {
                                parts.push(h);
                              }

                              this.each(parts, function (self, part) {
                              if (fs.isFile('storage/salesdiagnostics/SalesDiagnosticsDetail_prime_'+old_vgids[o]+'_'+date+'_part'+part+'.json') && compare_to != date){ 
                                console.log('Sales Prime Already Scraped!');
                              } else {
                                this.wait(1000, function() {
                                  
                                  this.thenOpen('https://vendorcentral.amazon.com/analytics/data/dashboard/primeNowSalesDiagnostic/report/primeNowSalesDiagnosticDetail?token='+token+'&vgId='+old_vgids[o]+'&mcId=0&product=ara',{
                                      method: 'POST',
                                      data:   {"requestId":"23399442-a038-4ba4-a7db-fee9c55c3c4b","reportParameters":[{"parameterId":"viewFilter","values":[{"val":"shippedCOGSLevel"}]},{"parameterId":"city","values":[{"val":"ALL"}]},{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"aggregationFilter","values":[{"val":"ASINLevel"}]},{"parameterId":"isPeriodToDate","values":[{"val":false}]},{"parameterId":"isCustomDateRange","values":[{"val":"false"}]},{"parameterId":"hideLastYear","values":[{"val":false}]},{"parameterId":"period","values":[{"val":"DAILY"}]},{"parameterId":"dataRefreshDate","values":[{"val":"0001116193202"}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]},{"parameterId":"parentASINVisibility","values":[{"val":true}]},{"parameterId":"eanVisibility","values":[{"val":true}]},{"parameterId":"isbn13Visibility","values":[{"val":true}]},{"parameterId":"upcVisibility","values":[{"val":true}]},{"parameterId":"janVisibility","values":[{"val":false}]},{"parameterId":"brandVisibility","values":[{"val":true}]},{"parameterId":"brandCodeVisibility","values":[{"val":false}]},{"parameterId":"subcatVisibility","values":[{"val":true}]},{"parameterId":"catVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeWidthVisibility","values":[{"val":true}]},{"parameterId":"authorVisibility","values":[{"val":true}]},{"parameterId":"bindingVisibility","values":[{"val":true}]},{"parameterId":"catalogNumberVisibility","values":[{"val":false}]},{"parameterId":"colorVisibility","values":[{"val":true}]},{"parameterId":"colorCountVisibility","values":[{"val":false}]},{"parameterId":"manufactureOnDemandVisibility","values":[{"val":false}]},{"parameterId":"listPriceVisibility","values":[{"val":false}]},{"parameterId":"modelStyleVisibility","values":[{"val":true}]},{"parameterId":"productGroupVisibility","values":[{"val":false}]},{"parameterId":"releaseDateVisibility","values":[{"val":false}]},{"parameterId":"periodStartDay","values":[{"val":date}]},{"parameterId":"periodEndDay","values":[{"val":date}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"shippedcogs","ascending":false}],"reportPagination":{"pageIndex":part,"pageSize":100}}},
                                      headers: {
                                          'Accept':'application/json',
                                          'Content-Type':'application/json',
                                      }
                                  });
                                  this.then(function(){
                                    console.log('SalesDiagnosticsDetail_prime_'+old_vgids[o]+'_'+date+'_part'+part+'.json');
                                    fs.write('storage/salesdiagnostics/SalesDiagnosticsDetail_prime_'+old_vgids[o]+'_'+date+'_part'+part+'.json', this.page.plainText);
                                  }); 
                                });
                              }
                              });
                            } else {
                              console.log('SalesDiagnosticsDetail_prime_'+old_vgids[o]+'_'+date+' : NONE');
                            }
                        });
                    });
                      //business
                      this.then(function() {
                        this.wait(1000, function () {
                          this.thenOpen('https://vendorcentral.amazon.com/analytics/data/dashboard/businessSalesDiagnostic/report/businessSalesDiagnosticDetail?token='+token+'&vgId='+old_vgids[o]+'&mcId=0&product=ara',{
                            method: 'POST',
                            data:   {"requestId":"3d2ae09e-77e1-4b5c-9b00-ec0e02cb8499","reportParameters":[{"parameterId":"viewFilter","values":[{"val":"shippedCOGSLevel"}]},{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"aggregationFilter","values":[{"val":"ASINLevel"}]},{"parameterId":"periodStartDay","values":[{"val":date}]},{"parameterId":"periodEndDay","values":[{"val":date}]},{"parameterId":"isPeriodToDate","values":[{"val":true}]},{"parameterId":"isCustomDateRange","values":[{"val":"false"}]},{"parameterId":"hideLastYear","values":[{"val":false}]},{"parameterId":"period","values":[{"val":"DAILY"}]},{"parameterId":"dataRefreshDate","values":[{"val":"0000078742193"}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]},{"parameterId":"parentASINVisibility","values":[{"val":true}]},{"parameterId":"eanVisibility","values":[{"val":true}]},{"parameterId":"isbn13Visibility","values":[{"val":true}]},{"parameterId":"upcVisibility","values":[{"val":true}]},{"parameterId":"janVisibility","values":[{"val":false}]},{"parameterId":"brandVisibility","values":[{"val":true}]},{"parameterId":"brandCodeVisibility","values":[{"val":false}]},{"parameterId":"subcatVisibility","values":[{"val":true}]},{"parameterId":"catVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeWidthVisibility","values":[{"val":true}]},{"parameterId":"authorVisibility","values":[{"val":true}]},{"parameterId":"bindingVisibility","values":[{"val":true}]},{"parameterId":"catalogNumberVisibility","values":[{"val":false}]},{"parameterId":"colorVisibility","values":[{"val":true}]},{"parameterId":"colorCountVisibility","values":[{"val":false}]},{"parameterId":"manufactureOnDemandVisibility","values":[{"val":false}]},{"parameterId":"listPriceVisibility","values":[{"val":false}]},{"parameterId":"modelStyleVisibility","values":[{"val":true}]},{"parameterId":"productGroupVisibility","values":[{"val":false}]},{"parameterId":"releaseDateVisibility","values":[{"val":false}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"shippedcogs","ascending":false}],"reportPagination":{"pageIndex":0,"pageSize":100}}},
                            headers: {
                                'Accept':'application/json',
                                'Content-Type':'application/json',
                            }
                          });
                        });

                        this.then(function() {

                        });

                        this.then(function(){
                          fs.write('storage/sales_business_count.json', this.page.plainText);

                          var obj = JSON.parse(this.getPageContent());

                            if( obj && obj['payload'] && obj['payload']['reportParts'] && obj['payload']['reportParts'][0] && obj['payload']['reportParts'][0]['rowCount'] ) {
                              var rowCount = obj['payload']['reportParts'][0]['rowCount'];
                              console.log('RowCount: '+rowCount+'');

                              var paginate = rowCount.replace(/\,/g,'');
                              paginate = parseInt(paginate,10);
                              var number = paginate/10000;
                              var pages = Math.floor(number);

                              var parts = [];
                              for (h = 0; h <= pages; h++) {
                                parts.push(h);
                              }

                              this.each(parts, function (self, part) {
                              if (fs.isFile('storage/salesdiagnostics/SalesDiagnosticsDetail_business_'+old_vgids[o]+'_'+date+'_part'+part+'.json') && compare_to != date){ 
                                console.log('Sales Business Already Scraped!');
                              } else {
                                this.wait(1000, function() {
                                  
                                  this.thenOpen('https://vendorcentral.amazon.com/analytics/data/dashboard/businessSalesDiagnostic/report/businessSalesDiagnosticDetail?token='+token+'&vgId='+old_vgids[o]+'&mcId=0&product=ara',{
                                      method: 'POST',
                                      data:   {"requestId":"3d2ae09e-77e1-4b5c-9b00-ec0e02cb8499","reportParameters":[{"parameterId":"viewFilter","values":[{"val":"shippedCOGSLevel"}]},{"parameterId":"asin","values":[{"val":"ALL"}]},{"parameterId":"aggregationFilter","values":[{"val":"ASINLevel"}]},{"parameterId":"periodStartDay","values":[{"val":date}]},{"parameterId":"periodEndDay","values":[{"val":date}]},{"parameterId":"isPeriodToDate","values":[{"val":true}]},{"parameterId":"isCustomDateRange","values":[{"val":"false"}]},{"parameterId":"hideLastYear","values":[{"val":false}]},{"parameterId":"period","values":[{"val":"DAILY"}]},{"parameterId":"dataRefreshDate","values":[{"val":"0000078742193"}]},{"parameterId":"categoryId","values":[{"val":"ALL"}]},{"parameterId":"subcategoryId","values":[{"val":"ALL"}]},{"parameterId":"brandId","values":[{"val":"ALL"}]},{"parameterId":"parentASINVisibility","values":[{"val":true}]},{"parameterId":"eanVisibility","values":[{"val":true}]},{"parameterId":"isbn13Visibility","values":[{"val":true}]},{"parameterId":"upcVisibility","values":[{"val":true}]},{"parameterId":"janVisibility","values":[{"val":false}]},{"parameterId":"brandVisibility","values":[{"val":true}]},{"parameterId":"brandCodeVisibility","values":[{"val":false}]},{"parameterId":"subcatVisibility","values":[{"val":true}]},{"parameterId":"catVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeVisibility","values":[{"val":true}]},{"parameterId":"apparelSizeWidthVisibility","values":[{"val":true}]},{"parameterId":"authorVisibility","values":[{"val":true}]},{"parameterId":"bindingVisibility","values":[{"val":true}]},{"parameterId":"catalogNumberVisibility","values":[{"val":false}]},{"parameterId":"colorVisibility","values":[{"val":true}]},{"parameterId":"colorCountVisibility","values":[{"val":false}]},{"parameterId":"manufactureOnDemandVisibility","values":[{"val":false}]},{"parameterId":"listPriceVisibility","values":[{"val":false}]},{"parameterId":"modelStyleVisibility","values":[{"val":true}]},{"parameterId":"productGroupVisibility","values":[{"val":false}]},{"parameterId":"releaseDateVisibility","values":[{"val":false}]}],"reportPaginationWithOrderParameter":{"reportOrders":[{"columnId":"shippedcogs","ascending":false}],"reportPagination":{"pageIndex":part,"pageSize":10000}}},
                                      headers: {
                                          'Accept':'application/json',
                                          'Content-Type':'application/json',
                                      }
                                  });
                                  this.then(function(){
                                    console.log('SalesDiagnosticsDetail_business_'+old_vgids[o]+'_'+date+'_part'+part+'.json');
                                    fs.write('storage/salesdiagnostics/SalesDiagnosticsDetail_business_'+old_vgids[o]+'_'+date+'_part'+part+'.json', this.page.plainText);
                                  });
                                });
                              }
                              });
                            }
                            else {
                              console.log('SalesDiagnosticsDetail_business_'+old_vgids[o]+'_'+date+' : NONE')
                            }
                        });
                    });
                });